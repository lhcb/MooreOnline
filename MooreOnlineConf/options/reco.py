###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
from Moore import options
from Moore.config import run_allen_reconstruction
from RecoConf.config import Reconstruction
from RecoConf.standalone import reco_prefilters, standalone_hlt2_global_reco
from Hlt2Conf.settings.hlt2_binds import config_pp_2024_with_monitoring
from RecoConf.hlt1_allen import allen_gaudi_config
from RecoConf.legacy_rec_hlt1_tracking import make_RetinaCluster_raw_bank, make_velo_full_clusters, make_RetinaClusters
from PyConf.Algorithms import VertexCompare

from RecoConf.legacy_rec_hlt1_tracking import (
    make_reco_pvs,
    make_PatPV3DFuture_pvs,
    make_VeloClusterTrackingSIMD,
)
from RecoConf.hlt2_tracking import (
    make_PrKalmanFilter_noUT_tracks,
    make_PrKalmanFilter_Seed_tracks,
    make_PrKalmanFilter_Velo_tracks,
    make_TrackBestTrackCreator_tracks,
)
from MooreOnlineConf.utils import update_and_reset

from Moore import options, run_moore
from RecoConf.global_tools import stateProvider_with_simplified_geom, trackMasterExtrapolator_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction
from RecoConf.hlt2_global_reco import reconstruction as hlt2_reconstruction, make_light_reco_pr_kf_without_UT
from RecoConf.hlt2_tracking import (
    make_TrackBestTrackCreator_tracks,
    make_PrKalmanFilter_noUT_tracks,
    make_PrKalmanFilter_Velo_tracks,
    make_PrKalmanFilter_Seed_tracks,
)
from RecoConf.decoders import default_VeloCluster_source
from RecoConf.protoparticles import make_charged_protoparticles
from RecoConf.event_filters import require_gec
from Hlt2Conf.settings.defaults import get_default_hlt1_filter_code_for_hlt2
from Moore.streams import Stream, Streams
from Hlt2Conf.lines.semileptonic import all_lines as full_lines  # all full-stream lines
import sys
from DDDB.CheckDD4Hep import UseDD4Hep

from Moore import options, run_moore
from RecoConf.global_tools import stateProvider_with_simplified_geom, trackMasterExtrapolator_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction
from RecoConf.hlt2_global_reco import reconstruction as hlt2_reconstruction, make_light_reco_pr_kf_without_UT
from RecoConf.hlt2_tracking import (
    make_TrackBestTrackCreator_tracks,
    make_PrKalmanFilter_noUT_tracks,
    make_PrKalmanFilter_Velo_tracks,
    make_PrKalmanFilter_Seed_tracks,
)
from RecoConf.decoders import default_VeloCluster_source
from RecoConf.protoparticles import make_charged_protoparticles
from RecoConf.event_filters import require_gec
from Hlt2Conf.settings.defaults import get_default_hlt1_filter_code_for_hlt2
from Moore.streams import Stream, Streams
from Hlt2Conf.lines.semileptonic import all_lines as full_lines  # all full-stream lines
import sys
from DDDB.CheckDD4Hep import UseDD4Hep

task_type = os.getenv("TASK_TYPE", "GenericTask")

## Global event cut specifications:
# If a GEC is applied (as is set explicitly in the bind below)
# prefer to make explicit so it's clear which a GEC is applied
GEC_cut = 10_000


def with_update_and_reset():

    reco = standalone_hlt2_global_reco(do_data_monitoring=True)

    if options.input_type.lower() == 'online':
        from PyConf.Algorithms import LHCb__IOVReset as IOVReset, OdinTypesFilter, HltRoutingBitsFilter
        from PyConf.application import make_odin, default_raw_banks
        odin_bb_filter = OdinTypesFilter(
            ODIN=make_odin(), BXTypes=['BeamCrossing'])
        odin_be_filter = OdinTypesFilter(
            ODIN=make_odin(), BXTypes=[
                'Beam1'
            ])  # only filter on SMOG collisions that fly into LHCb

        if task_type == "RecoPhysMon":
            rb_filter = HltRoutingBitsFilter(
                RawBanks=default_raw_banks('HltRoutingBits'),
                RequireMask=(1 << 14, 0, 0),  # Physics events
                PassOnError=False)
            return Reconstruction(
                "with_update_and_reset", [reco.node],
                filters=[
                    IOVReset(ODIN=make_odin()),
                    update_and_reset(), odin_bb_filter, rb_filter
                ])
        elif task_type == "RecoSMOGMon":
            rb_filter = HltRoutingBitsFilter(
                RawBanks=default_raw_banks('HltRoutingBits'),
                RequireMask=(1 << 15, 0, 0),  # SMOG physics
                PassOnError=False)
            return Reconstruction(
                "with_update_and_reset", [reco.node],
                filters=[
                    IOVReset(ODIN=make_odin()),
                    update_and_reset(), odin_be_filter, rb_filter
                ])
        else:
            rb_filter = HltRoutingBitsFilter(
                RawBanks=default_raw_banks('HltRoutingBits'),
                RequireMask=(1 << 1, 0, 0),  # Lumi events
                PassOnError=False)
            return Reconstruction(
                "with_update_and_reset", [reco.node],
                filters=[
                    IOVReset(ODIN=make_odin()),
                    update_and_reset(), odin_bb_filter, rb_filter
                ])

    return reco


# NOTE the switch to retina clusters is done in vp_retina_clusters.py
# the global event cut is added for the PbPb run case, in doubt remove for pp
with config_pp_2024_with_monitoring(), reco_prefilters.bind(
        gec=False,
        gec_cut=GEC_cut,
),\
    VertexCompare.bind(produceNtuple=False,produceHistogram=False,monitoring=True):
    run_allen_reconstruction(options, with_update_and_reset)
