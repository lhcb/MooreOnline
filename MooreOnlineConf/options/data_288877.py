###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
#############################################################################
from Moore import options

options.set_input_and_conds_from_testfiledb("2024_raw_hlt1_288877_tae")

options.evt_max = 10000
# options.use_iosvc = True
# options.event_store = "EvtStoreSvc"

options.histo_file = "histograms.root"
options.monitoring_file = "monitoring.json"
