###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
#############################################################################
from PyConf.application import default_raw_event, make_odin
from Moore import options
from RecoConf.config import Reconstruction, run_reconstruction
from RecoConf.standalone import reco_prefilters
from PyConf.Algorithms import MuonRawInUpgradeToHits, MuonMonitor

options.dddb_tag = 'muon_cabling_run3'
options.conddb_tag = 'muon_cabling_run3_fix_M2R2'


def muon_mon():
    raw = default_raw_event("Muon")
    odin = make_odin()

    rawToHits = MuonRawInUpgradeToHits(RawEventLocation=raw)
    monitor = MuonMonitor(ODINLocation=odin, MuonHits=rawToHits)

    algs = []
    if options.input_type.lower() == 'online':
        from MooreOnlineConf.utils import update_and_reset
        algs.append(update_and_reset())

    algs.append(rawToHits)
    algs.append(monitor)

    return Reconstruction('muon_mon', algs, reco_prefilters(gec=False))


run_reconstruction(options, muon_mon)
