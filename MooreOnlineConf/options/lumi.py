###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf.application import default_raw_banks, make_odin
from Moore import options
from RecoConf.config import Reconstruction, run_reconstruction
from PyConf.Algorithms import (
    HltRoutingBitsFilter,
    HltLumiSummaryDecoder,
    HltLumiSummaryMonitor,
    HltDecReportsDecoder,
    HltDecReportsMonitor,
    HltRoutingBitsMonitor,
)


def lumi():
    odin = make_odin()
    rb_filter = HltRoutingBitsFilter(
        RawBanks=default_raw_banks('HltRoutingBits'),
        RequireMask=(1 << 1, 0, 0),
        PassOnError=False)
    summary = HltLumiSummaryDecoder(
        RawBanks=default_raw_banks("HltLumiSummary")).OutputContainerName
    monitor = HltLumiSummaryMonitor(
        name="HltLumiSummaryMonitor", Input=summary, ODIN=odin)

    hlt1_dec_reports = HltDecReportsDecoder(
        RawBanks=default_raw_banks("HltDecReports"),
        SourceID="Hlt1").OutputHltDecReportsLocation
    dr_monitor = HltDecReportsMonitor(
        name="HltDecReportsMonitor", Input=hlt1_dec_reports)
    rb_monitor = HltRoutingBitsMonitor(
        name="HltRoutingBitsMonitor",
        RawBanks=default_raw_banks("HltRoutingBits"))

    filters = []
    if options.input_type.lower() == 'online':
        from MooreOnlineConf.utils import update_and_reset
        filters.append(update_and_reset())
    filters.append(rb_filter)

    return Reconstruction(
        'lumi', [monitor], filters=[dr_monitor, rb_monitor] + filters)


run_reconstruction(options, lumi)
