###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
In case the time for savesets needs to change: saverCyle in online.py 
Routing bits for trigger lines
odin type filter for bb/be/eb/ee etc. 
"""

from Moore import options
from PyConf.application import make_odin
from RecoConf.config import Reconstruction, run_reconstruction
from RecoConf.standalone import reco_prefilters
from RecoConf.legacy_rec_hlt1_tracking import (
    make_reco_pvs,
    make_PatPV3DFuture_pvs,
    make_RetinaClusters,
)
from PyConf.application import default_raw_banks
from RecoConf.hlt2_tracking import (
    get_global_measurement_provider,
    make_VPClus_hits,
    TrackBestTrackCreator,
)
from PyConf.Algorithms import (
    VPHitEfficiencyMonitor,
    VeloRetinaClusterTrackingSIMDFull,
    VeloClusterTrackingSIMDFull,
    fromPrVeloTracksV1TracksMerger,
    TrackEventFitter,
    TrackListRefiner,
    TrackSelectionToContainer,
)
import Functors as F
from PyConf.Tools import (
    TrackMasterFitter,
    TrackMasterExtrapolator,
    SimplifiedMaterialLocator,
    TrackInterpolator,
    TrackLinearExtrapolator,
)

from PyConf.utilities import DISABLE_TOOL
from PyConf.Algorithms import OdinTypesFilter, HltRoutingBitsFilter  #trigger + bxid filter
make_reco_pvs.global_bind(make_pvs_from_velo_tracks=make_PatPV3DFuture_pvs)
import sys, os

# to switch from FC/SP needs changed runVeloHitEff.sh
velo_clusters_algorithm_light = make_RetinaClusters
velo_hits_algorithm = VeloRetinaClusterTrackingSIMDFull
bankType = "VPRetinaCluster"
if not int(os.environ["VELO_SP"] == "0"):
    velo_clusters_algorithm_light = make_VPClus_hits
    velo_hits_algorithm = VeloClusterTrackingSIMDFull
    bankType = "VP"


def velo_mon():

    algs = []

    # BXTypes: NoBeam, Beam1, Beam2 BeamCrossing
    odinFilter = OdinTypesFilter(ODIN=make_odin(), BXTypes=['BeamCrossing'])

    if options.input_type.lower() == 'online':
        from MooreOnlineConf.utils import update_and_reset
        algs.append(update_and_reset())

    odin = make_odin()
    algs.append(odin)

    vpClustering = velo_hits_algorithm(
        RawBanks=default_raw_banks(bankType),
        MaxScatterSeeding=0.1,
        MaxScatterForwarding=0.1,
        MaxScatter3hits=0.02,
        SkipForward=4)

    clusters_other = vpClustering.HitsLocation
    vpTracks = vpClustering.TracksLocation
    vpTracks_backwards = vpClustering.TracksBackwardLocation

    vpTracks_v1 = fromPrVeloTracksV1TracksMerger(  # converts Pr -> v1 tracks and merges forward/backward
        InputTracksLocation1=vpTracks,
        InputTracksLocation2=vpTracks_backwards).OutputTracksLocation

    my_TrackMasterFitter = TrackMasterFitter(
        MeasProvider=get_global_measurement_provider(),
        MaterialLocator=SimplifiedMaterialLocator(),
        Extrapolator=TrackMasterExtrapolator(
            MaterialLocator=SimplifiedMaterialLocator()),
        MaxNumberOutliers=2,
        NumberFitIterations=10,
        FastMaterialApproximation=True,
        MaxUpdateTransports=10)

    fittedTracks = TrackEventFitter(
        TracksInContainer=vpTracks_v1,
        Fitter=(my_TrackMasterFitter),
        MaxChi2DoF=2.8,
        name="TrackEventFitter_{hash}").TracksOutContainer

    bestTracks = TrackBestTrackCreator(
        name="TrackBestTrackCreator_{hash}",
        TracksInContainers=[fittedTracks],
        DoNotRefit=True,
        AddGhostProb=False,
        FitTracks=False,
        MaxChi2DoF=2.8,
    ).TracksOutContainer

    trackExtrapolator = TrackLinearExtrapolator()
    trackInterpolator = TrackInterpolator(Extrapolator=trackExtrapolator)

    tracks_selection = TrackListRefiner(
        inputLocation=bestTracks,
        Code=F.require_all(
            F.NVPHITS >= 3,
            F.ETA >= 1.3,
            F.P >= 0,  # Here to make it explicit
            F.PT >= 0)).outputLocation
    filtered_tracks = TrackSelectionToContainer(
        name="TrackSelectionToContainer_{hash}",
        InputLocation=tracks_selection).OutputLocation

    # Loop over sensors.
    for sensor_under_study in range(208):

        my_vp_efficiency_alg_TMF = VPHitEfficiencyMonitor(
            name="VPHitEfficiencyMonitorSensor_{0}".format(sensor_under_study),
            TrackLocation=filtered_tracks,
            PrVPHitsLocation=clusters_other,
            ResidualTolerance=0.1,
            MaxTrackCov=100.0,
            SensorUnderStudy=sensor_under_study,
            Interpolator=trackInterpolator,
            Extrapolator=trackExtrapolator,
            ExpertMode=False,
        )
        algs.append(my_vp_efficiency_alg_TMF)

    return Reconstruction('velo_mon', algs, filters=[odinFilter])


options.msg_svc_format = "% F%56W%S%7W%R%T %0W%M"

with get_global_measurement_provider.bind(
        velo_hits=velo_clusters_algorithm_light,
        ignoreUT=True,
        ignoreFT=True,
        ut_provider=DISABLE_TOOL,
        ft_provider=DISABLE_TOOL):
    run_reconstruction(options, velo_mon)
