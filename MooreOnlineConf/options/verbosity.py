###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import logging
from Gaudi.Configuration import WARNING, INFO
from Moore import options
import OnlineEnvBase as OnlineEnv

options.output_level = OnlineEnv.OutputLevel

if options.output_level >= WARNING:
    options.python_logging_level = logging.WARNING
elif options.output_level >= INFO:
    options.python_logging_level = logging.INFO
else:
    options.python_logging_level = logging.DEBUG

# FIXME we also use OnlineEnv.OutputLevel in online.py
