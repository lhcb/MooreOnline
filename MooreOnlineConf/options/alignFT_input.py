###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os, sys
import fifo_log
import GaudiOnline
import OnlineEnvBase as OnlineEnv
from Configurables import DummyRunable
from Configurables import Online__AlgFlowManager as AlgFlowManager
from Configurables import HLTControlFlowMgr, ExecutionReportsWriter, EventLoopMgr

OnlineEnv.OutputLevel = 3

application = GaudiOnline.Passthrough(
    outputLevel=OnlineEnv.OutputLevel,
    partitionName=OnlineEnv.PartitionName,
    partitionID=OnlineEnv.PartitionID,
    classType=GaudiOnline.Class1)
application.setup_fifolog()
utgid = os.getenv('UTGID', 'AlignWrk')
fifo_log.logger_set_utgid(utgid)
fifo_log.logger_set_tag(OnlineEnv.PartitionName)

input_files = ['/scratch/shollitt/inputfiles/00146082_00000001_1.mdf']
application.setup_file_access(input_files)

# application.setup_hive(FlowManager("EventLoop"), 40)  <- simple event loop
flow = AlgFlowManager("EventLoop")
application.setup_hive(flow, 44)

# HACK: transfer options from HLTControlFlowMgr
cfm = HLTControlFlowMgr('HLTControlFlowMgr')
flow.CompositeCFNodes = cfm.CompositeCFNodes
flow.BarrierAlgNames = cfm.BarrierAlgNames

# HACK: tell the HltDecReports creator to use the online scheduler
# only works because there is exactly one instance of ExecutionReportsWriter
ExecutionReportsWriter().Scheduler = flow

application.setup_monitoring()
application.monSvc.DimUpdateInterval = 1

application.config.enablePause = True
# Use execMode = 1 for multi-threaded (async_queued) mode and
# use 0 (default) for single-threaded (sync) mode, i.e. debugging.
application.config.execMode = 1
application.config.numEventThreads = 1
application.config.numStatusThreads = 1
application.config.FILE_maxEventsIn = 10
application.app.Runable = DummyRunable()
application.app.TopAlg.insert(0, application.updateAndReset)
application.app.EvtSel = 'NONE'
application.app.EvtMax = -1
application.app.StopOnSignal = False  # = default
EventLoopMgr().Warnings = False

print('Setup complete....')
