###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options
from DDDB.CheckDD4Hep import UseDD4Hep
import os

options.input_type = 'Online'
options.output_type = 'Online'
options.dddb_tag = 'master'
options.conddb_tag = 'master'
options.geometry_version = 'run3/trunk'
options.conditions_version = 'master'
options.simulation = not UseDD4Hep

online_cond_path = '/group/online/hlt/conditions.run3/lhcb-conditions-database'
if UseDD4Hep:
    from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc
    dd4hep_svc = LHCb__Det__LbDD4hep__DD4hepSvc()
    dd4hep_svc.DetectorList = [
        '/world', 'VP', 'FT', 'Magnet', 'Rich1', 'Rich2', 'Ecal', 'Hcal',
        'Muon', 'UT'
    ]
    if os.path.exists(online_cond_path):
        dd4hep_svc.ConditionsLocation = 'file://' + online_cond_path
