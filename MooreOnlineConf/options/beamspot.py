###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
from Moore import options
from RecoConf.config import Reconstruction, run_reconstruction
from RecoConf.standalone import reco_prefilters
from RecoConf.legacy_rec_hlt1_tracking import (make_all_pvs, make_reco_pvs,
                                               make_PatPV3DFuture_pvs,
                                               make_VeloClusterTrackingSIMD)
from MooreOnlineConf.utils import update_and_reset
from DDDB.CheckDD4Hep import UseDD4Hep
from PyConf.Algorithms import BeamSpotMonitor
from PyConf.application import make_odin
from GaudiKernel.SystemOfUnits import mm, mm2

task_type = os.getenv("TASK_TYPE", "GenericTask")


def run_hlt1_pvs_with_beamspotmoni():
    """ Run the Hlt2 track reconstruction optimized for not having the UT detector

    Args:

    Returns:
        Reconstruction: Data and control flow of Hlt2 track reconstruction.

    """
    # Set output IR conditions path based on partition/context
    conditionsDbPath = "/group/online/alignment/test-conditions"
    import OnlineEnvBase as OnlineEnv
    if OnlineEnv.PartitionName == "LHCb":
        conditionsDbPath = "/group/online/hlt/conditions.run3/lhcb-conditions-database"
    elif OnlineEnv.PartitionName.startswith("TEST"):
        conditionsDbPath = "beamspotmon"

    pvs = make_all_pvs()

    from PyConf.Algorithms import VertexListRefiner
    selected_pvs = {
        "v1":
        VertexListRefiner(
            InputLocation=pvs["v1"],
            MinNumTracks=20,
            MinZ=-200 * mm,  # Set to exclude PVs from SMOG cell
            MaxZ=500 * mm).OutputLocation
    }

    beam_spot_moni = BeamSpotMonitor(
        name="BeamSpotMonitor",
        ODINLocation=make_odin(),
        PVContainer=selected_pvs["v1"],
        MinPVsForCalib=2500,  # Based on std dev of x and y and thresholds
        LogConditions=True,
        MakeDBRunFile=True,
        conditionsDbPath=conditionsDbPath,
        conditionsPathInDb="Conditions/LHCb/Online/InteractionRegion.yml/.pool",
        # Thresholds to be updated.  See MooreOnline!358 for discussions.
        MaxAbsDeltaMap={'rho': 0.05 * mm},
        HistPVXMin=-2.5 * mm,
        HistPVXMax=2.5 * mm,
        OnlineMode=True)
    data = [pvs["v1"], selected_pvs["v1"], beam_spot_moni]

    return Reconstruction('hlt1_pvs_with_bsm', data,
                          reco_prefilters(skipUT=True))


def with_update_and_reset():
    reco = run_hlt1_pvs_with_beamspotmoni()

    if options.input_type.lower() == 'online':
        from PyConf.Algorithms import LHCb__IOVReset as IOVReset, OdinTypesFilter, HltRoutingBitsFilter
        from PyConf.application import make_odin, default_raw_banks
        odin_bb_filter = OdinTypesFilter(
            ODIN=make_odin(), BXTypes=['BeamCrossing'])

        rb_filter = HltRoutingBitsFilter(
            RawBanks=default_raw_banks('HltRoutingBits'),
            RequireMask=(1 << 1, 0, 0),  # Lumi events
            PassOnError=False)
        return Reconstruction(
            "with_update_and_reset",
            [IOVReset(ODIN=make_odin()),
             update_and_reset(), reco.node],
            filters=[odin_bb_filter, rb_filter])
    return reco


# NOTE the switch to retina clusters is done in vp_retina_clusters.py
# the global event cut is added for the PbPb run case, in doubt remove for pp
with make_VeloClusterTrackingSIMD.bind(SkipForward=4),\
     make_PatPV3DFuture_pvs.bind(velo_open=True, use_3D_seeding=True, use_beam_spot_cut=False),\
     make_reco_pvs.bind(make_pvs_from_velo_tracks=make_PatPV3DFuture_pvs):
    run_reconstruction(options, with_update_and_reset)

if UseDD4Hep:
    from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc
    dd4hep_svc = LHCb__Det__LbDD4hep__DD4hepSvc()
    dd4hep_svc.DetectorList = ['/world', 'VP']
