###############################################################################
# (c) Copyright 2000-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf.application import (
    configure_input,
    configure,
    default_raw_banks,
    make_odin,
)
from PyConf.Algorithms import (
    PlumeRawToDigits,
    PlumeDigitMonitor,
    PlumeTAEMonitor,
)
from MooreOnlineConf.utils import (
    common_monitors_node,
    passes_rb,
    RoutingBit,
    decode_tae,
    if_then,
    run_all,
)
from Moore import options

try:
    import OnlineEnvBase as OnlineEnv
    TAE_HALF_WINDOW = OnlineEnv.TAE
except ImportError:
    TAE_HALF_WINDOW = 3


def make_plume_digits(name=""):
    raw = default_raw_banks("Plume")
    digits = PlumeRawToDigits(
        name=f"PlumeRawToDigits{name}", RawBankLocation=raw).Output
    return digits


def main():
    # the standard monitor
    monitor = PlumeDigitMonitor(
        name="PlumeDigitMonitor", Input=make_plume_digits(), ODIN=make_odin())

    # the TAE monitor
    is_tae, tae_decoding, tae_odins, tae_data = decode_tae(
        make_plume_digits, half_window=TAE_HALF_WINDOW)
    tae_monitor = PlumeTAEMonitor(
        name="PlumeTAEMonitor",
        ODINVector=list(tae_odins.values()),
        InputVector=list(tae_data.values()))

    # assemble the control flow
    top_node = run_all(
        "top",
        [
            common_monitors_node(),  # common monitoring to all tasks
            if_then("IfLUMI", passes_rb(RoutingBit.LUMI), monitor),
            if_then("IfTAE", is_tae, run_all("TAE",
                                             [tae_decoding, tae_monitor])),
        ])

    return top_node


configure_input(options)
configure(options, main())
