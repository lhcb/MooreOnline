###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration       #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import GaudiOnline
import OnlineEnvBase as OnlineEnv
from MooreOnlineConf.utils import alignment_options, ensure_output_dir
from Configurables import (AlignOnlineIterator, AlignUpdateTool,
                           LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc)

application = GaudiOnline.Application(
    outputLevel=OnlineEnv.OutputLevel,
    partitionName=OnlineEnv.PartitionName,
    partitionID=OnlineEnv.PartitionID,
    classType=GaudiOnline.Class1)
application.setup_fifolog()

application.setup_monitoring()
# application.monSvc.DimUpdateInterval = 1

#application.config.enablePause = True
application.config.enableStop = True
application.config.enableContinue = True
application.app.EvtSel = 'NONE'
application.app.EvtMax = -1
application.app.StopOnSignal = False  # = default

online_options = alignment_options(OnlineEnv)

ensure_output_dir(online_options.iterator_output_path, online_options.tag)
ensure_output_dir(online_options.analyzer_output_path, online_options.tag)

iterator = AlignOnlineIterator()
iterator.DerivativeFiles = [
    str(online_options.analyzer_output_path / f"derivatives-{node}.out")
    for node in online_options.nodes
]
iterator.AnalyzerHistograms = [
    str(online_options.analyzer_output_path / f"histograms-{node}_new.root")
    for node in online_options.nodes
]

iterator.RunNumber = min(map(int, online_options.runs))

# only create yml copiers if running in run control
online_mode = OnlineEnv.PartitionName == "LHCbA"
iterator.OnlineMode = online_mode

overlay_path = online_options.iterator_output_path / "OverlayRoot"
overlay_path.mkdir(exist_ok=True)
iterator.OverlayRoot = str(overlay_path)
DD4hepSvc().UseConditionsOverlay = True
AlignUpdateTool(name="updateTool").LogFile = str(
    online_options.iterator_output_path / "alignlog.txt")
