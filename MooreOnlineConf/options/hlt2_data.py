###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options
import glob

# options.input_files = sorted(
#     glob.glob("/daqarea1/fest/202110/mdf/30000000/*.mdf"))
# options.input_files = ["/scratch/rmatev/Run_0000227755_20220405-142719-939_UCEB01_0001.mdf"]
# options.input_files = ["/scratch/rmatev/aaa.mdf"]
# options.input_files = sorted(
#     glob.glob("/daqarea1/fest/202110/mdf/10000000/*.mdf"))
options.input_files = sorted(
    glob.glob("/daqarea1/fest/202110/mdf_hlt1/10000000/*.mdf"))

options.input_type = 'MDF'
options.dddb_tag = 'dddb-20210617'
options.conddb_tag = 'sim-20210617-vc-md100'

options.evt_max = 10000
# options.output_level = 1

options.histo_file = 'hlt2.histograms.root'
# options.output_file = 'hlt2_{stream}.mdf'
# options.output_type = 'MDF'

options.n_threads = 8
