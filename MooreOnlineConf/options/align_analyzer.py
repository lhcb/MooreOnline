###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
import GaudiOnline
import OnlineEnvBase as OnlineEnv
from Configurables import DummyRunable
from Configurables import Online__AlgFlowManager as AlgFlowManager
from Configurables import HLTControlFlowMgr, EventLoopMgr
from pathlib import Path
from MooreOnlineConf.utils import (
    alignment_options,
    distribute_files,
)
from Configurables import Gaudi__Histograming__Sink__Root
from Configurables import HistogramPersistencySvc
from Configurables import AlignAlgorithm
from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc

application = GaudiOnline.Passthrough(
    outputLevel=OnlineEnv.OutputLevel,
    partitionName=OnlineEnv.PartitionName,
    partitionID=OnlineEnv.PartitionID,
    classType=GaudiOnline.Class1)
application.setup_fifolog()

online_options = alignment_options(OnlineEnv)

print(f"Will write analyzer output to {online_options.analyzer_output_path}")

if OnlineEnv.PartitionName == "LHCbA":
    INPUT_DATA_PATH = Path("/calib/align/LHCb/Velo")

    files = [
        sorted((INPUT_DATA_PATH / run).iterdir())
        for run in online_options.runs
    ]
else:
    INPUT_DATA_PATH = Path("input_data")
    files = [sorted(INPUT_DATA_PATH.iterdir())]

files_per_node = distribute_files(online_options.nodes, files)

utgid = os.environ["UTGID"]
worker_id = utgid.split("_")[1]
try:
    input_files = files_per_node[worker_id]
except KeyError:
    # When testing we run multiple instances on the same node
    # TODO this should probably be done based on the partition name
    #      and also "nodes" should be renamed to workers everywhere.
    worker_id = utgid
    input_files = files_per_node[worker_id]

# TODO handle case where input_files is empty (here and in iterator)

derivfile = online_options.analyzer_output_path / f"derivatives-{worker_id}.out"
histofile = online_options.analyzer_output_path / f"histograms-{worker_id}.root"
print(f"analyzer node input files {input_files}")
input_files = [str(f) for f in input_files]

Gaudi__Histograming__Sink__Root(
    "Gaudi__Histograming__Sink__Root").FileName = str(
        histofile.with_stem(histofile.stem + "_new"))
HistogramPersistencySvc().OutputFile = str(histofile)
AlignAlgorithm().OutputDataFile = str(derivfile)
AlignAlgorithm().OnlineOverlayPath = str(
    online_options.iterator_output_path / "OverlayRoot")

print("iterator path", online_options.iterator_output_path)
DD4hepSvc().UseConditionsOverlay = True

application.setup_file_access(input_files)

flow = AlgFlowManager("EventLoop")
application.app.EventLoop = flow

# HACK: transfer options from HLTControlFlowMgr
cfm = HLTControlFlowMgr('HLTControlFlowMgr')
flow.CompositeCFNodes = cfm.CompositeCFNodes
flow.BarrierAlgNames = cfm.BarrierAlgNames

application.setup_monitoring()
application.monSvc.DimUpdateInterval = 1

application.config.enablePause = True
# Use execMode = 1 for multi-threaded (async_queued) mode and
# use 0 (default) for single-threaded (sync) mode, i.e. debugging.
application.config.execMode = 1
application.config.numEventThreads = 4
application.config.numStatusThreads = 1
application.config.FILE_maxEventsIn = 5000
application.app.Runable = DummyRunable()
application.app.TopAlg.insert(0, application.updateAndReset)
application.app.EvtSel = 'NONE'
application.app.EvtMax = 1000
application.app.StopOnSignal = False  # = default
# TODO: add back once ResettingSink is merged
# from Configurables import LHCb__Alignment__ResettingSink as ResettingSink
# resettingSink = ResettingSink()
# application.app.ExtSvc.append(resettingSink)
# print("ExtSvc", application.app.ExtSvc)
EventLoopMgr().Warnings = False
