###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Test CALO data.

MooreOnline/run gaudirun.py MooreOnline/MooreOnlineConf/options/{calo_data,calo}.py

"""
import glob
from Moore import options

options.input_files = [
    '/daqarea1/lhcb/data20211030/0000222606/Run_0000222606_20211030-060856-210_R2EB02.mdf'
]
# options.input_files = list(glob.glob('/daqarea1/lhcb/Run_0000222822_*.mdf'))

options.input_type = 'MDF'
options.evt_max = 10000
# options.output_level = 1

options.histo_file = 'calo.root'
