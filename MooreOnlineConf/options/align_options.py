###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
from Moore import options
from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc

options.input_type = 'Online'
options.dddb_tag = 'upgrade/master'
options.conddb_tag = 'upgrade/master'
options.geometry_version = "run3/trunk"
options.conditions_version = "master"

options.n_threads = 8

online_cond_path = '/group/online/hlt/conditions.run3/lhcb-conditions-database'
if os.path.exists(online_cond_path):
    DD4hepSvc().ConditionsLocation = 'file://' + online_cond_path
