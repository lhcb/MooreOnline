###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from RecoConf.decoders import default_VeloCluster_source
from AllenConf.velo_reconstruction import decode_velo

default_VeloCluster_source.global_bind(bank_type="VP")
decode_velo.global_bind(retina_decoding=False)
