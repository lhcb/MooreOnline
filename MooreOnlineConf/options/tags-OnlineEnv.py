###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
from pathlib import Path
from Moore import options
from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc
import OnlineEnvBase as OnlineEnv

options.input_type = 'Online'
options.output_type = 'Online'
options.geometry_version = OnlineEnv.DDDBTag
options.conditions_version = OnlineEnv.CondDBTag
if options.geometry_version == "master":
    options.geometry_version = "run3/trunk"
options.simulation = False

if not OnlineEnv.CondDBTag or OnlineEnv.CondDBTag == "master":
    online_cond_path = '/group/online/hlt/conditions.run3/lhcb-conditions-database'
    if os.path.exists(online_cond_path):
        dd4hep_svc = LHCb__Det__LbDD4hep__DD4hepSvc()
        dd4hep_svc.ConditionsLocation = 'file://' + online_cond_path

run_list_path = Path(OnlineEnv.__file__).with_name("RunList.opts")
run_list = []
if run_list_path.is_file():
    import re
    from PyConf.Algorithms import PatPV3DFuture

    def all_in_range(runs, first, last):
        return all(r >= first and r <= last for r in runs)

    # OnlineEnv.DeferredRuns = { "LHCb/0000273190", "LHCb/0000273192" };
    m = re.search(r"OnlineEnv.DeferredRuns *= *{(.*)} *;",
                  run_list_path.open().read())
    run_list = [int(s.removeprefix("LHCb/")) for s in eval(f"[{m.group(1)}]")]

# Manual setting of beam spot offsets
if run_list:
    if all_in_range(run_list, 267976, 268139):  # post TS1 VDM (June)
        offset = (0.1, 0.119)
    elif all_in_range(run_list, 268205, 270737):  # post TS1 pp
        offset = (0.1, -0.231)
    elif all_in_range(run_list, 275583, 276552):  # high beta run
        # The offset was not explicitly updated for the high beta run.
        # We simply used what was there for the VDM scans just before.
        offset = (0.1, 0.119)
    elif all_in_range(run_list, 276757, 276766):  # pp "reference" run
        offset = (0.064, -0.073)  # https://lblogbook.cern.ch/HLT/370
    elif all_in_range(run_list, 277410, 999999):
        # IR condition fallback is fine for LEAD23 (offset is small): https://lblogbook.cern.ch/HLT/386
        # IR condition implemented since 277473, see https://lblogbook.cern.ch/HLT/388
        offset = None
    else:
        raise RuntimeError(
            f"RunList {min(run_list)}..{max(run_list)} is not entirely in a known range!"
        )

    if offset is not None:
        print(f"WARNING forcing beam spot offset to {offset}", flush=True)
        PatPV3DFuture.global_bind(
            BeamLineOffsetX=offset[0], BeamLineOffsetY=offset[1])
