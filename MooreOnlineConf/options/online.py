###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
import GaudiOnline
import OnlineEnvBase as OnlineEnv
from Configurables import (
    Online__OutputAlg as OutputAlg,
    ApplicationMgr,
    OnlMonitorSink,
    HiveDataBrokerSvc,
    HLTControlFlowMgr,
)
from Gaudi.Configuration import allConfigurables

task_type = os.getenv("TASK_TYPE", "GenericTask")

application = GaudiOnline.Passthrough(
    outputLevel=OnlineEnv.OutputLevel,
    partitionName=OnlineEnv.PartitionName,
    partitionID=OnlineEnv.PartitionID,
    classType=GaudiOnline.Class1)
application.setup_fifolog()
application.setup_mbm_access('Events', True)

for name, configurable in allConfigurables.items():
    if isinstance(configurable, OutputAlg):
        writer = application.setup_mbm_output(
            'Output', name=configurable.name())
        writer.MBM_maxConsumerWait = 10
        writer.MBM_allocationSize = 1024 * 1024  # bytes

# Make sure we don't overwrite requests to publish histograms/counters
onl_sink = OnlMonitorSink()
# TODO make sure the default HistogramsToPublish=[] accepts everything and the lines below are not necessary
if not onl_sink.isPropertySet("HistogramsToPublish"):
    onl_sink.HistogramsToPublish = [(".*", ".*")]
if not onl_sink.isPropertySet("CountersToPublish"):
    onl_sink.CountersToPublish = [("Combiner", "# passed"),
                                  ("Prescaler", "#accept")]
if onl_sink not in ApplicationMgr().ExtSvc:
    ApplicationMgr().ExtSvc.append(onl_sink)

application.setup_monitoring(task_type)
application.updateAndReset.saveHistograms = 1
application.updateAndReset.saveSetDir = "/hist/Savesets"
# Never update the "bulk" DIM histogram service for HLT2 and monitoring tasks.
# (For HLT1 it has to be done since the adders rely on it)
application.monSvc.DimUpdateInterval = 1000000  # timer interval in seconds
application.monSvc.HistUpdateOnStop = False  # do not update on stop
# prevent duplicated entries in the output...
application.monSvc.disableDeclareInfoHistos = True
if task_type == "RecoMon":
    # intermediate saveset period in seconds, for RecoMon a bit more often than for the others
    application.updateAndReset.saverCycle = 300
elif task_type != "HLT2" and task_type != "CalibMon":
    # intermediate saveset period in seconds
    application.updateAndReset.saverCycle = 600
else:
    application.updateAndReset.saveSetFilePrefix = (
        "ByRun/${TASKNAME}/ToMerge/" +
        "${RUN10000}/${RUN1000}/${RUN}/${UTGID}-${RUN}-${TIME}")
    # big number such that we don't save intermediate savesets:
    application.updateAndReset.saverCycle = 3600 * 24 * 7
    application.updateAndReset.publishSaveSetLocation = False

# TODO we should remove update_and_reset from task python options
from MooreOnlineConf.utils import update_and_reset
uar_config = update_and_reset().configuration()
uar_algs = uar_config.apply()[0]
HiveDataBrokerSvc().DataProducers.extend(uar_algs)
HLTControlFlowMgr('HLTControlFlowMgr').PreambleAlgs = uar_algs
# TODO add histogram of number of executions and filter passed

if OnlineEnv.PartitionName.startswith("TEST"):
    application.updateAndReset.saveSetDir = "Savesets"
    # application.updateAndReset.saverCycle = 20

n_instances = int(os.getenv("NBOFSLAVES", "0")) + 1

try:
    n_threads = int(os.environ["NBOFTHREADS"])
    # NBOFTHREADS is populated from the "-numthreads" argument in Arch.xml
except (KeyError, ValueError):
    n_threads = 1
    if task_type == "HLT2":
        # run as many threads as we have cores available to this process
        # (In case of binding to a numa domain, we have
        # len(os.sched_getaffinity(0)) < multiprocessing.cpu_count() )
        n_threads = len(os.sched_getaffinity(0))

# Use execMode = 1 for multi-threaded (async_queued) mode and
# use 0 (default) for single-threaded (sync) mode, i.e. debugging.
application.config.execMode = 1
application.config.numEventThreads = n_threads
HLTControlFlowMgr().ThreadPoolSize = n_threads
# Enable controlling number of threads with a DIM command
application.config.numThreadSvcName = 'NumThreads'

event_store = allConfigurables["EventDataSvc"]
# Ensure enough event slots to "bridge" the short wait when
# switching event bursts.
event_store.EventSlots = max(
    event_store.getProp("EventSlots"),
    n_threads + max(int(0.25 * n_threads), 1))

if task_type == "HLT2":
    # for HLT2 we must use UserType=ONE, i.e. every event must be seen
    # by exactly one consumer (we have 2 HLT2 tasks per node).
    input_mbm_user_type = "ONE"
    # there is also UserType=VIP, i.e. every event must be seen by every
    # consumer.
else:
    # For monitoring tasks we use UserType=USER, i.e. the task may not see
    # all events if it can't process fast enough. However, for testing we
    # use "ONE" so that the reader does not keep on pumping events when
    # they are not consumer (i.e. a debugger is attached).
    input_mbm_user_type = "USER"
    if OnlineEnv.PartitionName.startswith("TEST"):
        input_mbm_user_type = "ONE"

if task_type == "HLT2":
    application.config.events_LowMark = 1000
    application.config.events_HighMark = 1200
    # There are relatively frequent, relatively slow events which block
    # the corresponding MBM connections. Until all events for a given
    # burst are processed, we cannot reuse a connection, so if we don't
    # have enough connections (relative to the CPU power available),
    # this can stall processing.
    # On the fastest Intel 2630-v4 we need 5 connections, and on the new
    # Xeon 8592 we need 20. We cannot increase beyond what the MBM config
    # allows (accounting for reader connections).
    # The connections are distributed equally among all instances and we
    # limit them to the number of threads (useful for e.g. HLT2Slim).
    application.config.MBM_numConnections = min(20 // n_instances, n_threads)
    # When processing PbPb data, we may need to tune this further
    # (in 2023 we needed 24), since some events take a very long time to process.
else:
    application.config.MBM_numConnections = 1

application.config.MBM_numEventThreads = 1
# Special setting for PbPb:
# application.config.MBM_numEventThreads = 2
application.config.MBM_requests = [
    'EvType=2;TriggerMask=0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF;' +
    f'VetoMask=0,0,0,0;MaskType=ANY;UserType={input_mbm_user_type};' +
    'Frequency=PERC;Perc=100.0'
]

if task_type != "HLT2":
    # Speed up monitoring tasks by not loading the full geometry
    from Configurables import LHCb__DetDesc__ReserveDetDescForEvent as reserveIOV
    reserveIOV("reserveIOV").PreloadGeometry = False

application.config.expandTAE = True
