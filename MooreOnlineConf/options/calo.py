###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
#############################################################################
from PyConf.application import default_raw_banks
from Moore import options
from PyConf.Algorithms import CaloFutureRawToDigits
from RecoConf.calorimeter_reconstruction import make_digits, make_clusters, make_digits
from RecoConf.reconstruction_objects import reconstruction
from RecoConf.hlt2_global_reco import reconstruction as hlt2_reconstruction, make_calo_only_reconstruction
from RecoConf.config import Reconstruction, run_reconstruction
from RecoConf.calo_data_monitoring import monitor_calo_digits, monitor_calo_clusters, monitor_calo_time_alignment, monitor_calo_pedestal, monitor_calo_led
from RecoConf.standalone import reco_prefilters
from Hlt2Conf.lines.monitoring.pi0_line import pi0_monitoring

options.dddb_tag = 'upgrade/master'
options.conddb_tag = 'upgrade/master'


def calo_moni():
    data = []
    if options.input_type.lower() == 'online':
        from MooreOnlineConf.utils import update_and_reset
        data.append(update_and_reset())

    ecal_raw = default_raw_banks("Calo")
    ecal_raw_error = default_raw_banks("CaloError")
    hcal_raw = default_raw_banks("Calo")
    hcal_raw_error = default_raw_banks("CaloError")

    digits = make_digits(calo_raw_bank=True)
    ecalClusters = make_clusters(digits["digitsEcal"])
    calo = digits | {
        "ecalClusters": ecalClusters,
    }

    data += monitor_calo_digits(
        calo,
        adcFilterEcal=-100,
        adcFilterHcal=-100,
        HistoMultiplicityMaxEcal=6000,
        HistoMultiplicityMaxHcal=4000)
    data += monitor_calo_clusters(ecalClusters, split_clusters=None)
    data += monitor_calo_time_alignment(
        calo, adcFilterEcal=-100, adcFilterHcal=-100)
    data += monitor_calo_pedestal(calo, adcFilterEcal=-100, adcFilterHcal=-100)
    data += monitor_calo_led(calo)

    with reconstruction.bind(from_file=False),\
        hlt2_reconstruction.bind(make_reconstruction=make_calo_only_reconstruction),\
        make_digits.bind(calo_raw_bank=True):
        data += pi0_monitoring(name="calo_only_pi0_moni")
    return Reconstruction('calo_moni', data, reco_prefilters(gec=False))


with CaloFutureRawToDigits.bind(UseParamsFromDB=False):
    run_reconstruction(options, calo_moni)
