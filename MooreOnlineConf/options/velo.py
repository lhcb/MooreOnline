###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import sys, os
from collections import namedtuple
from PyConf.application import default_raw_event, default_raw_banks, make_odin
from PyConf.Tools import TrackMasterFitter, TrackMasterExtrapolator, SimplifiedMaterialLocator, TrackInterpolator, TrackLinearExtrapolator
from PyConf.Algorithms import VeloRetinaClusterTrackingSIMD, fromPrVeloTracksV1TracksMerger, VeloClusterTrackingSIMD, TrackEventFitter
from PyConf.Algorithms import VPClusterMonitors, VPBunchMonitors, VPDQMonitors, TrackPV2HalfMonitor, TrackVertexMonitor, VPTAEMonitors, VPTrackMonitor
from PyConf.Algorithms import OdinTypesFilter, HltRoutingBitsFilter  #trigger + bxid filter
from PyConf.Algorithms import VoidFilter, HltDecReportsDecoder
from PyConf.control_flow import CompositeNode, NodeLogic
from PyConf.utilities import DISABLE_TOOL
from Moore import options
from RecoConf.config import Reconstruction, run_reconstruction
from RecoConf.standalone import reco_prefilters
from RecoConf.legacy_rec_hlt1_tracking import make_RetinaClusters, make_PatPV3DFuture_pvs, make_velo_full_clusters, get_default_ut_clusters
from RecoConf.legacy_rec_hlt1_tracking import get_global_clusters_on_track_tool_only_velo
from RecoConf.hlt2_tracking import TrackBestTrackCreator, get_global_measurement_provider, make_VPClus_hits
from RecoConf.hlt2_tracking import get_track_master_fitter
from RecoConf.decoders import default_VeloCluster_source
from Configurables import LHCbApp, ApplicationMgr, HiveDataBrokerSvc
import Functors as F

HiveDataBrokerSvc().OutputLevel = 0

from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc
dd4hepSvc = DD4hepSvc()
dd4hepSvc.DetectorList = [
    '/world', 'VP', 'FT', 'Magnet', 'Rich1', 'Rich2', 'Ecal', 'Hcal', 'Muon'
]  #Needed to aviod UT Geom


def get_my_track_master_fitter():
    with TrackMasterFitter.bind(
            MaxNumberOutliers=2,
            NumberFitIterations=10,
            FastMaterialApproximation=True):
        return get_track_master_fitter(
            clusters_on_track_tool=get_global_clusters_on_track_tool_only_velo)


def myDefRawEvent(banks):
    return default_raw_event("VPRetinaCluster")


velo_clusters_algorithm_light = make_RetinaClusters
velo_clusters_algorithm_full = make_velo_full_clusters
velo_tracking_algorithm = VeloRetinaClusterTrackingSIMD
bankType = "VPRetinaCluster"

#if the environment is SP then the retina clusters will have failed!
#if int(os.getenv("VELO_SP", "0")):
#note
if not int(os.environ["VELO_SP"] == "0"):
    velo_clusters_algorithm_light = make_VPClus_hits
    velo_tracking_algorithm = VeloClusterTrackingSIMD
    bankType = "VP"


def lumiFilter():
    return HltRoutingBitsFilter(
        name="RBFilterBitLumi",
        RawBanks=default_raw_banks('HltRoutingBits'),
        RequireMask=(1 << 1, 0, 0),
        PassOnError=False)


def return_filter(name, hlt_filter_code, filter_source_id):
    if not isinstance(hlt_filter_code, list):
        hlt_filter_code = [hlt_filter_code]
    assert all(
        'Decision' in line for line in
        hlt_filter_code), "hlt_filter_code must use the line DECISION_SUFFIX"
    if not all(filter_source_id in line for line in hlt_filter_code):
        raise ValueError(
            "A {!r} filter can only interpret {!r} line filters.".format(
                filter_source_id, filter_source_id))
    line_regex = "|".join(line for line in hlt_filter_code)
    print(type(default_raw_banks("HltDecReports")))
    hlt_dec_reports = HltDecReportsDecoder(
        RawBanks=default_raw_banks("HltDecReports"), SourceID=filter_source_id)
    return VoidFilter(
        name=name,
        Cut=F.DECREPORTS_RE_FILTER(
            Regex=line_regex,
            DecReports=hlt_dec_reports.OutputHltDecReportsLocation))


def velo_mon():
    algs = []
    if options.input_type.lower() == 'online':
        from MooreOnlineConf.utils import update_and_reset
        algs.append(update_and_reset())

    velo_clusters_light = velo_clusters_algorithm_light()
    velo_clusters_full = velo_clusters_algorithm_full()
    clusters = velo_tracking_algorithm(
        RawBanks=default_raw_banks(bankType),
        MaxScatterSeeding=0.1,
        MaxScatterForwarding=0.1,
        MaxScatter3hits=0.02,
        SkipForward=4)

    #collect the locations
    vpTracks = clusters.TracksLocation
    vpTracks_backwards = clusters.TracksBackwardLocation

    vpTracks_v1 = fromPrVeloTracksV1TracksMerger(  # converts Pr -> v1 tracks and merges forward/backward
        InputTracksLocation1=vpTracks,
        InputTracksLocation2=vpTracks_backwards,
        TrackAddClusterTool=get_global_clusters_on_track_tool_only_velo()
    ).OutputTracksLocation

    #maybe overkill but better safe than sorry
    my_TrackMasterFitter = get_my_track_master_fitter()

    #fit the tracks
    fittedTracks = TrackEventFitter(
        TracksInContainer=vpTracks_v1,
        Fitter=(my_TrackMasterFitter),
        MaxChi2DoF=2.8,
        name="TrackEventFitter_{hash}").TracksOutContainer

    #These are best velo ONLY tracks
    best_velo_tracks = TrackBestTrackCreator(
        name="TrackBestTrackCreator_{hash}",
        TracksInContainers=[fittedTracks],
        DoNotRefit=True,
        AddGhostProb=False,
        FitTracks=False,
        MaxChi2DoF=2.8,
    ).TracksOutContainer

    pvs = make_PatPV3DFuture_pvs({"v1": best_velo_tracks}, None)

    track_vertex_monitor = TrackVertexMonitor(
        name="TrackVertexMonitorAll",
        MaxXPV=40.0,
        MaxYPV=40.0,
        NumTracksPV=1,
        PVContainer=pvs['v1'],
        TrackContainer=best_velo_tracks)

    # needed?
    vertex_monitor = TrackPV2HalfMonitor(
        name="TrackPVHalfMonitorAll",
        TrackContainer=best_velo_tracks,
        ODINLocation=make_odin(),
        limPx=40.0,
        limPy=40.0)

    rebinValue = 0
    isLocal = True
    runLiteMonitor = False  #needs update VPCLusterMon
    if os.environ["PARTITION"] == "LHCb":
        rebinValue = 4
        runLiteMonitor = True
        isLocal = False

    cluster_monitor = VPClusterMonitors(
        name="VPClusterMonitors_BeamCrossing",
        ClusterLocation=velo_clusters_full,
        AllASICs=True,
        DebugMode=False,
        LocalFlag=isLocal,
        RebinValue=rebinValue,  # rebins in powers of 2, max_val = 8
        ODINLocation=make_odin())

    algs.append(track_vertex_monitor)
    algs.append(vertex_monitor)
    algs.append(cluster_monitor)

    # BXTypes: NoBeam, Beam1, Beam2 BeamCrossing
    odinFilter = OdinTypesFilter(ODIN=make_odin(), BXTypes=['BeamCrossing'])
    filters = [odinFilter]
    if os.environ["PARTITION"] == "LHCb":
        #filters.append(lumiFilter())
        filters.append(
            return_filter("hlt1_prefilter", [
                "Hlt1VeloMicroBiasVeloClosingDecision",
                "Hlt1VeloMicroBiasDecision"
            ], "Hlt1"))

    control_flow = filters + algs
    cf_node = CompositeNode(
        "beambeam_decision",
        control_flow,
        combine_logic=NodeLogic.LAZY_AND,
        force_order=True)

    return cf_node


def VPDQTrackNode(eventFilter="BeamCrossing"):

    velo_clusters_light = velo_clusters_algorithm_light()
    velo_clusters_full = velo_clusters_algorithm_full()

    data = []
    DQ_monitor = VPDQMonitors(
        name="VPDQMonitors{0}".format(eventFilter),
        allow_duplicate_instances_with_distinct_names=True,
        ClusterLocation=velo_clusters_full,
        RawEventLocation=default_raw_event("VPRetinaCluster"))
    data += [DQ_monitor]

    if eventFilter == "NoBeam":
        cluster_monitor = VPClusterMonitors(
            name="VPClusterMonitors_NoBeam",
            ClusterLocation=velo_clusters_full,
            AllASICs=True,
            DebugMode=False,
            LocalFlag=True,
            ODINLocation=make_odin())
        data.append(cluster_monitor)

    if not eventFilter == "NoBeam":
        clusters = velo_tracking_algorithm(
            RawBanks=default_raw_banks(bankType),
            MaxScatterSeeding=0.1,
            MaxScatterForwarding=0.1,
            MaxScatter3hits=0.02,
            SkipForward=4)

        #collect the locations
        vpTracks = clusters.TracksLocation
        vpTracks_backwards = clusters.TracksBackwardLocation

        vpTracks_v1 = fromPrVeloTracksV1TracksMerger(  # converts Pr -> v1 tracks and merges forward/backward
            InputTracksLocation1=vpTracks,
            InputTracksLocation2=vpTracks_backwards,
            TrackAddClusterTool=get_global_clusters_on_track_tool_only_velo()
        ).OutputTracksLocation

        #maybe overkill but better safe than sorry
        my_TrackMasterFitter = get_my_track_master_fitter()

        #fit the tracks
        fittedTracks = TrackEventFitter(
            TracksInContainer=vpTracks_v1,
            Fitter=(my_TrackMasterFitter),
            MaxChi2DoF=2.8,
            name="TrackEventFitter_{hash}").TracksOutContainer

        #These are best velo  ONLY tracks
        best_velo_tracks = TrackBestTrackCreator(
            name="TrackBestTrackCreator_{hash}",
            TracksInContainers=[fittedTracks],
            DoNotRefit=True,
            AddGhostProb=False,
            FitTracks=False,
            MaxChi2DoF=2.8,
        ).TracksOutContainer

        #https://gitlab.cern.ch/lhcb/LHCb/-/blob/master/Event/TrackEvent/include/Event/TrackEnums.h
        track_monitor_best = VPTrackMonitor(
            TrackContainer=best_velo_tracks,
            name="VPTrackMonitor_{0}".format(eventFilter),
            ClusterContainer=velo_clusters_light,
            ResidualDebug=False,
            allow_duplicate_instances_with_distinct_names=True,
            TrackEnum=1)
        data.append(track_monitor_best)

    # BXTypes: NoBeam, Beam1, Beam2 BeamCrossing
    filterL = HltRoutingBitsFilter(
        name="RBFilterBitLumi",
        RawBanks=default_raw_banks('HltRoutingBits'),
        RequireMask=(1 << 1, 0, 0),
        PassOnError=False)

    odinFilter = OdinTypesFilter(ODIN=make_odin(), BXTypes=[eventFilter])
    filters = [odinFilter]
    if os.environ["PARTITION"] == "LHCb":
        #filters.append(lumiFilter())
        filters.append(
            return_filter("hlt1_prefilter", [
                "Hlt1VeloMicroBiasVeloClosingDecision",
                "Hlt1VeloMicroBiasDecision"
            ], "Hlt1"))

    control_flow = filters + data
    cf_node = CompositeNode(
        "node_{0}".format(eventFilter),
        control_flow,
        combine_logic=NodeLogic.LAZY_AND,
        force_order=True)
    return cf_node


# wrapper for gaudi
class MySequence(namedtuple("MySequence", ["node"])):
    def __new__(cls, node):
        return super(MySequence, cls).__new__(cls, node)

    def __str__(self):
        return "MySequence({0})".format(self.node)


def my_sequence():
    main_node = velo_mon()
    beambeam = VPDQTrackNode("BeamCrossing")
    beam1 = VPDQTrackNode("Beam1")
    beam2 = VPDQTrackNode("Beam2")
    empty = VPDQTrackNode("NoBeam")

    velo_clusters_full = velo_clusters_algorithm_full()
    TAE_monitor = VPTAEMonitors(
        name="VPTAEMonitors",
        ClusterLocation=velo_clusters_full,
        ODINLocation=make_odin(),
        DebugMode=False)

    bunch_monitor = VPBunchMonitors(
        name="VPBunchMonitorsAll",
        ODINLocation=make_odin(),
        ClusterLocation=velo_clusters_full,
        ClearNoise=False,
        PrintInfo=False,
        DebugMode=False)

    print("\n\n\n\n")
    print(os.environ["PARTITION"])
    print("\n\n\n\n")
    finalNode = CompositeNode(
        "MainNode",
        [main_node, beambeam, beam1, beam2, empty, TAE_monitor, bunch_monitor],
        combine_logic=NodeLogic.NONLAZY_OR,
        force_order=False)
    return MySequence(finalNode)


options.conditions_version = 'master'
options.geometry_version = 'run3/trunk'
options.simulation = False

with TrackMasterFitter.bind(FastMaterialApproximation = True),\
    get_default_ut_clusters.bind(disable_ut=True),\
    get_global_measurement_provider.bind(velo_hits=velo_clusters_algorithm_light, ignoreUT=True, ignoreFT=True,ut_provider=DISABLE_TOOL,ft_provider=DISABLE_TOOL):
    run_reconstruction(options, my_sequence)
