###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Configuration for the CalibMon task
"""

from GaudiKernel.SystemOfUnits import (TeV, GeV, MeV, micrometer as um, ps)

import Functors as F
from Functors.math import in_range
from PyConf.Algorithms import (Monitor__ParticleRange, OdinTypesFilter,
                               HltRoutingBitsFilter)
from PyConf.application import (make_odin, default_raw_banks)

from Moore import (options, run_moore)
from Moore.config import (Hlt2Line, register_line_builder)

from RecoConf.event_filters import require_pvs
from RecoConf.global_tools import stateProvider_with_simplified_geom

from RecoConf.reconstruction_objects import (make_pvs, reconstruction,
                                             upfront_reconstruction)

from Hlt2Conf.standard_particles import (
    make_long_kaons,
    make_long_pions,
    make_long_protons,
)
from Hlt2Conf.algorithms_thor import (ParticleCombiner, ParticleFilter)
from Hlt2Conf.lines.pid.utils import filters as flt
from Hlt2Conf.lines.pid.utils.charmonium import (
    make_jpsi_to_ee, make_tag_electrons, make_probe_electrons)
from Hlt2Conf.lines.trackeff import KSVeloLong
from Hlt2Conf.settings.hlt2_binds import config_pp_2024

all_lines = {}

all_lines.update(KSVeloLong.monitoring_lines)


def _calibmon_filters():
    rb_filter = HltRoutingBitsFilter(
        RawBanks=default_raw_banks('HltRoutingBits'),
        RequireMask=(1 << 14, 0, 0),
        PassOnError=False)
    odin_bb_filter = OdinTypesFilter(
        ODIN=make_odin(), BXTypes=['BeamCrossing'])
    return upfront_reconstruction() + [
        odin_bb_filter, rb_filter,
        require_pvs(make_pvs())
    ]


@register_line_builder(all_lines)
def DstToD0Pi_D0ToKPi(name="Hlt2PID_DstToD0Pi_D0ToKPi"):
    pvs = make_pvs()
    kaons = ParticleFilter(
        make_long_kaons(),
        F.FILTER(
            F.require_all(F.P > 2 * GeV, F.PT > 250 * MeV, F.CHI2DOF < 3,
                          F.MINIPCHI2CUT(IPChi2Cut=16, Vertices=pvs))))
    pions = ParticleFilter(
        make_long_pions(),
        F.FILTER(
            F.require_all(F.P > 2 * GeV, F.PT > 250 * MeV, F.CHI2DOF < 3,
                          F.MINIPCHI2CUT(IPChi2Cut=16, Vertices=pvs))))
    slow_pions = ParticleFilter(
        make_long_pions(),
        F.FILTER(
            F.require_all(F.P > 1 * GeV, F.PT > 100 * MeV, F.CHI2DOF < 3)))
    d0s = ParticleCombiner(
        [kaons, pions],
        name='PID_D0ToKPi_Combiner_{hash}',
        DecayDescriptor="[D0 -> K- pi+]cc",
        CombinationCut=F.require_all(
            in_range(1769.84 * MeV, F.MASS, 1959.84 * MeV),
            F.PT > 1500 * MeV,
            F.MAXDOCACUT(80 * um),
        ),
        CompositeCut=F.require_all(
            in_range(1800 * MeV, F.MASS, 1915 * MeV),
            F.CHI2DOF < 10,
            F.BPVFDCHI2(pvs) > 49,
            F.BPVDIRA(pvs) > 0.99997,
            (F.MASSWITHHYPOTHESES(
                ("pi-", "K+")) < 1839.84 * MeV) | (F.MASSWITHHYPOTHESES(
                    ("pi-", "K+")) > 1889.84 * MeV),
            (F.MASSWITHHYPOTHESES(
                ("pi-", "pi+")) < 1839.84 * MeV) | (F.MASSWITHHYPOTHESES(
                    ("pi-", "pi+")) > 1889.84 * MeV),
            (F.CHILD(1, F.PT) > 1 * GeV) | (F.CHILD(2, F.PT) > 1 * GeV),
        ),
    )
    dsts = ParticleCombiner(
        [d0s, slow_pions],
        name='PID_DstToD0Pi_Combiner_{hash}',
        DecayDescriptor="[D*(2010)+ -> D0 pi+]cc",
        CombinationCut=F.require_all(
            in_range(1897.76 * MeV, F.MASS,
                     2122.76 * MeV), F.MASS - F.CHILD(1, F.MASS) > 135 * MeV,
            F.MASS - F.CHILD(1, F.MASS) < 160 * MeV),
        CompositeCut=F.require_all(
            in_range(1945.26 * MeV, F.MASS, 2085.26 * MeV), F.CHI2DOF < 10),
    )

    dst_hist_dict = {
        'nPVs': {
            'input': d0s,
            'variable': F.SIZE(pvs),
            'bins': 10,
            'range': (0, 10)
        },
        'D_PT': {
            'input': d0s,
            'variable': F.PT,
            'bins': 100,
            'range': (0, 40000)
        },
        'D_BPVIPCHI2': {
            'input': d0s,
            'variable': F.BPVIPCHI2(pvs),
            'bins': 100,
            'range': (0, 5000)
        },
        'K_ETA': {
            'input': kaons,
            'variable': F.ETA,
            'bins': 100,
            'range': (2, 5)
        },
        'K_P': {
            'input': kaons,
            'variable': F.P,
            'bins': 100,
            'range': (0, 250000)
        },
        'K_PT': {
            'input': kaons,
            'variable': F.PT,
            'bins': 100,
            'range': (0, 20000)
        },
        'K_TCHI2DOF': {
            'input': kaons,
            'variable': F.CHI2DOF,
            'bins': 100,
            'range': (0, 4)
        },
        'K_GHOSTPROB': {
            'input': kaons,
            'variable': F.GHOSTPROB,
            'bins': 100,
            'range': (0, 1)
        },
        'pi_ETA': {
            'input': pions,
            'variable': F.ETA,
            'bins': 100,
            'range': (2, 5)
        },
        'pi_P': {
            'input': pions,
            'variable': F.P,
            'bins': 100,
            'range': (0, 300000)
        },
        'pi_PT': {
            'input': pions,
            'variable': F.PT,
            'bins': 100,
            'range': (0, 25000)
        },
        'pi_TCHI2DOF': {
            'input': pions,
            'variable': F.CHI2DOF,
            'bins': 100,
            'range': (0, 4)
        },
        'pi_GHOSTPROB': {
            'input': pions,
            'variable': F.GHOSTPROB,
            'bins': 100,
            'range': (0, 1)
        },
    }

    dst_hist_list = []
    for histname, params in dst_hist_dict.items():
        pid_mon = Monitor__ParticleRange(
            Input=params['input'],
            Variable=params['variable'],
            HistogramName=f"/{name}/{histname}",
            Bins=params['bins'],
            Range=params['range'],
        )
        dst_hist_list.append(pid_mon)

    return Hlt2Line(
        name=name,
        algs=_calibmon_filters() + [dsts] + dst_hist_list,
        postscale=0)


@register_line_builder(all_lines)
def L0ToPPi_LL(name="Hlt2CalibMon_L0ToPPi_LL"):
    pvs = make_pvs()
    protons = ParticleFilter(
        make_long_protons(),
        F.FILTER(
            F.require_all(F.P > 2 * GeV, F.PT < 1 * TeV, F.CHI2DOF < 4,
                          F.MINIPCHI2CUT(IPChi2Cut=36, Vertices=pvs))))
    pions = ParticleFilter(
        make_long_pions(),
        F.FILTER(
            F.require_all(F.CHI2DOF < 4,
                          F.MINIPCHI2CUT(IPChi2Cut=36, Vertices=pvs))))
    l0lls = ParticleCombiner(
        [protons, pions],
        name='PID_L0ToPPi_LL_Combiner_{hash}',
        DecayDescriptor="[Lambda0 -> p+ pi-]cc",
        CombinationCut=F.require_all(
            in_range(1065.683 * MeV, F.MASS, 1165.683 * MeV)),
        CompositeCut=F.require_all(
            in_range(1095.683 * MeV, F.MASS, 1135.683 * MeV),
            F.CHI2DOF < 30,
            F.MINIPCHI2(pvs) < 50,
            (F.MASSWITHHYPOTHESES(
                ("pi+", "pi-")) < 477.611 * MeV) | (F.MASSWITHHYPOTHESES(
                    ("pi+", "pi-")) > 517.611 * MeV),
            F.BPVLTIME(pvs) > 2 * ps,
        ),
    )

    # We want to fill a bunch of histograms. Define a partial dictionary for Lambda mass plots, and bin edges for differential PID efficiencies
    l_mass_part_dict = {
        'input': l0lls,
        'bins': 100,
        'range': (1095.683, 1135.683)
    }
    pt_bins = (0, 1.5, 3, 6, 1000)
    p_bins = (3, 30, 60, 100)
    eta_bins = (2, 3.5, 4.9)
    pid_bins = (-1000, -10, -5, 0, 5)
    # This is the dict we will use to configure the monitoring alg. Define some 1D histograms at initialization
    l0_hist_dict = {
        'integrated__L0_M__p_PID_P-neg':
        dict(l_mass_part_dict, variable=F.MASS * (F.CHILD(1, F.PID_P) < 0)),
        'integrated__L0_M__p_PID_PK-neg':
        dict(
            l_mass_part_dict,
            variable=F.MASS * (F.CHILD(1, F.PID_P) - F.CHILD(1, F.PID_K) < 0)),
        'nPVs': {
            'input': l0lls,
            'variable': F.SIZE(pvs),
            'bins': 10,
            'range': (0, 10)
        },
        'L0_PT': {
            'input': l0lls,
            'variable': F.PT,
            'bins': 100,
            'range': (0, 2200)
        },
        'L0_BPVIPCHI2': {
            'input': l0lls,
            'variable': F.BPVIPCHI2(pvs),
            'bins': 100,
            'range': (0, 60)
        },
        'p_ETA': {
            'input': protons,
            'variable': F.ETA,
            'bins': 100,
            'range': (2, 5)
        },
        'p_P': {
            'input': protons,
            'variable': F.P,
            'bins': 100,
            'range': (5000, 50000)
        },
        'p_PT': {
            'input': protons,
            'variable': F.PT,
            'bins': 100,
            'range': (0, 100000)
        },
        'p_TCHI2DOF': {
            'input': protons,
            'variable': F.CHI2DOF,
            'bins': 100,
            'range': (0, 5)
        },
        'p_GHOSTPROB': {
            'input': protons,
            'variable': F.GHOSTPROB,
            'bins': 100,
            'range': (0, 1)
        },
    }
    # Update the dict in loops
    for pid_cut in pid_bins:
        l0_hist_dict[f"integrated__L0_M__p_PID_P-{pid_cut}"] = dict(
            l_mass_part_dict,
            variable=F.MASS * (F.CHILD(1, F.PID_P) > pid_cut))
        l0_hist_dict[f"integrated__L0_M__p_PID_PK-{pid_cut}"] = dict(
            l_mass_part_dict,
            variable=F.MASS *
            (F.CHILD(1, F.PID_P) - F.CHILD(1, F.PID_K) > pid_cut))
        for pt_idx in range(0, len(pt_bins) - 1):
            for p_idx in range(0, len(p_bins) - 1):
                l0_hist_dict[
                    f"pT_P-{pt_bins[pt_idx]}-{pt_bins[pt_idx+1]}__p_P-{p_bins[p_idx]}-{p_bins[p_idx+1]}__L0_M__p_PID_P-{pid_cut}"] = dict(
                        l_mass_part_dict,
                        variable=F.MASS *
                        (in_range(pt_bins[pt_idx] * GeV, F.CHILD(1, F.PT),
                                  pt_bins[pt_idx + 1] * GeV) & in_range(
                                      p_bins[p_idx] * GeV, F.CHILD(1, F.P),
                                      p_bins[p_idx + 1] * GeV) &
                         (F.CHILD(1, F.PID_P) > pid_cut)))
                l0_hist_dict[
                    f"pT_P-{pt_bins[pt_idx]}-{pt_bins[pt_idx+1]}__p_P-{p_bins[p_idx]}-{p_bins[p_idx+1]}__L0_M__p_PID_P-{pid_cut}"] = dict(
                        l_mass_part_dict,
                        variable=F.MASS *
                        (in_range(pt_bins[pt_idx] * GeV, F.CHILD(1, F.PT),
                                  pt_bins[pt_idx + 1] * GeV) & in_range(
                                      p_bins[p_idx] * GeV, F.CHILD(1, F.P),
                                      p_bins[p_idx + 1] * GeV) &
                         (F.CHILD(1, F.PID_P) - F.CHILD(1, F.PID_K) > pid_cut))
                    )
            for eta_idx in range(0, len(eta_bins) - 1):
                l0_hist_dict[
                    f"pT_P-{pt_bins[pt_idx]}-{pt_bins[pt_idx+1]}__p_ETA-{eta_bins[eta_idx]}-{eta_bins[eta_idx+1]}__L0_M__p_PID_P-{pid_cut}"] = dict(
                        l_mass_part_dict,
                        variable=F.MASS *
                        (in_range(pt_bins[pt_idx] * GeV, F.CHILD(1, F.PT),
                                  pt_bins[pt_idx + 1] * GeV) & in_range(
                                      eta_bins[p_idx], F.CHILD(1, F.ETA),
                                      eta_bins[eta_idx + 1]) &
                         (F.CHILD(1, F.PID_P) > pid_cut)))
                l0_hist_dict[
                    f"pT_P-{pt_bins[pt_idx]}-{pt_bins[pt_idx+1]}__p_ETA-{eta_bins[eta_idx]}-{eta_bins[eta_idx+1]}__L0_M__p_PID_P-{pid_cut}"] = dict(
                        l_mass_part_dict,
                        variable=F.MASS *
                        (in_range(pt_bins[pt_idx] * GeV, F.CHILD(1, F.PT),
                                  pt_bins[pt_idx + 1] * GeV) & in_range(
                                      eta_bins[p_idx], F.CHILD(1, F.ETA),
                                      eta_bins[eta_idx + 1]) &
                         (F.CHILD(1, F.PID_P) - F.CHILD(1, F.PID_K) > pid_cut))
                    )

    l0_hist_list = []
    for histname, params in l0_hist_dict.items():
        p_pid_mon = Monitor__ParticleRange(
            Input=params['input'],
            Variable=params['variable'],
            HistogramName=f"/{name}/{histname}",
            Bins=params['bins'],
            Range=params['range'],
        )
        l0_hist_list.append(p_pid_mon)

    return Hlt2Line(
        name=name,
        algs=_calibmon_filters() + [l0lls] + l0_hist_list,
        postscale=0)


def _make_bs(jpsis, kaons, pvs):
    return ParticleCombiner([jpsis, kaons],
                            name='PID_BToJpsiK_Combiner_{hash}',
                            DecayDescriptor="[B+ -> J/psi(1S) K+]cc",
                            CombinationCut=F.require_all(
                                in_range(4.1 * GeV, F.MASS, 6.1 * GeV), ),
                            CompositeCut=F.require_all(
                                in_range(4.2 * GeV, F.MASS, 6 * GeV),
                                F.CHI2 < 9,
                                F.BPVFDCHI2(pvs) > 150))


@register_line_builder(all_lines)
def BToJpsiK_JpsiToEmbremEpbremTagged(
        name="Hlt2PID_BToJpsiK_JpsiToEmbremEpbremTagged", prescale=1):
    pvs = make_pvs()
    kaons = flt.filter_particles_dll(
        flt.filter_particles(
            make_long_kaons(),
            pvs,
            pt_min=1 * GeV,
            p_min=3 * GeV,
            mipchi2_min=9,
        ),
        dll_lim=5,
        dll_type="k",
        min=True,
    )
    e_tags = make_tag_electrons(charge=+1, brem=True)
    e_probes = make_probe_electrons(charge=-1, brem=True)
    jpsis = make_jpsi_to_ee(e_neg=e_probes, e_pos=e_tags)
    bs = _make_bs(jpsis, kaons, pvs)

    histograms3 = {
        'Bmass_Jpsi_constraint_eprobe_PIDe+5': {
            'input':
            bs,
            'variable':
            (F.MASS - F.CHILD(1, F.MASS) + F.PDG_MASS("J/psi(1S)")) * (F.CHILD(
                1, F.CHILD(1, F.PID_E > 5))),
            'bins':
            50,
            'range': (5100, 5600)
        },
        'Bmass_Jpsi_constraint_eprobe_PIDe-5': {
            'input':
            bs,
            'variable':
            (F.MASS - F.CHILD(1, F.MASS) + F.PDG_MASS("J/psi(1S)")) * (F.CHILD(
                1, F.CHILD(1, F.PID_E < 5))),
            'bins':
            50,
            'range': (5100, 5600)
        },
        'Bmass_Jpsi_constraint_eprobe_PIDe+0': {
            'input':
            bs,
            'variable':
            (F.MASS - F.CHILD(1, F.MASS) + F.PDG_MASS("J/psi(1S)")) * (F.CHILD(
                1, F.CHILD(1, F.PID_E > 0))),
            'bins':
            50,
            'range': (5100, 5600)
        },
        'Bmass_Jpsi_constraint_eprobe_PIDe-0': {
            'input':
            bs,
            'variable':
            (F.MASS - F.CHILD(1, F.MASS) + F.PDG_MASS("J/psi(1S)")) * (F.CHILD(
                1, F.CHILD(1, F.PID_E < 0))),
            'bins':
            50,
            'range': (5100, 5600)
        },
    }

    hist3 = []
    for j, params in histograms3.items():
        histogram3 = Monitor__ParticleRange(
            Input=params['input'],
            Variable=params['variable'],
            HistogramName=f"/{name}/{j}",
            Bins=params['bins'],
            Range=params['range'],
        )
        hist3.append(histogram3)

    return Hlt2Line(
        name=name,
        algs=_calibmon_filters() + [jpsis, bs] + hist3,
        prescale=prescale,
        persistreco=True,
    )


@register_line_builder(all_lines)
def BToJpsiK_JpsiToEpbremEmbremTagged(
        name="Hlt2PID_BToJpsiK_JpsiToEpbremEmbremTagged", prescale=1):
    pvs = make_pvs()
    kaons = flt.filter_particles_dll(
        flt.filter_particles(
            make_long_kaons(),
            pvs,
            pt_min=1 * GeV,
            p_min=3 * GeV,
            mipchi2_min=9,
        ),
        dll_lim=5,
        dll_type="k",
        min=True,
    )
    e_tags = make_tag_electrons(charge=-1, brem=True)
    e_probes = make_probe_electrons(charge=+1, brem=True)
    jpsis = make_jpsi_to_ee(e_pos=e_probes, e_neg=e_tags)
    bs = _make_bs(jpsis, kaons, pvs)

    histograms4 = {
        'Bmass_Jpsi_constraint_eprobe_PIDe+5': {
            'input':
            bs,
            'variable':
            (F.MASS - F.CHILD(1, F.MASS) + F.PDG_MASS("J/psi(1S)")) * (F.CHILD(
                1, F.CHILD(1, F.PID_E > 5))),
            'bins':
            50,
            'range': (5100, 5600)
        },
        'Bmass_Jpsi_constraint_eprobe_PIDe-5': {
            'input':
            bs,
            'variable':
            (F.MASS - F.CHILD(1, F.MASS) + F.PDG_MASS("J/psi(1S)")) * (F.CHILD(
                1, F.CHILD(1, F.PID_E < 5))),
            'bins':
            50,
            'range': (5100, 5600)
        },
        'Bmass_Jpsi_constraint_eprobe_PIDe+0': {
            'input':
            bs,
            'variable':
            (F.MASS - F.CHILD(1, F.MASS) + F.PDG_MASS("J/psi(1S)")) * (F.CHILD(
                1, F.CHILD(1, F.PID_E > 0))),
            'bins':
            50,
            'range': (5100, 5600)
        },
        'Bmass_Jpsi_constraint_eprobe_PIDe-0': {
            'input':
            bs,
            'variable':
            (F.MASS - F.CHILD(1, F.MASS) + F.PDG_MASS("J/psi(1S)")) * (F.CHILD(
                1, F.CHILD(1, F.PID_E < 0))),
            'bins':
            50,
            'range': (5100, 5600)
        },
    }

    hist4 = []
    for j, params in histograms4.items():
        histogram4 = Monitor__ParticleRange(
            Input=params['input'],
            Variable=params['variable'],
            HistogramName=f"/{name}/{j}",
            Bins=params['bins'],
            Range=params['range'],
        )
        hist4.append(histogram4)

    return Hlt2Line(
        name=name,
        algs=_calibmon_filters() + [jpsis, bs] + hist4,
        prescale=prescale,
        persistreco=True,
    )


def make_lines():
    return [builder() for builder in all_lines.values()]


public_tools = [stateProvider_with_simplified_geom()]
with reconstruction.bind(from_file=False), config_pp_2024():
    run_moore(options, make_lines, public_tools)
