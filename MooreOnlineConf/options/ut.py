###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
#############################################################################
import logging, os, glob
from Gaudi.Configuration import VERBOSE, DEBUG, INFO
from PyConf.application import default_raw_event, make_odin
from Moore import options
from RecoConf.config import Reconstruction, run_reconstruction
from RecoConf.standalone import reco_prefilters
from DDDB.CheckDD4Hep import UseDD4Hep
from PRConfig.TestFileDB import test_file_db
from PRConfig.TestFileObjects import testfiles
from PyConf.Algorithms import UTRawBankToUTDigitsAlg, UTRawBankToUTNZSDigitsAlg
from PyConf.Algorithms import UTOnlineMonitor
#from PyConf.Algorithms import UTErrorBankToASICHeaderAlg, UTOnlineError
#from PyConf.Algorithms import UTOnlineError
from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc


def ut_mon():

    raw = default_raw_event("UT")
    odin = make_odin()

    #
    decoder = UTRawBankToUTDigitsAlg(
        name='UTRawToDigits',
        Type="ZS",
        RawEventLocation=raw
    digits = decoder.OutputDigitData
    ut_onlinemonitor = UTOnlineMonitor(
        name="UTOnlineMonitor",
        InputData=digits,
        ODINLocation=odin,
        RawEventLocation=default_raw_event("UT"))
    #
    decoderNZS = UTRawBankToUTNZSDigitsAlg(
        name="UTRawToDigitsNZS",
        Type="NZS",
        RawEventLocation=default_raw_event("UT"))
    digitsNZS = decoderNZS.OutputDigitData
    ut_onlinemonitorNZS = UTOnlineMonitor(
        name="UTOnlineMonitorNZS",
        InputData=digitsNZS,
        ODINLocation=odin,
        RawEventLocation=default_raw_event("UT"))
    #

    #decoderERR        = UTErrorBankToASICHeaderAlg(OutputLevel=INFO, name='UTErrorToASIC',    Type="ErrorUTSpecial", RawEventLocation=default_raw_event("UT"))
    #digitsERR    = decoderERR.OutputDigitData
    #ut_onlinemonitorERR  = UTOnlineError(  OutputLevel=INFO, name="UTOnlineERR", InputData=digitsERR )

    algs = []
    if options.input_type.lower() == 'online':
        from MooreOnlineConf.utils import update_and_reset
        algs.append(update_and_reset())

    algs.append(decoder)
    algs.append(decoderNZS)
    #algs.append(decoderERR)
    algs.append(ut_onlinemonitor)
    algs.append(ut_onlinemonitorNZS)
    #algs.append(ut_onlinemonitorERR)

    return Reconstruction('ut_mon', algs, reco_prefilters(gec=False))


run_reconstruction(options, ut_mon)
