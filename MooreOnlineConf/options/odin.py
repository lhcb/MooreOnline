###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
#############################################################################
from Gaudi.Configuration import ERROR
from PyConf.application import (configure_input, configure, default_raw_banks,
                                make_odin)
from PyConf.control_flow import CompositeNode, NodeLogic
from PyConf.Algorithms import ODINMonitor, HltRoutingBitsMonitor, OdinTypesFilter
from PyConf.Algorithms import ODINTAEMonitor, TESCheck
from MooreOnlineConf.utils import (
    common_monitors_node,
    decode_tae,
    if_then,
    run_all,
)
from Moore import options


def make_odin_tae(name=""):
    return make_odin(name="Decode_ODIN" + name)


def main():
    odin = make_odin()
    # the standard monitor
    monitor = ODINMonitor(name="ODINMon", Input=odin)

    # the TAE monitor
    is_tae, tae_decoding, tae_odins, tae_data = decode_tae(
        make_odin_tae, half_window=3)
    tae_monitor = ODINTAEMonitor(
        name="ODINTAEMonitor",
        ODINVector=list(tae_odins.values()),
        is_barrier=True)

    # assemble the control flow
    top_node = run_all(
        "top",
        [
            common_monitors_node(),  # common monitoring to all tasks
            monitor,
            if_then("IfTAE", is_tae, run_all("TAE",
                                             [tae_decoding, tae_monitor])),
        ])

    return top_node


configure_input(options)
configure(options, main())
