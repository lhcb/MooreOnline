#!/bin/python
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
## ============= the basic import ====================
#from Gaudi.Configuration import *
from Gaudi.Configuration import INFO
from Configurables import LHCb__Det__LbDD4hep__IOVProducer as IOVProducer, LHCb__Tests__FakeRunNumberProducer as FakeRunNumberProducer
from GaudiPython.Bindings import AppMgr
from Gaudi.Configuration import ApplicationMgr
from Configurables import Pi0MMap2Histo
from Configurables import GaudiSequencer
from Configurables import LHCbApp

import os

MSG_VERBOSE = 3
MSG_DEBUG = 3
MSG_INFO = 3
MSG_WARNING = 4
MSG_ERROR = 5
MSG_FATAL = 6
MSG_ALWAYS = 7

import argparse
parser = argparse.ArgumentParser(description="Input file delivery")
parser.add_argument("--inputfile", type=str, dest="inputfile", default=None)
parser.add_argument("--outputfile", type=str, dest="outputfile", default=None)
parser.add_argument(
    "--input_lambda_name", type=str, dest="input_lambda_name", default=None)
args = parser.parse_args()

inputfile = args.inputfile
outputfile = args.outputfile
input_lambda_name = args.input_lambda_name
tuplename = "Tuple/DecayTree"
year = 'Upgrade'

if '__main__' == __name__:

    LHCbApp().DataType = 'Upgrade'
    LHCbApp().CondDBtag = 'master'
    LHCbApp().DDDBtag = 'master'
    LHCbApp().Simulation = False
    LHCbApp().GeometryVersion = 'run3/trunk'
    # LHCbApp().OnlineMode = True

    pi02Histo = Pi0MMap2Histo("Pi0MMap2Histo")
    pi02Histo.nworker = 1
    pi02Histo.filenames = [inputfile]
    pi02Histo.outputDir = os.path.dirname(outputfile)
    pi02Histo.outputName = os.path.basename(outputfile)
    pi02Histo.OutputLevel = MSG_INFO
    pi02Histo.lambdaFileName = input_lambda_name

    mainSeq = GaudiSequencer("MainSeq")
    mainSeq.Members = [
        FakeRunNumberProducer(Start=268295, Step=0),
        IOVProducer(), pi02Histo
    ]
    ApplicationMgr(
        OutputLevel=INFO, AppName="Pi0MMap2Histo", EvtMax=1, EvtSel='NONE')
    ApplicationMgr().TopAlg.append(mainSeq)
    AppMgr().run(1)
