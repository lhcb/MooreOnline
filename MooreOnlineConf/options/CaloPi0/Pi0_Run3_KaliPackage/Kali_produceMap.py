#!/bin/python
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#--> convert the ROOT (TTree) to MMap file

from GaudiPython import gbl
pi0Calib = gbl.Calibration.Pi0Calibration
import argparse

parser = argparse.ArgumentParser(description="Input file delivery")
parser.add_argument("--inputfile", type=str, dest="inputfile", default=None)
parser.add_argument("--outputfile", type=str, dest="outputfile", default=None)
args = parser.parse_args()

inputfile = args.inputfile
outputfile = args.outputfile
tuplename = "Tuple/DecayTree"
if '__main__' == __name__:
    pi0Calib.Pi0CalibrationFile(inputfile, tuplename, outputfile)
