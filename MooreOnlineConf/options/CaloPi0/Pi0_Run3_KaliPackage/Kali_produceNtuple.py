###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import Functors as F
from FunTuple import FunctorCollection
from FunTuple import FunTuple_Particles as Funtuple
from PyConf.reading import get_particles, get_pvs, get_odin
from DaVinci.algorithms import create_lines_filter
from DaVinci import make_config, Options
import FunTuple.functorcollections as FCs
from PyConf.reading import get_rec_summary

from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc


def main(options: Options, pass_name: str, overlay_path: str):

    dd4hepSvc = DD4hepSvc()

    if pass_name == "SecondPass":
        dd4hepSvc.UseConditionsOverlay = True
        dd4hepSvc.ConditionsOverlayInitPath = overlay_path

    dd4hepSvc.DetectorList = [
        '/world', 'VP', 'FT', 'Magnet', 'Rich1', 'Rich2', 'Ecal', 'Hcal',
        'Muon'
    ]

    fields = {
        'pi0': 'pi0 -> gamma gamma',
        'g1': 'pi0 -> ^gamma gamma',
        'g2': 'pi0 -> gamma ^gamma',
    }

    pi0_variables = FunctorCollection({
        "ID": F.PARTICLE_ID,
        "m12": F.MASS,
        "PT": F.PT,
        "Eta": F.ETA
    })

    dau_variables = FunctorCollection({
        "ID":
        F.PARTICLE_ID,
        "PT":
        F.PT,
        "PE":
        F.ENERGY,
        "CellID":
        F.CALO_NEUTRAL_ID - 32768,
        "CaloNeutralE19":
        F.CALO_NEUTRAL_1TO9_ENERGY_RATIO,
        "Is_NotH":
        F.IS_NOT_H,
    })

    variables = {
        "pi0": pi0_variables,
        "g1": dau_variables,
        "g2": dau_variables,
    }

    pi02ggLine = "Hlt2Allpi0"
    pi02gg_data = get_particles(f"/Event/HLT2/{pi02ggLine}/Particles")
    my_filter = create_lines_filter(
        name="HDRFilter_pi02gg", lines=[pi02ggLine])

    odin = get_odin()

    # HLT1 decision
    Hlt1_dec = [
        "Hlt1Pi02GammaGammaDecision",
        "Hlt1TrackMVADecision",
    ]

    sel_info = FCs.SelectionInfo(selection_type="Hlt1", trigger_lines=Hlt1_dec)

    rec_summary = get_rec_summary()

    evt_variables = FunctorCollection({
        "nPVs":
        F.VALUE_OR(-1) @ F.RECSUMMARY_INFO(rec_summary, "nPVs")
    })

    evt_variables += FCs.EventInfo()

    my_tuple = Funtuple(
        name="Tuple",
        tuple_name='DecayTree',
        fields=fields,
        variables=variables,
        event_variables=evt_variables,
        inputs=pi02gg_data,
    )

    config = make_config(options, [my_filter, my_tuple])

    return config
