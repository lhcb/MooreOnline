###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from ruamel.yaml import YAML
import multiprocessing
import os


def MDFprodOption(inputfiles, outputfile, n_instances):
    filename = outputfile
    option_file_name = filename.replace(".mdf", ".yaml")
    output_manifest_file = filename.replace(".mdf", ".tck.json")

    threads_per_instance = int((multiprocessing.cpu_count() - 1) / n_instances)
    if threads_per_instance == 0:
        print(
            "### WARNING: More instances than cpu_threads. Setting threads_per_instance = 1. \n"
        )
        threads_per_instance = 1
    print(f"### threads_per_instance: {threads_per_instance}")

    f = open(option_file_name, "w")
    write_lines = [
        "input_files:\n",
    ] + ["- '%s'\n" % inputfile for inputfile in inputfiles] + [
        "input_type: 'RAW'\n", "evt_max: 100000 \n",
        "output_file: '%s'\n" % outputfile,
        "output_manifest_file: '%s' \n" % output_manifest_file,
        "input_process: 'Hlt2'\n", "input_raw_format: 0.5\n",
        "data_type: Upgrade\n", "simulation: False\n",
        f"n_threads: {threads_per_instance} \n",
        "scheduler_legacy_mode: False\n", "output_type: 'RAW' \n",
        "geometry_version : run3/2024.Q1.2-v00.00 \n",
        "conditions_version : 'master' \n", "print_freq: 1000 \n",
        "write_decoding_keys_to_git: False \n"
    ]
    f.writelines(write_lines)
    f.close()

    return option_file_name


def NtupleprodOption(inputfiles, outputfile):
    filename = inputfiles
    option_file_name = filename.replace(".mdf", ".yaml")
    json_file = filename.replace(".mdf", ".tck.json")

    f = open(option_file_name, "w")
    write_lines = [
        "input_files:\n",
    ] + [
        "- '%s'\n" % os.path.abspath(inputfile) for inputfile in [inputfiles]
    ] + [
        "input_manifest_file: '%s' \n" % os.path.abspath(json_file),
        "input_type: 'RAW'\n",
        "evt_max: -1\n",
        f"ntuple_file: '{os.path.join(os.getcwd(), outputfile)}'\n",
        #"ntuple_file: '%s'\n" % outputfile,
        "input_process: Hlt2\n",
        "input_stream: default"
        " \n",
        "n_threads: 1 \n",
        "input_raw_format: 0.5\n",
        "lumi: False\n",
        "data_type: Upgrade\n",
        "simulation: False\n",
        "conddb_tag: 'upgrade/master'\n",
        "dddb_tag: 'upgrade/master'\n",
        "geometry_version : run3/2024.Q1.2-v00.00 \n",
        "conditions_version : 'master' \n",
    ]
    f.writelines(write_lines)
    f.close()

    return option_file_name


def merge_lambda_yaml_files(FirstPass_lambda: str, SecondPass_lambda: str,
                            output_file_path: str):

    yaml = YAML()

    with open(FirstPass_lambda, 'r') as stream:
        calibFirst = yaml.load(stream)

    with open(SecondPass_lambda, 'r') as stream2:
        calibSecond = yaml.load(stream2)

    Lines_1 = calibFirst['data']
    Lines_2 = calibSecond['data']

    Num_of_hist = 11384  #number of histograms

    corr = 32768
    #Correction over the cell index

    Lambda_1 = [None] * Num_of_hist
    Lambda_2 = [None] * Num_of_hist
    Lambda_3 = [None] * Num_of_hist
    Merged_lambda = [[None, None]] * Num_of_hist

    output_merged_file = open(
        os.path.join(output_file_path, "merged_lambda.yml"), 'w')
    output_merged_file.write('data: [\n')

    for line1 in Lines_1:
        Lambda_1[line1[0] - corr] = float(line1[1])

    for line2 in Lines_2:
        Lambda_2[line2[0] - corr] = float(line2[1])

    for i in range(0, len(Lambda_1)):

        if (not Lambda_1[i] is None or not Lambda_2[i] is None):
            Lambda_3[i] = Lambda_1[i] * Lambda_2[i]
        else:
            continue

        if (Lambda_3[i] < 1.4 or Lambda_3[i] > 0.6):
            Merged_lambda.insert(i, [i + 32768, Lambda_3[i]])
        else:
            Merged_lambda.insert(i, [i + 32768, 1.0])

        output_merged_file.write(f"{Merged_lambda[i]}, \n")

    output_merged_file.write(']')
