###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import INFO
from PyConf.Algorithms import CaloFutureRawToDigits
from RecoConf.muonid import make_muon_hits
from GaudiPython.Bindings import AppMgr
from PyConf.application import metainfo_repos
from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc
from RecoConf.calorimeter_reconstruction import make_digits
from RecoConf.reconstruction_objects import reconstruction
from RecoConf.global_tools import stateProvider_with_simplified_geom

from RecoConf.decoders import default_ft_decoding_version, default_VeloCluster_source
from Hlt2Conf.algorithms_thor import ParticleFilter
import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import MeV
from Hlt2Conf.standard_particles import make_photons
from Hlt2Conf.algorithms_thor import ParticleCombiner
from Moore.lines import Hlt2Line
from Moore.config import register_line_builder
from RecoConf.reconstruction_objects import make_pvs, upfront_reconstruction
from PyConf import configurable

from Moore import Options, run_moore
from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc

all_lines = {}


@configurable
def pi0_filter(particles):
    cut = F.require_all(F.PT > 200 * (7 - F.ETA) * MeV)
    return ParticleFilter(particles, F.FILTER(cut))


@configurable
def photons_filter(particles):
    cut = F.require_all(F.CALO_NEUTRAL_1TO9_ENERGY_RATIO > 0.7,
                        F.IS_NOT_H > .7)
    return ParticleFilter(particles, F.FILTER(cut))


@configurable
def make_own_resolved_pi0s(particles=make_photons(),
                           MaxMass=350 * MeV,
                           PtCut=0. * MeV,
                           **kwargs):

    comb_code = F.require_all(in_range(0., F.MASS, MaxMass))
    mother_code = F.require_all(F.PT > PtCut)

    return ParticleCombiner(
        Inputs=[particles, particles],
        DecayDescriptor="pi0 -> gamma gamma",
        CombinationCut=comb_code,
        CompositeCut=mother_code,
        ParticleCombiner="ParticleAdder")


@register_line_builder(all_lines)
def All_pi0_line(name="Hlt2Allpi0", prescale=1.0):
    photons = photons_filter(make_photons(PtCut=300 * MeV, pv_maker=make_pvs))
    resolved_pi0 = make_own_resolved_pi0s(particles=photons, MaxMass=300 * MeV)
    pi0 = pi0_filter(resolved_pi0)

    return Hlt2Line(
        name=name, algs=upfront_reconstruction() + [pi0], prescale=prescale)


def main(options: Options, pass_name: str, overlay_path: str):

    metainfo_repos.global_bind(extra_central_tags=['commissioning'])
    dd4hepSvc = DD4hepSvc()

    if pass_name == "SecondPass":
        dd4hepSvc.UseConditionsOverlay = True
        dd4hepSvc.ConditionsOverlayInitPath = overlay_path

    dd4hepSvc.DetectorList = [
        '/world', 'VP', 'FT', 'Magnet', 'Rich1', 'Rich2', 'Ecal', 'Hcal',
        'Muon'
    ]

    def resolved_pi0():
        return [builder() for builder in all_lines.values()]

    from RecoConf.global_tools import (
        trackMasterExtrapolator_with_simplified_geom, )

    from RecoConf.hlt2_global_reco import (
        make_light_reco_pr_kf_without_UT, )

    from RecoConf.hlt2_global_reco import reconstruction as hlt2_reconstruction

    from RecoConf.ttrack_selections_reco import make_ttrack_reco

    public_tools = [
        trackMasterExtrapolator_with_simplified_geom(),
        stateProvider_with_simplified_geom(),
    ]

    with reconstruction.bind(from_file=False), default_VeloCluster_source.bind(
            bank_type="VPRetinaCluster"), make_digits.bind(
                calo_raw_bank=True), make_muon_hits.bind(
                    geometry_version=3), hlt2_reconstruction.bind(
                        make_reconstruction=make_light_reco_pr_kf_without_UT):
        return run_moore(
            options, resolved_pi0, public_tools, exclude_incompatible=False)
