###############################################################################
# (c) Copyright 2018 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#--> perform an iteration of the pi0 calibration: fill the histogram, perform the fit and extract the calibration constants.
#--> in default, only bad fits and cells with low statistics are saved; if VERBOSE invoked, all the fits are saved on disk.
#--> three types of input files are allowed: ROOT (TTree), MMap, ROOT (TH2D: names hists & hists_bg)
#--> any further questions, please contact Zhirui at zhirui@cern.ch

import os
import argparse
from Configurables import LHCbApp
from Configurables import LHCb__Det__LbDD4hep__IOVProducer as IOVProducer, LHCb__Tests__FakeRunNumberProducer as FakeRunNumberProducer
from GaudiPython import AppMgr
from Gaudi.Configuration import ApplicationMgr, INFO

parser = argparse.ArgumentParser(description="do_calibration")
parser.add_argument("--nIt", type=str, dest="nIt", default=None)
parser.add_argument("--passname", type=str, dest="passname", default=None)
parser.add_argument("--inputfile", type=str, dest="inputfile", default=None)
parser.add_argument("--outputfile", type=str, dest="outputfile", default=None)
parser.add_argument(
    "--output_lambda_name", type=str, dest="output_lambda_name", default=None)

args = parser.parse_args()
inputfile = args.inputfile
outputfile = args.outputfile
output_lambda_name = args.output_lambda_name
nIt = args.nIt
passname = args.passname

args = parser.parse_args()
tuplename = "Tuple/DecayTree"
year = 'Upgrade'
filetype = 'TH2D'

LHCbApp().DataType = 'Upgrade'
LHCbApp().CondDBtag = 'master'
LHCbApp().DDDBtag = 'master'
LHCbApp().Simulation = False
LHCbApp().GeometryVersion = 'run3/2024.Q1.2-v00.00'

from Configurables import Pi0CalibrationAlg
pi0Calib = Pi0CalibrationAlg("Pi0Calibration")
pi0Calib.OutputLevel = 1
pi0Calib.tupleFileName = inputfile
pi0Calib.tupleName = tuplename
pi0Calib.filetype = filetype
pi0Calib.outputDir = os.path.dirname(outputfile)
pi0Calib.lambdaFileName = output_lambda_name
pi0Calib.saveLambdaFile = output_lambda_name
pi0Calib.saveLambdaYMLFile = outputfile
pi0Calib.nIt = nIt
pi0Calib.passname = passname

from Configurables import Pi0CalibrationMonitor
pi0Moni = Pi0CalibrationMonitor("Pi0CalibrationMonitor")
pi0Moni.tupleFileName = inputfile
pi0Moni.tupleName = tuplename
pi0Moni.outputDir = os.path.dirname(outputfile)
pi0Moni.inputDir = os.path.dirname(outputfile)
pi0Moni.OutputLevel = 1
pi0Moni.nIt = nIt
pi0Moni.passname = passname

from Configurables import GaudiSequencer

mainSeq = GaudiSequencer("MainSeq")

pi0Moni.outputDir = pi0Moni.outputDir + f"/Iter{nIt}Mon_{passname}"
os.mkdir(pi0Moni.outputDir)
mainSeq.Members = [
    FakeRunNumberProducer(Start=256126, Step=0),
    IOVProducer(), pi0Calib, pi0Moni
]

ApplicationMgr(
    OutputLevel=INFO, AppName="Pi0Calibration", EvtMax=-1, EvtSel='NONE')
ApplicationMgr().TopAlg.append(mainSeq)
AppMgr().run(1)

import shutil

for file in os.listdir(pi0Calib.outputDir):
    if not "txt" in file: continue
    shutil.copyfile(
        os.path.join(pi0Calib.outputDir, file),
        os.path.join(pi0Moni.outputDir, file))
