#!/bin/python
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
## ============= the basic import ====================
#from Gaudi.Configuration import *
from Gaudi.Configuration import INFO
from Configurables import LHCb__Det__LbDD4hep__IOVProducer as IOVProducer, LHCb__Tests__FakeRunNumberProducer as FakeRunNumberProducer
from GaudiPython.Bindings import AppMgr
from Gaudi.Configuration import ApplicationMgr
from Configurables import Pi0MMap2Histo
from Configurables import GaudiSequencer
from Configurables import LHCbApp
from ROOT import RDataFrame, TChain, TFile, TF1, ROOT, TH2D, gROOT, TH1D, gPad, kRed, kGreen, kBlack
import ROOT as ROOT26, math
#from funcs import cellid2COLROW
from multiprocessing import Pool
import os

MSG_VERBOSE = 3
MSG_DEBUG = 3
MSG_INFO = 3
MSG_WARNING = 4
MSG_ERROR = 5
MSG_FATAL = 6
MSG_ALWAYS = 7
from tqdm import trange, tqdm
import argparse
parser = argparse.ArgumentParser(description="Input file delivery")
parser.add_argument("--inputfile", nargs='+', dest="inputfile", default=None)
parser.add_argument("--outputfile", type=str, dest="outputfile", default=None)
parser.add_argument(
    "--input_lambda_name", type=str, dest="input_lambda_name", default=None)
parser.add_argument("--nworker", type=int, dest="nworker", default=None)
args = parser.parse_args()

inputfile = args.inputfile
outputfile = args.outputfile
input_lambda_name = args.input_lambda_name
nworker = args.nworker
tuplename = "Tuple/DecayTree"

if '__main__' == __name__:
    import ROOT
    ROOT.ROOT.EnableImplicitMT(nworker)
    ch = ROOT.TChain(tuplename)
    for f in inputfile:
        ch.Add(f)

    df = ROOT.RDataFrame(ch)

    # fill histograme with the different cuts for the different areas
    ROOT.gInterpreter.Declare('''
    bool in_tight_cut_region(int cellid){
      int col  = ( (cellid &    63) >>  0 );
      int row  = ( (cellid &  4032) >>  6 );
      int area = ( (cellid & 12288) >> 12 );

      double ds;
      if (area == 0) ds = 121.2;
      if (area == 1) ds = 121.2 / 2;
      if (area == 2) return false;

      double x = ds * ( col -  32 + .5 );
      double y = ds * (  32 - row + .5 );

      if (abs(y / x) < 0.3) return true;
      else return false;
    }
    ''')

    df = df.Define(
        "in_tight_cut_regions",
        "in_tight_cut_region(g1_CellID)||in_tight_cut_region(g2_CellID)")
    df = df.Filter(
        "nPVs <= 1 && (in_tight_cut_regions&&pi0_PT>1700)||(!(in_tight_cut_regions))"
    )

    # scale candidate's mass. The func can be used in RDataFrame
    m = "pi0_m12"
    if os.path.exists(input_lambda_name):
        lambdas = []
        with open(input_lambda_name, "r") as f:
            for line in f.readlines():
                line = line.strip('\n').split('  ')
                lambdas.append((int(line[0]), float(line[1])))
        lambdas = f"{lambdas}".replace('(', '{').replace(')', '}').replace(
            '[', '{').replace(']', '}')

        ROOT.gInterpreter.Declare(f'''
          std::map <int, float> lambdas = {lambdas};

          float m_scale(int g1_cellid, int g2_cellid, float pi0_m){{
            float g1_lambda = 1;
            float g2_lambda = 1;
            if (lambdas.count(g1_cellid)) g1_lambda = lambdas[g1_cellid];
            if (lambdas.count(g2_cellid)) g2_lambda = lambdas[g2_cellid];
            return pi0_m * sqrt( g1_lambda * g2_lambda );
          }}
        ''')
        m = "pi0_m12_scaled"
        df = df.Define("pi0_m12_scaled",
                       "m_scale(g1_CellID, g2_CellID, pi0_m12)")
    else:
        print("Lambda file not found. Create one to start")
        with open(input_lambda_name, "w") as f:
            f.write("")
        print(f"File {input_lambda_name} created !")
    h = TH2D("hists", ";cell id;#pi^{0} mass [MeV]", 11384, 1, 11385, 100, 0,
             250)
    h1 = df.Histo2D(
        ("h1", ";cell id;#pi^{0} mass [MeV]", 11384, 1, 11385, 100, 0, 250),
        "g1_CellID", m)

    h2 = df.Histo2D(
        ("h2", ";cell id;#pi^{0} mass [MeV]", 11384, 1, 11385, 100, 0, 250),
        "g2_CellID", m)

    h.Add(h1.GetValue())
    h.Add(h2.GetValue())
    print(f"number of entries {h.GetEntries()}")
    outf = ROOT.TFile(
        os.path.join(
            os.path.dirname(outputfile), os.path.basename(outputfile)),
        "recreate")
    h.Write("hists")
    outf.Close()

os.path.join(os.path.dirname(outputfile), os.path.basename(input_lambda_name))
