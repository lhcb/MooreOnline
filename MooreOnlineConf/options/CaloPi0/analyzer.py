###############################################################################
# (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from MooreOnlineConf.Communicator import Communicator, State
import os, time, random, sys
import shlex
import subprocess
import shutil

sys.path.append("Pi0_Run3_KaliPackage")  # FIXME remove

from MooreOnlineConf.utils import (
    alignment_options,
    ensure_output_dir,
    distribute_files,
)

from Pi0_Run3_KaliPackage.Kali_Analyzer import NtupleprodOption, MDFprodOption
from pathlib import Path

NIteration = 7


def subprocess_run(*args, **kwargs):
    print(f"subprocess.run(): {args} {kwargs}")
    return subprocess.run(*args, **kwargs)


def MDFprod(input_files: list, output_file: str, secondPass: str,
            overlay_path: str, n_instances: int):
    print(f"MDFprod: {input_files} -> {output_file}")

    options_file = MDFprodOption(input_files, str(output_file),
                                 int(n_instances))
    print(options_file)
    kali_path = os.path.dirname(
        __file__) + "/Pi0_Run3_KaliPackage/Kali_produceMDF.py:main"
    subprocess_run(
        ["lbexec", kali_path, options_file, secondPass, overlay_path],
        check=True)


def Ntupleprod(input_file: str, output_file: str, secondPass: str,
               work_dir: str, overlay_path: str):
    print(f"Ntupleprod: {input_file}, {overlay_path} -> {output_file}")

    options_file = os.path.abspath(
        NtupleprodOption(str(input_file), str(output_file)))
    print(options_file)
    kali_path = os.path.dirname(
        __file__) + "/Pi0_Run3_KaliPackage/Kali_produceNtuple.py:main"
    if not os.path.exists(work_dir): os.mkdir(work_dir)
    #subprocess_run(
    #    #["lb-run", "DaVinci/v64r4", "lbexec", kali_path, options_file, secondPass, overlay_path],
    #    [
    #        "/swdev/calo/stack/DaVinci/run", "lbexec", kali_path, options_file,
    #        #"/scratch/lesantor/stack/DaVinci/run", "lbexec", kali_path, options_file,
    #        secondPass, overlay_path
    #    ],
    #    check=True)
    subprocess_run(
        f"cd {work_dir};\
          env -i /cvmfs/lhcb.cern.ch/lib/var/lib/LbEnv/3149/stable/linux-64/bin/lb-run -c x86_64_v3-el9-gcc13-opt+g DaVinci/v64r5 lbexec {kali_path} {options_file} {secondPass} {overlay_path};\
          ",
        shell=True,
        check=True)
    shutil.rmtree(work_dir)


def MMapprod(input_file: str, output_file: str):
    print(f"MMapprod: {input_file} -> {output_file}")
    cmd = "python " + os.path.dirname(
        __file__
    ) + f"/Pi0_Run3_KaliPackage/Kali_produceMap.py --inputfile {input_file} --outputfile {output_file}"
    subprocess_run(shlex.split(cmd))


def Histoprod(input_file: str, output_file: str, input_lambda_name: str):
    print(f"Histoprod: {input_file} -> {output_file}")
    cmd = "python " + os.path.dirname(
        __file__
    ) + f"/Pi0_Run3_KaliPackage/Kali_produceHist.py --inputfile {input_file} --outputfile {output_file} --input_lambda_name {input_lambda_name}"
    subprocess_run(shlex.split(cmd))


def Hist2Doprod(input_file: str, output_file: str, input_lambda_name: str,
                nworker: int):
    print(f"Histoprod: {input_file} -> {output_file}")
    #for input_file in input_file:
    cmd = "python " + os.path.dirname(
        __file__
    ) + f"/Pi0_Run3_KaliPackage/Kali_produceHist2D.py --inputfile {input_file} --outputfile {output_file} --input_lambda_name {input_lambda_name} --nworker {nworker}"
    subprocess_run(shlex.split(cmd))


def run(node, input_files, work_directory, iterator_output_path, n_instances):
    com = Communicator(f"AligWrk_{node}")

    # FSM loop
    state = State.NOT_READY
    com.set_status(state)
    n_it = 0
    p_it = 1

    while True:

        command = com.get_command()
        if command.startswith('configure') and state == State.NOT_READY:
            state = State.READY
        elif command.startswith('start') and state == State.READY:
            state = State.RUNNING
            com.set_status(state)
            n_it += 1
            pass_name = "FirstPass" if p_it == 1 else "SecondPass"
            print(f"Analyzer: {pass_name},  iteration: {n_it}")

            if p_it >= 3:
                break

            if n_it == 1:
                # First pass: Produces ntuples with default DB (lambdas == 1)
                print("## Running MDFprod! \n")

                MDFprod(
                    input_files=input_files,
                    output_file=work_directory /
                    f"{pass_name}_selected_{node}.mdf",
                    secondPass=pass_name,
                    overlay_path=os.path.abspath(
                        os.path.join(iterator_output_path,
                                     "new-constants-FirstPass"))
                    if p_it == 2 else
                    "git:/cvmfs/lhcb.cern.ch/lib/lhcb/git-conddb/lhcb-conditions-database.git",
                    n_instances=n_instances)

                print("## Running Ntupleprod! \n")
                Ntupleprod(
                    input_file=work_directory /
                    f"{pass_name}_selected_{node}.mdf",
                    output_file=work_directory / f"{node}_{pass_name}.root",
                    work_dir=work_directory / f"{node}_{pass_name}",
                    secondPass=pass_name,
                    overlay_path=os.path.abspath(
                        os.path.join(iterator_output_path,
                                     "new-constants-FirstPass"))
                    if p_it == 2 else
                    "git:/cvmfs/lhcb.cern.ch/lib/lhcb/git-conddb/lhcb-conditions-database.git"
                )

            print(f"## Running Histo2Dprod {n_it}! \n")
            Hist2Doprod(
                input_file=work_directory / f"{node}_{pass_name}.root",
                #input_file=
                #"/scratch/lesantor/stack/backup80M/output/CaloPi0/analyzer/0000290068/TEST_N8190604_AlignWrk_1_SecondPass.root",
                output_file=work_directory /
                f"{node}_{pass_name}_{n_it}_histos.root",
                input_lambda_name=os.path.join(iterator_output_path,
                                               f"{pass_name}_lambda.txt"),
                nworker=n_instances
            )  #nworker is number of threads that RDataFrame used

            print(
                f"## The lambda file in the interation {n_it} from {pass_name} comes from {pass_name}_lambda \n"
            )

            if n_it == NIteration:
                n_it = 0
                p_it += 1

            state = State.PAUSED
        elif command.startswith('stop') and state == State.PAUSED:
            state = State.READY
        elif command.startswith('reset'):
            state = State.NOT_READY
        elif command.startswith('unload'):
            state = State.OFFLINE
            com.set_status(state)
            break
        else:
            print('analyzer: bad transition from %s to %s' % (state, command))
            state = State.ERROR
            com.set_status(state)
            break

        time.sleep(random.uniform(0.5, 1.5))
        # Set the status
        com.set_status(state)


if __name__ == '__main__':
    import OnlineEnvBase as OnlineEnv
    online_options = alignment_options(OnlineEnv)

    ensure_output_dir(online_options.analyzer_output_path, online_options.tag)
    print()
    ensure_output_dir(online_options.iterator_output_path, online_options.tag)
    print(
        f"Will write analyzer output to {online_options.analyzer_output_path}")

    if OnlineEnv.PartitionName == "LHCbA" or online_options.runs:
        INPUT_DATA_PATH = Path("/calib/align/LHCb/Calo")

        files = [
            sorted((INPUT_DATA_PATH / run).iterdir())
            for run in online_options.runs
        ]
    else:
        INPUT_DATA_PATH = Path("input_data")

    files_per_node = distribute_files(online_options.nodes, files)

    utgid = os.environ["UTGID"]
    worker_id = utgid.split("_")[1]

    try:
        input_files = files_per_node[worker_id]
    except KeyError:
        # When testing we run multiple instances on the same node
        # TODO this should probably be done based on the partition name
        #      and also "nodes" should be renamed to workers everywhere.
        worker_id = utgid
        input_files = files_per_node[worker_id]

    run(worker_id,
        input_files=input_files,
        work_directory=online_options.analyzer_output_path,
        iterator_output_path=online_options.iterator_output_path,
        n_instances=len(online_options.nodes))
