###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from MooreOnlineConf.Communicator import Communicator, State
from MooreOnlineConf.utils import (
    alignment_options,
    ensure_output_dir,
)
import subprocess
import shlex
import os
import shutil
from ruamel.yaml import YAML
from Pi0_Run3_KaliPackage.Kali_Analyzer import merge_lambda_yaml_files

# TODO this will not work in nightly tests....
ONLINE_CONDDB_PATH = "/group/online/hlt/conditions.run3/lhcb-conditions-database"
CONDITION_PATH = "Conditions/Ecal/Calibration.yml"

NIteration = 7


def MergeHistograms(input_files: list, output_file: str):
    print(f"MergeHistograms: {input_files} -> {output_file}")
    cmd = f"hadd -f {output_file} {' '.join(input_files)}"
    subprocess.run(shlex.split(cmd))


def DoCalibration(nIt: int, passname: str, input_file: str, output_file: str,
                  output_lambda_name: str):
    print(f"DoCalibration: {input_file} -> {output_file}")
    cmd = "python " + os.path.dirname(
        __file__
    ) + f"/Pi0_Run3_KaliPackage/Kali_doCalibration.py --nIt {nIt}  --passname {passname} --passname {passname} --inputfile {input_file} --outputfile {output_file} --output_lambda_name {output_lambda_name} "
    subprocess.run(shlex.split(cmd))


def WriteOverlay(input_file: str, output_dir: str, initial_dir: str):
    initial_cond_file = os.path.join(initial_dir, CONDITION_PATH)
    output_cond_file = os.path.join(output_dir, CONDITION_PATH)
    print(
        f"WriteOverlay: {initial_cond_file} + {input_file} -> {output_cond_file}"
    )

    yaml = YAML()

    with open(initial_cond_file) as f:
        cond = yaml.load(f)
    with open(input_file) as f:
        calib = yaml.load(f)

    cond['Calibration']['data'] = calib['data']

    os.makedirs(os.path.dirname(output_cond_file))
    with open(output_cond_file, 'w', encoding='utf8') as outfile:
        yaml.dump(cond, outfile)

    print(f"calibration constants written to {output_dir}")


def run(online_options, work_directory):
    # Start the communicator:
    com = Communicator('AligDrv_0')
    # FSM loop
    state = State.NOT_READY
    com.set_status(state)
    n_it = 0
    p_it = 1

    while True:
        command = com.get_command()
        if command == 'configure' and state == State.NOT_READY:
            initial_constants_src = os.path.join(
                ONLINE_CONDDB_PATH, CONDITION_PATH,
                "0")  # FIXME the "0" needs to be determined dynamically
            initial_constants_dst = work_directory / "initial-constants" / CONDITION_PATH
            initial_constants_dst.parent.mkdir(parents=True)
            shutil.copy(initial_constants_src, initial_constants_dst)

            state = State.READY
        elif command == 'start' and state == State.READY:
            state = State.RUNNING
        elif command == 'pause' and state == State.RUNNING:
            state = State.PAUSED
            com.set_status(state)
            n_it += 1
            pass_name = "FirstPass" if p_it == 1 else "SecondPass"
            print(f"Itertator: {pass_name},  iteration: {n_it}")

            if p_it >= 3:
                print("ERROR iterator should not be running a third pass")
                raise RuntimeError()

            hist_files = [
                str(online_options.analyzer_output_path /
                    f"{node}_{pass_name}_{n_it}_histos.root")
                for node in online_options.nodes
            ]

            print(f"## Running MergeHistograms {n_it}! \n")
            MergeHistograms(
                input_files=hist_files,
                output_file=work_directory / f"{pass_name}_{n_it}_histos.root")
            #Do fits and produce new lambdas
            print(f"## Running DoCalibration {n_it}! \n")
            DoCalibration(
                nIt=str(n_it),
                passname=pass_name,
                input_file=work_directory / f"{pass_name}_{n_it}_histos.root",
                output_file=work_directory / f"{pass_name}_{n_it}.yml",
                output_lambda_name=work_directory / f"{pass_name}_lambda.txt")

            if p_it == 1 and n_it == NIteration:
                print(f"## Running WriteOverlay {pass_name}/{n_it}\n")
                WriteOverlay(
                    input_file=work_directory / f"{pass_name}_{n_it}.yml",
                    output_dir=work_directory / f"new-constants-{pass_name}",
                    initial_dir=work_directory / "initial-constants")
                if p_it == 1:
                    n_it = 0
                    p_it += 1
                state = State.RUNNING
            elif p_it == 2 and n_it == NIteration:
                print("## Merge the first and the second pass lambdas ! \n")
                merge_lambda_yaml_files(
                    os.path.join(work_directory, "FirstPass_7.yml"),
                    os.path.join(work_directory, "SecondPass_7.yml"),
                    work_directory)
                print("## Running WriteOverlay over merged files! \n")
                WriteOverlay(
                    input_file=work_directory / "merged_lambda.yml",
                    output_dir=work_directory / f"new-constants-{pass_name}",
                    initial_dir=work_directory / "initial-constants")

                print('iterator done')
                state = State.READY
            else:
                state = State.RUNNING

        elif command == 'stop' and state in (State.RUNNING, State.READY):
            state = State.READY
        elif command == 'reset':
            state = State.NOT_READY
        elif command == 'unload':
            state = State.OFFLINE
            com.set_status(state)
            break
        else:
            print('iterator: bad transition from %s to %s' % (state, command))
            state = State.ERROR
            com.set_status(state)
            break
        # Set the status
        com.set_status(state)


if __name__ == '__main__':
    import OnlineEnvBase as OnlineEnv
    online_options = alignment_options(OnlineEnv)

    ensure_output_dir(online_options.iterator_output_path, online_options.tag)

    print(
        f"Will write iterator output to {online_options.iterator_output_path}")

    run(online_options, work_directory=online_options.iterator_output_path)
