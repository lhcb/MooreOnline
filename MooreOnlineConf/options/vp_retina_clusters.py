###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf.Algorithms import VeloRetinaClusterTrackingSIMD
from RecoConf.legacy_rec_hlt1_tracking import (make_VeloClusterTrackingSIMD,
                                               make_RetinaClusters)
from RecoConf.decoders import default_VeloCluster_source

default_VeloCluster_source.global_bind(bank_type="VPRetinaCluster")
make_VeloClusterTrackingSIMD.global_bind(
    algorithm=VeloRetinaClusterTrackingSIMD)
