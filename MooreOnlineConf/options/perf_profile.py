###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import PerfProfile, HLTControlFlowMgr
HLTControlFlowMgr('HLTControlFlowMgr').PreambleAlgs = HLTControlFlowMgr(
    'HLTControlFlowMgr').PreambleAlgs + [
        PerfProfile(
            FIFOPath="perf_control.fifo",
            StartFromEventN=1000,
            # if we run for 10min: 300s * 50 Evts/s = 15k
            StopAtEventN=15000,
        )
    ]
