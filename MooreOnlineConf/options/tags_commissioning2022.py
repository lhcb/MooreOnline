###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options
from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc

options.input_type = 'Online'
options.output_type = 'Online'
options.dddb_tag = 'master'
options.conddb_tag = 'AlignmentV5_2023_01_31_VPSciFiRich'
options.simulation = False

LHCb__Det__LbDD4hep__DD4hepSvc(DetectorList=[
    '/world', 'VP', 'FT', 'Magnet', 'Rich1', 'Rich2', 'Ecal', 'Hcal', 'Muon'
])
