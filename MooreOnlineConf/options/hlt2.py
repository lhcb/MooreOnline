###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
from Moore import options
from PyConf.application import metainfo_repos, retrieve_encoding_dictionary

options.event_store = 'EvtStoreSvc'

# for running online, we need to make sure that the keys already exist on cvmfs
if os.getenv("WRITE_ENCODING_KEYS", "0") == "0":
    options.write_decoding_keys_to_git = False
    metainfo_repos.global_bind(repos=[])  # only use repos on cvmfs
    retrieve_encoding_dictionary.global_bind(
        require_key_present=True)  # require key is in repo
# optional
# options.require_specific_decoding_keys = ["b80bb34f", "cf39d86c"]
