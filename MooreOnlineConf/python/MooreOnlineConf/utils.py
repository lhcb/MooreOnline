###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
import shutil
import re
from dataclasses import dataclass
from datetime import datetime
from pathlib import Path
from enum import IntEnum
from PyConf.application import default_raw_event, make_odin, default_raw_banks
from PyConf.Algorithms import TESCheck, OdinTypesFilter
from PyConf.Algorithms import HltRoutingBitsMonitor, HltRoutingBitsFilter
from PyConf.control_flow import CompositeNode, NodeLogic
from Configurables import Online__BanksToRawEvent


def run_all(name, children, force_order=False):
    return CompositeNode(
        name,
        children=children,
        combine_logic=NodeLogic.NONLAZY_OR,
        force_order=force_order)


def and_(name, children, force_order=False):
    return CompositeNode(
        name,
        children=children,
        combine_logic=NodeLogic.LAZY_AND,
        force_order=force_order,
    )


def if_then(name, predicate, then):
    return CompositeNode(
        name,
        children=[predicate, then],
        combine_logic=NodeLogic.LAZY_AND,
        force_order=True,
    )


def tae_name(offset: int):
    if not isinstance(offset, int):
        raise TypeError("offset must be integer")
    if abs(offset) > 9:
        raise ValueError(f"Offsets larger than 9 not supported (got {offset})")
    return "" if offset == 0 else f"{'Prev' if offset < 0 else 'Next'}{abs(offset)}"


# def tae_prefix(offset: int):
#     name = tae_name(offset)
#     return f"/Event/{name}" if name else "/Event"


def common_monitors_node():
    # FIXME in LOCAL there are no routing bits so we get an error "unexpected number of HltRoutingBits banks"
    # TODO add histogram of the event time
    rb_monitor = HltRoutingBitsMonitor(
        name="HltRoutingBitsMonitor",
        RawBanks=default_raw_banks("HltRoutingBits"))
    # odin_monitor = ODINMonitor(name="ODINMon", Input=make_odin())
    return run_all("Common", [rb_monitor])


class RoutingBit(IntEnum):
    LUMI = 1
    PHYSICS = 14
    SMOG = 15


def passes_rb(bit: RoutingBit):
    if bit > 31:
        raise NotImplementedError("Filtering on bits above 31 not supported")
    # TODO monitor how many/what event survive after this filter (or any other filter?)
    # i.e. copy of common_monitors_node with dedicated names of algorithms?
    return HltRoutingBitsFilter(
        name=f"RBFilterBit{bit:03}",
        RawBanks=default_raw_banks('HltRoutingBits'),
        RequireMask=(1 << bit, 0, 0),
        PassOnError=False)


def _has_tae_bx(offset):
    name = tae_name(offset)

    with default_raw_banks.bind(tae_pos=name):
        tae_input = default_raw_banks(
            "ODIN").producer.inputs["RawEventLocation"]
        if tae_input.producer.type == Online__BanksToRawEvent:
            tae_input = tae_input.producer.inputs["RawData"]

        check = TESCheck(
            name=f"TESCheck{name}",
            Inputs=[tae_input.location],
            Stop=False,
            OutputLevel=5)
        # data_bx = make_data(name=name)
        return check


# def _is_tae(half_window):
#     # The following is not enough because an event with non-zero TAE
#     # index might be selected for another reason.
#     # odin = make_odin()
#     # return OdinTypesFilter(ODIN=odin, TAEIndexMoreThan=0)
#     children = []
#     for offset in range(-half_window, half_window + 1):
#         if offset != 0:
#             children.append(_has_tae_bx(offset))
#     return CompositeNode(
#         "HasAnySideBX",
#         children=children,
#         combine_logic=NodeLogic.LAZY_OR)


def decode_tae(make_data, half_window):
    children = []
    check_bxs = {}
    odin_bxs = {}
    data_bxs = {}
    for offset in range(-half_window, half_window + 1):
        name = tae_name(offset)
        with default_raw_banks.bind(tae_pos=name):
            odin_bx = make_odin(name="Decode_ODIN" + name)
            data_bx = make_data(name=name)

        check_bxs[offset] = _has_tae_bx(offset)
        odin_bxs[offset] = odin_bx
        data_bxs[offset] = data_bx
        children.append(
            and_(
                f"TAEBX{name}",
                [odin_bx] + ([data_bx] if data_bx != odin_bx else []),
                force_order=True))

    # is_tae = _is_tae(half_window)
    is_tae = and_("HasAllTAEBX", check_bxs.values())
    cf_node = run_all("DecodeTAE", children)
    return is_tae, cf_node, odin_bxs, data_bxs


def update_and_reset():
    from PyConf.Algorithms import Online__UpdateAndReset
    odin = default_raw_event("ODIN")
    rawdata = odin.producer.inputs["RawData"]  # the evt_data_t object
    return Online__UpdateAndReset(name='UpdateAndReset', RawData=rawdata)


def load_opts(f, key_strip="OnlineEnv."):
    from Gaudi.Main import parseOpt

    result = {}
    for line in f:
        line = line.strip()
        if line.startswith("//"): continue
        m = re.match(r"^(?P<key>\S+)\s*=\s*(?P<value>.*)\s*;$", line)
        if not m:
            raise ValueError(f"cannot parse {line!r}")
        key = m.group("key")
        if not key.startswith(key_strip):
            raise ValueError(f"key {key!r} does not start with {key_strip!r}")
        key = key[len(key_strip):]
        result[key] = parseOpt(m.group("value"))
    return result


def chunks(lst, n):
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i:i + n]


def distribute_files(nodes, files):
    """
    """
    n_files_per_run = min(len(fs) for fs in files)
    n_files_per_run_per_node = max(n_files_per_run // len(nodes), 1)
    x = list(
        zip(*[
            chunks(files_per_run, n_files_per_run_per_node)
            for files_per_run in files
        ]))
    files_per_node = [sum(lists, []) for lists in x]
    return dict(
        zip(nodes, files_per_node + [[]] * (len(nodes) - len(files_per_node))))


# files_per_run = [list(range(10)), list(range(100, 100+10)), list(range(1000,1000+8))]
# nodes = ['a', 'b']
# distribute_files(nodes, files_per_run)
# distribute_files(nodes*12, files_per_run)


@dataclass
class OnlineAlignmentOpts:
    tag: str
    runs: list[str]
    nodes: list[str]
    analyzer_output_path: Path
    iterator_output_path: Path


def alignment_options(OnlineEnv) -> OnlineAlignmentOpts:
    with open(OnlineEnv.__file__) as f:
        # "#Auto generated RunInfo options for partition:LHCbA activity:Alignment|Rich1  2022.11.23 18:55:39.313"
        line = f.readline()
        assert "generated RunInfo options" in line
        dt = ' '.join(line.rsplit(" ", 2)[-2:]).rsplit(".", 1)[0]
        tag = datetime.strptime(dt,
                                "%Y.%m.%d %H:%M:%S").strftime("%Y%m%d%H%M%S")
    with open(Path(OnlineEnv.__file__).parent / "RunList.opts") as f:
        runs = sorted(load_opts(f)["DeferredRuns"])
    with open(Path(OnlineEnv.__file__).parent / "NodeList.opts") as f:
        nodes = sorted(load_opts(f)["NodeList"])
    if not runs:
        raise ValueError("DeferredRuns is empty")
    if not nodes:
        raise ValueError("NodeList is empty")

    activity = OnlineEnv.Activity.removeprefix("Alignment|").removeprefix(
        "Calibration|")
    #make sure leading zeroes are in run numbers
    runs = [f'{int(run):010d}' for run in runs]
    data_id = runs[0] if len(runs) == 1 else f"{runs[0]}-{runs[-1]}"

    prefix = Path()
    if OnlineEnv.PartitionName == "LHCbA":
        prefix = Path("/calib/align")

    analyzer_output_path = prefix / activity / "analyzer" / data_id
    iterator_output_path = prefix / activity / "iterator" / data_id

    return OnlineAlignmentOpts(tag, runs, nodes, analyzer_output_path,
                               iterator_output_path)


def ensure_output_dir(output_path, tag):
    tag_path = output_path / "tag"

    probe_tag = None
    try:
        with open(tag_path) as f:
            probe_tag = f.read().strip()
    except FileNotFoundError:
        shutil.rmtree(output_path, ignore_errors=True)

    # back up the output path if necessary
    try:
        if probe_tag is not None and probe_tag != tag:
            output_path.rename(output_path.with_suffix("." + probe_tag))
    except OSError as e:
        import errno
        if e.errno == errno.ENOTEMPTY:
            pass  # another worker already renamed the directory
        else:
            raise
    output_path.mkdir(parents=True, exist_ok=True)

    utgid = os.environ["UTGID"]
    local_tag_path = tag_path.with_suffix("." + utgid)
    with open(local_tag_path, "w") as f:
        f.write(tag + "\n")
    local_tag_path.replace(tag_path)
