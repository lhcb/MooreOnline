2018-05-23 MooreOnline v28r2p1
========================================

Production version for 2018
----------------------------------------
Based on Gaudi v29r4, LHCb v44r2p1, Lbcom v22r0p3, Rec v23r2p1, Phys v25r4, Hlt v28r2p1, Moore v28r2p1
This version is released on the master branch.

- Disable sending of histograms by the MonitorSvc in HLT2., !53 (@raaij)
