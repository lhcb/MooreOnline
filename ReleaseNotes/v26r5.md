2017-07-07 MooreOnline v26r5
========================================

Production release for 2017 data taking
----------------------------------------
Based on Gaudi v28r2, LHCb v42r5, Rec v21r5, Phys v23r5,
Hlt v26r5, Moore v26r5, Online v6r5

- Patches
  - Temporarily add Online/Hlt2Monitoring from lhcb/Online!107
    Commit is 4ce15e5143eeec26e0b238a618222af5567c0104

- Do not set routing bit 96, !40 (@rmatev)
- Check service before sending data and avoid blocking, !38 (@raaij)
  - Check if transmitter service is available and OK before sending data
  - Add timeouts to send calls to avoid blocking
