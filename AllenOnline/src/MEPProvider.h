/*****************************************************************************\
* (c) Copyright 2018-2020 CERN for the benefit of the LHCb Collaboration      *
\*****************************************************************************/
#pragma once

#include <algorithm>
#include <any>
#include <array>
#include <atomic>
#include <cassert>
#include <chrono>
#include <condition_variable>
#include <deque>
#include <mutex>
#include <numeric>
#include <thread>
#include <vector>

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <Gaudi/Accumulators.h>
#include <Gaudi/Accumulators/Histogram.h>
#include <Gaudi/Property.h>
#include <Kernel/meta_enum.h>

#include <GaudiKernel/Service.h>

#include <Event/RawBank.h>

#include <MBM/bmdef.h>
#include <RTL/Logger.h>

#include <MDF/StreamDescriptor.h>

#include <AllenOnline/TransposeMEP.h>

#ifdef HAVE_MPI
#  include <hwloc.h>
#endif

namespace {
  using namespace Allen::Units;
  using namespace std::string_literals;

  constexpr auto bank_header_size = sizeof( LHCb::RawBank ) - sizeof( unsigned int );
} // namespace

namespace MEP {
  meta_enum_class( ProviderSource, int, Unknown = 0, Files, MBM, MPI )
}

class AllenConfiguration;

// backward compatibility with Gaudi < v39 where FSMCallbackHolder does not exist
#include <GAUDI_VERSION.h>
#if GAUDI_MAJOR_VERSION < 39
using BaseClass = Service;
#else
#  include <Gaudi/FSMCallbackHolder.h>
using BaseClass = Gaudi::FSMCallbackHolder<Service>;
#endif

/**
 * @brief      Configuration parameters for the MEPProvider
 */

/**
 * @brief      Provide events from MEP files in either MEP or transposed layout
 *
 * @details    The provider has three main components
 *             - a prefetch thread to read from the current input
 *               file into prefetch buffers
 *             - N transpose threads that read from prefetch buffers
 *               and fill the per-bank-type slices with transposed sets
 *               of banks and the offsets to individual bank inside a
 *               given set
 *             - functions to obtain a transposed slice and declare it
 *               for refilling
 *
 *             Access to prefetch buffers and slices is synchronised
 *             using mutexes and condition variables.
 *
 * @param      Number of slices to fill
 * @param      Number of events per slice
 * @param      MDF filenames
 * @param      Configuration struct
 *
 */
class MEPProvider final : public BaseClass, public InputProvider {
public:
  MEPProvider( std::string name, ISvcLocator* loc );

  /**
   * @brief      Obtain event IDs of events stored in a given slice
   *
   * @param      slice index
   *
   * @return     EventIDs of events in given slice
   */
  EventIDs event_ids( size_t slice_index, std::optional<size_t> first = std::nullopt,
                      std::optional<size_t> last = std::nullopt ) const override;
  /**
   * @brief      Get event mask in a given slice (ODIN erro bank)
   *
   * @param      slice index
   *
   * @return     event mask
   */
  std::vector<char> event_mask( size_t slice_index ) const override;

  /**
   * @brief      Obtain banks from a slice
   *
   * @param      BankType
   * @param      slice index
   *
   * @return     Banks and their offsets
   */
  BanksAndOffsets banks( BankTypes bank_type, size_t slice_index ) const override;

  /**
   * @brief      Get a slice that is ready for processing; thread-safe
   *
   * @param      optional timeout
   *
   * @return     (good slice, timed out, slice index, number of events in slice)
   */
  std::tuple<bool, bool, bool, size_t, size_t, std::any>
  get_slice( std::optional<unsigned int> timeout = std::nullopt ) override;

  /**
   * @brief      Declare a slice free for reuse; thread-safe
   *
   * @param      slice index
   *
   * @return     void
   */
  void slice_free( size_t slice_index ) override;

  bool release_buffers() override;

  void event_sizes( size_t const slice_index, gsl::span<unsigned int const> const selected_events,
                    std::vector<size_t>& sizes ) const override;

  void copy_banks( size_t const slice_index, unsigned int const event, gsl::span<char> buffer ) const override;

  StatusCode initialize() override;

  StatusCode start() override;

  StatusCode stop() override;

  StatusCode finalize() override;

private:
  struct TransposeStatus {

    TransposeStatus()                               = default;
    TransposeStatus( TransposeStatus const& other ) = default;
    TransposeStatus& operator=( TransposeStatus const& other ) = default;
    TransposeStatus( size_t i, std::optional<size_t> n ) : slice_index{i}, n_transposed{std::move( n )} {}

    size_t                slice_index  = 0;
    std::optional<size_t> n_transposed = std::nullopt;
  };

  StatusCode init_mpi();

  int init_bm();

  size_t count_writable() const;

  std::tuple<bool, bool> allocate_storage( size_t i_read );

  /**
   * @brief      Open an input file; called from the prefetch thread
   *
   * @return     success
   */
  bool open_file() const;

  std::tuple<std::vector<BufferStatus>::iterator, size_t>
  get_mep_buffer( std::function<bool( BufferStatus const& )> pred, std::function<bool()> wait_pred,
                  std::vector<BufferStatus>::iterator start, std::condition_variable& cond,
                  std::unique_lock<std::mutex>& lock );

  bool prepare_mep( size_t i_buffer, size_t n_events );

  void read_error();

  // mep reader thread
  void mep_read();

  // MPI reader thread
  void mpi_read();

  // buffer manager reader thread
  void bm_read( const std::string& buffer_name );

  /**
   * @brief      Function to run in each thread transposing events
   *
   * @param      thread ID
   *
   * @return     void
   */
  void transpose( int thread_id );

  StatusCode numa_membind( char const* mem, size_t size, int const numa_node ) const;

  // Slices
  size_t                         m_packing_factor = 0;
  std::vector<std::vector<char>> m_read_buffers;
  std::vector<char*>             m_mpi_buffers;
  MEP::Slices                    m_net_slices;

  // data members for mpi thread
  bool                    m_started  = false;
  bool                    m_stopping = false;
  std::mutex              m_control_mutex;
  std::condition_variable m_control_cond;
  std::condition_variable m_start_cond;

  // data members for mpi thread
  std::mutex              m_buffer_mutex;
  std::condition_variable m_transpose_cond;
  std::condition_variable m_receive_cond;

#ifdef HAVE_MPI
  std::vector<std::tuple<int, int>> m_domains;
  hwloc_topology_t                  m_topology;

  char** m_mpiArgv = nullptr;
  int    m_mpiArgc = 1;
  int    m_rank    = -1;
#endif

  std::vector<BufferStatus>           m_buffer_status;
  std::vector<BufferStatus>::iterator m_buffer_reading;

  std::mutex               m_mbm_mutex;
  std::vector<bool>        m_buffer_event;
  std::vector<std::thread> m_input_threads;
  size_t                   m_ninput_threads = 0;
  size_t                   m_input_started  = 0;

  // Atomics to flag errors and completion
  mutable std::atomic<bool> m_read_error     = false;
  std::atomic<bool>         m_input_done     = false;
  std::atomic<bool>         m_transpose_done = false;
  std::atomic<bool>         m_done           = false;

  // Memory slices, N for each raw bank type
  Allen::Slices                                m_slices;
  std::vector<std::tuple<int, size_t, size_t>> m_slice_to_buffer;
  std::vector<decltype( LHCb::ODIN::data )>    m_odins;

  // Array to store the version of banks per bank type
  mutable std::array<int, NBankTypes> m_banks_version;

  // Mutex, condition varaible and queue for parallel transposition of slices
  std::mutex                 m_transpose_mut;
  std::condition_variable    m_transposed_cond;
  std::list<TransposeStatus> m_transposed;

  // Keep track of what slices are free
  std::mutex              m_slice_mut;
  std::condition_variable m_slice_cond;
  std::vector<bool>       m_slice_free;

  // Threads transposing data
  std::vector<std::thread> m_transpose_threads;

  // Array to store the number of banks per bank type
  mutable std::array<unsigned int, NBankTypes> m_mfp_count;
  mutable bool                                 m_sizes_known = false;

  // Run and event numbers present in each slice
  std::vector<EventIDs>          m_event_ids;
  std::vector<std::vector<char>> m_event_masks;

  // Storage for the currently open input file
  mutable std::optional<LHCb::StreamDescriptor::Access> m_input;

  // Iterator that points to the filename of the currently open file
  mutable std::vector<std::string>::const_iterator m_current;

  AllenConfiguration const* m_allenConfig;

  // MBM variables
  std::vector<BMID>        m_bmIDs;
  size_t                   n_buffers() const { return std::get<0>( m_bufferConfig.value() ); }
  std::vector<char const*> m_registered_buffers;

  size_t n_receivers() const { return m_receivers.size(); }

  std::unordered_set<BankTypes> m_bank_types;

  size_t m_alloc_tries = 0u;

  Gaudi::Property<size_t>                   m_allocate_retries{this, "AllocateRetries", 10};
  Gaudi::Property<size_t>                   m_nslices{this, "NSlices", 6};
  Gaudi::Property<size_t>                   m_events_per_slice{this, "EventsPerSlice", 1000};
  Gaudi::Property<std::vector<std::string>> m_connections{this, "Connections"};

  Gaudi::Property<MEP::ProviderSource> m_source{this, "Source", MEP::ProviderSource::MBM};
  Gaudi::Property<MEP::MBMCom>         m_mbm_com{this, "MBMComMethod", MEP::MBMCom::FIFO};

  // number of prefetch buffers and transpose threads
  Gaudi::Property<std::pair<int, int>> m_bufferConfig{
      this,
      "BufferConfig",
      {8, 5},
      [this]( auto& ) -> void {
        // Sanity check on the number of buffers and threads
        auto [n_buf, n_transpose] = m_bufferConfig.value();
        if ( n_buf < 2 ) {
          warning() << "Too few read buffers requested, setting it to 2" << endmsg;
          n_buf = 2;
        }

        if ( n_transpose < 2 ) {
          warning() << "Too few transpose threads requested increasing the number of threads to " << 2 << endmsg;
          n_transpose = 2;
        }
        m_bufferConfig.set( {n_buf, n_transpose} );
      },
      Gaudi::Details::Property::ImmediatelyInvokeHandler{true}};
  Gaudi::Property<int>  m_window_size{this, "MPIWindow", 4};
  Gaudi::Property<bool> m_non_stop{this, "LoopOnMEPs", false};
  Gaudi::Property<bool> m_preload{this, "Preload", false};
  Gaudi::Property<long> m_nevents{this, "EvtMax", -1};
  Gaudi::Property<bool> m_transpose_mep{this, "TransposeMEPs", false};
  Gaudi::Property<bool> m_all_bank_types{this, "ProvideAllBankTypes", true};

  // Mapping of receiver card to MPI rank to receive from
  Gaudi::Property<std::map<std::string, int>> m_receivers{this, "Receivers", {}};
  Gaudi::Property<std::vector<int>>           m_buffer_numa{this, "BufferNUMA", {}};
  Gaudi::Property<std::vector<std::string>>   m_requests{this, "Requests", {}};
  Gaudi::Property<bool>                       m_thread_per_buffer{this, "ThreadPerBuffer", true};
  Gaudi::Property<bool>                       m_mask_top5{this, "MaskSourceIDTop5", false};

  Gaudi::Accumulators::Counter<> m_mep_null{this, "MEP_is_NULL"};
  Gaudi::Accumulators::Counter<> m_mep_bad_magic{this, "MEP_magic_invalid"};
  Gaudi::Accumulators::Counter<> m_mep_empty{this, "MEP_no_MFPs"};
  Gaudi::Accumulators::Counter<> m_mep_without_odin{this, "MEP_without_ODIN_MFP"};
  Gaudi::Accumulators::Counter<> m_slice_without_odin{this, "GPU_batch_without_ODIN"};
  Gaudi::Accumulators::Counter<> m_odin_wrong_type{this, "ODIN_wrong_type"};
  Gaudi::Accumulators::Counter<> m_odin_event_errors{this, "ODIN_bad_event_number"};

  std::unordered_map<unsigned int, unsigned int> m_mfp_bin_mapping;
  using Histo1U = Gaudi::Accumulators::Histogram<1, Gaudi::Accumulators::atomicity::full, float>;
  std::unique_ptr<Histo1U> m_bad_mfps;
  constexpr static int     s_nsd = static_cast<int>( SourceIdSys::SourceIdSys_TDET );
  Histo1U m_bad_mfps_sd{this, "BadMFPsSubdet", "Bad MFPs per subdetector", {s_nsd + 1, -0.5f, s_nsd + 0.5f}};

  std::unique_ptr<Gaudi::Accumulators::Counter<>> m_mepsInput;
  std::unique_ptr<Gaudi::Accumulators::Counter<>> m_eventsInput;
  std::unique_ptr<Gaudi::Accumulators::Counter<>> m_eventsToBatches;
  std::unique_ptr<Gaudi::Accumulators::Counter<>> m_mbInput;
};
