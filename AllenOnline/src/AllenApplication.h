/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <dlfcn.h>

#include <chrono>
#include <cmath>
#include <iostream>

#include <CPP/Event.h>
#include <Gaudi/Property.h>
#include <GaudiKernel/AppReturnCode.h>
#include <GaudiKernel/IAppMgrUI.h>
#include <GaudiKernel/IMessageSvc.h>
#include <GaudiKernel/IMonitorSvc.h>
#include <GaudiKernel/IProperty.h>
#include <GaudiKernel/ISvcLocator.h>
#include <GaudiKernel/SmartIF.h>
#include <RTL/rtl.h>
#include <RTL/strdef.h>
#include <dim/dis.hxx>

#include <Gaucho/IGauchoMonitorSvc.h>
#include <GaudiOnline/OnlineApplication.h>
#include <ZeroMQ/IZeroMQSvc.h>

#include <Allen/OutputHandler.h>
#include <Dumpers/IUpdater.h>

#include "AllenConfiguration.h"

class AllenApplication : public Online::OnlineApplication, public Allen::NonEventData::IUpdater {
public:
  // Specialized constructor
  AllenApplication( Options opts );
  // Default destructor
  virtual ~AllenApplication();

  /// Cancel the application: Cancel IO request/Event loop
  int cancel() override;

  /// Internal: Initialize the application            (NOT_READY  -> READY)
  int configureApplication() override;
  /// Internal: Finalize the application              (READY      -> NOT_READY)
  int finalizeApplication() override;

  /// Internal: Start the application                 (READY      -> RUNNING)
  int startApplication() override;
  /// Stop the application                            (RUNNING    -> READY)
  int stop() override;
  /// Pause the application                           (RUNNING    -> PAUSED)
  int pauseProcessing() override;
  /// Continue the application                        (PAUSED -> RUNNING )
  int continueProcessing() override;

  // Main function running the Allen event loop
  void allenLoop( std::string_view config, std::string_view config_source );

  bool initMPI();

  // Updater
  void update( gsl::span<unsigned const> odin_data ) override;

  void registerConsumer( std::string const& id, std::unique_ptr<Allen::NonEventData::Consumer> c ) override;

  void registerProducer( std::string const& id, Allen::NonEventData::Producer p ) override;

private:
  OutputHandler* makeOutput();

  void runChange();

  /// Reference to the monitoring service
  SmartIF<IGauchoMonitorSvc> m_monSvc;

  /// Handles to helper service to properly name burst counters
  SmartIF<IService> m_monMEPs;
  /// Handles to helper service to properly name event counters
  SmartIF<IService> m_monEvents;

  // ZeroMQSvc
  SmartIF<IZeroMQSvc> m_zmqSvc;

  unsigned long m_runNumber = 0;

  Allen::NonEventData::IUpdater* m_updater = nullptr;
  SmartIF<AllenConfiguration>    m_allenConfig;

  std::string const                  m_controlConnection   = "inproc://AllenApplicationControl";
  std::string const                  m_runChangeConnection = "inproc://AllenApplicationRunChange";
  std::map<std::string, std::string> m_options;

  IInputProvider*                m_provider = nullptr;
  std::unique_ptr<OutputHandler> m_outputHolder;
  OutputHandler*                 m_output = nullptr;

  size_t m_nSlices = 0;

  // dlopen stuff to workaround segfault in genconf.exe
  // void* m_handle = nullptr;
  // typedef int (
  //   *allen_t)(std::map<std::string, std::string>, Allen::NonEventData::IUpdater*, IZeroMQSvc* zmqSvc,
  //   std::string_view);
  // allen_t m_allenFun = nullptr;

  std::thread                  m_allenThread;
  std::optional<zmq::socket_t> m_allenControl = std::nullopt;
};
