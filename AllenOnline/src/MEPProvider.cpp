/*****************************************************************************\
* (c) Copyright 2018-2020 CERN for the benefit of the LHCb Collaboration      *
\*****************************************************************************/

#include <pthread.h>
#include <thread>

#include <Allen/BankMapping.h>
#include <Allen/BankTypes.h>
#include <Allen/InputProvider.h>
#include <Allen/InputReader.h>
#include <Allen/Logger.h>
#include <Allen/MEPTools.h>
#include <Allen/Provider.h>
#include <Allen/SliceUtils.h>
#include <Allen/Timer.h>
#include <Allen/sourceid.h>
#include <Allen/write_mdf.hpp>
#include <Backend/BackendCommon.h>

#include <MDF/MDFHeader.h>
#include <MDF/StreamDescriptor.h>

#include <Event/RawBank.h>

#include <MBM/Requirement.h>

#include <AllenOnline/ReadMEP.h>
#include <AllenOnline/TransposeMEP.h>

#include "AllenConfiguration.h"
#include "MEPProvider.h"

#include <TROOT.h>

#ifdef HAVE_MPI
#  include <AllenOnline/MPIConfig.h>
#  include <hwloc.h>
#endif

DECLARE_COMPONENT( MEPProvider )

// The MEPProvider has three possible sources: MPI, the BufferManager
// and files.
//
// In case of file input, MEPs are read into m_read_buffers and
// reading will continue until all read buffers are full or the
// maximum number of events has been reached. The memory in the read
// buffers is owned by the MEPProvider.
//
// In case of MPI input, memory to receive MEPs is allocated with MPI
// and the pointers are stored in m_mpi_buffers. Memory allocated with
// MPI is registered to the device runtime.
//
// In case of BufferManager input, a number of input threads equal to
// the number of buffers is started. This ensures a 1-to-1 mapping
// between buffers and BMIDs stored in m_bmIDs. All of the memory
// allocated by the BufferManager is registered with the device
// runtime.

MEPProvider::MEPProvider( std::string name, ISvcLocator* loc ) : BaseClass{name, loc}, m_mfp_count{0} {}

EventIDs MEPProvider::event_ids( size_t slice_index, std::optional<size_t> first, std::optional<size_t> last ) const {
  auto const& ids = m_event_ids[slice_index];
  return {ids.begin() + ( first ? *first : 0 ), ids.begin() + ( last ? *last : ids.size() )};
}

std::vector<char> MEPProvider::event_mask( size_t slice_index ) const { return m_event_masks[slice_index]; }

BanksAndOffsets MEPProvider::banks( BankTypes bank_type, size_t slice_index ) const {
  auto ib = to_integral( bank_type );
  assert( static_cast<size_t>( ib ) < m_slices.size() );
  assert( slice_index < m_slices[ib].size() );
  auto const& slice = m_slices[ib][slice_index];
  // auto const& [banks, data_size, offsets, offsets_size] = m_slices[ib][slice_index];

  BanksAndOffsets bno;
  auto&           spans = bno.fragments;
  spans.reserve( slice.fragments.size() );
  for ( auto s : slice.fragments ) { spans.emplace_back( s ); }

  bno.fragments_mem_size = m_transpose_mep.value() ? slice.offsets[slice.n_offsets - 1] : slice.fragments_mem_size;
  bno.offsets            = slice.offsets;
  bno.sizes              = slice.sizes;
  bno.types              = slice.types;
  bno.version            = ( m_all_bank_types.value() || m_bank_types.count( bank_type ) ) ? m_banks_version[ib] : -1;

  if ( msgLevel( MSG::VERBOSE ) ) {
    verbose() << "slice " << std::setw( 3 ) << slice_index << " bank type " << ib << " size " << std::setw( 12 )
              << bno.fragments_mem_size << endmsg;
  }

  return bno;
}

std::tuple<bool, bool, bool, size_t, size_t, std::any> MEPProvider::get_slice( std::optional<unsigned int> timeout ) {
  bool                           timed_out  = false;
  std::optional<TransposeStatus> transposed = std::nullopt;
  std::any                       odin_span;
  std::unique_lock<std::mutex>   lock{m_transpose_mut};

  auto transposed_value = []( TransposeStatus const& s ) { return s.n_transposed.has_value(); };

  auto has_transposed = [this, &transposed_value] {
    return std::any_of( m_transposed.begin(), m_transposed.end(), transposed_value );
  };

  auto get_transposed = [this, &transposed_value]() -> TransposeStatus {
    auto it = std::find_if( m_transposed.begin(), m_transposed.end(), transposed_value );
    if ( it != m_transposed.end() ) {
      auto status = *it;
      m_transposed.erase( it );
      return status;
    } else {
      return {};
    }
  };

  if ( !m_read_error ) {
    // If no transposed slices are ready for processing, wait until
    // one is; use a timeout if requested
    if ( !has_transposed() ) {
      auto wakeup = [this, &has_transposed] { return ( has_transposed() || m_read_error || m_transpose_done ); };
      if ( timeout ) {
        timed_out = !m_transposed_cond.wait_for( lock, std::chrono::milliseconds{*timeout}, wakeup );
      } else {
        m_transposed_cond.wait( lock, wakeup );
      }
    }

    if ( !m_read_error && has_transposed() && ( !timeout || ( timeout && !timed_out ) ) ) {
      transposed = get_transposed();
      if ( transposed->n_transposed && *( transposed->n_transposed ) > 0 ) {
        odin_span = gsl::span<unsigned const>{m_odins[transposed->slice_index]};
      }
    }
  }

  // Check both transposed status that are in progress or waiting for
  // a slice and the ones that are ready for consumption
  bool done = m_transpose_done && m_transposed.empty();

  // Check if I/O and transposition is done and return a slice index
  if ( !timed_out && transposed && !m_read_error ) {
    size_t n_events = transposed->n_transposed ? *( transposed->n_transposed ) : 0u;
    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "get_slice returning " << transposed->slice_index << "; error " << m_read_error << " done " << done
              << " n_filled " << ( n_events != 0 ? std::to_string( n_events ) : std::string{} ) << endmsg;
    }
    ( *m_eventsToBatches ) += n_events;
    return {!m_read_error, done, timed_out, transposed->slice_index, n_events, odin_span};
  } else {
    if ( timed_out && msgLevel( MSG::DEBUG ) ) {
      debug() << "get_slice timed out; error " << m_read_error << " done " << done << endmsg;
    }
    return {!m_read_error, done, timed_out, 0, 0, odin_span};
  }
}

void MEPProvider::slice_free( size_t slice_index ) {
  // Check if a slice was acually in use before and if it was, only
  // notify the transpose threads that a free slice is available
  bool freed = false, set_writable = false;
  int  i_buffer = 0;
  {
    std::unique_lock<std::mutex> lock{m_slice_mut};
    if ( !m_slice_free[slice_index] ) {
      std::unique_lock<std::mutex> buffer_lock{m_buffer_mutex};

      m_slice_free[slice_index] = true;
      freed                     = true;
      m_odins[slice_index].fill( 0 );

      // Clear relation between slice and buffer
      i_buffer     = std::get<0>( m_slice_to_buffer[slice_index] );
      auto& status = m_buffer_status[i_buffer];
      --status.work_counter;

      m_slice_to_buffer[slice_index] = {-1, 0, 0};

      // If MEPs are not transposed and the respective buffer is no
      // longer in use, set it to writable
      if ( status.work_counter == 0 &&
           ( std::find_if( m_slice_to_buffer.begin(), m_slice_to_buffer.end(), [i_buffer]( const auto& entry ) {
               return std::get<0>( entry ) == i_buffer;
             } ) == m_slice_to_buffer.end() ) ) {
        status.writable = true;
        set_writable    = true;
        if ( m_source == MEP::ProviderSource::MBM ) {
          std::unique_lock<std::mutex> mbm_lock{m_mbm_mutex};
          if ( m_buffer_event[i_buffer] ) {
            ::mbm_free_event( m_bmIDs[i_buffer] );
            m_buffer_event[i_buffer] = false;
          }
        }

        if ( status.work_counter == 0 ) {
          if ( m_done && std::all_of( m_buffer_status.begin(), m_buffer_status.end(), []( BufferStatus const& stat ) {
                 return stat.intervals.empty() && stat.work_counter == 0;
               } ) ) {
            m_transpose_done = true;
            m_transpose_cond.notify_all();
          }
        }

        if ( msgLevel( MSG::DEBUG ) ) {
          debug() << "Freed MEP buffer " << i_buffer << "; writable: " << count_writable() << endmsg;
          for ( auto const& status : m_buffer_status ) {
            verbose() << std::setw( 4 ) << status.index << std::setw( 3 ) << status.writable << std::setw( 4 )
                      << status.work_counter;
            for ( auto interval : status.intervals ) {
              verbose() << std::setw( 5 ) << std::get<0>( interval ) << std::setw( 5 ) << std::get<1>( interval );
            }
            verbose() << endmsg;
          }
        }
      }
    }
  }
  if ( freed ) {
    if ( msgLevel( MSG::DEBUG ) ) debug() << "Freed slice " << slice_index << endmsg;
    m_slice_cond.notify_one();
  }
  if ( set_writable ) {
    if ( msgLevel( MSG::DEBUG ) ) debug() << "Set buffer " << i_buffer << " writable" << endmsg;
    m_receive_cond.notify_all();
  }
}

void MEPProvider::event_sizes( size_t const slice_index, gsl::span<unsigned int const> const selected_events,
                               std::vector<size_t>& sizes ) const {
  int    i_buffer       = 0;
  size_t interval_start = 0, interval_end = 0;
  std::tie( i_buffer, interval_start, interval_end ) = m_slice_to_buffer[slice_index];
  auto const& blocks                                 = m_net_slices[i_buffer].blocks;
  for ( unsigned int i = 0; i < selected_events.size(); ++i ) {
    auto event = selected_events[i];
    sizes[i] +=
        std::accumulate( blocks.begin(), blocks.end(), 0ul, [event, interval_start]( size_t s, const auto& block ) {
          auto const fragment_size = block.bank_sizes[interval_start + event];
          return s + bank_header_size + Allen::padded_bank_size( fragment_size );
        } );
  }
}

void MEPProvider::copy_banks( size_t const slice_index, unsigned int const event, gsl::span<char> buffer ) const {
  auto [i_buffer, interval_start, interval_end] = m_slice_to_buffer[slice_index];
  const auto mep_event                          = interval_start + event;

  auto const& slice  = m_net_slices[i_buffer];
  size_t      offset = 0;

  for ( size_t i_block = 0; i_block < slice.blocks.size(); ++i_block ) {
    auto const& block  = slice.blocks[i_block];
    auto const* header = block.header;

    // All banks are taken directly from the block data to be able
    // to treat banks needed by Allen and banks not needed by Allen
    // in the same way
    auto const fragment_offset = slice.offsets[i_block][mep_event];
    auto       fragment_size   = block.bank_sizes[mep_event];

    assert( ( offset + bank_header_size + Allen::padded_bank_size( fragment_size ) ) <=
            static_cast<size_t>( buffer.size() ) );
    auto const source_id = m_mask_top5.value() ? header->src_id & 0x7FF : header->src_id;
    offset += Allen::add_raw_bank( block.bank_types[mep_event], header->block_version, source_id,
                                   {block.payload + fragment_offset, fragment_size}, buffer.data() + offset );
  }
}

StatusCode MEPProvider::initialize() {

  auto sc = BaseClass::initialize();
  if ( !sc.isSuccess() ) return sc;

  // initialize bank version, needed for banks of subdetectors not present in input data
  std::fill( m_banks_version.begin(), m_banks_version.end(), -1 );

  m_slice_free.resize( m_nslices.value(), true );
  m_event_ids.resize( m_nslices.value() );
  m_event_masks.resize( m_nslices.value() );
  m_odins.assign( m_nslices.value(), decltype( LHCb::ODIN::data ){0} );

  m_buffer_status.resize( n_buffers() );
  for ( size_t i = 0; i < m_buffer_status.size(); ++i ) { m_buffer_status[i].index = i; }

  auto config = service( "AllenConfiguration/AllenConfiguration", false ).as<AllenConfiguration>();
  if ( !config ) {
    error() << "Failed to retrieve AllenConfiguration." << endmsg;
    return StatusCode::FAILURE;
  }
  m_allenConfig = config.get();

  if ( m_all_bank_types.value() ) {
    m_bank_types = DataBankTypes;
  } else {
    auto const& [sequence, source] = m_allenConfig->sequence();
    if ( sequence.empty() ) {
      error() << "Failed to obtain sequence" << endmsg;
      return StatusCode::FAILURE;
    }
    const auto config_reader = ConfigurationReader( sequence );
    m_bank_types             = config_reader.configured_bank_types();
  }

  if ( m_bank_types.empty() ) {
    error() << "No bank types specified" << endmsg;
    return StatusCode::FAILURE;
  } else {
    info() << "Providing banks for";
    std::vector<BankTypes> types{m_bank_types.begin(), m_bank_types.end()};
    std::sort( types.begin(), types.end() );
    for ( auto bt : types ) info() << " " << ::bank_name( bt );
    info() << endmsg;
  }

  auto eventsSvc = dynamic_cast<Service*>( service<IService>( "AllenIOMon/Events", true ).get() );
  if ( !eventsSvc ) {
    error() << "Failed to obtain Events service for monitoring" << endmsg;
    return StatusCode::FAILURE;
  }

  auto burstsSvc = dynamic_cast<Service*>( service<IService>( "AllenIOMon/Bursts", true ).get() );
  if ( !burstsSvc ) {
    error() << "Failed to obtain Bursts service for monitoring" << endmsg;
    return StatusCode::FAILURE;
  }

  m_mepsInput       = std::make_unique<Gaudi::Accumulators::Counter<>>( burstsSvc, "IN" );
  m_eventsInput     = std::make_unique<Gaudi::Accumulators::Counter<>>( eventsSvc, "IN" );
  m_eventsToBatches = std::make_unique<Gaudi::Accumulators::Counter<>>( eventsSvc, "SLICED" );
  m_mbInput         = std::make_unique<Gaudi::Accumulators::Counter<>>( eventsSvc, "MB_IN" );

#ifdef HAVE_MPI
  if ( !m_buffer_numa.value().empty() && m_buffer_numa.value().size() != n_buffers() ) {
    error() << "Buffer NUMA domains must be specified for all buffers" << endmsg;
    return StatusCode::FAILURE;
  }

  // Allocate and initialize topology object.
  hwloc_topology_init( &m_topology );

// discover everything, in particular I/O devices like
// InfiniBand cards
#  if HWLOC_API_VERSION >= 0x20000
  hwloc_topology_set_io_types_filter( m_topology, HWLOC_TYPE_FILTER_KEEP_IMPORTANT );
#  else
  hwloc_topology_set_flags( m_topology, HWLOC_TOPOLOGY_FLAG_WHOLE_SYSTEM | HWLOC_TOPOLOGY_FLAG_IO_DEVICES );
#  endif
  // Perform the topology detection.
  hwloc_topology_load( m_topology );

  auto n_numa = hwloc_get_nbobjs_by_type( m_topology, HWLOC_OBJ_NUMANODE );
  for ( auto domain : m_buffer_numa ) {
    if ( domain >= n_numa ) {
      error() << "Illegal NUMA domain specified: " << domain << endmsg;
      return StatusCode::FAILURE;
    }
  }
#endif

  std::optional<size_t> n_events = std::nullopt;
  if ( m_nevents.value() >= 0 ) n_events = static_cast<size_t>( m_nevents.value() );

  init_input( m_nslices, m_events_per_slice, m_bank_types,
              m_transpose_mep.value() ? IInputProvider::Layout::Allen : IInputProvider::Layout::MEP, n_events );

  if ( m_transpose_mep ) {
    info() << "Providing events in Allen layout by transposing MEPs" << endmsg;
  } else {
    info() << "Providing events in MEP layout" << endmsg;
    ;
  }

  m_buffer_reading = m_buffer_status.begin();

  if ( m_source == MEP::ProviderSource::MPI ) {
    sc = init_mpi();
    if ( !sc.isSuccess() ) return sc;
  } else if ( m_source == MEP::ProviderSource::Files ) {
    m_read_buffers.resize( n_buffers() );
    // Initialize the current input filename
    m_current = m_connections.begin();
  }
  m_net_slices.resize( n_buffers() );

  // Allocate space to store event ids
  for ( size_t n = 0; n < m_nslices.value(); ++n ) {
    m_event_ids[n].reserve( m_events_per_slice.value() );
    m_event_masks[n].resize( m_events_per_slice.value(), 0 );
  }

  return sc;
}

StatusCode MEPProvider::start() {
  if ( m_started ) { return StatusCode::SUCCESS; }

  auto sc = BaseClass::start();
  if ( !sc.isSuccess() ) return sc;

  std::unique_lock<std::mutex> lock{m_control_mutex};
  m_stopping       = false;
  m_input_done     = false;
  m_transpose_done = false;
  m_done           = false;

  // start MPI receive, MEP reading thread or BM thread
  if ( m_source == MEP::ProviderSource::MPI && m_input_threads.empty() ) {
#ifdef HAVE_MPI
    m_ninput_threads = 1;
    m_input_threads.emplace_back( std::thread{&MEPProvider::mpi_read, this} );
#else
    error() << "MPI requested, but no MPI support built in." << endmsg;
    return StatusCode::FAILURE;
#endif
  } else if ( m_source == MEP::ProviderSource::Files && m_input_threads.empty() ) {
    m_ninput_threads = 1;

    // Open the first file from the main thread
    if ( !open_file() ) { return StatusCode::FAILURE; }

    m_input_threads.emplace_back( std::thread{&MEPProvider::mep_read, this} );
  } else if ( m_source == MEP::ProviderSource::MBM ) {
    auto sc = init_bm();
    if ( sc != MBM_NORMAL ) { return StatusCode::FAILURE; }
    m_ninput_threads = m_thread_per_buffer.value() ? m_connections.size() : 1;
    if ( m_thread_per_buffer.value() ) {
      for ( auto buffer : m_connections.value() ) {
        debug() << "Starting bm_read thread for " << buffer << endmsg;
        m_input_threads.emplace_back( std::thread{&MEPProvider::bm_read, this, buffer} );
      }
    } else {
      m_input_threads.emplace_back( std::thread{&MEPProvider::bm_read, this, ""} );
    }
  }

  // Start the transpose threads
  if ( m_transpose_threads.empty() && !m_read_error ) {
    for ( int i = 0; i < std::get<1>( m_bufferConfig.value() ); ++i ) {
      m_transpose_threads.emplace_back( [this, i] { transpose( i ); } );
    }
  }

  bool const sizes_known = m_sizes_known;
  m_start_cond.wait( lock, [this] { return m_input_started == m_input_threads.size(); } );

  debug() << "Input threads started" << endmsg;

  m_started = true;
  if ( !sizes_known ) {
    m_control_cond.notify_one();
  } else {
    m_control_cond.notify_all();
  }
  return sc;
};

StatusCode MEPProvider::stop() {
  {
    std::unique_lock<std::mutex> lock{m_control_mutex};
    m_stopping = true;
  }

  // When we get stop, we give up on any pending data
  m_receive_cond.notify_all();

  if ( m_source == MEP::ProviderSource::MBM ) {
    std::unique_lock<std::mutex> lock{m_buffer_mutex};
    debug() << "Cancelling MBM requests; n_writable: " << count_writable() << endmsg;
    // Cancel all requests to the buffer manager for those who are waiting
    for ( size_t b = 0; b < m_buffer_status.size(); ++b ) {
      if ( !m_buffer_status[b].writable ) ::mbm_cancel_request( m_bmIDs[b] );
    }
  }

  // Notify input threads in case they're waiting and there hasn't been any data.
  m_control_cond.notify_all();

  for ( auto& input_thread : m_input_threads ) { input_thread.join(); }
  m_input_threads.clear();

  m_input_done = true;

  m_transpose_cond.notify_all();
  m_slice_cond.notify_all();

  for ( auto& transpose_thread : m_transpose_threads ) { transpose_thread.join(); }
  m_transpose_threads.clear();

  m_transpose_done = true;

  {
    std::unique_lock<std::mutex> lock{m_control_mutex};
    m_started       = false;
    m_input_started = 0;
  }

  return BaseClass::stop();
};

StatusCode MEPProvider::finalize() {
  m_done           = true;
  m_transpose_done = true;
  m_transpose_cond.notify_all();
  m_control_cond.notify_all();
  for ( auto& transpose_thread : m_transpose_threads ) { transpose_thread.join(); }

  return BaseClass::finalize();
}

bool MEPProvider::release_buffers() {
  for ( size_t b = 0; b < m_registered_buffers.size(); ++b ) {
    auto const* buffer_address = m_registered_buffers[b];
    try {
      Allen::host_unregister( const_cast<char*>( buffer_address ) );
      debug() << "Successfully uregistered BM memory for buffer " << b << " with device runtime." << endmsg;
    } catch ( const std::invalid_argument& e ) {
      error() << "Failed to unregister BM memory for buffer " << b << " with device runtime : " << e.what() << endmsg;
      return false;
    }
  }
  m_registered_buffers.clear();

  // Reset buffer statuses
  for ( auto& status : m_buffer_status ) {
    status.writable     = true;
    status.work_counter = 0;
    status.intervals.clear();
  }
  m_buffer_reading = m_buffer_status.begin();
  m_buffer_event.assign( m_buffer_event.size(), false );

  // Reset slice statuses
  m_slice_free.assign( m_slice_free.size(), true );
  m_slice_to_buffer.assign( m_slice_to_buffer.size(), std::tuple{-1, 0ul, 0ul} );

  for ( size_t b = 0; b < m_bmIDs.size(); ++b ) {
    auto bmid = m_bmIDs[b];
    if ( bmid != MBM_INV_DESC ) { ::mbm_exclude( bmid ); }
    m_bmIDs[b] = MBM_INV_DESC;
  }
  return true;
}

StatusCode MEPProvider::init_mpi() {
#ifdef HAVE_MPI

  // MPI initialization
  auto len      = name().length();
  int  provided = 0;
  m_mpiArgv     = new char*[1];
  m_mpiArgv[0]  = new char[len];
  ::strncpy( m_mpiArgv[0], name().c_str(), len );
  MPI_Init_thread( &m_mpiArgc, &m_mpiArgv, MPI_THREAD_MULTIPLE, &provided );
  if ( provided != MPI_THREAD_MULTIPLE ) {
    error() << "Failed to initialize MPI multi thread support." << endmsg;
    return StatusCode::FAILURE;
  }

  // Communication size
  int comm_size = 0;
  MPI_Comm_size( MPI_COMM_WORLD, &comm_size );
  if ( comm_size > MPI::comm_size ) {
    error() << "This program requires at most " << MPI::comm_size << " processes." << endmsg;
    return StatusCode::FAILURE;
  }

  // MPI: Who am I?
  MPI_Comm_rank( MPI_COMM_WORLD, &m_rank );

  if ( m_rank != MPI::receiver ) {
    error() << "AllenApplication can only function as MPI receiver." << endmsg;
    return StatusCode::FAILURE;
  }

  m_domains.reserve( m_receivers.size() );

  hwloc_obj_t osdev = nullptr;

  if ( !m_receivers.empty() ) {
    // Find NUMA domain of receivers
    while ( ( osdev = hwloc_get_next_osdev( m_topology, osdev ) ) ) {
      // We're interested in InfiniBand cards
      if ( osdev->attr->osdev.type == HWLOC_OBJ_OSDEV_OPENFABRICS ) {
        auto parent = hwloc_get_non_io_ancestor_obj( m_topology, osdev );
        auto it     = m_receivers.find( osdev->name );
        if ( it != m_receivers.end() ) {
          m_domains.emplace_back( it->second, parent->os_index );
          debug() << "Located receiver device " << it->first << " in NUMA domain " << parent->os_index << endmsg;
        }
      }
    }
    if ( m_domains.size() != m_receivers.size() ) {
      error() << "Failed to locate some receiver devices " << endmsg;
      return StatusCode::FAILURE;
    }
  } else {
    error() << "MPI requested, but no receivers specified" << endmsg;
    return StatusCode::FAILURE;
  }

  // Get last node. There's always at least one.
  [[maybe_unused]] auto n_numa = hwloc_get_nbobjs_by_type( m_topology, HWLOC_OBJ_NUMANODE );
  assert( static_cast<size_t>( n_numa ) == m_domains.size() );

  std::vector<size_t> packing_factors( m_receivers.size() );
  for ( size_t receiver = 0; receiver < m_receivers.size(); ++receiver ) {
    auto const receiver_rank = std::get<0>( m_domains[receiver] );
    MPI_Recv( &packing_factors[receiver], 1, MPI_SIZE_T, receiver_rank, MPI::message::packing_factor, MPI_COMM_WORLD,
              MPI_STATUS_IGNORE );
  }

  if ( !std::all_of( packing_factors.begin(), packing_factors.end(),
                     [v = packing_factors.back()]( auto const p ) { return p == v; } ) ) {
    error() << "All MEPs must have the same packing factor" << endmsg;
    return StatusCode::FAILURE;

  } else {
    m_packing_factor = packing_factors.back();
  }

  // Allocate as many net slices as configured, of expected size
  // Packing factor can be done dynamically if needed
  size_t n_bytes = std::lround( m_packing_factor * average_event_size * bank_size_fudge_factor * kB );
  for ( size_t i = 0; i < n_buffers(); ++i ) {
    char* contents = nullptr;
    MPI_Alloc_mem( n_bytes, MPI_INFO_NULL, &contents );

    // Only bind explicitly if there are multiple receivers,
    // otherwise assume a memory allocation policy is in effect
    if ( m_domains.size() > 1 ) {
      auto numa_node = std::get<1>( m_domains[i % m_receivers.size()] );
      auto sc        = numa_membind( contents, n_bytes, numa_node );
      if ( sc.isFailure() ) return sc;
    }

    Allen::host_register( contents, n_bytes, Allen::hostRegisterDefault );
    m_net_slices[i] = {nullptr, {contents, n_bytes}, 0u, MEP::Blocks{}, MEP::SourceOffsets{}, n_bytes};
    m_mpi_buffers.emplace_back( contents );
  }
  return StatusCode::SUCCESS;
#else
  error() << "MPI requested, but Allen was not built with MPI support." << endmsg;
  return StatusCode::FAILURE;
#endif
}

int MEPProvider::init_bm() {
  m_bmIDs.resize( n_buffers() );

  auto const partition   = m_allenConfig->partition.value();
  auto const partitionID = m_allenConfig->partitionID.value();

  m_buffer_event.resize( n_buffers() );
  m_buffer_event.assign( n_buffers(), false );

  std::vector<BMID> first( m_connections.size(), MBM_INV_DESC );

  for ( size_t b = 0; b < n_buffers(); ++b ) {
    auto buffer_name = m_connections[b % m_connections.size()];
    if ( m_allenConfig->partitionBuffers.value() ) {
      std::stringstream stream;
      stream << std::hex << partitionID;
      buffer_name += "_";
      buffer_name += partition.empty() ? stream.str() : partition;
    }

    auto pn = RTL::processName() + "." + std::to_string( b );
    BMID bmid;
    if ( b < m_connections.size() ) {
      bmid = ::mbm_include_read( buffer_name.c_str(), pn.c_str(), partitionID, static_cast<int>( m_mbm_com.value() ) );
      if ( bmid == MBM_INV_DESC ) {
        error() << "MBM: Failed to connect to MBM buffer " << buffer_name << endmsg;
        return MBM_ERROR;
      }

      // Keep track of the master connection BMID
      first[b % m_connections.size()] = bmid;

      // register buffer manager memory with the device runtime
      size_t      buffer_size    = 0;
      char const* buffer_address = mbm_buffer_address( bmid );
      mbm_buffer_size( bmid, &buffer_size );
      if ( m_registered_buffers.size() < m_connections.size() ) {
        try {
          Allen::host_register( const_cast<char*>( buffer_address ), buffer_size, Allen::hostRegisterReadOnly );
          debug() << "Successfully registered BM memory for buffer " << buffer_name << " with device runtime."
                  << endmsg;
        } catch ( const std::invalid_argument& e ) {
          error() << "Failed to register BM memory for buffer " << buffer_name << " with device runtime : " << e.what()
                  << endmsg;
          return MBM_ERROR;
        }
        m_registered_buffers.push_back( buffer_address );
      }
    } else {
      bmid = ::mbm_connect( first[b % m_connections.size()], pn.c_str(), partitionID );
    }

    using namespace std::chrono_literals;
    std::this_thread::sleep_for( 50ms );

    for ( auto r : m_requests ) {
      MBM::Requirement rq{r};
      int sc = ::mbm_add_req( bmid, rq.evtype, rq.trmask, rq.vetomask, rq.maskType, rq.userType, rq.freqType, rq.freq );
      if ( sc != MBM_NORMAL ) {
        error() << "MBM: Failed to add MBM requirement: " << r << endmsg;
        return MBM_ERROR;
      }
    }
    info() << "MEP Buffer " << b << " is included in MBM buffer " << buffer_name << endmsg;
    m_bmIDs[b] = bmid;
  }
  return MBM_NORMAL;
}

size_t MEPProvider::count_writable() const {
  return std::accumulate( m_buffer_status.begin(), m_buffer_status.end(), 0ul,
                          []( size_t s, BufferStatus const& stat ) { return s + stat.writable; } );
}

std::tuple<bool, bool> MEPProvider::allocate_storage( size_t i_read ) {
  if ( m_sizes_known ) return {true, false};

  // Count number of banks per flavour
  bool count_success = false;

  // Offsets are to the start of the event, which includes the header
  auto&       slice    = m_net_slices[i_read];
  auto const* mep      = slice.mep;
  size_t      n_blocks = mep->header.n_MFPs;

  m_packing_factor = slice.packing_factor;

  size_t const eps        = m_events_per_slice.value();
  auto         n_interval = m_packing_factor / eps;
  auto         rest       = m_packing_factor % eps;
  for ( auto& s : m_buffer_status ) { s.intervals.reserve( 2 * ( n_interval + rest ) ); }

  for ( auto& slice : m_net_slices ) {
    // The number of blocks in a MEP is known, use it to allocate
    // temporary storage used during transposition
    slice.blocks.resize( n_blocks );
    slice.offsets.resize( n_blocks );
    for ( auto& offsets : slice.offsets ) {
      offsets.resize( m_packing_factor + 1 );
      Allen::host_register( offsets.data(), offsets.capacity(), Allen::hostRegisterDefault );
    }
  }

  std::tie( count_success, m_mfp_count, m_banks_version ) = MEP::fill_counts( mep );
  if ( !count_success ) {
    ++m_alloc_tries;
    if ( m_alloc_tries < m_allocate_retries.value() ) {
      return {false, false};
    } else {
      error() << "Memory allocation failed" << endmsg;
      error() << "Failed to obtain good MEP with all MFPs valid after " << m_allocate_retries.value()
              << " tries; giving up." << endmsg;
      return {false, true};
    }
  }

  // Allocate histogram that keeps track of how many bad MFPs there
  // are per source ID. We know that the MEP we got has all MFPs
  // valid.
  std::vector<std::string> bin_labels;
  bin_labels.reserve( mep->header.n_MFPs );
  std::vector<EB::MFP const*> mfps( mep->header.n_MFPs );

  // Sort the MFPs by source ID
  for ( size_t i = 0; i < mep->header.n_MFPs; ++i ) { mfps[i] = mep->at( i ); }
  std::sort( mfps.begin(), mfps.end(),
             []( EB::MFP const* a, EB::MFP const* b ) { return a->header.src_id < b->header.src_id; } );

  // Loop over the sorted MFPs and fill both the mapping from source
  // ID to bin and the bin labels
  int n_bins = std::accumulate( mfps.begin(), mfps.end(), 0, [this, &bin_labels]( int s, EB::MFP const* mfp ) {
    auto const source_id = mfp->header.src_id;
    m_mfp_bin_mapping.emplace( source_id, s );
    auto const* sd = SourceId_sysstr( source_id );
    bin_labels.emplace_back( std::to_string( source_id ) + " (" + sd + " " +
                             std::to_string( SourceId_num( source_id ) ) + ")" );
    return s + 1;
  } );
  m_bad_mfps = std::make_unique<Histo1U>(
      this, "BadMFPs", "Bad MFPs per source ID",
      Gaudi::Accumulators::Axis<float>{
          static_cast<unsigned>( n_bins ), -0.5f, n_bins - 0.5f, {}, std::move( bin_labels )} );

  // Allocate slice memory that will contain transposed banks ready
  // for processing by the Allen kernels
  auto size_fun = [this, eps]( BankTypes bank_type ) -> std::tuple<size_t, size_t, size_t> {
    auto       it       = BankSizes.find( bank_type );
    auto const sd_index = to_integral( bank_type );

    auto aps      = eps < 100 ? 100 : eps;
    auto n_blocks = m_mfp_count[sd_index];
    auto n_sizes  = aps * ( ( n_blocks + 2 ) / 2 + 1 );

    if ( it == end( BankSizes ) ) {
      throw std::out_of_range{std::string{"Bank type "} + bank_name( bank_type ) + " has no known size"};
    }
    // In case of direct MEP output, no memory should be allocated.
    if ( !m_transpose_mep.value() ) {
      // 0 to not allocate fragment memory; -1 to correct for +1 in allocate_slices: re-evaluate
      return {0, n_sizes, 2 + n_blocks + ( 1 + eps ) * ( 1 + n_blocks ) - 2};
    } else {
      // Lookup LHCb bank type corresponding to Allen bank type

      // When events are transposed from the read buffer into
      // the per-rawbank-type slices, a check is made each time
      // to see if there is enough space available in a slice.
      // To avoid having to read every event twice to get the
      // size of all the banks.
      auto n_bytes = std::lround( ( ( 1 + m_mfp_count[sd_index] ) * sizeof( uint32_t ) + it->second * kB ) * aps *
                                      bank_size_fudge_factor +
                                  2 * MB ); // FIXME for the banks_count
      return {n_bytes, n_sizes, eps};
    }
  };
  m_slices = allocate_slices( m_nslices, m_bank_types, size_fun );

  m_slice_to_buffer = std::vector<std::tuple<int, size_t, size_t>>( m_nslices, std::tuple{-1, 0ul, 0ul} );

  m_sizes_known = true;
  return {true, false};
}

bool MEPProvider::open_file() const {
  if ( m_current == m_connections.end() ) {
    if ( m_non_stop.value() ) {
      m_current = m_connections.begin();
    } else {
      info() << "No more files to read." << endmsg;
      return false;
    }
  }

  if ( m_input && m_input->ioDesc >= 0 ) { m_input->close(); }

  auto filename = *m_current;
  ++m_current;
  m_input = LHCb::StreamDescriptor::bind( filename );
  if ( m_input->ioDesc < 0 ) {
    error() << "IO object for connection " << filename << " failed to open" << endmsg;
    return false;
  } else {
    info() << "Opened " << filename << endmsg;
    return true;
  }
}

std::tuple<std::vector<IInputProvider::BufferStatus>::iterator, size_t>
MEPProvider::get_mep_buffer( std::function<bool( IInputProvider::BufferStatus const& )> pred,
                             std::function<bool()> wait_pred, std::vector<IInputProvider::BufferStatus>::iterator start,
                             std::condition_variable& cond, std::unique_lock<std::mutex>& lock ) {
  // Obtain a prefetch buffer to read into, if none is available,
  // wait until one of the transpose threads is done with its
  // prefetch buffer
  auto find_buffer = [this, start, &pred] {
    auto it = std::find_if( start, m_buffer_status.end(), pred );
    if ( it == m_buffer_status.end() ) {
      it = std::find_if( m_buffer_status.begin(), start, pred );
      if ( it == start ) it = m_buffer_status.end();
    }
    return it;
  };

  auto it = find_buffer();
  if ( it == m_buffer_status.end() && !m_transpose_done ) {
    cond.wait( lock, [this, &it, &find_buffer, wait_pred] {
      it = find_buffer();
      return it != m_buffer_status.end() || m_read_error || wait_pred();
    } );
  }
  return {it, distance( m_buffer_status.begin(), it )};
}

bool MEPProvider::prepare_mep( size_t i_buffer, size_t n_events ) {
  auto& slice = m_net_slices[i_buffer];

  if ( slice.packing_factor > m_packing_factor ) {
    error() << "MEP encountered with packing factor larger than first MEP." << endmsg;
    read_error();
    return false;
  }

  // Find blocks
  std::function<void( size_t )> bad_mfp = [this]( size_t source_id ) {
    auto const sd_index = SourceId_sys( source_id );
    ++m_bad_mfps_sd[sd_index];
    auto it = m_mfp_bin_mapping.find( source_id );
    if ( it == m_mfp_bin_mapping.end() ) {
      warning() << "Failed to find bin in bad MFP histogram for source ID " << source_id << endmsg;
    } else {
      ++( *m_bad_mfps )[it->second];
    }
  };

  if ( !MEP::find_blocks( slice.mep, slice.blocks, bad_mfp ) ) { return false; }

  // Fill fragment offsets
  MEP::fragment_offsets( slice.blocks, slice.offsets );

  if ( n_events == 0 ) return true;

  std::unique_lock<std::mutex> lock{m_buffer_mutex};

  auto& status = m_buffer_status[i_buffer];
  assert( status.work_counter == 0 );

  auto& intervals = status.intervals;

  size_t const eps        = m_events_per_slice.value();
  auto         n_interval = n_events / eps;
  auto         rest       = n_events % eps;
  if ( rest ) {
    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "Set interval (rest): " << n_interval * eps << "," << n_interval * eps + rest << endmsg;
    }
    intervals.emplace_back( n_interval * eps, n_interval * eps + rest );
  }
  for ( size_t i = n_interval; i != 0; --i ) {
    if ( msgLevel( MSG::DEBUG ) ) { debug() << "Set interval: " << ( i - 1 ) * eps << "," << i * eps << endmsg; }
    intervals.emplace_back( ( i - 1 ) * eps, i * eps );
  }
  status.work_counter = intervals.size();

  return true;
}

void MEPProvider::read_error() {
  m_read_error = true;
  m_transpose_cond.notify_all();
  m_receive_cond.notify_all();
  m_transposed_cond.notify_one();
}

// mep reader thread
void MEPProvider::mep_read() {

  // Set thread name for easier debugging
  auto thread_name = std::string{"mep_read"};
  pthread_setname_np( pthread_self(), thread_name.c_str() );

  bool receive_done = false;

  size_t            preloaded = 0;
  std::vector<bool> preloaded_buffer( n_buffers(), false );
  bool const        preloading = m_preload.value();

  auto to_read = this->n_events();
  if ( to_read && msgLevel( MSG::DEBUG ) ) debug() << "Reading " << *to_read << " events" << endmsg;
  auto to_publish = 0;

  while ( !receive_done && !m_input_done && !m_read_error && ( !preloading || ( preloading && !m_stopping ) ) ) {
    // If we've been stopped, wait for start or exit
    if ( !m_started ) {
      std::unique_lock<std::mutex> lock{m_control_mutex};
      ++m_input_started;
      if ( m_input_started == m_ninput_threads ) { m_start_cond.notify_one(); }
      debug() << "mep_read waiting for start" << endmsg;
      m_control_cond.wait( lock, [this] { return m_started || m_stopping; } );
    }

    if ( m_input_done || ( preloading && m_stopping ) ) break;

    size_t i_buffer;
    {
      std::unique_lock<std::mutex> lock{m_buffer_mutex};
      std::tie( m_buffer_reading, i_buffer ) =
          get_mep_buffer( []( BufferStatus const& s ) { return s.writable; },
                          [this, preloading] { return m_input_done || ( preloading && m_stopping ); }, m_buffer_reading,
                          m_receive_cond, lock );
      if ( m_buffer_reading != m_buffer_status.end() ) {
        m_buffer_reading->writable = false;
        assert( m_buffer_reading->work_counter == 0 );
      } else {
        continue;
      }
    }

    if ( msgLevel( MSG::DEBUG ) ) debug() << "Writing to MEP slice index " << i_buffer << endmsg;

    auto& read_buffer = m_read_buffers[i_buffer];
    auto& slice       = m_net_slices[i_buffer];

    bool success = false, eof = false;

    while ( !success || eof ) {
      if ( !preloading || ( preloaded < n_buffers() && !preloaded_buffer[i_buffer] ) ) {
        if ( !m_input ) {
          error() << "No input for connection " << *( m_current - 1 ) << endmsg;
          read_error();
          break;
        }
        std::tie( eof, success, slice.mep, slice.packing_factor, slice.mep_data ) =
            MEP::read_mep( *m_input, read_buffer, info() );
        if ( !eof && success && msgLevel( MSG::DEBUG ) )
          debug() << "Read mep with packing factor " << slice.packing_factor << endmsg;

        if ( success && !eof && preloading ) {
#ifdef HAVE_MPI
          if ( m_buffer_numa.value().empty() ) {
            auto const numa_node = m_buffer_numa[i_buffer];
            auto const sc        = numa_membind( read_buffer.data(), read_buffer.capacity(), numa_node );
            if ( sc.isSuccess() ) {
              info() << "Bound preloaded MEP read buffer " << i_buffer << " memory to node " << numa_node << endmsg;
            } else {
              error() << "Failed to bind preloaded MEP read buffer " << i_buffer << " memory to node " << numa_node
                      << " " << strerror( errno ) << endmsg;
              read_error();
              break;
            }
          }
#endif
          // Register memory with CUDA
          try {
            Allen::host_register( read_buffer.data(), read_buffer.capacity(), Allen::hostRegisterDefault );
          } catch ( std::invalid_argument const& ) {
            error() << "Failed to register memory with the CUDA runtime." << endmsg;
            read_error();
            break;
          }
          preloaded_buffer[i_buffer] = true;
          ++preloaded;
        }
      } else {
        success = true;
        eof     = false;
        if ( msgLevel( MSG::DEBUG ) ) {
          debug() << "Using MEP already read into buffer " << i_buffer << "; preloaded " << preloaded << endmsg;
        }
      }

      if ( !eof && success ) {
        if ( to_read ) {
          to_publish = std::min( *to_read, size_t{slice.packing_factor} );
          *to_read -= to_publish;
        } else {
          to_publish = slice.packing_factor;
        }
      }

      if ( ( success && to_read && *to_read == 0 ) || ( eof && !open_file() ) ) {
        // Try to open the next file, if there is none, prefetching
        // is done.
        if ( !m_read_error && msgLevel( MSG::DEBUG ) ) { debug() << "Prefetch done" << endmsg; }
        receive_done = true;
        if ( preloading && preloaded < n_buffers() ) {
          error() << "Could not read sufficient MEPs for preloading" << endmsg;
          read_error();
        }
        break;
      }

      if ( !eof && !success ) {
        // Error encountered
        read_error();
        break;
      }
    }

    if ( m_read_error ) {
      break;
    } else if ( !m_sizes_known ) {
      auto [success, give_up] = allocate_storage( i_buffer );
      if ( !success ) {
        if ( !give_up ) {
          m_buffer_reading->writable = false;
          if ( preloading ) {
            preloaded_buffer[i_buffer] = false;
            --preloaded;
          }
          continue;
        } else {
          read_error();
          break;
        }
      }
    }

    // Notify a transpose thread that a new buffer of events is
    // ready. If prefetching is done, wake up all threads
    if ( !m_read_error ) {
      auto const n_events = ( to_read ? to_publish : size_t{slice.packing_factor} );

      if ( !eof && to_publish != 0 ) {
        // Set intervals for offset calculation/transposition
        auto has_odin = prepare_mep( i_buffer, n_events );
        if ( !has_odin ) {
          ++m_mep_without_odin;
          std::unique_lock<std::mutex> lock{m_buffer_mutex};
          m_buffer_status[i_buffer].writable = true;
          if ( preloading ) {
            preloaded_buffer[i_buffer] = false;
            --preloaded;
          }
          continue;
        } else {
          // Monitor this MEP
          ( *m_mepsInput ) += 1;
          ( *m_eventsInput ) += n_events;
          ( *m_mbInput ) += ( 2 * slice.mep->bytes() + 1 ) / ( 2 * 1024 * 1024 );
        }
      } else {
        // We didn't read anything, so free the buffer we got again
        std::unique_lock<std::mutex> lock{m_buffer_mutex};
        m_buffer_status[i_buffer].writable = true;
        if ( preloading ) {
          preloaded_buffer[i_buffer] = false;
          --preloaded;
        }
      }

      if ( receive_done ) {
        m_input_done = receive_done;
        if ( msgLevel( MSG::DEBUG ) ) debug() << "Prefetch notifying all" << endmsg;
        m_transpose_cond.notify_all();
      } else if ( !eof ) {
        if ( msgLevel( MSG::DEBUG ) ) debug() << "Prefetch notifying one" << endmsg;
        m_transpose_cond.notify_one();
      }
    }
  }
}

// MPI reader thread
void MEPProvider::mpi_read() {
#ifdef HAVE_MPI

  // Set thread name for easier debugging
  auto thread_name = std::string{"mpi_read"};
  pthread_setname_np( pthread_self(), thread_name.c_str() );

  std::vector<MPI_Request> requests( m_window_size );

  // Iterate over the slices
  size_t                                  reporting_period = 5;
  std::vector<std::tuple<size_t, size_t>> data_received( m_receivers.size() );
  std::vector<size_t>                     n_meps( m_receivers.size() );
  Timer                                   t;
  Timer                                   t_origin;
  bool                                    mpi_error = false;

  for ( size_t i = 0; i < m_receivers.size(); ++i ) {
    auto [mpi_rank, numa_domain] = m_domains[i];
    MPI_Recv( &n_meps[i], 1, MPI_SIZE_T, mpi_rank, MPI::message::number_of_meps, MPI_COMM_WORLD, MPI_STATUS_IGNORE );
  }
  size_t number_of_meps = std::accumulate( n_meps.begin(), n_meps.end(), 0u );

  size_t current_mep = 0;
  while ( !m_done && !m_read_error && ( m_non_stop.value() || current_mep < number_of_meps ) ) {
    // If we've been stopped, wait for start or exit
    if ( !m_started ) {
      std::unique_lock<std::mutex> lock{m_control_mutex};
      ++m_input_started;
      if ( m_input_started == m_ninput_threads ) { m_start_cond.notify_one(); }
      debug() << "mpi_read waiting for start" << endmsg;
      m_control_cond.wait( lock, [this] { return m_started || m_done; } );
    }

    if ( m_done ) break;

    // Obtain a prefetch buffer to read into, if none is available,
    // wait until one of the transpose threads is done with its
    // prefetch buffer
    size_t i_buffer;
    {
      std::unique_lock<std::mutex> lock{m_buffer_mutex};
      std::tie( m_buffer_reading, i_buffer ) =
          get_mep_buffer( []( BufferStatus const& s ) { return s.writable; }, [this] { return m_stopping; },
                          m_buffer_reading, m_receive_cond, lock );
      if ( m_buffer_reading != m_buffer_status.end() ) {
        m_buffer_reading->writable = false;
        assert( m_buffer_reading->work_counter == 0 );
      } else {
        continue;
      }
    }

    auto receiver                 = i_buffer % m_receivers.size();
    auto [sender_rank, numa_node] = m_domains[receiver];

    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "Receiving from rank " << sender_rank << " into buffer " << i_buffer << " NUMA domain " << numa_node
              << endmsg;
    }

    auto&  slice    = m_net_slices[i_buffer];
    char*& contents = m_mpi_buffers[i_buffer];

    size_t mep_size = 0;
    MPI_Recv( &mep_size, 1, MPI_SIZE_T, sender_rank, MPI::message::event_size, MPI_COMM_WORLD, MPI_STATUS_IGNORE );

    // Reallocate if needed
    if ( mep_size > slice.slice_size ) {
      slice.slice_size = mep_size * bank_size_fudge_factor;
      // Unregister memory
      Allen::host_unregister( contents );

      // Free memory
      MPI_Free_mem( contents );

      // Allocate new memory
      MPI_Alloc_mem( slice.slice_size, MPI_INFO_NULL, &contents );

      // Only bind explicitly if there are multiple receivers,
      // otherwise assume a memory allocation policy is in effect
      if ( m_domains.size() > 1 ) {
        // Bind memory to numa domain of receiving card
        auto numa_obj = hwloc_get_obj_by_type( m_topology, HWLOC_OBJ_NUMANODE, numa_node );
        auto s = hwloc_set_area_membind( m_topology, contents, slice.slice_size, numa_obj->nodeset, HWLOC_MEMBIND_BIND,
                                         HWLOC_MEMBIND_BYNODESET );
        if ( s != 0 ) {
          read_error();
          break;
        }
      }

      // Register memory with CUDA
      try {
        Allen::host_register( contents, slice.slice_size, Allen::hostRegisterDefault );
      } catch ( std::invalid_argument const& ) {
        read_error();
        break;
      }

      slice.mep_data = gsl::span{contents, static_cast<events_size>( slice.slice_size )};
    }

    // Number of full-size (MPI::mdf_chunk_size) messages
    int n_messages = mep_size / MPI::mdf_chunk_size;
    // Size of the last message (if the MFP size is not a multiple of MPI::mdf_chunk_size)
    int rest = mep_size - n_messages * MPI::mdf_chunk_size;
    // Number of parallel sends
    int n_sends = n_messages > m_window_size.value() ? m_window_size.value() : n_messages;

    // Initial parallel sends
    for ( int k = 0; k < n_sends; k++ ) {
      char* message = contents + k * MPI::mdf_chunk_size;
      MPI_Irecv( message, MPI::mdf_chunk_size, MPI_BYTE, sender_rank, MPI::message::event_send_tag_start + k,
                 MPI_COMM_WORLD, &requests[k] );
    }
    // Sliding window sends
    for ( int k = n_sends; k < n_messages; k++ ) {
      int r;
      MPI_Waitany( m_window_size, requests.data(), &r, MPI_STATUS_IGNORE );
      char* message = contents + k * MPI::mdf_chunk_size;
      MPI_Irecv( message, MPI::mdf_chunk_size, MPI_BYTE, sender_rank, MPI::message::event_send_tag_start + k,
                 MPI_COMM_WORLD, &requests[r] );
    }
    // Last send (if necessary)
    if ( rest ) {
      int r;
      MPI_Waitany( m_window_size, requests.data(), &r, MPI_STATUS_IGNORE );
      char* message = contents + n_messages * MPI::mdf_chunk_size;
      MPI_Irecv( message, rest, MPI_BYTE, sender_rank, MPI::message::event_send_tag_start + n_messages, MPI_COMM_WORLD,
                 &requests[r] );
    }
    // Wait until all chunks have been sent
    MPI_Waitall( n_sends, requests.data(), MPI_STATUSES_IGNORE );

    slice.mep      = reinterpret_cast<EB::MEP const*>( contents );
    slice.mep_data = gsl::span{contents, static_cast<events_size>( mep_size )};

    auto const* mep      = slice.mep;
    auto const* mfp      = mep->at( 0 );
    slice.packing_factor = mfp->header.n_banks;

    if ( !m_sizes_known ) auto [success, give_up] = allocate_storage( i_buffer );
    if ( !success ) {
      if ( !give_up ) {
        m_buffer_reading->writable = false;
        continue;
      } else {
        read_error();
        break;
      }
    }
  }

  auto& [meps_received, bytes_received] = data_received[receiver];
  bytes_received += mep_size;
  meps_received += 1;

  if ( t.get_elapsed_time() >= reporting_period ) {
    const auto seconds         = t.get_elapsed_time();
    auto       total_rate      = 0.;
    auto       total_bandwidth = 0.;
    for ( size_t i_rec = 0; i_rec < m_receivers.size(); ++i_rec ) {
      auto& [mr, br]            = data_received[i_rec];
      auto [rec_rank, rec_node] = m_domains[i_rec];

      const double rate      = (double)mr / seconds;
      const double bandwidth = ( (double)( br * 8 ) ) / ( 1024 * 1024 * 1024 * seconds );
      total_rate += rate;
      total_bandwidth += bandwidth;
      printf( "[%lf, %lf] Throughput: %lf MEP/s, %lf Gb/s; Domain %2i; Rank %2i\n", t_origin.get_elapsed_time(),
              seconds, rate, bandwidth, rec_node, rec_rank );

      br = 0;
      mr = 0;
    }
    if ( m_receivers.size() > 1 ) {
      printf( "[%lf, %lf] Throughput: %lf MEP/s, %lf Gb/s\n", t_origin.get_elapsed_time(), seconds, total_rate,
              total_bandwidth );
    }
    t.restart();
  }

  // Notify a transpose thread that a new buffer of events is
  // ready. If prefetching is done, wake up all threads
  if ( !mpi_error ) {
    auto has_odin = prepare_mep( i_buffer, size_t{slice.packing_factor} );
    if ( !has_odin ) {
      ++m_mep_without_odin;
      std::unique_lock<std::mutex> lock{m_buffer_mutex};
      m_buffer_status[i_buffer].writable = true;
      continue;
    } else {
      // Monitor this MEP
      ( *m_mepsInput ) += 1;
      ( *m_eventsInput ) += n_events;
      ( *m_mbInput ) += ( 2 * slice.mep->bytes() + 1 ) / ( 2 * 1024 * 1024 );

      if ( msgLevel( MSG::DEBUG ) ) debug() << "Prefetch notifying one" << endmsg;
      m_transpose_cond.notify_one();
    }
  }

  current_mep++;
}

if ( !m_done ) {
  m_done = true;
  if ( msgLevel( MSG::DEBUG ) ) debug() << "Prefetch notifying all" << endmsg;
  m_transpose_cond.notify_all();
}
#endif
}

// buffer manager reader thread
void MEPProvider::bm_read( const std::string& buffer_name ) {

  auto const partitionID = m_allenConfig->partitionID.value();
  auto       to_read     = this->n_events();
  if ( to_read )
    error() << "Number of events makes no sense when receiving from"
            << " the buffer manager: ignoring" << endmsg;

  size_t       buffer_index  = 0;
  size_t const n_con         = m_connections.size();
  auto         select_buffer = [&buffer_index, n_con]( BufferStatus const& s ) {
    return s.writable && ( ( s.index % n_con ) == buffer_index );
  };

  auto buffer_reading = m_buffer_status.begin();
  if ( m_thread_per_buffer.value() ) {
    auto it        = std::find( m_connections.begin(), m_connections.end(), buffer_name );
    buffer_index   = std::distance( m_connections.begin(), it );
    buffer_reading = m_buffer_status.begin() + buffer_index;
  }

  // Set thread name for easier debugging
  auto thread_name = std::string{"bm_read_"} + std::to_string( buffer_index );
  pthread_setname_np( pthread_self(), thread_name.c_str() );

  while ( !m_done && !m_stopping && !m_read_error ) {
    // If we've been stopped, wait for start or exit
    if ( !m_started ) {
      std::unique_lock<std::mutex> lock{m_control_mutex};
      ++m_input_started;
      debug() << "bm_read " << buffer_name << " Waiting for start" << endmsg;
      if ( m_input_started == m_ninput_threads ) { m_start_cond.notify_one(); }
      m_control_cond.wait( lock, [this] { return m_started || m_done || m_stopping; } );
    }

    if ( m_done || m_stopping ) break;

    size_t i_buffer = 0;
    {
      std::unique_lock<std::mutex> lock{m_buffer_mutex};
      std::tie( buffer_reading, i_buffer ) = get_mep_buffer(
          select_buffer, [this] { return m_stopping; }, buffer_reading, m_receive_cond, lock );
      if ( buffer_reading != m_buffer_status.end() ) {
        buffer_reading->writable = false;
        assert( buffer_reading->work_counter == 0 );
      } else {
        continue;
      }
    }
    if ( m_done || m_stopping ) {
      if ( buffer_reading != m_buffer_status.end() ) { buffer_reading->writable = true; }
      break;
    }

    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "Buffer " << buffer_name << " " << buffer_index << " writing to MEP slice index " << i_buffer
              << endmsg;
    }

    if ( !m_thread_per_buffer.value() ) { buffer_index = ( buffer_index + 1 ) % n_con; }

    auto& slice = m_net_slices[i_buffer];

    bool cancelled = false;

    unsigned int trmask[BM_MASK_SIZE];
    int          ev_type = 0, *ev_data = 0;
    long         ev_len = 0;

    if ( msgLevel( MSG::DEBUG ) ) debug() << "Waiting for MEP " << i_buffer << endmsg;

#ifndef NDEBUG
    {
      std::unique_lock<std::mutex> lock{m_mbm_mutex};
      assert( !m_buffer_event[i_buffer] );
    }
#endif
    auto sc = ::mbm_get_event( m_bmIDs[i_buffer], &ev_data, &ev_len, &ev_type, trmask, partitionID );
    if ( sc == MBM_NORMAL ) {
      auto const* mep = reinterpret_cast<EB::MEP const*>( ev_data );

      // Check incoming MEP
      if ( mep == nullptr || !mep->is_magic_valid() || mep->header.n_MFPs == 0 ) {
        if ( mep == nullptr )
          ++m_mep_null;
        else if ( !mep->is_magic_valid() )
          ++m_mep_bad_magic;
        else
          ++m_mep_empty;

        ::mbm_free_event( m_bmIDs[i_buffer] );
        buffer_reading->writable = true;

        continue;
      } else {
        std::unique_lock<std::mutex> lock{m_mbm_mutex};
        m_buffer_event[i_buffer] = true;
      }

      slice.mep            = mep;
      slice.mep_data       = {reinterpret_cast<char const*>( ev_data ), slice.mep->bytes()};
      slice.slice_size     = static_cast<size_t>( ev_len );
      auto const* mfp      = slice.mep->at( 0 );
      slice.packing_factor = mfp->header.n_banks;

      ( *m_mepsInput ) += 1;
      ( *m_eventsInput ) += slice.packing_factor;
      ( *m_mbInput ) += ( 2 * slice.mep->bytes() + 1 ) / ( 2 * 1024 * 1024 );

      if ( msgLevel( MSG::DEBUG ) ) debug() << "Got mep with packing factor " << slice.packing_factor << endmsg;
    } else if ( sc == MBM_REQ_CANCEL ) {
      std::unique_lock<std::mutex> lock{m_buffer_mutex};
      m_buffer_status[i_buffer].writable = true;
      cancelled                          = true;
      if ( msgLevel( MSG::DEBUG ) ) debug() << "Got cancel" << endmsg;
    }

    if ( !m_sizes_known && !cancelled ) {
      auto [success, give_up] = allocate_storage( i_buffer );
      if ( !success ) {
        if ( !give_up ) {
          ::mbm_free_event( m_bmIDs[i_buffer] );
          buffer_reading->writable = true;
          continue;
        } else {
          read_error();
          break;
        }
      } else {
        m_control_cond.notify_all();
      }
    }

    assert( cancelled || slice.packing_factor <= m_packing_factor );

    // Notify a transpose thread that a new buffer of events is
    // ready. If prefetching is done, wake up all threads
    if ( !cancelled ) {
      auto has_odin = prepare_mep( i_buffer, size_t{slice.packing_factor} );
      if ( !has_odin ) {
        ++m_mep_without_odin;
        std::unique_lock<std::mutex> lock{m_buffer_mutex};
        ::mbm_free_event( m_bmIDs[i_buffer] );
        m_buffer_status[i_buffer].writable = true;
        continue;
      } else {
        debug() << "Prefetch notifying transpose threads" << endmsg;
        m_transpose_cond.notify_all();
      }
    } else {
      break;
    }
  }
}

/**
 * @brief      Function to run in each thread transposing events
 *
 * @param      thread ID
 *
 * @return     void
 */
void MEPProvider::transpose( int thread_id ) {

  // Set thread name for easier debugging
  auto thread_name = std::string{"transpose_"} + std::to_string( thread_id );
  pthread_setname_np( pthread_self(), thread_name.c_str() );

  size_t                     i_buffer = 0;
  std::tuple<size_t, size_t> interval;
  std::optional<size_t>      slice_index = std::nullopt;

  bool   good = false, transpose_full = false;
  size_t n_transposed = 0;
  auto   transpose_it = m_transposed.end();

  auto has_intervals = [thread_id, n_input = m_ninput_threads]( BufferStatus const& s ) {
    return !s.intervals.empty() && ( ( s.index % n_input ) == ( thread_id % n_input ) );
  };

  auto no_intervals = [this] {
    return std::all_of( m_buffer_status.begin(), m_buffer_status.end(),
                        []( auto const& s ) { return s.intervals.empty(); } );
  };

  std::vector<BufferStatus>::iterator buffer_transpose = m_buffer_status.begin();
  size_t                              interval_start = 0u, interval_end = 0u;

  while ( !m_read_error && !m_transpose_done ) {
    // Get a buffer to read from
    {
      std::unique_lock lock{m_buffer_mutex};
      std::tie( buffer_transpose, i_buffer ) = get_mep_buffer(
          has_intervals, [this]() -> bool { return m_input_done || m_transpose_done; }, buffer_transpose,
          m_transpose_cond, lock );
      if ( m_transpose_done || m_read_error ) {
        break;
      } else if ( m_input_done && no_intervals() ) {
        m_transpose_done = true;
        break;
      } else if ( buffer_transpose == m_buffer_status.end() ) {
        continue;
      }
      auto& status = *buffer_transpose;
      assert( !status.intervals.empty() );

      interval = status.intervals.back();
      status.intervals.pop_back();
      std::tie( interval_start, interval_end ) = interval;

      if ( msgLevel( MSG::DEBUG ) ) {
        debug() << "Transpose " << thread_id << ": Got MEP slice index " << i_buffer << " interval [" << interval_start
                << "," << interval_end << ")" << endmsg;
      }
    }

    {
      std::unique_lock lock{m_transpose_mut};
      transpose_it = m_transposed.emplace( m_transposed.end(), *slice_index, std::nullopt );
    }

    // Get a slice to write to
    if ( !slice_index ) {
      if ( msgLevel( MSG::DEBUG ) ) debug() << "Transpose " << thread_id << ": Getting slice index" << endmsg;
      auto it = m_slice_free.end();
      {
        std::unique_lock<std::mutex> lock{m_slice_mut};
        it = find( m_slice_free.begin(), m_slice_free.end(), true );
        if ( it == m_slice_free.end() ) {
          if ( msgLevel( MSG::DEBUG ) ) debug() << "Transpose " << thread_id << ": Waiting for free slice" << endmsg;
          m_slice_cond.wait( lock, [this, &it] {
            it = std::find( m_slice_free.begin(), m_slice_free.end(), true );
            return it != m_slice_free.end() || m_done || m_stopping;
          } );
          // If transpose is done and there is no slice, we were
          // woken up be the desctructor before a slice was declared
          // free. In that case, exit without transposing
          if ( ( m_done || m_stopping ) && it == m_slice_free.end() ) {
            m_transposed.erase( transpose_it );
            break;
          }
        }
        *it         = false;
        slice_index = distance( m_slice_free.begin(), it );
        if ( msgLevel( MSG::DEBUG ) )
          debug() << "Transpose " << thread_id << ": Got slice index " << *slice_index << endmsg;
      }
      {
        std::unique_lock<std::mutex> lock{m_buffer_mutex};
        // Keep track of what buffer this slice belonged to
        m_slice_to_buffer[*slice_index] = {i_buffer, interval_start, interval_end};
      }
    }

    // Reset the slice
    auto& event_ids  = m_event_ids[*slice_index];
    auto& event_mask = m_event_masks[*slice_index];
    reset_slice( m_slices, *slice_index, m_bank_types, event_ids, !m_transpose_mep.value() );

    // MEP data
    auto& slice = m_net_slices[i_buffer];

    // Look for the ODIN block an ODIN bank that is not an error bank and decode the first one we find
    bool     have_odin = false, have_invalid_odin = false;
    unsigned run_number = 0, odin_event_errors = 0;

    auto odin_block = std::find_if( slice.blocks.begin(), slice.blocks.end(), []( MEP::Block const& block ) {
      return MEP::source_id_type( block.header->src_id ) == BankTypes::ODIN;
    } );

    if ( odin_block != slice.blocks.end() ) {
      auto const& odin_offsets = slice.offsets[std::distance( slice.blocks.begin(), odin_block )];
      auto const* odin_sizes   = odin_block->bank_sizes;

      for ( size_t i_event = interval_start; i_event < interval_end; ++i_event ) {
        auto const odin_type = static_cast<LHCb::RawBank::BankType>( odin_block->bank_types[i_event] );

        // First check if the bank type makes sense, i.e. not an error bank
        bool valid_odin = ( odin_type == LHCb::RawBank::ODIN );
        if ( valid_odin ) {
          auto const offset = odin_offsets[i_event];
          auto const size   = odin_sizes[i_event];
          auto       odin =
              MEP::decode_odin( odin_block->payload, offset, size, m_banks_version[to_integral( BankTypes::ODIN )] );

          // Then check if the event number makes sense
          valid_odin = ( odin.eventNumber() == odin_block->header->ev_id + i_event );
          if ( valid_odin && odin.runNumber() != 0 && !have_odin ) {
            have_odin             = true;
            m_odins[*slice_index] = odin.data;
            run_number            = odin.runNumber();
          } else if ( !valid_odin ) {
            ++odin_event_errors;
          }
        } else if ( odin_type >= LHCb::RawBank::LastType ) {
          have_invalid_odin = true;
        }
        event_mask[i_event - interval_start] = valid_odin;
      }
    }

    // Update the interval to "cut off" the part at the end of the
    // interval with consecutive ODIN errors. If ODINs with invalid
    // bank types have been detected, cut off an extra event.

    // Note that the event mask size is not guaranteed to be the same
    // as the interval size, because intervals may be smaller if the
    // number of events per size is not an integer divider of the
    // packing factor. Correct for that if necessary.
    auto   rbegin     = event_mask.rbegin() + ( event_mask.size() - ( interval_end - interval_start ) );
    auto   last_valid = std::find_if( rbegin, event_mask.rend(), []( auto e ) { return e != 0; } );
    size_t n_good     = std::distance( event_mask.begin(), last_valid.base() );
    if ( n_good > 0 ) n_good -= have_invalid_odin;
    std::get<1>( interval ) = interval_end = interval_start + n_good;

    if ( have_odin ) {
      size_t const mep_event_start = odin_block->header->ev_id;
      for ( size_t i_event = interval_start; i_event < interval_end; ++i_event ) {
        if ( event_mask[i_event - interval_start] ) {
          event_ids.emplace_back( run_number, mep_event_start + i_event );
        } else {
          event_ids.emplace_back( 0, 0 );
        }
      }
    } else {
      for ( size_t i_event = interval_start; i_event < interval_end; ++i_event ) { event_ids.emplace_back( 0, 0 ); }
    }

    // Transpose or calculate offsets
    if ( !have_odin ) {
      good           = true;
      transpose_full = false;
      n_transposed   = 0;
    } else if ( m_transpose_mep.value() ) {
      // Transpose the events into the slice
      std::tie( good, transpose_full, n_transposed ) =
          MEP::transpose_events( m_slices, *slice_index, m_bank_types, m_mfp_count, slice.blocks, slice.offsets,
                                 interval, m_mask_top5.value() );
      if ( msgLevel( MSG::DEBUG ) ) {
        debug() << "Transpose " << thread_id << ": Transposed slice " << *slice_index << "; good: " << good
                << "; full: " << transpose_full << "; n_transposed: " << n_transposed << endmsg;
      }
    } else {
      // Calculate fragment offsets in MEP per sub-detector
      std::tie( good, transpose_full, n_transposed ) =
          MEP::mep_offsets( m_slices, *slice_index, m_bank_types, m_mfp_count, slice.blocks, slice.offsets, interval,
                            m_mask_top5.value() );
      if ( msgLevel( MSG::DEBUG ) ) {
        debug() << "Transpose " << thread_id << ": Calculated MEP offsets for slice " << *slice_index
                << "; good: " << good << "; full: " << transpose_full << "; n_transposed: " << n_transposed << endmsg;
      }
    }

    if ( odin_block != slice.blocks.end() ) {
      m_odin_wrong_type += std::accumulate( event_mask.begin(), event_mask.end(), 0,
                                            []( size_t s, const char entry ) { return s + ( entry == 0 ); } ) -
                           odin_event_errors;
      m_odin_event_errors += odin_event_errors;
    }

    if ( m_read_error || !good ) {
      std::unique_lock<std::mutex> lock{m_buffer_mutex};
      m_read_error = true;
      m_transpose_cond.notify_one();
      break;
    }

    if ( have_odin ) {
      std::scoped_lock lock{m_transpose_mut, m_buffer_mutex};

      // Update transposed status
      transpose_it->slice_index  = *slice_index;
      transpose_it->n_transposed = n_transposed;

      // If not all events were transposed, some extra work is needed.
      if ( n_transposed != std::get<1>( interval ) - std::get<0>( interval ) ) {
        auto& status = m_buffer_status[i_buffer];
        status.work_counter += 1;
        status.intervals.emplace_back( std::get<0>( interval ) + n_transposed, std::get<1>( interval ) );
      }
      // Check if we're done
      else if ( m_input_done && no_intervals() && m_transposed.empty() ) {
        m_transpose_done = true;
      }
    } else {
      // Discard the slice if there is no valid ODIN
      {
        std::scoped_lock lock{m_transpose_mut, m_buffer_mutex};
        m_transposed.erase( transpose_it );
      }
      if ( msgLevel( MSG::DEBUG ) ) {
        debug() << "Transpose " << thread_id << ": No ODIN; discard slice " << *slice_index << endmsg;
      }
      ++m_slice_without_odin;
      slice_free( *slice_index );
    }

    // The slice is good, Notify any threads waiting in get_slice that a slice is available
    if ( m_transpose_done ) {
      m_transpose_cond.notify_all();
    } else if ( have_odin ) {
      m_transposed_cond.notify_one();
    }
    slice_index.reset();
  }
}

#ifdef HAVE_MPI
StatusCode MEPProvider::numa_membind( char const* mem, size_t size, int const numa_node ) const {
  auto numa_obj = hwloc_get_obj_by_type( m_topology, HWLOC_OBJ_NUMANODE, numa_node );
  auto s        = hwloc_set_area_membind( m_topology, mem, size, numa_obj->nodeset, HWLOC_MEMBIND_BIND,
                                   HWLOC_MEMBIND_BYNODESET | HWLOC_MEMBIND_STRICT | HWLOC_MEMBIND_MIGRATE );
  if ( s != 0 ) {
    error() << "Failed to bind memory to node " << numa_obj->os_index << " " << strerror( errno ) << endmsg;
    return StatusCode::FAILURE;
  } else {
    return StatusCode::SUCCESS;
  }
}
#else
StatusCode MEPProvider::numa_membind( char const*, size_t, int const ) const { return StatusCode::FAILURE; }
#endif
