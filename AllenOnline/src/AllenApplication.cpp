/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <dlfcn.h>

#include <chrono>
#include <cmath>
#include <fstream>
#include <iostream>
#include <regex>
#include <thread>
#include <unordered_set>

#include <filesystem>

#include <Gaudi/Property.h>
#include <GaudiKernel/AppReturnCode.h>
#include <GaudiKernel/IAppMgrUI.h>
#include <GaudiKernel/IMessageSvc.h>
#include <GaudiKernel/IMonitorSvc.h>
#include <GaudiKernel/IProperty.h>
#include <GaudiKernel/ISvcLocator.h>
#include <GaudiKernel/SmartIF.h>

#include <CPP/Event.h>
#include <RTL/rtl.h>
#include <RTL/strdef.h>
#include <dim/dis.hxx>

#include <GaudiOnline/OnlineApplication.h>

#include <Allen/Allen.h>
#include <Allen/BankMapping.h>
#include <Allen/BankTypes.h>
#include <Allen/Provider.h>
#include <Allen/TCK.h>

#include "MEPProvider.h"

#ifdef HAVE_MPI
#  include <AllenOnline/MPIConfig.h>
#endif

#include "AllenApplication.h"
#include "AllenConfiguration.h"

// #include "EBProvider.h"

namespace {
  using namespace std::string_literals;
  namespace fs = std::filesystem;
} // namespace

/// Factory instantiation
DECLARE_COMPONENT( AllenApplication )

/// Specialized constructor
AllenApplication::AllenApplication( Options opts ) : OnlineApplication( opts ) {}

// Default destructor
AllenApplication::~AllenApplication() {
  // if (m_handle) {
  //   dlclose(m_handle);
  // }
}

/// Stop the application                             (RUNNING    -> READY)
int AllenApplication::stop() {
  m_zmqSvc->send( *m_allenControl, "STOP", zmq::send_flags::sndmore );
  m_zmqSvc->send( *m_allenControl, m_allenConfig->stopTimeout.value() );

  // This will stop input
  fireIncident( "DAQ_STOP" );

  StatusCode sc = app->stop();
  if ( !sc.isSuccess() ) {
    m_logger->error( "%s Class %d application: Failed to execute the "
                     "transition to state %s",
                     "app->stop()", 1, stateName( ST_READY ) );
    fireIncident( "DAQ_ERROR" );
    return declareState( ST_ERROR );
  }

  zmq::pollitem_t items[] = {{*m_allenControl, 0, zmq::POLLIN, 0}};
  m_zmqSvc->poll( &items[0], 1, -1 );
  if ( items[0].revents & zmq::POLLIN ) {
    auto msg = m_zmqSvc->receive<std::string>( *m_allenControl );
    if ( msg == "READY" ) {
      m_logger->info( "Allen event loop is stopped" );
    } else {
      m_logger->error( "Allen event loop failed to stop" );
      return Online::ONLINE_ERROR;
    }
  }

  m_provider->release_buffers();

  if ( m_runNumber != 0 ) {
    m_monSvc->update( m_runNumber ).ignore();
    m_monSvc->resetHistos( nullptr );
    m_runNumber = 0;
  }

  fireIncident( "DAQ_STOPPED" );
  fireIncident( "APP_STOPPED" );
  return declareState( ST_READY );
}

/// Cancel the application: Cancel IO request/Event loop
int AllenApplication::cancel() { return Online::ONLINE_OK; }

/// Internal: Initialize the application            (NOT_READY  -> READY)
int AllenApplication::configureApplication() {
  int ret = OnlineApplication::configureApplication();
  if ( ret != Online::ONLINE_OK ) return ret;

  // dlopen libAllenLib
  // m_handle = dlopen("libAllenLib.so", RTLD_LAZY);
  // if (!m_handle) {
  //   m_logger->error("Failed to dlopen libAllenLib");
  //   return Online::ONLINE_ERROR;
  // }

  // // reset errors
  // dlerror();
  // // load the symbol
  // m_allenFun = (allen_t) dlsym(m_handle, "allen");
  // const char* dlsym_error = dlerror();
  // if (dlsym_error) {
  //   m_logger->error("Failed to get 'allen' from libAllenLib");
  //   dlclose(m_handle);
  //   return Online::ONLINE_ERROR;
  // }

  SmartIF<ISvcLocator> sloc = app.as<ISvcLocator>();

  m_allenConfig = sloc->service( "AllenConfiguration/AllenConfiguration" ).as<AllenConfiguration>();
  if ( !m_allenConfig ) {
    m_logger->throwError( "Failed to retrieve AllenConfiguration." );
    return Online::ONLINE_ERROR;
  }

  m_zmqSvc = sloc->service<IZeroMQSvc>( "ZeroMQSvc" );
  if ( !m_zmqSvc ) {
    m_logger->error( "Failed to retrieve IZeroMQSvc." );
    return Online::ONLINE_ERROR;
  }

  SmartIF<IService> updater = sloc->service<IService>( "AllenUpdater" );
  if ( !updater.get() ) {
    m_logger->error( "Failed to retrieve AllenUpdater." );
    return Online::ONLINE_ERROR;
  }
  m_updater = dynamic_cast<Allen::NonEventData::IUpdater*>( updater.get() );
  if ( m_updater == nullptr ) {
    m_logger->error( "Failed to cast AllenUpdater" );
    return Online::ONLINE_ERROR;
  }

  SmartIF<IService> mepProvider = sloc->service<IService>( "MEPProvider" );
  if ( !mepProvider.get() ) {
    m_logger->error( "Failed to retrieve MEPProvider." );
    return Online::ONLINE_ERROR;
  }

  std::string value;
  auto        sc = mepProvider.as<IProperty>()->getProperty( "NSlices", value );
  if ( !sc.isSuccess() ) {
    m_logger->error( "Failed to get NSlices property from MEPProvider" );
    return Online::ONLINE_ERROR;
  } else {
    using Gaudi::Parsers::parse;
    sc = parse( m_nSlices, value );
    if ( !sc.isSuccess() ) {
      m_logger->error( "Failed to parse NSlices property" );
      return Online::ONLINE_ERROR;
    }
  }

  if ( !m_config->monitorType.empty() ) {
    m_monSvc = sloc->service<IGauchoMonitorSvc>( m_config->monitorType );
    if ( !m_monSvc ) {
      m_logger->error( "Cannot access monitoring service of type %s.", m_config->monitorType.c_str() );
      return Online::ONLINE_ERROR;
    }
  }

  m_provider = dynamic_cast<IInputProvider*>( mepProvider.get() );
  if ( m_provider == nullptr ) {
    m_logger->error( "Failed to cast MEPProvider" );
    return Online::ONLINE_ERROR;
  }

  auto const& [sequence, source] = m_allenConfig->sequence();
  if ( sequence.empty() ) {
    m_logger->error( "Failed to obtain sequence" );
    return Online::ONLINE_ERROR;
  }

  m_options = {{"v", std::to_string( 6 - m_config->outputLevel() )},
               {"t", m_allenConfig->nThreads.toString()},
               {"m", m_allenConfig->memory.toString()},
               {"host-memory", m_allenConfig->hostMemory.toString()},
               {"params", m_allenConfig->paramDir},
               {"device", m_allenConfig->device.value()},
               {"s", std::to_string( m_nSlices )},
               {"tck-from-odin", std::to_string( m_allenConfig->tckFromODIN.value() )},
               {"disable-run-changes", std::to_string( !m_allenConfig->runChanges.value() )},
               {"monitoring-filename", ""}};

  m_output = makeOutput();
  if ( !m_allenConfig->output.value().empty() && !m_output ) {
    m_logger->error( "Failed to create output for "s + m_allenConfig->output.value() );
    return Online::ONLINE_ERROR;
  }

  m_allenControl = m_zmqSvc->socket( zmq::PAIR );
  m_allenControl->bind( m_controlConnection.c_str() );

  m_allenThread = std::thread{&AllenApplication::allenLoop, this, sequence, source};

  zmq::pollitem_t items[] = {{*m_allenControl, 0, zmq::POLLIN, 0}};
  m_zmqSvc->poll( &items[0], 1, -1 );
  if ( items[0].revents & zmq::POLLIN ) {
    auto msg = m_zmqSvc->receive<std::string>( *m_allenControl );
    if ( msg == "READY" ) { m_logger->info( "Allen event loop is ready" ); }
  }

  return ret;
}

/// Internal: Finalize the application              (READY      -> NOT_READY)
int AllenApplication::finalizeApplication() {
  m_zmqSvc->send( *m_allenControl, "RESET" );

  zmq::pollitem_t items[] = {{*m_allenControl, 0, zmq::POLLIN, 0}};
  m_zmqSvc->poll( &items[0], 1, -1 );
  if ( items[0].revents & zmq::POLLIN ) {
    auto msg = m_zmqSvc->receive<std::string>( *m_allenControl );
    if ( msg == "NOT_READY" ) {
      m_logger->info( "Allen event loop has exited" );

      m_allenThread.join();
    } else {
      m_logger->error( "Allen event loop failed to exit" );
      return Online::ONLINE_ERROR;
    }
  }

  return OnlineApplication::finalizeApplication();
}

/// Internal: Start the application                 (READY      -> RUNNING)
int AllenApplication::startApplication() {
  StatusCode sc = app->start();
  if ( !sc.isSuccess() ) { return Online::ONLINE_ERROR; }

  m_zmqSvc->send( *m_allenControl, "START" );

  zmq::pollitem_t items[] = {{*m_allenControl, 0, zmq::POLLIN, 0}};
  m_zmqSvc->poll( &items[0], 1, -1 );
  if ( items[0].revents & zmq::POLLIN ) {
    auto msg = m_zmqSvc->receive<std::string>( *m_allenControl );
    if ( msg == "RUNNING" ) {
      m_logger->info( "Allen event loop is running" );
    } else {
      m_logger->error( "Allen event loop returned error" );
      return Online::ONLINE_ERROR;
    }
  }

  fireIncident( "DAQ_RUNNING" );
  fireIncident( "APP_RUNNING" );
  return Online::ONLINE_OK;
}

/// Pause the application                            (RUNNING    -> READY)
int AllenApplication::pauseProcessing() {
  m_logger->debug( "Pause the application." );
  return OnlineApplication::pauseProcessing();
}

/// Continue the application                        (PAUSED -> RUNNING )
int AllenApplication::continueProcessing() {
  m_logger->debug( "Resume application processing." );
  return OnlineApplication::continueProcessing();
}

void AllenApplication::allenLoop( std::string_view config, std::string_view config_source ) {
  auto status =
      allen( m_options, config, config_source, this, m_provider, m_output, m_zmqSvc.get(), m_controlConnection );
  if ( status != 0 ) {
    m_logger->error( "Allen event loop exited with error" );
    error();
  }
}

void AllenApplication::update( gsl::span<unsigned const> odin_data ) {
  LHCb::ODIN odin{odin_data};
  auto const run = odin.runNumber();

  // Monitoring run change
  if ( m_runNumber == 0 ) {
    m_runNumber = run;
    m_monSvc->setRunNo( run );
  } else if ( run > m_runNumber ) {
    m_monSvc->update( m_runNumber ).ignore();
    m_monSvc->resetHistos( nullptr );
    m_monSvc->setRunNo( run );
    m_runNumber = run;
  }

  // Update conditions by forwarding to the real updater
  m_updater->update( odin_data );
}

void AllenApplication::registerConsumer( std::string const& id, std::unique_ptr<Allen::NonEventData::Consumer> c ) {
  m_updater->registerConsumer( id, std::move( c ) );
}

void AllenApplication::registerProducer( std::string const& id, Allen::NonEventData::Producer p ) {
  m_updater->registerProducer( id, std::move( p ) );
}

OutputHandler* AllenApplication::makeOutput() {
  auto output = m_allenConfig->output.value();
  if ( output.empty() ) {
    m_logger->warning( "No output is configured, selected events will be lost" );
    return {};
  }

  std::string connection;
  auto        p = output.find( "://" );
  std::string output_type;

  if ( !output.empty() && p == std::string::npos ) {
    output_type = "file";
    connection  = output;
  } else if ( !output.empty() ) {
    output_type = output.substr( 0, p );
    connection  = output;
  }

  if ( output_type == "file" || output_type == "tcp" ) {
    auto options           = m_options;
    options["output-file"] = connection;
    m_outputHolder         = Allen::output_handler( m_provider, m_zmqSvc.get(), std::move( options ) );
    return m_outputHolder.get();
  } else if ( output_type == "mbm" ) {
    SmartIF<ISvcLocator> sloc      = app.as<ISvcLocator>();
    auto                 outputSvc = sloc->service( connection.substr( p + 3, std::string::npos ), false );

    if ( !outputSvc ) {
      m_logger->error( "Failed to retrieve MBMOutput." );
      return nullptr;
    }

    auto* mbmOutput = dynamic_cast<OutputHandler*>( outputSvc.get() );
    if ( mbmOutput == nullptr ) { m_logger->error( "Failed to cast MBMOutput to OutputHandler" ); }
    return mbmOutput;
  } else {
    m_logger->error( "Unknown output type: "s + output_type );
    return nullptr;
  }
}
