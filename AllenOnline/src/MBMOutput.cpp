/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/algorithm/string/split.hpp>

#include <Gaudi/Accumulators.h>
#include <GaudiKernel/Service.h>

#include <MDF/MDFHeader.h>

#include <MBM/bmdef.h>
#include <RTL/Logger.h>

#include "AllenConfiguration.h"
#include <Allen/OutputHandler.h>
#include <AllenOnline/TransposeMEP.h>

namespace Allen {

  class MBMOutput final : public Service, public OutputHandler {
  public:
    MBMOutput( std::string name, ISvcLocator* loc );

    /// Callback when space is availible (not really used)
    static int spaceCallback( void* /* param */ );

    StatusCode initialize() override;

    StatusCode start() override;

  protected:
    gsl::span<char> buffer( size_t thread_id, size_t buffer_size, size_t n_events ) override;

    bool write_buffer( size_t thread_id ) override;

    void output_done() override;

    void cancel() override;

  private:
    struct MBMSlot {
      BMID            id = MBM_INV_DESC;
      gsl::span<char> buffer;
      size_t          n_events = 0;
    };

    SmartIF<AllenConfiguration> m_allenConfig;

    // Output buffer name
    Gaudi::Property<std::string> m_bufferName{this, "BufferName", "Output"};

    // Do output checksum
    Gaudi::Property<bool> m_checksum{this, "Checksum", false};

    // How many events to declare to the buffer manager in one batch
    Gaudi::Property<size_t> m_outputBatchSize{this, "OutputBatchSize", 100};

    // Number of output threads
    Gaudi::Property<size_t> m_nThreads{this, "NThreads", 2};

    // buffer manager communication method
    Gaudi::Property<MEP::MBMCom> m_mbm_com{this, "MBMComMethod", MEP::MBMCom::FIFO};

    // Process name
    std::string m_processName;

    // partition ID
    unsigned m_partitionID = 0;

    // MBM buffer
    std::vector<MBMSlot> m_buffers;

    bool m_cancelled = false;

    std::unique_ptr<Gaudi::Accumulators::Counter<>> m_burstsOutput;
    std::unique_ptr<Gaudi::Accumulators::Counter<>> m_eventsOutput;
    std::unique_ptr<Gaudi::Accumulators::Counter<>> m_mbOutput;
  };

} // namespace Allen

DECLARE_COMPONENT( Allen::MBMOutput )

Allen::MBMOutput::MBMOutput( std::string name, ISvcLocator* loc ) : Service{name, loc} {}

int Allen::MBMOutput::spaceCallback( void* /* param */ ) { return MBM_NORMAL; }

gsl::span<char> Allen::MBMOutput::buffer( size_t thread_id, size_t buffer_size, size_t n_events ) {
  if ( m_cancelled ) { return {}; }

  auto& slot    = m_buffers[thread_id];
  slot.n_events = n_events;

  int* buf = nullptr;
  auto sc  = ::mbm_get_space_a( slot.id, buffer_size, &buf, spaceCallback, this );
  if ( sc == MBM_NORMAL ) {
    sc = ::mbm_wait_space( slot.id );
    if ( sc == MBM_NORMAL ) {
      slot.buffer = {reinterpret_cast<char*>( buf ), static_cast<events_size>( buffer_size )};
      return slot.buffer;
    }
  } else {
    error() << m_processName << " failed to get space of size " << buffer_size << " in buffer " << m_bufferName.value()
            << endmsg;
  }
  return {};
}

StatusCode Allen::MBMOutput::initialize() {
  auto sc = Service::initialize();
  if ( !sc ) return sc;

  m_allenConfig = serviceLocator()->service( "AllenConfiguration/AllenConfiguration" ).as<AllenConfiguration>();
  if ( !m_allenConfig ) {
    error() << "Failed to retrieve AllenConfiguration." << endmsg;
    return StatusCode::FAILURE;
  }

  SmartIF<IService> mepProvider = serviceLocator()->service<IService>( "MEPProvider" );
  if ( !mepProvider.get() ) {
    error() << "Failed to retrieve MEPProvider." << endmsg;
    return StatusCode::FAILURE;
  }
  auto* inputProvider = dynamic_cast<IInputProvider*>( mepProvider.get() );
  if ( inputProvider == nullptr ) {
    error() << "Failed to cast MEPProvider" << endmsg;
    return StatusCode::FAILURE;
  }

  m_processName = RTL::processName();

  m_partitionID                = m_allenConfig->partitionID.value();
  std::string connection       = m_bufferName.value();
  auto const  partition        = m_allenConfig->partition.value();
  bool        partitionBuffers = m_allenConfig->partitionBuffers.value();
  if ( partitionBuffers ) {
    connection += "_";
    std::stringstream hexID;
    hexID << std::hex << m_partitionID;
    connection += partition.empty() ? hexID.str() : partition;
  }

  info() << "Connect with " << m_nThreads.value() << " threads to MBM buffer: " << connection << endmsg;

  auto eventsSvc = dynamic_cast<Service*>( service<IService>( "AllenIOMon/Events", true ).get() );
  if ( !eventsSvc ) {
    error() << "Failed to obtain Events service for monitoring" << endmsg;
    return StatusCode::FAILURE;
  }

  auto burstsSvc = dynamic_cast<Service*>( service<IService>( "AllenIOMon/Bursts", true ).get() );
  if ( !burstsSvc ) {
    error() << "Failed to obtain Bursts service for monitoring" << endmsg;
    return StatusCode::FAILURE;
  }

  m_burstsOutput = std::make_unique<Gaudi::Accumulators::Counter<>>( burstsSvc, "OUT" );
  m_eventsOutput = std::make_unique<Gaudi::Accumulators::Counter<>>( eventsSvc, "OUT" );
  m_mbOutput     = std::make_unique<Gaudi::Accumulators::Counter<>>( eventsSvc, "MB_OUT" );

  m_buffers.reserve( m_nThreads.value() );

  init( inputProvider, connection, m_nThreads.value(), m_outputBatchSize.value(), m_checksum.value() );
  return StatusCode::SUCCESS;
}

StatusCode Allen::MBMOutput::start() {
  auto sc = Service::start();
  if ( !sc ) return sc;

  m_cancelled = false;

  for ( size_t id = 0; id < m_nThreads.value(); ++id ) {
    BMID bmID;
    if ( id == 0 ) {
      bmID = ::mbm_include_write( connection().c_str(), m_processName.c_str(), m_partitionID,
                                  static_cast<int>( m_mbm_com.value() ) );
    } else {
      bmID = mbm_connect( m_buffers[0].id, m_processName.c_str(), m_partitionID );
    }
    m_buffers.emplace_back( MBMSlot{bmID, gsl::span<char>{}, 0u} );

    if ( bmID == MBM_INV_DESC ) {
      error() << "MBMOutput: failed to connect to MBM buffer for thread " << id << " " << connection() << endmsg;
      return StatusCode::FAILURE;
    } else {
      info() << "MBMOutput: thread " << id << " connected to MBM buffer " << connection() << endmsg;
    }
  }
  return sc;
}

bool Allen::MBMOutput::write_buffer( size_t thread_id ) {
  if ( m_cancelled ) { return false; }

  auto const& slot = m_buffers[thread_id];

  ( *m_burstsOutput ) += 1;
  ( *m_eventsOutput ) += slot.n_events;
  ( *m_mbOutput ) += ( 2 * slot.buffer.size_bytes() + 1 ) / ( 2 * 1024 * 1024 );

  unsigned int mask[]       = {m_partitionID, ~0x0U, ~0x0U, ~0x0U};
  void*        free_address = nullptr;
  long         free_len     = 0;

  auto sc = ::mbm_declare_event( slot.id, slot.buffer.size_bytes(), EVENT_TYPE_EVENT, mask, 0, &free_address, &free_len,
                                 m_partitionID );
  if ( sc == MBM_REQ_CANCEL ) {
    return false;
  } else if ( sc == MBM_NORMAL ) {
    sc = ::mbm_send_space( slot.id );
    if ( sc == MBM_REQ_CANCEL ) {
      return false;
    } else {
      return true;
    }
  } else {
    error() << "MBMOutput: failed to write buffer " << sc << endmsg;
    return false;
  }
}

void Allen::MBMOutput::cancel() {
  if ( m_cancelled ) return;
  for ( auto const& slot : m_buffers ) {
    if ( slot.id != MBM_INV_DESC ) { ::mbm_cancel_request( slot.id ); }
  }
  m_cancelled = true;
}

void Allen::MBMOutput::output_done() {
  m_cancelled = false;
  for ( auto const& slot : m_buffers ) {
    if ( slot.id != MBM_INV_DESC ) { ::mbm_exclude( slot.id ); }
  }
  m_buffers.clear();
}
