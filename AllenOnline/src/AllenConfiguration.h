/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <filesystem>

#include <Allen/Provider.h>
#include <GaudiKernel/Service.h>

class AllenConfiguration : public Service {
public:
  /// Retrieve interface ID
  static const InterfaceID& interfaceID() {
    // Declaration of the interface ID.
    static const InterfaceID iid( "AllenConfiguration", 0, 0 );
    return iid;
  }

  /// Query interfaces of Interface
  StatusCode queryInterface( const InterfaceID& riid, void** ppv ) override;
  AllenConfiguration( std::string name, ISvcLocator* svcloc );

  virtual ~AllenConfiguration();

  std::tuple<std::string, std::string> sequence() const;

  Gaudi::Property<float>        stopTimeout{this, "StopTimeout", 5.};
  Gaudi::Property<unsigned int> nThreads{this, "NThreads", 8};
  Gaudi::Property<std::string>  output{this, "Output", ""};
  Gaudi::Property<bool>         runChanges{this, "EnableRunChanges", true};
  Gaudi::Property<std::string>  device{this, "Device", "0"};
  Gaudi::Property<std::string>  paramDir{this, "ParamDir", "${PARAMFILESROOT}", [this]( auto& ) -> void {
                                          auto const            dir = resolveEnvVars( paramDir.value() );
                                          std::filesystem::path p{dir};
                                          if ( !std::filesystem::exists( p ) || !std::filesystem::is_directory( p ) ) {
                                            throw GaudiException{
                                                "Allen parameter directory file does not exist or is not a directory",
                                                name(), StatusCode::FAILURE};
                                          }
                                          paramDir.set( dir );
                                        }};
  Gaudi::Property<unsigned>     memory{this, "Memory", 1000};
  Gaudi::Property<unsigned>     hostMemory{this, "HostMemory", 200};
  Gaudi::Property<unsigned>     partitionID{this, "PartitionID", 0};
  Gaudi::Property<bool>         partitionBuffers{this, "PartitionBuffers", true};
  Gaudi::Property<std::string>  partition{this, "Partition", ""};
  Gaudi::Property<bool>         tckFromODIN{this, "TCKFromODIN", true};

private:
  static std::string resolveEnvVars( std::string s ) {
    std::regex  envExpr{"\\$\\{([A-Za-z0-9_]+)\\}"};
    std::smatch m;
    while ( std::regex_search( s, m, envExpr ) ) {
      std::string rep;
      System::getEnv( m[1].str(), rep );
      s = s.replace( m[1].first - 2, m[1].second + 1, rep );
    }
    return s;
  }
  Gaudi::Property<std::string> m_json{
      this, "JSON", "{}", [this]( auto& ) -> void {
        if ( !m_json.value().empty() && m_json.value()[0] == '{' ) {
          m_sequence = m_json.value();
          return;
        }

        auto const json_path       = resolveEnvVars( m_json.value() );
        auto [from_tck, repo, tck] = Allen::config_from_tck( json_path );
        if ( from_tck ) {
          std::filesystem::path j{repo};
          m_tck = tck;
          if ( !std::filesystem::exists( j ) || !std::filesystem::is_directory( j ) ) {
            throw GaudiException{"Git repository " + json_path + " does not exist or is not a directory", name(),
                                 StatusCode::FAILURE};
          }
          m_json.set( j.string() );
        } else {
          std::filesystem::path j{json_path};
          if ( !std::filesystem::exists( j ) || !std::filesystem::is_regular_file( j ) ) {
            throw GaudiException{"JSON file " + json_path + " does not exist or is not a directory", name(),
                                 StatusCode::FAILURE};
          }
          m_json.set( json_path );
        }
      }};

  std::string         m_tck;
  mutable std::string m_sequence;
};
