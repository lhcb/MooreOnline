/*****************************************************************************\
* (c) Copyright 2018-2020 CERN for the benefit of the LHCb Collaboration      *
\*****************************************************************************/
#include <cassert>
#include <cstring>
#include <sstream>

#include <Allen/sourceid.h>
#include <Event/ODIN.h>

#include <AllenOnline/TransposeMEP.h>

namespace {
  template <typename T>
  T* event_entries( gsl::span<unsigned int> offsets, unsigned const event ) {
    return reinterpret_cast<T*>( &offsets[0] ) + offsets[event];
  }
} // namespace

BankTypes MEP::source_id_type( uint16_t src_id ) {
  auto const sd = SourceId_sys( src_id );
  auto       it = Allen::subdetectors.find( static_cast<SourceIdSys>( sd ) );
  return it == Allen::subdetectors.end() ? BankTypes::Unknown : it->second;
}

std::tuple<bool, std::array<unsigned int, NBankTypes>, std::array<int, NBankTypes>>
MEP::fill_counts( EB::MEP const* mep ) {

  bool                             all_valid = true;
  bool                             has_odin  = false;
  std::array<unsigned, NBankTypes> count{0};
  std::array<int, NBankTypes>      versions;

  // initialize bank version, needed for banks of subdetectors not present in input data
  std::fill( versions.begin(), versions.end(), -1 );

  for ( size_t i = 0; i < mep->header.n_MFPs; ++i ) {
    auto const* mfp        = mep->at( i );
    auto const  allen_type = source_id_type( mep->header.src_ids()[i] );
    if ( allen_type != BankTypes::Unknown ) {
      auto const sd_index = to_integral( allen_type );
      if ( mfp->is_header_valid() ) {
        if ( SourceId_sys( mfp->header.src_id ) == SourceIdSys::SourceIdSys_ODIN ) has_odin = true;
        versions[sd_index] = mfp->header.block_version;
        ++count[sd_index];
      } else {
        all_valid = false;
      }
    }
  }

  return {all_valid && has_odin, count, versions};
}

LHCb::ODIN MEP::decode_odin( char const* odin_data, unsigned const offset, unsigned const size_bytes,
                             const uint8_t version ) {
  LHCb::span<std::uint32_t const> odin_span{reinterpret_cast<std::uint32_t const*>( odin_data + offset ),
                                            size_bytes / sizeof( uint32_t )};
  if ( version == 6 ) {
    return LHCb::ODIN::from_version<6>( odin_span );
  } else {
    return LHCb::ODIN{odin_span};
  }
}

std::optional<size_t> MEP::find_blocks( EB::MEP const* mep, Blocks& blocks,
                                        std::function<void( size_t )> const& bad_mfp ) {
  // Fill blocks in temporary container
  std::optional<size_t> odin_block_index;
  size_t                n_good = 0;
  blocks.resize( mep->header.n_MFPs );

  for ( size_t i_block = 0; i_block < blocks.size(); ++i_block ) {
    EB::MFP const* mfp = mep->at( i_block );
    if ( mfp->is_header_valid() ) {
      blocks[n_good] = Block{mfp};
      if ( SourceId_sys( mfp->header.src_id ) == SourceIdSys::SourceIdSys_ODIN ) odin_block_index = n_good;
      ++n_good;
    } else {
      bad_mfp( mep->header.src_ids()[i_block] );
    }
  }

  blocks.resize( n_good );
  std::sort( blocks.begin(), blocks.end(),
             []( Block const& a, Block const& b ) { return a.header->src_id < b.header->src_id; } );

#ifndef NDEBUG
  size_t total_block_size = std::accumulate( blocks.begin(), blocks.end(), 0u,
                                             []( size_t s, const MEP::Block& b ) { return s + b.header->bytes(); } );
  assert( total_block_size <= mep->bytes() );
#endif
  return odin_block_index;
}

void MEP::fragment_offsets( MEP::Blocks const& blocks, MEP::SourceOffsets& offsets ) {

  // Reset input offsets
  for ( auto& o : offsets ) { std::fill( o.begin(), o.end(), 0 ); }

  // Loop over all bank sizes in all blocks
  for ( size_t i_block = 0; i_block < blocks.size(); ++i_block ) {
    auto const& block           = blocks[i_block];
    auto const  align           = block.header->align;
    auto&       o               = offsets[i_block];
    uint32_t    fragment_offset = 0;
    for ( size_t i = 0; i < block.header->n_banks; ++i ) {
      o[i]            = fragment_offset;
      auto const size = block.bank_sizes[i];
      fragment_offset += ( size + EB::get_padding( size, 1 << align ) );
    }
    o[block.header->n_banks] = fragment_offset;
  }
}

size_t MEP::allen_offsets( Allen::Slices& slices, int const slice_index,
                           std::unordered_set<BankTypes> const&        bank_types,
                           std::array<unsigned int, NBankTypes> const& mfp_count, MEP::Blocks const& blocks,
                           std::tuple<size_t, size_t> const& interval ) {

  auto [event_start, event_end] = interval;

  // Loop over all bank sizes in all blocks
  for ( size_t i_block = 0; i_block < blocks.size(); ++i_block ) {
    auto const& block      = blocks[i_block];
    auto const* bank_sizes = block.bank_sizes;
    auto const  allen_type = source_id_type( block.header->src_id );
    auto const  sd_index   = to_integral( allen_type );

    if ( bank_types.count( allen_type ) ) {
      auto& event_offsets = slices[sd_index][slice_index].offsets;

      for ( size_t i = event_start; i < event_end; ++i ) {
        // Anticipate offset structure already here, i.e. don't assign to the first one
        auto idx = i - event_start + 1;
        // Allen raw bank format has the sourceID followed by the raw bank data.
        event_offsets[idx] += sizeof( uint32_t ) + bank_sizes[i] + EB::get_padding( bank_sizes[i], sizeof( unsigned ) );
      }
    }
  }

  // Prefix sum over sizes per bank type per event to get the output
  // "Allen" offsets per bank type per event
  size_t n_frag = ( event_end - event_start );
  for ( auto allen_type : bank_types ) {
    auto const sd_index = to_integral( allen_type );
    auto&      slice    = slices[sd_index][slice_index];
    slice.offsets[0]    = 0;
    auto preamble_words = 2 + mfp_count[sd_index];
    for ( size_t i = 1; i <= ( event_end - event_start ) && i <= n_frag; ++i ) {

      // Allen raw bank format has the number of banks and the bank
      // offsets in a preamble
      slice.offsets[i] += preamble_words * sizeof( uint32_t ) + slice.offsets[i - 1];

      // Check for sufficient space
      if ( slice.offsets[i] > slice.fragments_mem_size ) {
        n_frag = i - 1;
        break;
      }
    }
  }

  // Set offsets_size here to make sure it's consistent with the max
  for ( auto allen_type : bank_types ) {
    auto const sd_index                     = to_integral( allen_type );
    slices[sd_index][slice_index].n_offsets = n_frag + 1;
  }
  return n_frag;
}

std::tuple<bool, bool, size_t> MEP::mep_offsets( Allen::Slices& slices, int const slice_index,
                                                 std::unordered_set<BankTypes> const&        subdetectors,
                                                 std::array<unsigned int, NBankTypes> const& mfp_count,
                                                 MEP::Blocks const& blocks, MEP::SourceOffsets const& input_offsets,
                                                 std::tuple<size_t, size_t> const& interval, bool mask_top5 ) {

  auto [event_start, event_end] = interval;

  BankTypes prev_type    = BankTypes::Unknown;
  size_t    offset_index = 0;

  for ( size_t i_block = 0; i_block < blocks.size(); ++i_block ) {
    auto const& block      = blocks[i_block];
    auto const  source_id  = block.header->src_id;
    auto const  allen_type = source_id_type( source_id );
    auto const  sd_index   = to_integral( allen_type );
    auto        n_blocks   = mfp_count[sd_index];
    auto const* bank_sizes = block.bank_sizes;
    auto const* bank_types = block.bank_types;

    if ( subdetectors.count( allen_type ) ) {
      auto& slice         = slices[sd_index][slice_index];
      auto& fragments     = slice.fragments;
      auto& offsets       = slice.offsets;
      auto& offsets_size  = slice.n_offsets;
      auto& sizes_offsets = slice.sizes;
      auto& types_offsets = slice.types;

      // Calculate block offset and size
      auto const&  source_offsets  = input_offsets[i_block];
      size_t const interval_offset = source_offsets[event_start];
      size_t const interval_size   = source_offsets[event_end] - interval_offset;

#ifndef NDEBUG
      size_t payload_size = block.header->bytes() - block.header->header_size();
      assert( ( interval_offset + interval_size ) <=
              ( payload_size + EB::get_padding( payload_size, 1 << block.header->align ) ) );
#endif

      // Calculate offsets
      if ( allen_type != prev_type ) {
        offsets[0]            = mfp_count[sd_index];
        offsets[1]            = event_end - event_start;
        offsets[2 + n_blocks] = 0;
        offset_index          = 0;

        sizes_offsets[offset_index] = 2 * mfp_count[to_integral( allen_type )];
        types_offsets[offset_index] = 4 * mfp_count[to_integral( allen_type )];

        prev_type = allen_type;
      } else {
        sizes_offsets[offset_index] = sizes_offsets[offset_index - 1] + event_end - event_start;
        types_offsets[offset_index] = types_offsets[offset_index - 1] + event_end - event_start;
      }

      // Copy bank sizes
      auto* sizes = event_entries<uint16_t>( sizes_offsets, offset_index );
      std::memcpy( sizes, bank_sizes + event_start, ( event_end - event_start ) * sizeof( uint16_t ) );

      // Copy bank types
      auto* types = event_entries<uint8_t>( types_offsets, offset_index );
      std::memcpy( types, bank_types + event_start, ( event_end - event_start ) );

      // Store source ID
      offsets[2 + offset_index] = mask_top5 ? source_id & 0x7FF : source_id;

      // Initialize the first offsets using the block sizes,
      if ( offset_index < mfp_count[sd_index] - 1 ) {
        offsets[2 + n_blocks + offset_index + 1] = offsets[2 + n_blocks + offset_index] + interval_size;
      }

      // Fill fragment offsets
      size_t oi = 0, idx = 0;
      for ( size_t i = event_start; i < event_end; ++i ) {
        idx         = i - event_start + 1;
        oi          = 2 + n_blocks * ( 1 + idx ) + offset_index;
        offsets[oi] = offsets[oi - n_blocks] + source_offsets[i + 1] - source_offsets[i];
      }
      // Update offsets_size
      offsets_size = oi;

      // Store block span for this interval
      fragments.emplace_back( const_cast<char*>( block.payload ) + interval_offset, interval_size );
      slice.fragments_mem_size += interval_size;

      ++offset_index;
    }
  }
  return {true, false, event_end - event_start};
}

bool MEP::transpose_mep( Allen::Slices& slices, int const slice_index,
                         std::unordered_set<BankTypes> const&        subdetectors,
                         std::array<unsigned int, NBankTypes> const& mfp_count, MEP::Blocks const& blocks,
                         MEP::SourceOffsets const& input_offsets, std::tuple<size_t, size_t> const& interval,
                         bool mask_top5 ) {
  auto [start_event, end_event] = interval;

  // Loop over all bank data of this event
  size_t bank_index = 1;
  auto   prev_type  = BankTypes::Unknown;

  for ( size_t i_block = 0; i_block < blocks.size(); ++i_block ) {
    auto const& block          = blocks[i_block];
    auto const  source_id      = block.header->src_id;
    auto        allen_type     = source_id_type( source_id );
    auto&       source_offsets = input_offsets[i_block];
    auto const* bank_sizes     = block.bank_sizes;
    auto const* bank_types     = block.bank_types;

    if ( !subdetectors.count( allen_type ) ) {
      prev_type = allen_type;
    } else {
      auto        sd_index      = to_integral( allen_type );
      auto&       transposed    = slices[sd_index][slice_index].fragments[0];
      auto const& event_offsets = slices[sd_index][slice_index].offsets;
      auto&       sizes_offsets = slices[sd_index][slice_index].sizes;
      auto&       types_offsets = slices[sd_index][slice_index].types;

      auto const n_events = end_event - start_event;

      if ( allen_type != prev_type ) {
        bank_index       = 1;
        auto n_sd_blocks = mfp_count[sd_index];

        for ( size_t i = 0; i < n_events; ++i ) {
          sizes_offsets[i] = 2 * ( end_event - start_event + 1 ) + i * n_sd_blocks;
          types_offsets[i] = 4 * ( end_event - start_event + 1 ) + i * n_sd_blocks;
        }
        prev_type = allen_type;
      }

      for ( size_t i = 0; i < n_events; ++i ) {
        event_entries<uint16_t>( sizes_offsets, i )[bank_index - 1] = bank_sizes[start_event + i];
        event_entries<uint8_t>( types_offsets, i )[bank_index - 1]  = bank_types[start_event + i];
      }

      for ( size_t i_event = start_event; i_event < end_event && i_event < block.header->n_banks; ++i_event ) {
        // Three things to write for a new set of banks:
        // - number of banks/offsets
        // - offsets to individual banks
        // - bank data

        auto preamble_words = 2 + mfp_count[sd_index];

        // Initialize point to write from offset of previous set
        // All bank offsets are uint32_t so cast to that type
        auto* banks_write = reinterpret_cast<uint32_t*>( transposed.data() + event_offsets[i_event - start_event] );

        // Where to write the offsets
        auto* banks_offsets_write = banks_write + 1;

        if ( bank_index == 1 ) {
          // Write the number of banks
          banks_write[0]         = mfp_count[sd_index];
          banks_offsets_write[0] = 0;
        }

        // get offset for this bank and store offset for next bank
        auto offset    = banks_offsets_write[bank_index - 1];
        auto frag_size = bank_sizes[i_event] + EB::get_padding( bank_sizes[i_event], sizeof( unsigned ) );
        banks_offsets_write[bank_index] = offset + frag_size + sizeof( uint32_t );

        // Where to write the bank data itself
        banks_write += preamble_words;

        // Write sourceID; offset in 32bit words
        auto word_offset         = offset / sizeof( uint32_t );
        banks_write[word_offset] = mask_top5 ? source_id & 0x7FF : source_id;

        // Write bank data
        std::memcpy( banks_write + word_offset + 1, block.payload + source_offsets[i_event], frag_size );
      }

      ++bank_index;
    }
  }
  return true;
}

std::tuple<bool, bool, size_t> MEP::transpose_events( Allen::Slices& slices, int const slice_index,
                                                      std::unordered_set<BankTypes> const&        bank_types,
                                                      std::array<unsigned int, NBankTypes> const& mfp_count,
                                                      MEP::Blocks const&                          blocks,
                                                      MEP::SourceOffsets const&                   source_offsets,
                                                      std::tuple<size_t, size_t> const& interval, bool mask_top5 ) {
  auto [event_start, event_end] = interval;

  bool success = true;

  auto to_transpose = allen_offsets( slices, slice_index, bank_types, mfp_count, blocks, interval );

  transpose_mep( slices, slice_index, bank_types, mfp_count, blocks, source_offsets,
                 {event_start, event_start + to_transpose}, mask_top5 );

  return {success, to_transpose != ( event_end - event_start ), to_transpose};
}
