/*****************************************************************************\
* (c) Copyright 2018-2020 CERN for the benefit of the LHCb Collaboration      *
\*****************************************************************************/
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <cassert>
#include <cstring>
#include <iostream>
#include <limits>
#include <vector>

#include <gsl/gsl>

#include <Allen/BankTypes.h>
#include <EventBuilding/MEP_tools.hpp>
#include <EventBuilding/MFP_tools.hpp>
#include <MDF/MDFHeader.h>
#include <MDF/PosixIO.h>

#include <AllenOnline/ReadMEP.h>

/**
 * @brief      Read a mep from a file
 *
 * @param      file descriptor to read from
 * @param      buffer to store data in
 *
 * @return     (eof, success, mep_header, span of mep data)
 */
std::tuple<bool, bool, EB::MEP const*, unsigned, gsl::span<char const>>
MEP::read_mep( LHCb::StreamDescriptor::Access& input, std::vector<char>& buffer, MsgStream& out_stream ) {
  // Allocate space for the first few words of the MEP header
  buffer.resize( sizeof( EB::MEP_header ) );

  // Read the first few words of the MEP header
  // Why is the number of bytes returned as an int...
  int read_sc = input.read( &buffer[0], sizeof( EB::MEP_header ) );
  if ( read_sc == 0 ) {
    out_stream << "Cannot read more data (Header). End-of-File reached." << endmsg;
    return {true, false, nullptr, 0u, {}};
  } else if ( read_sc < 0 ) {
    out_stream << "Failed to read header " << strerror( errno ) << endmsg;
    return {false, false, nullptr, 0u, {}};
  }

  // Check magic pattern
  EB::MEP const* mep = reinterpret_cast<EB::MEP const*>( buffer.data() );
  if ( !mep->is_magic_valid() ) {
    out_stream << "Wrong magic pattern in MEP header: 0x" << std::hex << mep->header.magic << endmsg;
    return {false, false, nullptr, 0u, {}};
  }

  // The size of the MEP has been read, so allocate space for that
  // (don't forget to redo the pointers in case the memory was
  // reallocated elsewhere)
  size_t data_size = mep->bytes();
  buffer.resize( data_size + EB::get_padding( data_size, 1 << 16 ) );
  char* mep_buffer = &buffer[0];
  std::memset( mep_buffer + buffer.size() - ( 1 << 16 ) - 1, 0, ( 1 << 16 ) );
  mep = reinterpret_cast<EB::MEP const*>( mep_buffer );

  char*  pos       = mep_buffer + sizeof( EB::MEP_header );
  size_t remaining = data_size - sizeof( EB::MEP_header );
  while ( remaining > 0 ) {
    size_t chunk = std::min( remaining, static_cast<size_t>( std::numeric_limits<int>::max() / 2 ) );
    // Read the next chunk
    out_stream << "Reading " << chunk << " bytes" << endmsg;
    read_sc = input.read( pos, static_cast<int>( chunk ) );
    if ( read_sc <= 0 ) {
      out_stream << "Failed to read MEP" << strerror( errno ) << endmsg;
      return {false, false, nullptr, 0u, {}};
    }
    remaining -= chunk;
    pos += chunk;
  }

  // Get the packing factor
  auto const* mfp = reinterpret_cast<EB::MFP const*>( mep->at( 0 ) );

  return {false, true, mep, mfp->header.n_banks, {buffer.data(), data_size}};
}
