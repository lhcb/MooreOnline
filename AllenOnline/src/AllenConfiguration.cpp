/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <fstream>

#include "AllenConfiguration.h"
#include "GaudiKernel/Service.h"

#include <Allen/TCK.h>

DECLARE_COMPONENT( AllenConfiguration )

/// Query interfaces of Interface
StatusCode AllenConfiguration::queryInterface( const InterfaceID& riid, void** ppv ) {
  if ( AllenConfiguration::interfaceID().versionMatch( riid ) ) {
    *ppv = this;
    addRef();
    return StatusCode::SUCCESS;
  }
  return Service::queryInterface( riid, ppv );
}

AllenConfiguration::AllenConfiguration( std::string name, ISvcLocator* svcloc ) : Service( name, svcloc ) {}

std::tuple<std::string, std::string> AllenConfiguration::sequence() const {
  if ( !m_sequence.empty() ) { return {m_sequence, "json"}; }

  std::string source;

  if ( !m_tck.empty() ) {
    // Load from TCK
    auto repo = m_json.value();
    source    = repo + ":" + m_tck;
    LHCb::TCK::Info tck_info{};
    std::tie( m_sequence, tck_info ) = Allen::sequence_from_git( repo, m_tck );

    auto [check, check_error] = Allen::TCK::check_projects( nlohmann::json::parse( tck_info.metadata ) );

    if ( m_sequence.empty() ) {
      error() << "Failed to obtain sequence for TCK " << m_tck << " from repository " << repo << endmsg;
    } else if ( !check ) {
      error() << "TCK " << m_tck << ": " << check_error << endmsg;
      return {};
    } else {
      info() << "TCK " << m_tck << " loaded " << tck_info.type << " sequence with label " << tck_info.label
             << " from git repository " << repo << endmsg;
    }
  } else if ( !m_json.value().empty() ) {
    // Load from file
    std::ifstream sequence_file{m_json.value()};
    if ( !sequence_file.is_open() ) {
      error() << "Failed to load sequence from " << m_json.value() << endmsg;
    } else {
      info() << "Configuring Allen from JSON file " << m_json << endmsg;
      m_sequence = std::string{std::istreambuf_iterator<char>{sequence_file}, std::istreambuf_iterator<char>{}};
      source     = m_json.value();
    }
  }
  return {m_sequence, source};
}

AllenConfiguration::~AllenConfiguration() {}
