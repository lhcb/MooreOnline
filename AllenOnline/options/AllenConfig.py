#!/usr/bin/env python3
###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
import sys
import socket
from itertools import chain
from pathlib import Path
from PyConf.application import (ApplicationOptions, ComponentConfig,
                                setup_component, configure)
from PyConf.control_flow import CompositeNode, NodeLogic
from PyConf.Algorithms import LHCb__IOVReset as IOVReset
from Gaudi.Configuration import importOptions, allConfigurables
from Configurables import ApplicationMgr, OnlMonitorSink
from Configurables import AllenConfiguration
from Configurables import MonitorSvc
from Configurables import Online__Configuration as OnlineConfiguration
from Configurables import MEPProvider, MessageSvc
from Allen.config import (setup_allen_non_event_data_service, allen_odin,
                          configured_bank_types)
from Allen.tck import property_from_git
from Configurables import Gaudi__RootCnvSvc as RootCnvSvc
from DDDB.CheckDD4Hep import UseDD4Hep

try:
    import OnlineEnvBase as OnlineEnv
    importOptions("$MBM_SETUP_OPTIONS")
    mbm_setup = allConfigurables['OnlineEnv']
    run_online = True
    output_level = OnlineEnv.OutputLevel
    partition = OnlineEnv.PartitionName
    partition_id = OnlineEnv.PartitionID
    allen_sequence = OnlineEnv.HLTType
    dddb_tag = OnlineEnv.DDDBTag
    if "/" not in dddb_tag:
        dddb_tag = "run3/" + dddb_tag
    conddb_tag = OnlineEnv.CondDBTag
    initial_tck = getattr(OnlineEnv, "InitialTCK", 0)
    ignore_odin_tck = int(os.getenv("IGNORE_ODIN_TCK", "0"))
except ImportError:
    run_online = False
    output_level = 3
    partition = 'Allen'
    partition_id = 0
    allen_sequence = 'hlt1_pp_no_ut'
    dddb_tag = 'run3/trunk'
    conddb_tag = 'master'
    initial_tck = 0
    ignore_odin_tck = 1

integration_test = False

options = ApplicationOptions(_enabled=False)
options.simulation = not UseDD4Hep
options.data_type = 'Upgrade'
options.input_type = 'MDF'
options.dddb_tag = dddb_tag
options.conddb_tag = conddb_tag
options.geometry_version = dddb_tag
options.conditions_version = conddb_tag

online_cond_path = '/group/online/hlt/conditions.run3/lhcb-conditions-database'
if run_online:
    if os.path.exists(online_cond_path):
        if UseDD4Hep:
            from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc
            DD4hepSvc().ConditionsLocation = 'file://' + online_cond_path
    make_odin = allen_odin
else:
    from PyConf.application import make_odin

options.finalize()
config = ComponentConfig()

appMgr = ApplicationMgr()
appMgr.AppName = ""

# Configure the JSON property and figure out which detectors are required
sequence_json = None
if initial_tck != 0:
    # Configure from TCK
    tck = hex(initial_tck)
    print(f"Configuring from TCK {tck}")
    from PyConf.filecontent_metadata import FILE_CONTENT_METADATA, _is_repo

    if allen_sequence.endswith(".git") and os.path.exists(allen_sequence):
        # For the testbench, the HLTType (and thus allen_sequence
        # variable) is set to the path to the git repository if a TCK is
        # also configured
        repo = allen_sequence
    else:
        # Otherwise allow the repository to be set in an special
        # environment variable, but use the file content metadata repo
        repo = os.getenv('TCK_REPO', '/cvmfs/lhcb.cern.ch/lib/lhcb/tcks.git')
    if not _is_repo(repo):
        raise RuntimeError(
            "Failed to find filecontent metadata repo for TCKs at {}".format(
                repo))

    sequence_config = repo + ":" + tck

    # Figure out the required subdetectors by looking at the
    # configuration retrieved for the given TCK from the git
    # repository
    from Allen.tck import property_from_git
    providers = property_from_git(Path(repo), tck, ".*_banks", "bank_type")
    required_subdetectors = set(
        chain.from_iterable(props.values() for props in providers.values()))
else:
    # Load configuration from JSON file
    sequence_config = os.path.expandvars(
        "${ALLEN_INSTALL_DIR}/constants/") + allen_sequence + ".json"
    with open(sequence_config) as json_file:
        sequence_json = json_file.read()
    required_subdetectors = configured_bank_types(sequence_json)

online_conf = OnlineConfiguration("Application")
online_conf.debug = False
online_conf.classType = 1
online_conf.autoStart = False
online_conf.monitorType = 'MonitorSvc'
online_conf.logDeviceType = 'RTL::Logger::LogDevice'
online_conf.logDeviceFormat = '%TIME%LEVEL%-8NODE: %-32PROCESS %-20SOURCE'
online_conf.OutputLevel = 3
online_conf.IOOutputLevel = output_level

if run_online:
    import fifo_log
    online_conf.logDeviceType = 'fifo'
    online_conf.logDeviceFormat = '%-8LEVEL %-24SOURCE'
    fifo_log.logger_set_tag(partition)
    fifo_log.logger_start()

### GPU settings ###

# for pp; number of GPU streams
n_threads = 16  # default number of threads for production running

# GPU memory per GPU stream
memory = 1400

# host memory per GPU stream
host_memory = 400

# Number of events per GPU batch
events_per_slice = 1000

# Maximum number of events in a output batch. The number of output
# batches is determined per GPU batch and given by:
#
# n_output_batches = n_accepted / output_batch_size + (n_accepted % output_batch_size != 0) + has_TAE
#
# where n_accepted is the number of accepted events for a given GPU
# batch and has_TAE is 1 if there are TAE events in the GPU batch and
# 0 otherwise
output_batch_size = 1000

# Communication method for MBM connections. Has to match whatever is
# setup on the MBM server side
mbm_com_method = "FIFO"

if output_batch_size > events_per_slice:
    print(
        "WARNING: AllenConfig.py: output_batch_size > events_per_slice has no effect; "
        + "events_per_slice will be used.")

# for PbPb
if "PbPb" in allen_sequence:
    n_threads = 16
    memory = 1400
    host_memory = 200
    events_per_slice = 1000  #In 2023 PbPb conditions, most input to HLT1 is empty events
    output_batch_size = 300

if run_online:
    try:
        n_threads = int(os.environ["NBOFTHREADS"])
        # NBOFTHREADS is populated from the "-numthreads" argument in Arch.xml
    except (KeyError, ValueError):
        pass

allen_conf = AllenConfiguration()
allen_conf.StopTimeout = 5.
allen_conf.NThreads = n_threads
allen_conf.Memory = memory
allen_conf.HostMemory = host_memory
# Device is a string so the PCI ID can also be given
# allen_conf.Device = "01:00.0"
allen_conf.Device = "0"
allen_conf.JSON = sequence_config
allen_conf.ParamDir = os.getenv("PARAMFILESROOT")
allen_conf.Partition = partition
allen_conf.PartitionBuffers = True
allen_conf.PartitionID = partition_id
allen_conf.EnableRunChanges = UseDD4Hep
allen_conf.TCKFromODIN = not ignore_odin_tck

if run_online:
    from Configurables import Allen__MBMOutput as MBMOutput
    output_svc = MBMOutput("MBMOutput")
    output_svc.OutputLevel = 2
    # FIXME: checksums disabled until performance is improved
    output_svc.Checksum = False
    output_svc.BufferName = mbm_setup.Allen_Output
    output_svc.OutputBatchSize = output_batch_size
    output_svc.NThreads = 2
    output_svc.MBMComMethod = mbm_com_method
    appMgr.ExtSvc += [output_svc]
    allen_conf.Output = "mbm://" + output_svc.getFullName()
elif integration_test:
    allen_conf.Output = "tcp://192.168.1.101:35000"
else:
    allen_conf.Output = "allen_output.mdf"

mep_provider = MEPProvider()
mep_provider.NSlices = 24
# Events per slice should be set, and memory reserved per thread as well
mep_provider.EventsPerSlice = events_per_slice
mep_provider.OutputLevel = 3
# Number of MEP buffers and number of transpose/offset/input threads
mep_provider.BufferConfig = (6, 4)
mep_provider.TransposeMEPs = False
mep_provider.ThreadPerBuffer = False

if run_online:
    mep_provider.Source = "MBM"
    mep_provider.MBMComMethod = mbm_com_method
    # /group/online/dataflow/cmtuser/OnlineRelease/Online/FarmConfig/job/createEnvironment.sh sets
    # the NBOFSLAVES environment variable to the value of the -instances command line argument,
    # which is set by the SMI controller to the total number of Allen instances in the architecture,
    # *minus one*.
    n_instances = int(os.getenv("NBOFSLAVES")) + 1
    # The UTGID is set to {PARTITION}_{HOSTNAME}_Allen_{INSTANCE}, where {INSTANCE} is the instance
    # number, starting from zero.
    instance = int(os.getenv("UTGID").split("_")[3])

    # Unsupported configuration
    if n_instances > 3:
        print(
            f"ERROR: Unsupported HLT1 DAQ configuration with {n_instances} instances"
        )
        sys.exit(1)

    # The MBM options have a variable named Allen_Input{N} for each input buffer.
    input_buffers = [
        getattr(mbm_setup, a) for a in sorted(dir(mbm_setup))
        if a.startswith('Allen_Input')
    ]
    # Special case for sodin01, where there is a single BU and all Allen instances must read only
    # from that BU's output buffer
    if socket.gethostname() == 'sodin01':
        mep_provider.Connections = [input_buffers[0]]
    # Two cases to connect to all BU output buffers:
    # - There is only one Allen instance
    # - This is the third Allen instance
    elif (n_instances == 1 or instance == 2):
        mep_provider.Connections = input_buffers
    # There are multiple Allen instances and this the first or second instance
    else:
        mep_provider.Connections = [input_buffers[instance]]
    mep_provider.Requests = [
        'EvType=1;TriggerMask=0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF;VetoMask=0,0,0,0;MaskType=ANY;UserType=ONE;Frequency=PERC;Perc=100.0'
    ]
elif integration_test:
    mep_provider.Source = "MPI"
    mep_provider.Receivers = {"mlx5_0": 1}
    mep_provider.LoopOnMEPs = True
else:
    mep_provider.Source = "Files"
    mep_dir = "/daqarea1/fest/mep"
    mep_provider.Connections = sorted([
        os.path.join(mep_dir, mep_file) for mep_file in os.listdir(mep_dir)
        if mep_file.endswith('.mep')
    ])
    mep_provider.LoopOnMEPs = False

monSvc = MonitorSvc('MonitorSvc')
monSvc.ExpandNameInfix = '<proc>/'
monSvc.ExpandCounterServices = True
monSvc.UniqueServiceNames = True
monSvc.UseDStoreNames = True
monSvc.PartitionName = partition
monSvc.DimUpdateInterval = 5

appMgr.MessageSvcType = 'MessageSvc'
appMgr.OutputLevel = 3
messageSvc = MessageSvc('MessageSvc')
messageSvc.Format = '% F%8W%L%T %25W%L%S %0W%M'
messageSvc.OutputLevel = 3

# Add the services that will produce the non-event-data
monSink = OnlMonitorSink(
    CountersToPublish=[(".*", ".*")], HistogramsToPublish=[(".*", ".*")])
appMgr.ExtSvc = [monSvc, monSink] + appMgr.ExtSvc

appMgr.EvtSel = "NONE"

# Key services, order matters!
rootSvc = RootCnvSvc("RootCnvSvc", EnableIncident=1)
appMgr.ExtSvc = [
    'ToolSvc', 'AuditorSvc', 'ZeroMQSvc', "Gaudi::IODataManager/IODataManager",
    rootSvc, allen_conf, 'Online::Configuration/Application', 'MEPProvider'
] + appMgr.ExtSvc

# Copied from PyConf.application.configure_input
config.add(
    setup_component(
        'DDDBConf', Simulation=options.simulation, DataType=options.data_type))
if UseDD4Hep:
    from Configurables import DDDBConf
    DDDBConf(
        Simulation=options.simulation,
        GeometryVersion=(options.geometry_version or options.dddb_tag),
        ConditionsVersion=(options.conditions_version or options.conddb_tag),
        DataType=options.data_type)
else:
    config.add(
        setup_component(
            'CondDB',
            Upgrade=True,
            Tags={
                'DDDB': options.dddb_tag,
                'SIMCOND': options.conddb_tag,
            }))

# Get the subdetectors that are needed from the json file

# Setup non-event-data and finalize the configuration
cf_node = setup_allen_non_event_data_service(
    allen_event_loop=True, bank_types=required_subdetectors)

# Workaround for the fact that IOVs are open ended, which means that
# when a conditions are not reloaded on a change of run.
cf_node = CompositeNode(
    "allen_non_event_data_with_reset", [IOVReset(ODIN=make_odin()), cf_node],
    combine_logic=NodeLogic.LAZY_AND,
    force_order=True)

config.update(configure(options, cf_node, make_odin=make_odin))
