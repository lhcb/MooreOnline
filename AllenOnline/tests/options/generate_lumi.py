###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from AllenCore.configuration_options import is_allen_standalone
is_allen_standalone.global_bind(standalone=True)

import importlib
from AllenCore.generator import generate

lumi_sequence = importlib.import_module("lumi_sequence")
generate(lumi_sequence.sequence(), "lumi.json")
