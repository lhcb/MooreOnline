###############################################################################
# (c) Copyright 2019-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
import argparse
import json

from AllenCore.configuration_options import is_allen_standalone
is_allen_standalone.global_bind(standalone=True)

from AllenConf.persistency import register_decision_ids
from AllenOnline.utils import allen_json_sequence, get_allen_hlt1_decision_ids
from PyConf.filecontent_metadata import flush_key_registry

parser = argparse.ArgumentParser()
parser.add_argument("sequence", type=str)
args = parser.parse_args()

passed = False

with allen_json_sequence.bind(sequence=args.sequence), flush_key_registry():
    # Get the decision IDs from the sequence JSON and register them
    ids = get_allen_hlt1_decision_ids()
    encoding_key = register_decision_ids(ids)

    if encoding_key:
        # Write a new sequence JSON that contains the TCK set to the
        # encoding key
        sequence, json_filename = allen_json_sequence()

        with open(os.path.expandvars(json_filename)) as json_file:
            sequence_json = json.load(json_file)

        dec_reporter_name = [
            a[1] for a in sequence_json["sequence"]["configured_algorithms"]
            if a[0] == "dec_reporter::dec_reporter_t"
        ][0]
        sequence_json[dec_reporter_name]['tck'] = encoding_key

        with open(sequence + '.json', 'w') as json_file:
            json.dump(sequence_json, json_file)

        passed = True

print("PASSED" if passed else "FAILED")
