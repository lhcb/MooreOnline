###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from AllenCore.configuration_options import is_allen_standalone
is_allen_standalone.global_bind(standalone=True)

import importlib
from AllenConf.odin import tae_filter
from AllenCore.generator import generate

tae_sequence = importlib.import_module("tae_activity_sequence")

with tae_filter.bind(accept_sub_events=False):
    generate(tae_sequence.sequence(), "tae_activity.json")
