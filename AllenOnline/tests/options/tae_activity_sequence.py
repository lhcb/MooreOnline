###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
###############################################################################
from PyConf.control_flow import NodeLogic, CompositeNode
from AllenConf.persistency import make_global_decision, make_gather_selections, make_routingbits_writer, make_sel_report_writer
from AllenConf.utils import line_maker
from AllenConf.filters import make_tae_activity_filter
from AllenConf.validators import rate_validation
from AllenConf.hlt1_photon_lines import make_single_calo_cluster_line
from AllenConf.hlt1_reconstruction import hlt1_reconstruction
from AllenConf.hlt1_calibration_lines import make_passthrough_line, make_tae_line
from AllenConf.odin import odin_error_filter, tae_filter, make_event_type
from AllenConf.lumi_reconstruction import lumi_reconstruction


def sequence():

    reconstructed_objects = hlt1_reconstruction(with_ut=False)
    ecal_clusters = reconstructed_objects["ecal_clusters"]

    lines = []
    lumiline_name = "Hlt1ODINLumi"

    tae_activity = True
    routingbit_map = {
        # RB 1 Lumi after HLT1
        '^Hlt1.*Lumi.*': 1,
        # RB 6 TAE passthrough
        'Hlt1TAEPassthrough': 6,
        # RB 31 TAE activity test
        'Hlt1ActivityPassthrough': 31,
    }

    prefilters = [odin_error_filter("odin_error_filter")]
    if tae_activity:

        tae_activity_filter = make_tae_activity_filter(
            reconstructed_objects["long_tracks"],
            reconstructed_objects["velo_tracks"],
            min_tracks=1.)

        tae_filters = CompositeNode(
            "taefilter_node",
            [tae_activity_filter, tae_filter()],
            NodeLogic.LAZY_AND,
            force_order=True)
    else:
        tae_filters = tae_filter()

    with line_maker.bind(prefilter=prefilters + [tae_filters]):
        lines += [
            line_maker(
                make_passthrough_line(name="Hlt1TAEPassthrough", pre_scaler=1))
        ]

    with line_maker.bind(prefilter=prefilters + [tae_activity_filter]):
        lines += [
            line_maker(
                make_passthrough_line(
                    name="Hlt1ActivityPassthrough", pre_scaler=1))
        ]

    odin_lumi_event = make_event_type(event_type='Lumi')
    with line_maker.bind(prefilter=prefilters + [odin_lumi_event]):
        lines.append(
            line_maker(
                make_passthrough_line(name=lumiline_name, pre_scaler=1.)))

    line_algorithms = [tup[0] for tup in lines]

    global_decision = make_global_decision(lines=line_algorithms)

    lines = CompositeNode(
        "AllLines", [tup[1] for tup in lines],
        NodeLogic.NONLAZY_OR,
        force_order=False)

    tae_sequence = CompositeNode(
        "ActivityTAE", [
            lines, global_decision,
            make_routingbits_writer(
                lines=line_algorithms, rb_map=routingbit_map),
            rate_validation(lines=line_algorithms)
        ],
        NodeLogic.NONLAZY_AND,
        force_order=True)

    gather_selections = make_gather_selections(lines=line_algorithms)

    lumi_node = CompositeNode(
        "AllenLumiNode",
        lumi_reconstruction(
            gather_selections=gather_selections,
            lumiline_name=lumiline_name,
            with_muon=False)["algorithms"],
        NodeLogic.NONLAZY_AND,
        force_order=False)

    lumi_with_prefilter = CompositeNode(
        "LumiWithPrefilter",
        prefilters + [lumi_node],
        NodeLogic.LAZY_AND,
        force_order=True)

    hlt1_node = CompositeNode(
        "AllenWithLumi", [tae_sequence, lumi_with_prefilter],
        NodeLogic.NONLAZY_AND,
        force_order=False)

    return hlt1_node
