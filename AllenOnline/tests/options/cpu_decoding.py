###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options
from RecoConf.config import run_reconstruction, Reconstruction
from RecoConf.rich_reconstruction import make_rich_pixels, default_rich_reco_options
from RecoConf.calorimeter_reconstruction import make_ecal_digits
from RecoConf.muonid import make_muon_hits
from PyConf.application import default_raw_event, default_raw_banks, configured_ann_svc
from PyConf.Algorithms import HltSelReportsDecoder, HltDecReportsDecoder, LHCb__UnpackRawEvent
from Configurables import ApplicationMgr
from RecoConf.legacy_rec_hlt1_tracking import (make_RetinaClusters,
                                               make_PrStorePrUTHits_hits)
from RecoConf.hlt2_tracking import make_PrStoreSciFiHits_hits


def make_selreports():
    return HltSelReportsDecoder(
        RawBanks=default_raw_banks("HltSelReports"),
        DecReports=default_raw_banks("HltDecReports"),
        SourceID='Hlt1',
        DecoderMapping="TCKANNSvc").OutputHltSelReportsLocation


def make_decreports():
    return HltDecReportsDecoder(
        RawBanks=default_raw_banks("HltDecReports"),
        SourceID='Hlt1',
        DecoderMapping="TCKANNSvc").OutputHltDecReportsLocation


mgr = ApplicationMgr()
mgr.ExtSvc += [configured_ann_svc('TCKANNSvc')]


def decoding():

    ecal_raw = default_raw_banks("Calo")
    ecal_raw_error = default_raw_banks("CaloError")

    decoders = [
        loc.producer for loc in [
            make_RetinaClusters(detector="VPRetinaCluster"),
            # make_PrStorePrUTHits_hits(),
            make_PrStoreSciFiHits_hits(),
            make_rich_pixels(default_rich_reco_options())['RichDecodedData'],
            make_ecal_digits(ecal_raw, ecal_raw_error),
            make_muon_hits(geometry_version=3),
            make_decreports(),
            make_selreports()
        ]
    ]

    return Reconstruction('decoding', decoders, [])


default_raw_event.global_bind(raw_event_format=0.5)
run_reconstruction(options, decoding)
