###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Check decoding and encoding of HltLumiSummary
"""
from GaudiConf import IOHelper
from Gaudi.Configuration import VERBOSE
import GaudiPython as GP
from PyConf.Algorithms import HltLumiWriter, HltLumiSummaryDecoder, HltRoutingBitsFilter, VoidFilter, HltDecReportsDecoder, HltLumiSummaryDecoder, HltLumiSummaryMonitor, PrintHeader, LHCb__SelectViewForHltSourceID
from PyConf.application import configure_input, configure, configured_ann_svc
from PyConf.application import default_raw_event, default_raw_banks, make_odin, force_location
from Moore import options
from PyConf.control_flow import CompositeNode, NodeLogic
import Functors as F

import argparse
import json
import warnings

defaultFile = "mep_lumi.mdf"
NEW_OUTPUT_RAW_EVENT = "/Event/DAQ/NewHltLumiEvent"
NEW_OUTPUT_VIEW = "/Event/DAQ/NewHltLumi/View"
NEW_LUMI_SUMMARY_LOC = "Hlt/NewLumiSummary"


def routing_bits(rbbanks):
    """Return a list with the 96 routing bit values."""
    assert rbbanks.size() == 1  # stop if we have multiple rb banks
    d = rbbanks[0].data()  # get only bank
    bits = "{:032b}{:032b}{:032b}".format(d[2], d[1], d[0])
    return list(map(int, reversed(bits)))


parser = argparse.ArgumentParser()
parser.add_argument("--input-mdf", default=defaultFile, help="Input MDF file")
parser.add_argument(
    "--schema-json-file",
    type=str,
    default="",
    help=
    "optional JSON input to specify lumi schemata not present in the ANN repository"
)

args = parser.parse_args()

decKeys = {}
try:
    with open(args.schema_json_file) as f:
        j = json.load(f)
        decKeys = {
            int(k, 16): json.dumps(v)
            for (k, v) in j["DecodingKeys"].items()
        }
except FileNotFoundError:
    pass

configured_ann_svc.global_bind(LumiOverrule=decKeys)

options.input_files = [args.input_mdf]
options.input_type = "MDF"
options.data_type = "Upgrade"
options.simulation = True
options.dddb_tag = "master"
options.conddb_tag = "master"
options.gaudipython_mode = True

config = configure_input(options)

# Workaround for ROOT-10769
with warnings.catch_warnings():
    warnings.simplefilter("ignore")
    import cppyy

rawevent = default_raw_event("HltLumiSummary")
odin = default_raw_banks("ODIN")
odin_handle = make_odin()

dec_banks = default_raw_banks("HltDecReports")

dec_decoder = HltDecReportsDecoder(RawBanks=dec_banks, SourceID='Hlt1')

rb_banks = default_raw_banks("HltRoutingBits")
rb_filter = HltRoutingBitsFilter(
    name="RequireLumiRoutingBit",
    RequireMask=(1 << 1, 0, 0),
    RawBanks=default_raw_banks("HltRoutingBits"),
    PassOnError=False,
)

lumi_banks = default_raw_banks("HltLumiSummary")

lumi_views = LHCb__SelectViewForHltSourceID(
    name="SelectHlt1Lumi", Input=lumi_banks, SourceID="Hlt1")
#
##Decode, re-encode and re-decode
lumi_decoder = HltLumiSummaryDecoder(
    name="HltLumiSummaryFirstDecoder", RawBanks=lumi_views)
summary = lumi_decoder.OutputContainerName
monitor = HltLumiSummaryMonitor(
    name="HltLumiSummaryMonitor",
    Input=summary,
    ODIN=odin_handle,
    OutputLevel=VERBOSE)
print_event = PrintHeader(name="PrintHeader", ODINLocation=odin_handle)

writer = HltLumiWriter(
    name="HltLumiWriter",
    InputBank=summary,
    outputs={
        "OutputRawEvent": force_location(NEW_OUTPUT_RAW_EVENT),
        "OutputView": force_location(NEW_OUTPUT_VIEW),
    },
    SourceID='Hlt1')
redecoder = HltLumiSummaryDecoder(
    name="HltLumiSummaryReDecoder",
    RawBanks=writer.OutputView,
    outputs={"OutputContainerName": force_location(NEW_LUMI_SUMMARY_LOC)})

#need full algorithm list for gaudipython to work
top_node = CompositeNode("Top", [
    rawevent, odin, odin_handle, dec_banks, dec_decoder, rb_banks, rb_filter,
    lumi_banks, lumi_views, lumi_decoder, summary, writer, redecoder
])
configure(options, top_node)

appMgr = GP.AppMgr()
TES = appMgr.evtSvc()
LHCb = GP.gbl.LHCb

counters_dict = dict()
error = False
appMgr.run(1)
while TES["/Event/DAQ"]:
    dec_reports = TES[dec_decoder.OutputHltDecReportsLocation.location]
    lumi_report = dec_reports.decReport("Hlt1ODINLumiDecision")
    if not lumi_report.decision():
        appMgr.run(1)
        continue

    event_dict = dict()

    odin = TES[odin_handle.location]
    if not odin:
        print("ODIN TES location not found")
        error = True
        continue
    summary = TES[lumi_decoder.OutputContainerName.location]
    if not summary:
        print("LumiSummary TES location not found")
        error = True
        continue
    newSummary = TES[redecoder.OutputContainerName.location]
    if not newSummary:
        print("LumiSummary new TES location not found")
        error = True
        continue

    for (name, value) in summary.extraInfo().items():
        if newSummary.info(name, -1) != value:
            print("Lumi counter " + name + " differs")
            print("changed from ", value, " to ", newSummary.info(name, -1))
            error = True

    for (name, value) in summary.extraInfo().items():
        if name in event_dict:
            event_dict[name] += value
        else:
            event_dict[name] = value

    counters_dict[odin.eventNumber()] = event_dict
    appMgr.run(1)

print_freq = 20
i = 0
for eventNumber in sorted(counters_dict.keys()):
    if i % print_freq == 0:
        single_event_dict = counters_dict[eventNumber]
        print("-------------- Event %d --------------" % eventNumber)
        for name in sorted(single_event_dict.keys()):
            value = single_event_dict[name]
            print("%s: %f" % (name, value))

    i += 1

if error:
    exit("Test failed - see output")  # exit with a non-zero code
