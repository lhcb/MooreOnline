###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options
from DDDB.CheckDD4Hep import UseDD4Hep

options.print_freq = 100
options.data_type = 'Upgrade'
options.simulation = not UseDD4Hep
options.input_type = 'MDF'
options.input_files = ["mep_tae.mdf"]
if not UseDD4Hep:
    options.dddb_tag = "upgrade/dddb-20221004"
    options.conddb_tag = "upgrade/mu_VP_SciFi_macromicrosurvey_from20220923"
else:
    options.geometry_version = 'run3/trunk'  #does this need to go to detdesc or does it stay here?
    options.conditions_version = "master"
