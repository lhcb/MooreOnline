###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
###############################################################################
import os
import sys
import json
import argparse
from collections import defaultdict
from Configurables import LHCbApp
from Configurables import GaudiSequencer
from Configurables import ApplicationMgr
from Configurables import (AuditorSvc, SequencerTimerTool)
from Configurables import IODataManager
from Configurables import createODIN, HltDecReportsDecoder
from Configurables import LHCb__UnpackRawEvent as UnpackRawEvent
from DDDB.CheckDD4Hep import UseDD4Hep
from GaudiConf import IOHelper

from AllenCore.configuration_options import is_allen_standalone
is_allen_standalone.global_bind(standalone=True)

from AllenOnline.utils import allen_json_sequence, get_allen_hlt1_decision_ids
from GaudiPython.Bindings import AppMgr, gbl
from PyConf.application import configured_ann_svc

parser = argparse.ArgumentParser()
parser.add_argument("mdf", nargs=1)
parser.add_argument("rates", nargs=1)
parser.add_argument("sequence", nargs=1)
args = parser.parse_args()

rates = {}
rates_filename = args.rates[0]
with open(rates_filename) as rates_file:
    rates = json.load(rates_file)

sequence_file = os.path.expandvars(args.sequence[0])
sequence_name = os.path.basename(sequence_file)
sequence_name = os.path.splitext(sequence_name)[0]

with allen_json_sequence.bind(sequence=sequence_name, json=sequence_file):
    decs = get_allen_hlt1_decision_ids()

app = LHCbApp(DataType="Upgrade", EvtMax=-1, Simulation=not UseDD4Hep)

if not UseDD4Hep:
    app.DDDBtag = "upgrade/dddb-20221004"
    app.CondDBtag = "upgrade/mu_VP_SciFi_macromicrosurvey_from20220923"
else:
    app.DDDBtag = "run3/trunk"
    app.CondDBtag = "master"

check_seq = GaudiSequencer("CheckODINSeq")

unpack_raw = UnpackRawEvent(
    RawEventLocation='DAQ/RawEvent',
    RawBankLocations=['DAQ/RawBanks/ODIN', 'DAQ/RawBanks/HltDecReports'],
    BankTypes=['ODIN', 'HltDecReports'])

dec_reports = HltDecReportsDecoder(
    RawBanks='DAQ/RawBanks/HltDecReports',
    SourceID='Hlt1',
    DecoderMapping="TCKANNSvc",
    OutputHltDecReportsLocation='Hlt1/DecReports')

check_seq.Members = [unpack_raw, createODIN(), dec_reports]

ApplicationMgr().TopAlg = [check_seq]

IOHelper('MDF').inputFiles(args.mdf, clear=True)

# Some extra stuff for timing table
ApplicationMgr().ExtSvc += ['ToolSvc', 'AuditorSvc']
ApplicationMgr().ExtSvc += [configured_ann_svc(name='TCKANNSvc')]

ApplicationMgr().AuditAlgorithms = True
AuditorSvc().Auditors += ['TimingAuditor']
SequencerTimerTool().OutputLevel = 4

# Some extra stuff to save histograms
ApplicationMgr().HistogramPersistency = "NONE"

# No error messages when reading MDF
IODataManager().DisablePFNWarning = True

gaudi = AppMgr()
gaudi.initialize()
TES = gaudi.evtSvc()

events = set()

decs = defaultdict(int)

n_evt = 0
while app.EvtMax == -1 or n_evt < app.EvtMax:
    gaudi.run(1)
    if not TES['/Event']:
        break

    n_evt += 1

    odin = TES['DAQ/ODIN']
    events.add((odin.runNumber(), odin.eventNumber()))

    reps = TES['Hlt1/DecReports']
    for n in reps.decisionNames():
        decs[str(n)[:-8]] += reps.decReport(n).decision()

if len(events) != n_evt:
    sys.exit("Found %d duplicate events" % (n_evt - len(events)))
elif n_evt != rates['Inclusive']:
    sys.exit("Wrong number of events %d, but should be %d" %
             (n_evt, rates['Inclusive']))

wrong_decs = False
for n, n_dec in decs.items():
    if rates[n] != n_dec:
        wrong_decs = True
        print("Wrong number of decisions for %d: %d should be %d" % (n, n_dec,
                                                                     rates[n]))

if wrong_decs:
    sys.exit("Wrong number of decisions")
