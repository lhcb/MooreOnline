/*****************************************************************************\
* (c) Copyright 2018-2020 CERN for the benefit of the LHCb Collaboration      *
\*****************************************************************************/
#pragma once

#include <iostream>
#include <vector>

#include <gsl/gsl>

#include <EventBuilding/MEP_tools.hpp>
#include <GaudiKernel/MsgStream.h>
#include <MDF/StreamDescriptor.h>

namespace MEP {
  std::tuple<bool, bool, EB::MEP const*, unsigned, gsl::span<char const>>
  read_mep( LHCb::StreamDescriptor::Access& input, std::vector<char>& buffer, MsgStream& out_stream );
}
