/*****************************************************************************\
* (c) Copyright 2018-2020 CERN for the benefit of the LHCb Collaboration      *
\*****************************************************************************/
#pragma once

#include <algorithm>
#include <array>
#include <atomic>
#include <chrono>
#include <condition_variable>
#include <deque>
#include <mutex>
#include <numeric>
#include <thread>
#include <vector>

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <Event/ODIN.h>
#include <Event/RawBank.h>
#include <Kernel/meta_enum.h>

#include <Allen/AllenUnits.h>
#include <Allen/BankMapping.h>
#include <Allen/Common.h>
#include <Allen/TransposeTypes.h>

#include <EventBuilding/MEP_tools.hpp>
#include <EventBuilding/MFP_tools.hpp>

namespace {
  using namespace Allen::Units;
} // namespace

namespace MEP {

  meta_enum_class( MBMCom, int, Unknown = 10, None = 0, FIFO = 1, Asio = 2, Unix = 4, SHM1 = 8, SHM2 = 9 );

  using SourceOffsets = std::vector<std::vector<uint32_t>>;

  struct Block {
    Block() = default;
    Block( EB::MFP const* b )
        : mfp{b}
        , header{&( mfp->header )}
        , bank_sizes{mfp->header.bank_sizes()}
        , bank_types{mfp->header.bank_types()}
        , payload{static_cast<char const*>( mfp->payload() )} {}

    EB::MFP const*            mfp        = nullptr;
    EB::MFP_header const*     header     = nullptr;
    EB::bank_size_type const* bank_sizes = nullptr;
    EB::bank_type_type const* bank_types = nullptr;
    char const*               payload    = nullptr;
  };

  using Blocks = std::vector<Block>;

  struct Slice {
    EB::MEP const*        mep = nullptr;
    gsl::span<char const> mep_data;
    unsigned              packing_factor = 0u;
    Blocks                blocks;
    SourceOffsets         offsets;
    size_t                slice_size = 0u;
  };
  using Slices = std::vector<MEP::Slice>;

  BankTypes source_id_type( uint16_t src_id );

  LHCb::ODIN decode_odin( char const* odin_data, unsigned const offset, unsigned const size_bytes,
                          const uint8_t version );

  /**
   * @brief      Fill the array the contains the number of banks per type
   *
   * @param      MEP
   *
   * @return     (success, number of banks per bank type; 0 if the bank is not needed, bank version per type, map of
   * source ID to bin label)
   */
  std::tuple<bool, std::array<unsigned int, NBankTypes>, std::array<int, NBankTypes>> fill_counts( EB::MEP const* mep );

  std::optional<size_t> find_blocks( EB::MEP const* mep, Blocks& blocks, std::function<void( size_t )> const& badMFP );

  void fragment_offsets( Blocks const& blocks, std::vector<std::vector<uint32_t>>& offsets );

  size_t allen_offsets( Allen::Slices& slices, int const slice_index, std::unordered_set<BankTypes> const& bank_types,
                        std::array<unsigned int, NBankTypes> const& mfp_count, Blocks const& blocks,
                        std::tuple<size_t, size_t> const& interval );

  std::tuple<bool, bool, size_t> mep_offsets( Allen::Slices& slices, int const slice_index,
                                              std::unordered_set<BankTypes> const&        bank_types,
                                              std::array<unsigned int, NBankTypes> const& banks_count,
                                              MEP::Blocks const& blocks, MEP::SourceOffsets const& input_offsets,
                                              std::tuple<size_t, size_t> const& interval, bool mask_top5 = false );

  /**
   * @brief      Transpose events to Allen layout
   *
   * @param      slices to fill with transposed banks, slices are addressed by bank type
   * @param      index of bank slices
   * @param      number of banks per event
   * @param      event ids of banks in this slice
   * @param      start of bank data for this event
   *
   * @return     tuple of: (success, slice is full)
   */
  bool transpose_mep( Allen::Slices& slices, int const slice_index, std::unordered_set<BankTypes> const& bank_types,
                      std::array<unsigned int, NBankTypes> const& mfp_count, Blocks const& blocks,
                      SourceOffsets const& input_offsets, std::tuple<size_t, size_t> const& interval,
                      bool mask_top5 = false );

  /**
   * @brief      Transpose MEP to Allen layout
   *
   * @param      slices to fill with transposed banks, slices are addressed by bank type
   * @param      index of bank slices
   * @param      number of banks per event
   * @param      event ids of banks in this slice
   * @param      start of bank data for this event
   *
   * @return     tuple of: (success, slice is full, number of events transposed)
   */
  std::tuple<bool, bool, size_t> transpose_events( Allen::Slices& slices, int const slice_index,
                                                   std::unordered_set<BankTypes> const&        bank_types,
                                                   std::array<unsigned int, NBankTypes> const& mfp_count,
                                                   Blocks const& blocks, SourceOffsets const& source_offsets,
                                                   std::tuple<size_t, size_t> const& interval, bool mask_top5 = false );
} // namespace MEP
