###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
import re
import json


class TAEBatch:
    def __init__(self):
        self.reset()

    def reset(self):
        self.start = None
        self.index = 0
        self.central = None
        self.window = None
        self.prev_run = None
        self.lines = []


###################################################################################
# this test checks the tae activity filter functionality
# It requires and MDF created from the tae_plus_activity HLT1 sequence
# where a dedicated routing bit is set to flag events passing the activity filter
# It checks that
#    1) There are no incomplete TAE batches
#    2) all central TAE events have the activity RB set
#    3) There are no duplicate central TAE events
#    4) Non central TAE sub-events with activity are duplicated
#    5) There are no dangling TAE events without activity (complementary to 4)
###################################################################################


def check_activity_tae(stdout, activity_rb='31'):

    line_expr = re.compile(
        r"run\s+(?P<run>\d+) event\s+(?P<event>\d+)" +
        r".+TAE: (?P<tae>[01]) first (?P<tae_first>[01])" +
        r" window\s+(?P<tae_window>\d+) central (?P<tae_central>[01])" +
        r" RBs: \[(?P<rbs>[ \d,]*)\]")

    problems = []
    event_info = []
    tae_batches = []
    event_list = []
    for line in stdout.split('\n'):
        if (m := line_expr.match(line)) is not None:
            run = m.group("run")
            event = m.group("event")
            tae_info = {
                k: int(m.group(k))
                for k in ("tae", "tae_first", "tae_window", "tae_central")
            }
            rbs = m.group("rbs")
            event_info.append((int(run), int(event), tae_info, rbs, line))
            event_list.append(int(event))

    if not event_info:
        problems.append("No events read")

    unique_events = set((i[0], i[1]) for i in event_info)

    tae_batch = TAEBatch()
    tae_first_counter = 0
    for run, event, tae_info, rbs, line in event_info:
        if tae_batch.prev_run is None:
            tae_batch.prev_run = run
        elif tae_batch.prev_run != run:
            tae_batch.reset()
            tae_batch.prev_run = run
        if tae_info['tae']:
            if tae_info['tae_first']:
                if tae_batch.start is not None:
                    problems.append(
                        f"{run}, {event}, Incomplete TAE batch starting at {tae_batch.start[1]}"
                    )
                    tae_batch.reset()
                    continue
                if activity_rb and activity_rb in rbs and tae_first_counter < 1:  #first tae event with activity, should be duplicated, skip the checks the first time
                    print(
                        f'first TAE event {event} with activity, the first time it occurs it is the duplicate in the normal stream, so I will skip it'
                    )
                    tae_first_counter += 1
                    continue
                tae_batch.start = (run, event)
                tae_batch.lines.append(line)
                tae_first_counter = 0  #reset the activity counter, for future events
            elif tae_batch.start is not None:
                tae_batch.index += 1
                if (event - tae_batch.index) != tae_batch.start[1]:
                    problems.append(f"Wrong event number {event} at" +
                                    f" index {tae_batch.index} in TAE batch" +
                                    f" starting at {tae_batch.start}")
                    tae_batch.reset()
                    continue
                else:
                    tae_batch.lines.append(line)
            else:
                if activity_rb and activity_rb not in rbs:  #TAE subevents with activity can be dangling if their central TAE event did not pass the activity filter
                    problems.append(
                        f"Dangling TAE subevent {event} without activity")
                tae_batch.reset()
                continue

            if tae_info['tae_central']:
                tae_batch.central = (run, event)
                tae_batch.window = event - tae_batch.start[1]
                if activity_rb and activity_rb not in rbs:
                    problems.append(
                        f"TAE central event {event} found without activity RB set. RBs actually set are: {rbs}"
                    )
                if event_list.count(event) != 1:
                    print(event_info.count(event))
                    problems.append(f"TAE central event {event} dublicated")
            else:
                if len(rbs) > 0 and event_list.count(event) != 2:
                    problems.append(
                        f"TAE non-central event {event} with activity not dublicated"
                    )
            if tae_batch.window is not None and event - 2 * tae_batch.window == tae_batch.start[
                    1]:
                tae_batches.append(tae_batch)
                tae_batch = TAEBatch()

    if not tae_batches:
        problems.append(f"No TAE batches found in {len(event_info)} events")

    return problems, tae_batches


def write_rates_json(output, causes, filename):
    rate_lines = []
    add_rate = False

    for line in output.split("\n"):
        line = line.strip()
        if line.startswith("rate_validator validation"):
            add_rate = True
            continue
        if add_rate:
            rate_lines.append(line)
        if line.startswith('Inclusive:'):
            add_rate = False
            break

    rates = {}
    rate_expr = re.compile(r"([^:]+):[ ]+([0-9]+)/[ ]+([0-9]+)")
    for rate_line in rate_lines:
        m = rate_expr.search(rate_line)
        if m is None:
            causes.append("Failed to extract rate from " + rate_line)
        else:
            rates[m.group(1)] = int(m.group(2))

    with open(filename, "w") as json_file:
        json.dump(rates, json_file)


if __name__ == '__main__':
    import argparse
    import pprint

    parser = argparse.ArgumentParser()
    parser.add_argument("filename", nargs=1)
    args = parser.parse_args()
    with open(args.filename[0]) as f:
        problems, tae_batches = check_tae(f.read())
        if problems:
            pprint.pprint(problems)
        else:
            for batch in tae_batches:
                for line in batch.lines:
                    print(line)
                print('')
