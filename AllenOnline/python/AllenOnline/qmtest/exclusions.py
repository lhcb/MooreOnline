###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from GaudiConf.QMTest.LHCbTest import BlockSkipper
from GaudiTesting.BaseTest import RegexpReplacer, LineSkipper
from GaudiConf.QMTest.LHCbExclusions import preprocessor as LHCbPreprocessor
from RecConf.QMTest.exclusions import preprocessor as RecPreprocessor
from Moore.qmtest.exclusions import skip_scheduler
from Allen.qmtest.exclusions import skip_lbdd4hep, skip_detdesc

skip_counters = BlockSkipper("Bursts", "|        10 |")

skip_start = BlockSkipper("mdf_files = mep_passthrough.mdf"
                          "Application Manager Configured successfully")

json_config = RegexpReplacer("(Configuring Allen from JSON file).*", r"\1")

skip_mep_provider = (LineSkipper(regexps=[r"MEPProvider.*"]) +
                     LineSkipper(regexps=[r"Wrote .* events to .*"]) +
                     LineSkipper(regexps=[r"Setting number of slices to .*"]) +
                     BlockSkipper("scifi_raw_bank_decoder_aef54905",
                                  "velo_consolidate_tracks") + json_config)

skip_configure = BlockSkipper("# --> Including file",
                              "Application Manager Configured successfully")

assertion_counter = (RegexpReplacer(" \(\d+ assertions in \d+ test cases\)") +
                     RegexpReplacer("(Providing events in).*", r"\1"))

skipper = LineSkipper(
    regexps=["Opened mep_passthrough.mdf", "Cannot read more data.*"])

remove_finalize = BlockSkipper(
    " INFO No more events in event selection",
    " INFO Application Manager Terminated successfully")

preprocessor = (skip_start + skipper + skip_detdesc + LHCbPreprocessor +
                json_config + assertion_counter)
moore_preprocessor = skip_configure + skip_lbdd4hep + skip_detdesc + RecPreprocessor + skip_scheduler
