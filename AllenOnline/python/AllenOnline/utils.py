###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os, logging, json
from PyConf import configurable
from Allen.config import allen_json_sequence

log = logging.getLogger(__name__)


def get_allen_hlt1_decision_ids():
    """Read Allen HLT1 decision IDs using the JSON sequence file.

    """
    _, config_file_path = allen_json_sequence()
    try:
        with open(os.path.expandvars(config_file_path)) as config_file:
            config = json.load(config_file)
    except:
        log.error(
            "Could not find Allen sequence file %s; make sure you have built Allen",
            config_file_path)
        raise

    # Line names are stored in the JSON as a single string separated by commas
    barrier_algorithm_name = [
        a[1] for a in config["sequence"]["configured_algorithms"]
        if a[2] == "BarrierAlgorithm"
    ][0]
    active_lines = str(config[barrier_algorithm_name]["names_of_active_lines"])
    decision_names = [name + "Decision" for name in active_lines.split(",")]

    return {name: idx for idx, name in enumerate(decision_names, 1)}
