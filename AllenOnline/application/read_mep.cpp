/*****************************************************************************\
* (c) Copyright 2018-2020 CERN for the benefit of the LHCb Collaboration      *
\*****************************************************************************/
#include <cassert>
#include <cstring>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <map>
#include <string>
#include <unordered_set>

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <fmt/format.h>

#include <boost/algorithm/string.hpp>
#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/program_options.hpp>

#include <GaudiKernel/Bootstrap.h>
#include <GaudiKernel/IAppMgrUI.h>
#include <GaudiKernel/IProperty.h>
#include <GaudiKernel/IStateful.h>
#include <GaudiKernel/ISvcLocator.h>
#include <GaudiKernel/MsgStream.h>
#include <GaudiKernel/SmartIF.h>

#include <Event/ODIN.h>
#include <Event/RawBank.h>

#include <Allen/Logger.h>
#include <Allen/MEPTools.h>
#include <Allen/SliceUtils.h>
#include <Allen/read_mdf.hpp>
#include <Allen/sourceid.h>

#include <MDF/StreamDescriptor.h>

#include <EventBuilding/MEP_tools.hpp>
#include <EventBuilding/MFP_tools.hpp>

#include <AllenOnline/ReadMEP.h>
#include <AllenOnline/TransposeMEP.h>

using namespace std;
namespace po = boost::program_options;
namespace ba = boost::algorithm;

struct CaloRawBank {
  uint32_t        source_id = 0;
  uint32_t const* data      = nullptr;
  uint32_t const* end       = nullptr;

  // For MEP format
  CaloRawBank( const uint32_t sid, const char* fragment, const uint16_t s )
      : source_id{sid}
      , data{reinterpret_cast<uint32_t const*>( fragment )}
      , end{reinterpret_cast<uint32_t const*>( fragment + s )} {
    assert( s % sizeof( uint32_t ) == 0 );
  }
};

struct ODINRawBank {

  uint32_t const* data = nullptr;
  uint16_t        size = 0;

  /// Constructor from MEP layout
  ODINRawBank( const uint32_t, const char* fragment, uint16_t s ) {
    data = reinterpret_cast<uint32_t const*>( fragment );
    size = s;
  }
};

namespace {
  const std::array<std::tuple<char, std::uint16_t, short>, 5> event_type_chars = {
      {{'P', 0x02, 0}, {'N', 0x04, 1}, {'L', 0x08, 2}, {'1', 0x10, 3}, {'2', 0x20, 4}}};
}

int main( int argc, char* argv[] ) {

  string  filename;
  string  dump;
  ssize_t n_meps = 0;
  size_t  n_skip = 0;

  // Declare the supported options.
  po::options_description desc( "Allowed options" );
  // clang-format off
  desc.add_options()
    ( "help,h", "produce help message" )
    ( "filename", po::value<string>( &filename ), "filename pattern" )
    ( "n_meps,n", po::value<ssize_t>( &n_meps ), "number of MEPs" )
    ( "skip,s", po::value<size_t>( &n_skip )->default_value( 0 ), "number of events to skip" )
    ( "dump", po::value<string>( &dump ), "dump bank content (source_id_type,start_event[,end_event],bank_number" )
    ( "count-banks", "count raw banks by bank type" );
  // clang-format on

  po::positional_options_description p;
  p.add( "filename", 1 );
  p.add( "n_meps", 1 );

  po::variables_map vm;
  po::store( po::command_line_parser( argc, argv ).options( desc ).positional( p ).run(), vm );
  po::notify( vm );

  if ( vm.count( "help" ) ) {
    std::cout << desc << "\n";
    return 1;
  }

  SmartIF<IStateful> app  = Gaudi::createApplicationMgr();
  auto               prop = app.as<IProperty>();
  bool               sc   = prop->setProperty( "JobOptionsType", "\"NONE\"" ).isSuccess();
  sc &= app->configure();
  sc &= app->initialize();
  sc &= app->start();
  if ( !sc ) { return 1; }
  SmartIF<ISvcLocator> sloc   = app.as<ISvcLocator>();
  auto                 msgSvc = sloc->service<IMessageSvc>( "MessageSvc" );
  MsgStream            info{msgSvc.get(), "allen_mep_read"};
  info.activate();

  auto gaudi_exit = [&app]( int code ) {
    bool sc = app->stop().isSuccess();
    sc &= app->finalize();
    return code & !sc;
  };

  std::string dump_type;
  unsigned    dump_start = 0;
  long        dump_end   = 0;
  unsigned    dump_bank  = 0;
  if ( !dump.empty() ) {
    vector<string> entries;
    ba::split( entries, dump, boost::is_any_of( "," ) );

    if ( entries.size() != 3 && entries.size() != 4 ) {
      info << "Invalid dump specification: " << dump << "\n";
      return gaudi_exit( 1 );
    }

    for ( uint16_t i = 0; i < static_cast<uint16_t>( SourceIdSys::SourceIdSys_TDET ); ++i ) {
      auto const* dt = SourceId_sysstr( i << 11 );
      if ( dt != nullptr && entries[0] == std::string{dt} ) { dump_type = dt; }
    }
    if ( dump_type.empty() ) {
      info << "Invalid source type: " << entries[0] << "\n";
      return gaudi_exit( 1 );
    }

    dump_start = boost::lexical_cast<unsigned>( entries[1] );
    if ( entries.size() == 3 ) {
      dump_end  = dump_start + 1;
      dump_bank = boost::lexical_cast<unsigned>( entries[2] );
    } else {
      dump_end  = boost::lexical_cast<long>( entries[2] );
      dump_bank = boost::lexical_cast<unsigned>( entries[3] );
    }
  }

  // Some storage for reading the events into
  bool eof = false, success = false;

  auto input = LHCb::StreamDescriptor::bind( filename );
  if ( input.ioDesc != 0 ) {
    info << "Opened " << filename << endmsg;
  } else {
    info << "Failed to open file " << filename << " " << strerror( errno ) << endmsg;
    return gaudi_exit( 1 );
  }

  vector<char> data;

  EventIDs event_ids;

  MEP::Slices mep_slices( 1 );
  auto&       slice = mep_slices[0];

  Allen::Slices bank_slices;

  std::unordered_set bank_types = {BankTypes::ODIN, BankTypes::ECal};

  // 160 bytes per fragment, 60 fragments
  std::vector<char> calo_banks;
  calo_banks.resize( 9600 );

  std::vector<unsigned> number_of_banks( static_cast<unsigned>( LHCb::RawBank::LastType ), 0u );

  std::unordered_set<LHCb::RawBank::BankType> daq_errors;
  for ( int i = 0; i < LHCb::RawBank::LastType; ++i ) {
    auto bt = static_cast<LHCb::RawBank::BankType>( i );
    if ( LHCb::RawBank::typeName( bt ).rfind( "DaqError", 0 ) == 0 ) { daq_errors.emplace( bt ); }
  }

  unsigned daq_error_offset = *std::min_element( daq_errors.begin(), daq_errors.end() );
  std::array<std::vector<unsigned>, SourceIdSys::SourceIdSys_TDET> sd_daq_errors;

  for ( auto& counters : sd_daq_errors ) { counters.assign( daq_errors.size(), 0u ); }

  std::unordered_set<uint16_t> subdetectors;

  for ( ssize_t i_mep = 0; ( n_meps == -1 || i_mep < n_meps ) && !eof; ++i_mep ) {

    std::tie( eof, success, slice.mep, slice.packing_factor, slice.mep_data ) = MEP::read_mep( input, data, info );
    auto const* mep                                                           = slice.mep;
    if ( !success ) {
      return gaudi_exit( 1 );
    } else {
      std::cout << "Read mep with packing factor " << slice.packing_factor << " #MFPs: " << mep->header.n_MFPs << "\n";
    }

    if ( i_mep == 0 ) {
      event_ids.reserve( slice.packing_factor );
      slice.blocks.resize( mep->header.n_MFPs, MEP::Blocks::value_type{} );
      slice.offsets.resize( mep->header.n_MFPs );
      for ( auto& offsets : slice.offsets ) { offsets.resize( slice.packing_factor + 1 ); }
    }

    std::function<void( size_t )> bad_mfp = []( size_t source_id ) {
      auto const* sd = SourceId_sysstr( source_id );
      std::cout << "ERROR: bad MFP for " << sd << " with source ID " << source_id << "\n";
    };
    MEP::find_blocks( mep, slice.blocks, bad_mfp );

    MEP::fragment_offsets( slice.blocks, slice.offsets );

    auto [success, mfp_count, banks_version] = MEP::fill_counts( mep );

    if ( i_mep == 0 ) {
      bank_slices = allocate_slices(
          1, bank_types, [pf = slice.packing_factor, &mc = mfp_count]( auto bt ) -> std::tuple<size_t, size_t, size_t> {
            auto n_blocks = mc[to_integral( bt )];
            auto n_sizes  = pf * ( ( n_blocks + 1 ) / 2 + 1 );
            return {0ul, n_sizes, 2 + n_blocks + ( 1 + pf ) * ( 1 + n_blocks ) - 2};
          } );
    } else {
      reset_slice( bank_slices, 0, bank_types, event_ids, true );
    }

    MEP::mep_offsets( bank_slices, 0, bank_types, mfp_count, slice.blocks, slice.offsets, {0ul, slice.packing_factor},
                      false );

    // In MEP layout the fragmets are split into MFPs that are not
    // contiguous in memory. When the data is copied to the device the
    // MFPs are copied into device memory back-to-back, making them
    // contiguous; the offsets are prepared with this in mind.

    auto const& first_block = slice.blocks.front();
    std::cout << "MEP with packing: " << std::setw( 4 ) << first_block.header->n_banks
              << " event_id: " << std::setw( 6 ) << first_block.header->ev_id << "\n";

    // Decode first ODIN
    auto const  odin_index = to_integral( BankTypes::ODIN );
    auto const& odin_slice = bank_slices[odin_index][0];
    // There is always only a single ODIN MFP
    auto odin_banks = odin_slice.fragments[0];

    auto decode_odin = [& versions = banks_version, &odin_slice, &odin_banks]( unsigned event_number ) {
      auto const& odin_offsets = odin_slice.offsets;
      auto const& odin_sizes   = odin_slice.sizes;
      auto const& odin_types   = odin_slice.types;
      auto        odin_bank    = MEP::raw_bank<ODINRawBank>( odin_banks.data(), odin_offsets.data(), odin_sizes.data(),
                                                   odin_types.data(), event_number, 0 );
      LHCb::ODIN  odin;
      if ( versions[odin_index] == 7 ) {
        odin = LHCb::ODIN{{odin_bank.data, 10}};
      } else {
        odin = LHCb::ODIN::from_version<6>( {odin_bank.data, 10} );
      }
      return odin;
    };

    auto       first_odin = decode_odin( 0 );
    auto const tck        = first_odin.triggerConfigurationKey();
    std::cout << "ODIN version: " << static_cast<unsigned>( first_odin.version() ) << " TCK "
              << fmt::format( "{:#010x}", tck ) << " run: " << std::setw( 7 ) << first_odin.runNumber()
              << " event: " << std::setw( 12 ) << first_odin.eventNumber() << "\n";

    // Print block information
    std::string prev_det;
    unsigned    block_counter = 0;

    for ( size_t i_block = 0; i_block < slice.blocks.size(); ++i_block ) {

      auto const& block = slice.blocks[i_block];

      // block offsets are in number of 4-byte words
      auto const  source_id = block.header->src_id;
      auto const* dt        = SourceId_sysstr( source_id );
      std::string det       = dt != nullptr ? dt : "Unknown";
      std::string fill( 7 - det.size(), ' ' );

      subdetectors.insert( SourceId_sys( source_id ) );

      if ( prev_det != det ) {
        block_counter = 0;
        prev_det      = det;
      } else {
        ++block_counter;
      }

      size_t      padded_size = 0;
      auto const* bank_sizes  = block.bank_sizes;
      for ( size_t i = 0; i < block.header->n_banks; ++i ) {
        padded_size += bank_sizes[i] + EB::get_padding( bank_sizes[i], 1 << block.header->align );
      }

      bool dump_block = !dump.empty() && det == dump_type && dump_bank == block_counter;

      if ( dump.empty() || dump_block ) {
        std::cout << "MFP"
                  << " magic: 0x" << std::hex << block.header->magic << std::dec << " source_id: " << std::setw( 6 )
                  << source_id << " top5: " << std::setw( 2 ) << SourceId_sys( source_id ) << fill << " (" << det
                  << ") " << std::setw( 5 ) << SourceId_num( source_id ) << " version: " << std::setw( 2 )
                  << unsigned{block.header->block_version} << " align: " << std::setw( 3 )
                  << pow( 2, unsigned{block.header->align} ) << " size: " << std::setw( 8 )
                  << block.header->bytes() - block.header->header_size() << " padded_size: " << std::setw( 8 )
                  << padded_size << "\n";
      }

      size_t skip = n_skip;
      for ( unsigned evt = 0; evt < slice.packing_factor; ++evt ) {
        if ( skip != 0 && skip-- > 0 ) continue;

        // Count bank types and DAQ errors per SD
        auto bank_type = block.bank_types[evt];
        if ( daq_errors.count( static_cast<LHCb::RawBank::BankType>( bank_type ) ) ) {
          ++sd_daq_errors[SourceId_sys( source_id )][bank_type - daq_error_offset];
        }
        ++number_of_banks[bank_type];
      }

      if ( dump_block ) {
        for ( unsigned evt = dump_start; evt < ( dump_end == -1 ? slice.packing_factor : dump_end ); ++evt ) {
          if ( n_skip != 0 && n_skip-- > 0 ) continue;

          if ( dump_type == "ODIN" ) {
            auto odin = decode_odin( evt );

            // Build a string with 1 specific char per event type
            auto        event_type = odin.eventType();
            std::string evt_type   = "      ";
            for ( auto [c, b, p] : event_type_chars ) {
              if ( event_type & b ) evt_type[p] = c;
            }

            // Add calibration triggers as another char to the same string
            if ( odin.triggerType() == static_cast<unsigned short>( LHCb::ODIN::TriggerTypes::CalibrationTrigger ) )
              evt_type[5] = 'C';
            if ( std::all_of( evt_type.begin(), evt_type.end(), []( char const a ) { return a == ' '; } ) ) {
              std::stringstream tt;
              tt << "U" << std::setw( 5 ) << unsigned{odin.triggerType()};
              evt_type = tt.str();
            }

            unsigned const bank_type = MEP::bank_type( odin_banks.data(), odin_slice.types.data(), evt, 0 );
            std::cout << "ODIN version" << std::setw( 3 ) << unsigned{block.header->block_version} << " type "
                      << std::setw( 2 ) << bank_type << " run: " << std::setw( 6 ) << odin.runNumber() << " event "
                      << std::setw( 12 ) << odin.eventNumber() << " " << evt_type << " "
                      << ( odin.isTAE() ? " TAE" : "    " ) << " bunch ID " << std::setw( 6 ) << odin.bunchId() << "\n";
          } else {
            MDF::dump_hex( block.payload + slice.offsets[i_block][evt], block.bank_sizes[evt] );
          }
        }
      } else if ( n_skip != 0 ) {
        n_skip -= std::min( n_skip, size_t{slice.packing_factor} );
      }
    }
  }

  // Print number of banks per bank type for those that have a non-zero number
  if ( vm.count( "count-banks" ) ) {
    std::cout << "\nnumber of banks:\n";
    for ( int i = LHCb::RawBank::L0Calo; i < LHCb::RawBank::LastType; ++i ) {
      auto n_banks = number_of_banks[i];
      if ( n_banks > 0 ) {
        std::cout << std::left << std::setw( 25 )
                  << LHCb::RawBank::typeName( static_cast<LHCb::RawBank::BankType>( i ) ) << std::setw( 8 ) << n_banks
                  << "\n";
      }
    }

    // Print the number of DAQ error banks per SD to allow disentangling them
    std::cout << "\nnumber of DAQ error banks per SD:\n";
    std::vector<std::tuple<LHCb::RawBank::BankType, std::string>> daq_error_names;
    daq_error_names.reserve( daq_errors.size() );
    std::transform( daq_errors.begin(), daq_errors.end(), std::back_inserter( daq_error_names ), []( auto bt ) {
      return std::tuple{bt, LHCb::RawBank::typeName( bt )};
    } );
    std::sort( daq_error_names.begin(), daq_error_names.end() );

    std::vector<std::tuple<uint16_t, std::string>> sds;
    for ( uint16_t sd : subdetectors ) {
      auto const* n = SourceId_sysstr( sd << 11 );
      if ( n != nullptr ) { sds.emplace_back( sd, n ); }
    }
    std::sort( sds.begin(), sds.end() );

    for ( size_t i = 0; i < daq_error_names.size() + 1; ++i ) {
      if ( i == 0 ) {
        std::cout << std::setw( 25 ) << "";
      } else {
        auto [bt, name] = daq_error_names[i - 1];
        std::cout << std::left << std::setw( 25 ) << name;
      }

      for ( auto [sd, sdn] : sds ) {
        if ( i == 0 ) {
          std::cout << " " << std::setw( 7 ) << sdn;
        } else {
          std::cout << " " << std::setw( 7 ) << sd_daq_errors[sd][i - 1];
        }
      }
      std::cout << "\n";
    }
    std::cout << "\n";
  }

  return gaudi_exit( 0 );
}
