/*****************************************************************************\
* (c) Copyright 2018-2020 CERN for the benefit of the LHCb Collaboration      *
\*****************************************************************************/
#include <iostream>
#include <map>
#include <optional>
#include <string>
#include <vector>

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <boost/algorithm/string.hpp>
#include <boost/program_options.hpp>

#include <AllenOnline/MPIConfig.h>
#include <AllenOnline/ReadMEP.h>

namespace MPI {
  int rank;

  std::string rank_str() {
    if ( rank == receiver ) {
      return "MPI::Receiver: ";
    } else {
      return "MPI::Sender: ";
    }
  }
} // namespace MPI

namespace {
  namespace po = boost::program_options;
  namespace ba = boost::algorithm;
} // namespace

int main( int argc, char* argv[] ) {

  std::string mep_input;
  size_t      window_size    = 4;
  bool        non_stop       = false;
  size_t      number_of_meps = 0;

  po::options_description desc( "Allowed options" );
  desc.add_options()( "help", "produce help message" )( "mep", po::value<std::string>( &mep_input ),
                                                        "mep files as comma-separated list" )(
      "mpi-window-size", po::value<size_t>( &window_size )->default_value( 4 ),
      "MPI sliding window size" )( "non-stop", po::value<bool>( &non_stop )->default_value( false ), "input file" )(
      "n-meps,n", po::value<size_t>( &number_of_meps )->default_value( 0 ), "#MEPs" );

  po::positional_options_description p;
  p.add( "mep", 1 );

  po::variables_map vm;
  po::store( po::command_line_parser( argc, argv ).options( desc ).positional( p ).run(), vm );
  po::notify( vm );

  if ( vm.count( "help" ) ) {
    std::cout << desc << "\n";
    return 1;
  }

  // The sender is in charge of reading all MDF files and passing
  // them to the receiver.

  if ( !vm.count( "mep" ) ) {
    std::cout << MPI::rank_str() << "Required argument --mep not supplied. Exiting application.\n";
    return -1;
  }

#if defined( HAVE_MPI )
  std::vector<std::string> connections;
  ba::split( connections, mep_input, ba::is_any_of( "," ) );

  // Create requests of appropiate size
  std::vector<MPI_Request> requests( window_size );

  // Read all files in connections
  std::vector<std::tuple<EB::MEP const*, gsl::span<char const>>> meps;

  std::cout << MPI::rank_str() << "Reading "
            << ( number_of_meps != 0 ? std::to_string( number_of_meps ) : std::string{"all"} ) << " meps from files\n";

  std::vector<char>     data;
  gsl::span<char const> mep_span;
  size_t                n_meps_read = 0;

  std::optional<unsigned> packing_factor = std::nullopt;

  for ( const auto& connection : connections ) {
    bool           eof = false, success = true;
    EB::MEP const* mep = nullptr;
    unsigned       pf  = 0;

    auto input = LHCb::StreamDescriptor::bind( connection );
    if ( input.ioDesc != 0 ) { std::cout << "Opened " << connection << "\n"; }

    while ( success && !eof ) {
      std::cout << "." << std::flush;

      std::tie( eof, success, mep, pf, mep_span ) = MEP::read_mep( input, data, std::cout, std::cerr );
      if ( !packing_factor ) {
        packing_factor = pf;
      } else if ( *packing_factor != pf ) {
        std::cout << "Got MEP with different packing factor: " << pf << " instead of: " << *packing_factor << "\n";
        return 1;
      }

      if ( !eof && success ) {
        char* contents = nullptr;
        MPI_Alloc_mem( mep_span.size(), MPI_INFO_NULL, &contents );

        // Populate contents with stream buf
        std::copy_n( mep_span.data(), mep_span.size(), contents );
        ++n_meps_read;

        meps.emplace_back( mep, gsl::span<char>{contents, mep_span.size()} );
      }
      if ( n_meps_read >= number_of_meps && number_of_meps != 0 ) {
        input.close();
        goto send;
      }
    }
    input.close();
  }

send:

  if ( meps.empty() || !packing_factor ) {
    std::cout << "Failed to read MEPs from file\n";
    return 1;
  }

  // MPI initialization
  MPI_Init( &argc, &argv );

  // Communication size
  int comm_size;
  MPI_Comm_size( MPI_COMM_WORLD, &comm_size );
  if ( comm_size > MPI::comm_size ) {
    std::cout << "This program requires at most " << MPI::comm_size << " processes.\n";
    return -1;
  }

  // MPI: Who am I?
  MPI_Comm_rank( MPI_COMM_WORLD, &MPI::rank );

  auto const& first_mep = std::get<0>( *meps.begin() );

  size_t pf = *packing_factor;
  std::cout << "\n"
            << MPI::rank_str() << "MEP header: " << first_mep->header.n_MFPs << ", " << pf << ", " << first_mep->bytes()
            << "\n";
  MPI_Send( &pf, 1, MPI_SIZE_T, MPI::receiver, MPI::message::packing_factor, MPI_COMM_WORLD );

  MPI_Send( &n_meps_read, 1, MPI_SIZE_T, MPI::receiver, MPI::message::number_of_meps, MPI_COMM_WORLD );

  // Test: Send all the files
  size_t current_mep = 0;
  while ( non_stop || current_mep < meps.size() ) {

    // Get event data
    auto const [mep, mep_span]       = meps[current_mep];
    const char*  current_event_start = mep_span.data();
    const size_t current_event_size  = mep_span.size_bytes();

    // Notify the event size
    MPI_Send( &current_event_size, 1, MPI_SIZE_T, MPI::receiver, MPI::message::event_size, MPI_COMM_WORLD );

    // Number of full-size (MPI::mdf_chunk_size) messages
    size_t n_messages = current_event_size / MPI::mdf_chunk_size;
    // Size of the last message (if the MFP size is not a multiple of MPI::mdf_chunk_size)
    size_t rest = current_event_size - n_messages * MPI::mdf_chunk_size;
    // Number of parallel sends
    size_t n_sends = n_messages > window_size ? window_size : n_messages;

    // Initial parallel sends
    for ( size_t k = 0; k < n_sends; k++ ) {
      const char* message = current_event_start + k * MPI::mdf_chunk_size;
      MPI_Isend( message, MPI::mdf_chunk_size, MPI_BYTE, MPI::receiver, MPI::message::event_send_tag_start + k,
                 MPI_COMM_WORLD, &requests[k] );
    }
    // Sliding window sends
    for ( size_t k = n_sends; k < n_messages; k++ ) {
      int r;
      MPI_Waitany( window_size, requests.data(), &r, MPI_STATUS_IGNORE );
      const char* message = current_event_start + k * MPI::mdf_chunk_size;
      MPI_Isend( message, MPI::mdf_chunk_size, MPI_BYTE, MPI::receiver, MPI::message::event_send_tag_start + k,
                 MPI_COMM_WORLD, &requests[r] );
    }
    // Last send (if necessary)
    if ( rest ) {
      int r;
      MPI_Waitany( window_size, requests.data(), &r, MPI_STATUS_IGNORE );
      const char* message = current_event_start + n_messages * MPI::mdf_chunk_size;
      MPI_Isend( message, rest, MPI_BYTE, MPI::receiver, MPI::message::event_send_tag_start + n_messages,
                 MPI_COMM_WORLD, &requests[r] );
    }
    // Wait until all chunks have been sent
    MPI_Waitall( n_sends, requests.data(), MPI_STATUSES_IGNORE );

    if ( non_stop ) {
      current_mep = ( current_mep + 1 ) % meps.size();
    } else {
      ++current_mep;
    }
  }

  MPI_Finalize();
  return 0;
#else
  std::cerr << "Not built with MPI support\n";
  return 1;
#endif
}
