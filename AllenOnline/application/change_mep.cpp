/*****************************************************************************\
* (c) Copyright 2018-2020 CERN for the benefit of the LHCb Collaboration      *
\*****************************************************************************/
#include <cassert>
#include <cstring>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <map>
#include <string>
#include <unordered_set>

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <fmt/format.h>

#include <boost/lexical_cast.hpp>
#include <boost/program_options.hpp>

#include <GaudiKernel/Bootstrap.h>
#include <GaudiKernel/IAppMgrUI.h>
#include <GaudiKernel/IProperty.h>
#include <GaudiKernel/IStateful.h>
#include <GaudiKernel/ISvcLocator.h>
#include <GaudiKernel/MsgStream.h>
#include <GaudiKernel/SmartIF.h>

#include <Event/ODIN.h>
#include <Event/RawBank.h>

#include <Allen/read_mdf.hpp>

#include <MDF/StreamDescriptor.h>

#include <EventBuilding/MEP_tools.hpp>
#include <EventBuilding/MFP_tools.hpp>

#include <AllenOnline/ReadMEP.h>
#include <AllenOnline/TransposeMEP.h>

using namespace std;
namespace po = boost::program_options;

int main( int argc, char* argv[] ) {

  string      filename, output_filename;
  std::string tck_str;
  ssize_t     n_meps = 0;
  unsigned    run;

  // Declare the supported options.
  po::options_description desc( "Allowed options" );
  // clang-format off
  desc.add_options()
    ( "help,h", "produce help message" )
    ( "filename", po::value<string>( &filename ), "filename pattern" )
    ( "output",   po::value<string>( &output_filename ), "output filename" )
    ( "n_meps,n", po::value<ssize_t>( &n_meps ), "number of MEPs" )
    ( "tck",      po::value<std::string>( &tck_str ), "new TCK" )
    ( "run",      po::value<unsigned>( &run ), "new run number" );
  // clang-format on

  po::positional_options_description p;
  p.add( "filename", 1 );
  p.add( "n_meps", 1 );
  p.add( "output", 1 );

  po::variables_map vm;
  po::store( po::command_line_parser( argc, argv ).options( desc ).positional( p ).run(), vm );
  po::notify( vm );

  if ( vm.count( "help" ) ) {
    std::cout << desc << "\n";
    return 1;
  }

  unsigned tck = std::stoi( tck_str, nullptr, 16 );

  SmartIF<IStateful> app  = Gaudi::createApplicationMgr();
  auto               prop = app.as<IProperty>();
  bool               sc   = prop->setProperty( "JobOptionsType", "\"NONE\"" ).isSuccess();
  sc &= app->configure();
  sc &= app->initialize();
  sc &= app->start();
  if ( !sc ) { return 1; }
  SmartIF<ISvcLocator> sloc   = app.as<ISvcLocator>();
  auto                 msgSvc = sloc->service<IMessageSvc>( "MessageSvc" );
  MsgStream            info{msgSvc.get(), "allen_mep_read"};
  info.activate();

  auto gaudi_exit = [&app]( int code ) {
    bool sc = app->stop().isSuccess();
    sc &= app->finalize();
    return code & !sc;
  };

  // Some storage for reading the events into
  bool eof = false, success = false;

  auto input = LHCb::StreamDescriptor::bind( filename );
  if ( input.ioDesc != 0 ) {
    info << "Opened " << filename << endmsg;
  } else {
    info << "Failed to open file " << filename << " " << strerror( errno ) << endmsg;
    return gaudi_exit( 1 );
  }

  vector<char> data;

  EventIDs event_ids;

  MEP::Slices mep_slices( 1 );
  auto&       slice = mep_slices[0];

  for ( ssize_t i_mep = 0; ( n_meps == -1 || i_mep < n_meps ) && !eof; ++i_mep ) {

    std::tie( eof, success, slice.mep, slice.packing_factor, slice.mep_data ) = MEP::read_mep( input, data, info );
    auto const* mep                                                           = slice.mep;
    if ( !success ) {
      return gaudi_exit( 1 );
    } else {
      std::cout << "Read mep with packing factor " << slice.packing_factor << " #MFPs: " << mep->header.n_MFPs << "\n";
    }

    if ( i_mep == 0 ) {
      slice.blocks.resize( mep->header.n_MFPs, MEP::Blocks::value_type{} );
      slice.offsets.resize( mep->header.n_MFPs );
      for ( auto& offsets : slice.offsets ) { offsets.resize( slice.packing_factor + 1 ); }
    }

    std::function<void( size_t )> bad_mfp = []( size_t source_id ) {
      auto const* sd = SourceId_sysstr( source_id );
      std::cout << "ERROR: bad MFP for " << sd << " with source ID " << source_id << "\n";
    };
    auto odin_block_index = MEP::find_blocks( mep, slice.blocks, bad_mfp );
    if ( !odin_block_index ) {
      std::cout << "ERROR: No ODIN MFP"
                << "\n";
      return gaudi_exit( 1 );
    }

    MEP::fragment_offsets( slice.blocks, slice.offsets );

    auto const& odin_block = slice.blocks[*odin_block_index];
    std::cout << "MEP with packing: " << std::setw( 4 ) << odin_block.header->n_banks << " event_id: " << std::setw( 6 )
              << odin_block.header->ev_id << "\n";

    // Decode first ODIN
    auto const& odin_offsets = slice.offsets[*odin_block_index];
    auto        decode_odin  = [version = odin_block.header->block_version, &odin_block,
                        &odin_offsets]( unsigned event_number ) {
      auto const* odin_data = reinterpret_cast<unsigned const*>( odin_block.payload + odin_offsets[event_number] );
      LHCb::ODIN  odin;
      if ( version == 7 ) {
        odin = LHCb::ODIN{{odin_data, 10}};
      } else {
        odin = LHCb::ODIN::from_version<6>( {odin_data, 10} );
      }
      return odin;
    };

    auto first_odin = decode_odin( 0 );
    std::cout << "ODIN version: " << static_cast<unsigned>( first_odin.version() ) << " run: " << std::setw( 7 )
              << first_odin.runNumber() << " event: " << std::setw( 12 ) << first_odin.eventNumber() << "\n";
    if ( !tck_str.empty() ) {
      std::cout << "Changing TCK from " << fmt::format( "{:#010x}", first_odin.triggerConfigurationKey() ) << " to "
                << fmt::format( "{:#010x}", tck ) << "\n";
    }
    if ( run != 0 ) {
      std::cout << "Changing run from " << std::to_string( first_odin.runNumber() ) << " to " << std::to_string( run )
                << "\n";
    }

    char* payload = const_cast<char*>( odin_block.payload );
    for ( unsigned evt = 0; evt < slice.packing_factor; ++evt ) {
      auto* odin_data = reinterpret_cast<unsigned*>( payload + odin_offsets[evt] );
      using namespace LHCb::ODINImplementation;
      if ( !tck_str.empty() ) {
        details::set_bits<LHCb::ODIN::TriggerConfigurationKeySize, LHCb::ODIN::TriggerConfigurationKeyOffset>(
            {odin_data, 10}, tck );
      }
      if ( run != 0 ) {
        details::set_bits<LHCb::ODIN::RunNumberSize, LHCb::ODIN::RunNumberOffset>( {odin_data, 10}, run );
      }
    }
  }

  std::ofstream output_file{output_filename, std::ios::binary};
  output_file.write( slice.mep_data.data(), slice.mep_data.size() );
  output_file.close();

  return gaudi_exit( 0 );
}
