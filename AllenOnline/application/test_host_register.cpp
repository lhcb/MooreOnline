/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <Backend/BackendCommon.h>
#include <MBM/bmdef.h>
#include <RTL/rtl.h>

#include <chrono>
#include <iomanip>
#include <iostream>
#include <thread>

int main() {

  const std::string buffer_name = "Events_0_TDET";
  auto              pn          = RTL::processName() + ".0";
  BMID              bmid        = ::mbm_include_read( buffer_name.c_str(), pn.c_str(), 32768, BM_COM_FIFO );

  if ( bmid == MBM_INV_DESC ) {
    std::cerr << "MBM: Failed to connect to MBM buffer " << buffer_name << std::endl;
    return -1;
  }

  // register buffer manager memory with the device runtime
  size_t buffer_size    = 0;
  char*  buffer_address = const_cast<char*>( ::mbm_buffer_address( bmid ) );
  ::mbm_buffer_size( bmid, &buffer_size );

  std::cout << "MBM buffer: " << reinterpret_cast<void*>( buffer_address ) << " " << buffer_size << "\n";

  bool runtime_error = false;
  try {
    Allen::set_device( 0, 0 );
  } catch ( const std::invalid_argument& e ) {
    std::cerr << "Failed to set device: " << e.what() << "\n";
    runtime_error = true;
  }

  if ( !runtime_error ) {
    try {
      Allen::host_register( buffer_address, buffer_size, Allen::hostRegisterReadOnly );
      std::cout << "Successfully registered\n";
    } catch ( const std::invalid_argument& e ) { std::cerr << "Failed to register: " << e.what() << "\n"; }
  }

  using namespace std::chrono_literals;
  std::this_thread::sleep_for( 5s );

  ::mbm_exclude( bmid );

  return 0;
}
