/*****************************************************************************\
* (c) Copyright 2018-2020 CERN for the benefit of the LHCb Collaboration      *
\*****************************************************************************/
#include <cassert>
#include <cstring>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <map>
#include <string>
#include <unordered_set>

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <boost/algorithm/string.hpp>
#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/program_options.hpp>

#include <GaudiKernel/Bootstrap.h>
#include <GaudiKernel/IAppMgrUI.h>
#include <GaudiKernel/IProperty.h>
#include <GaudiKernel/IStateful.h>
#include <GaudiKernel/ISvcLocator.h>
#include <GaudiKernel/MsgStream.h>
#include <GaudiKernel/SmartIF.h>

#include <Event/ODIN.h>
#include <Event/RawBank.h>

#include <Allen/Logger.h>
#include <Allen/MEPTools.h>
#include <Allen/SliceUtils.h>
#include <Allen/read_mdf.hpp>
#include <Allen/sourceid.h>

#include <MDF/StreamDescriptor.h>

#include <EventBuilding/MEP_tools.hpp>
#include <EventBuilding/MFP_tools.hpp>

#include <AllenOnline/ReadMEP.h>
#include <AllenOnline/TransposeMEP.h>

using namespace std;
namespace po = boost::program_options;
namespace ba = boost::algorithm;

struct CaloRawBank {
  uint32_t        source_id = 0;
  uint32_t const* data      = nullptr;
  uint32_t const* end       = nullptr;

  // For MEP format
  CaloRawBank( const uint32_t sid, const char* fragment, const uint16_t s )
      : source_id{sid}
      , data{reinterpret_cast<uint32_t const*>( fragment )}
      , end{reinterpret_cast<uint32_t const*>( fragment + s )} {
    assert( s % sizeof( uint32_t ) == 0 );
  }
};

struct ODINRawBank {

  uint32_t const* data = nullptr;
  uint16_t        size = 0;

  /// Constructor from MEP layout
  ODINRawBank( const uint32_t, const char* fragment, uint16_t s ) {
    data = reinterpret_cast<uint32_t const*>( fragment );
    size = s;
  }
};

std::vector<char> contiguous_mfps( Allen::Slice const& mep_data ) {
  // To make direct use of the offsets, the MFPs need to be copied
  // into temporary storage
  auto const&  mfps = mep_data.fragments;
  vector<char> mep_fragments( mep_data.fragments_mem_size, '\0' );
  char*        destination = &mep_fragments[0];
  for ( gsl::span<char const> mfp : mfps ) {
    ::memcpy( destination, mfp.data(), mfp.size_bytes() );
    destination += mfp.size_bytes();
  }
  return mep_fragments;
}

int main( int argc, char* argv[] ) {

  string filename;
  size_t eps         = 0;
  size_t n_intervals = 100;

  // Declare the supported options.
  // clang-format off
  po::options_description desc( "Allowed options" );
  desc.add_options()
    ( "help,h", "produce help message" )
    ( "filename",           po::value<string>( &filename ),    "filename pattern" )
    ( "intervals,i",        po::value<size_t>( &n_intervals ), "number of intervals to run" )
    ( "events-per-slice,n", po::value<size_t>( &eps ),         "events per slice" );
  // clang-format on

  po::positional_options_description p;
  p.add( "filename", 1 );

  po::variables_map vm;
  po::store( po::command_line_parser( argc, argv ).options( desc ).positional( p ).run(), vm );
  po::notify( vm );

  if ( vm.count( "help" ) ) {
    std::cout << desc << "\n";
    return 1;
  }

  SmartIF<IStateful> app  = Gaudi::createApplicationMgr();
  auto               prop = app.as<IProperty>();
  bool               sc   = prop->setProperty( "JobOptionsType", "\"NONE\"" ).isSuccess();
  sc &= app->configure();
  sc &= app->initialize();
  sc &= app->start();
  if ( !sc ) { return 1; }
  SmartIF<ISvcLocator> sloc   = app.as<ISvcLocator>();
  auto                 msgSvc = sloc->service<IMessageSvc>( "MessageSvc" );
  MsgStream            info{msgSvc.get(), "allen_mep_read"};
  info.activate();

  auto gaudi_exit = [&app]( int code ) {
    bool sc = app->stop().isSuccess();
    sc &= app->finalize();
    return code & !sc;
  };

  // Some storage for reading the events into
  bool eof = false, success = false;

  auto input = LHCb::StreamDescriptor::bind( filename );
  if ( input.ioDesc != 0 ) {
    info << "Opened " << filename << endmsg;
  } else {
    info << "Failed to open file " << filename << " " << strerror( errno ) << endmsg;
    return gaudi_exit( 1 );
  }

  vector<char> data;

  EventIDs event_ids;

  MEP::Slices mep_slices( 1 );
  auto&       slice = mep_slices[0];

  Allen::Slices bank_slices;

  std::unordered_set bank_types = {BankTypes::ODIN, BankTypes::VP, BankTypes::FT, BankTypes::MUON, BankTypes::ECal};

  std::tie( eof, success, slice.mep, slice.packing_factor, slice.mep_data ) = MEP::read_mep( input, data, info );
  auto const* mep                                                           = slice.mep;
  if ( !success ) {
    return gaudi_exit( 1 );
  } else {
    info << "Read mep with packing factor " << slice.packing_factor << " #MFPs: " << mep->header.n_MFPs << endmsg;
  }

  event_ids.reserve( slice.packing_factor );
  slice.blocks.resize( mep->header.n_MFPs, MEP::Blocks::value_type{} );
  slice.offsets.resize( mep->header.n_MFPs );
  for ( auto& offsets : slice.offsets ) { offsets.resize( slice.packing_factor + 1 ); }

  std::function<void( size_t )> bad_mfp = [&info]( size_t source_id ) {
    auto const* sd = SourceId_sysstr( source_id );
    info << "ERROR: bad MFP for " << sd << " with source ID " << source_id << endmsg;
  };
  MEP::find_blocks( mep, slice.blocks, bad_mfp );

  MEP::fragment_offsets( slice.blocks, slice.offsets );

  auto [count_success, mfp_count, banks_version] = MEP::fill_counts( mep );

  bank_slices =
      allocate_slices( 1, bank_types, [eps, &mc = mfp_count]( auto bt ) -> std::tuple<size_t, size_t, size_t> {
        auto n_blocks = mc[to_integral( bt )];
        auto n_sizes  = eps * ( ( n_blocks + 1 ) / 2 + 1 );
        return {0ul, n_sizes, 2 + n_blocks + ( 1 + eps ) * ( 1 + n_blocks ) - 2};
      } );

  std::vector<std::tuple<size_t, size_t>> intervals;

  auto n_ivs = slice.packing_factor / eps;
  auto rest  = slice.packing_factor % eps;
  if ( rest ) {
    info << "Set interval (rest): " << n_ivs * eps << "," << n_ivs * eps + rest << endmsg;
    intervals.emplace_back( n_ivs * eps, n_ivs * eps + rest );
  }
  for ( size_t i = n_ivs; i != 0; --i ) {
    info << "Set interval: " << ( i - 1 ) * eps << "," << i * eps << endmsg;
    intervals.emplace_back( ( i - 1 ) * eps, i * eps );
  }
  auto interval = intervals.begin();

  auto start = std::chrono::system_clock::now();

  for ( size_t i = 0; i < n_intervals; ++i ) {
    reset_slice( bank_slices, 0, bank_types, event_ids, true );
    MEP::mep_offsets( bank_slices, 0, bank_types, mfp_count, slice.blocks, slice.offsets, *interval, false );
    ++interval;
    if ( interval == intervals.end() ) interval = intervals.begin();
  }

  auto                          end     = std::chrono::system_clock::now();
  std::chrono::duration<double> elapsed = end - start;
  info << std::setprecision( 2 ) << std::fixed << n_intervals / elapsed.count() << " intervals/s" << endmsg;

  return gaudi_exit( 0 );
}
