/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "OnlineRawBanks.h"

#include "Event/RawBank.h"
#include "Event/RawEvent.h"
#include "LHCbAlgs/Transformer.h"
#include <map>
#include <string>

namespace Online {

  /** @class RawEventToBanks
   *
   * Transform a LHCb::RawEvent into a vector of banks as required by Online::OutputAlg.
   */
  class RawEventToBanks final : public LHCb::Algorithm::Transformer<OnlineRawBanks( LHCb::RawEvent const& )> {
  public:
    RawEventToBanks( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer( name, pSvcLocator, KeyValue{"RawEvent", LHCb::RawEventLocation::Default},
                       KeyValue{"RawData", "DAQ/RawData"} ){};

    OnlineRawBanks operator()( LHCb::RawEvent const& rawEvent ) const override {
      OnlineRawBanks rawData;
      rawData.reserve( rawEvent.size() );
      for ( auto const type : LHCb::RawBank::types() ) {
        for ( const LHCb::RawBank* b : rawEvent.banks( type ) ) {
          if ( !b ) continue;
          rawData.emplace_back( b, b->data() );
        }
      }
      return rawData;
    }
  };

}; // namespace Online

DECLARE_COMPONENT( Online::RawEventToBanks )
