/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "GaudiKernel/IRunable.h"
#include "GaudiKernel/Service.h"
class DummyRunable : public extends<Service, IRunable> {
public:
  /// inherit contructor
  using extends::extends;
  /// IRunable implementation : Run the class implementation
  StatusCode run() override { return StatusCode::SUCCESS; }
};
DECLARE_COMPONENT( DummyRunable )
