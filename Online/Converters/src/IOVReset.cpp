/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include <Event/ODIN.h>
#include <GaudiKernel/ServiceHandle.h>
#include <LHCbAlgs/Consumer.h>

#ifdef USE_DD4HEP
#  include <LbDD4hep/IDD4hepSvc.h>
#endif

namespace LHCb {

  /**
   * Clear the IOV slice cache when a change of run is detected.
   * This is a temporary workaround for the fact that IOVs are currently open ended.
   */
  class IOVReset : public Algorithm::Consumer<void( const ODIN& )> {
  public:
    IOVReset( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator, KeyValue{"ODIN", ODINLocation::Default} ) {}

    void operator()( [[maybe_unused]] const ODIN& odin ) const override {
#ifdef USE_DD4HEP
      auto runNumber = m_runNumber;
      if ( odin.runNumber() > runNumber ) {
        // note that multiple threads can enter here concurrently
        m_runNumber = odin.runNumber();
        // force a reload of the conditions at the next possible occasion
        warning() << "Got new run " << odin.runNumber() << ": clearing condition cache." << endmsg;
        m_dd4hepSvc->clear_slice_cache(); // thread safe
      }
#endif
    }

  private:
#ifdef USE_DD4HEP
    ServiceHandle<Det::LbDD4hep::IDD4hepSvc> m_dd4hepSvc{this, "DD4hepSvc", "LHCb::Det::LbDD4hep::DD4hepSvc",
                                                         "DD4Hep Service"};
    mutable unsigned int                     m_runNumber = 0;
#endif
  };

  DECLARE_COMPONENT( IOVReset )

} // namespace LHCb
