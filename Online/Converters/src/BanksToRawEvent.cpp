/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "OnlineRawBanks.h"

#include "Event/RawBank.h"
#include "Event/RawEvent.h"
#include "LHCbAlgs/Transformer.h"
#include <map>
#include <string>

namespace Online {

  /** @class BanksToRawEvent
   *
   * Transform a vector of banks as produced by Online::InputAlg into LHCb::RawEvent.
   */
  class BanksToRawEvent final : public Gaudi::Functional::Transformer<LHCb::RawEvent( OnlineRawBanks const& )> {
  public:
    BanksToRawEvent( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer( name, pSvcLocator, KeyValue{"RawData", "DAQ/RawData"},
                       KeyValue{"RawEvent", LHCb::RawEventLocation::Default} ){};

    LHCb::RawEvent operator()( OnlineRawBanks const& rawData ) const override {
      LHCb::RawEvent outputRawEvent;
      for ( auto const& [header, data] : rawData ) {
        if ( !header ) {
          ++m_missing_header;
          continue;
        }
        if ( !data ) {
          ++m_missing_data;
          continue;
        }
        if ( m_bankType == LHCb ::RawBank::BankType::LastType ||
             LHCb ::RawBank::BankType( header->type() ) == m_bankType ) {
          outputRawEvent.adoptBank( outputRawEvent.createBank( header->sourceID(),
                                                               LHCb::RawBank::BankType( header->type() ),
                                                               header->version(), header->size(), data ),
                                    true );
        }
      }
      return outputRawEvent;
    };

  private:
    LHCb ::RawBank::BankType     m_bankType = LHCb ::RawBank::BankType::LastType;
    Gaudi::Property<std::string> m_bankTypeStr{this,
                                               "BankType",
                                               "ALL",
                                               [this]( auto& ) {
                                                 if ( m_bankTypeStr == "ALL" ) {
                                                   m_bankType = LHCb ::RawBank::BankType::LastType;
                                                 } else {
                                                   parse( m_bankType, m_bankTypeStr ).orThrow();
                                                 }
                                               },
                                               Gaudi::Details::Property::ImmediatelyInvokeHandler{true},
                                               "BankType to select, use ALL for all banks"};

    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_missing_header{
        this, "Raw bank header is nullptr, skipping bank", 100};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_missing_data{this, "Raw bank data is nullptr, skipping bank",
                                                                       100};
  };

} // namespace Online

DECLARE_COMPONENT( Online::BanksToRawEvent )
