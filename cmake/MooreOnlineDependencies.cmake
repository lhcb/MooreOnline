###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
if(NOT COMMAND lhcb_find_package)
  # Look for LHCb find_package wrapper
  find_file(LHCbFindPackage_FILE LHCbFindPackage.cmake)
  if(LHCbFindPackage_FILE)
      include(${LHCbFindPackage_FILE})
  else()
      # if not found, use the standard find_package
      macro(lhcb_find_package)
          find_package(${ARGV})
      endmacro()
  endif()
endif()

# -- Public dependencies
lhcb_find_package(Moore REQUIRED)
lhcb_find_package(Online REQUIRED)
lhcb_find_package(Alignment REQUIRED)

find_package(PkgConfig REQUIRED)
pkg_check_modules(zmq libzmq REQUIRED IMPORTED_TARGET)  # for ZeroMQ
pkg_check_modules(sodium libsodium REQUIRED IMPORTED_TARGET)

# -- Private dependencies
if(WITH_MooreOnline_PRIVATE_DEPENDENCIES)
    find_package(Python REQUIRED Interpreter)
    find_package(MPI QUIET COMPONENTS C CXX)
    find_package(cppgsl REQUIRED)
    find_package(Boost REQUIRED program_options)

    pkg_check_modules(hwloc QUIET IMPORTED_TARGET hwloc)
endif()
