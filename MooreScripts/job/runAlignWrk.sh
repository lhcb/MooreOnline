#!/bin/bash
###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
set -euo pipefail
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
echo "starting analyzer of activity ${ACTIVITY}"
echo "bianry tag ${BINARY_TAG}"

##########added to get environment
## get variables like ACTIVITY
export CMTCONFIG=x86_64_v2-el9-gcc12-do0;
#
. /group/online/dataflow/cmtuser/OnlineRelease/setup.${CMTCONFIG}.vars;
. ${FARMCONFIGROOT}/job/createEnvironment.sh  $*;
act=${ACTIVITY}
echo "INFO: Iterator activity ${ACTIVITY} ${PARTITION}"

export BINARY_TAG=x86_64_v3-el9-gcc12-opt
##########

sub_activity=${ACTIVITY#"Alignment|"}
echo "RUNNING ACTIVITY " ${DIR}/${sub_activity}/runAnalyzer.sh
source ${DIR}/${sub_activity}/runAnalyzer.sh
