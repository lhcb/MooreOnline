#!/bin/bash
###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
set -euo pipefail
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source "$DIR/../setupTask.sh"

setup_options_path MONITORING

vp_options=$HUMBOLDTROOT/options/UseRetina.py

#first_run=$(grep DeferredRuns ${INFO_OPTIONS%OnlineEnv.opts}RunList.opts | grep -o '0000[0-9]*')
#settings=$(curl -sL http://rundb-internal.lbdaq.cern.ch/api/run/$first_run/ | jq -r '.triggerConfiguration')

settings=$(python -c 'import OnlineEnvBase; print(OnlineEnvBase.HLTType)')

echo "HLTTYPE $settings"
if [[ $settings == *"veloSP" ]]; then
    echo "SciFi alignment: configuring VELO SP algorithms based on current trigger config $settings"
    vp_options=
fi

exec_gaudirun \
    $MOOREONLINECONFROOT/options/verbosity.py \
    $vp_options \
    $MOOREONLINECONFROOT/options/align_options.py \
    $HUMBOLDTROOT/options/AlignFTModules_Analyzer_v1.py \
    $MOOREONLINECONFROOT/options/align_analyzer_Tracker.py
