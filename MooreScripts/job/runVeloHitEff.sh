#!/bin/bash
###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
set -euo pipefail
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source "$DIR/setupTask.sh"

setup_options_path MONITORING
settings=$(python -c 'import OnlineEnvBase; print(OnlineEnvBase.HLTType)')


export VELO_SP=0
vp_options=$MOOREONLINECONFROOT/options/vp_retina_clusters.py
if [[ $settings == *"veloSP" ]]; then
    echo "RecoMon: configuring VELO SP algorithms based on current trigger config $settings"
    vp_options=
    export VELO_SP=1
fi

setup_options_path MONITORING
exec_gaudirun \
    $MOOREONLINECONFROOT/options/tags-master.py \
    $MOOREONLINECONFROOT/options/verbosity.py \
    $vp_options \
    $MOOREONLINECONFROOT/options/veloHitEff.py \
    $MOOREONLINECONFROOT/options/online.py
