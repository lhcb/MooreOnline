#!/bin/bash
###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
set -euo pipefail
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source "$DIR/setupTask.sh"

setup_options_path

MBM_SETUP_OPTIONS=${MOORESCRIPTSROOT}/tests/options/HLT1/MBM_setup.opts

application=Online::OnlineApplication
cd_working_dir
dump_environment

export ADDER_TYPE=EventBuilder;
exec -a ${UTGID} genPython.exe `which gaudirun.py` --application=${application} ${MOORESCRIPTSROOT}/options/TestBenchAdder.py
