#!/bin/bash
###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
set -euo pipefail
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
echo "INFO: About to run $DIR/../setupTask.sh"
source "$DIR/../setupTask.sh"

#set -x
setup_options_path MONITORING

echo "INFO: pwd = $(pwd)"
echo "INFO: cd into $WORKING_DIR"
cd $WORKING_DIR
echo "INFO: pwd = $(pwd)"

# Run the RICH2 Iterator
.  /group/rich/sw/alignment/stack/Panoptes/build.x86_64_v3-el9-gcc12-opt/panoptesenv.sh
python -c "from PyMirrAlignOnline import Iterator; Iterator.run(2)"
