#!/bin/bash
###############################################################################
# (c) Copyright 2000-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
set -euo pipefail
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source "$DIR/setupTask.sh"

setup_options_path MONITORING
settings=$(python -c 'import OnlineEnvBase; print(OnlineEnvBase.HLTType)')

vp_options=$MOOREONLINECONFROOT/options/vp_retina_clusters.py
if [[ $settings == *"veloSP" ]]; then
    echo "CalibMon: configuring VELO SP algorithms based on current trigger config $settings"
    vp_options=
fi

exec_gaudirun \
    $MOOREONLINECONFROOT/options/tags-master.py \
    $MOOREONLINECONFROOT/options/verbosity.py \
    $vp_options \
    $MOOREONLINECONFROOT/options/calib.py \
    $MOOREONLINECONFROOT/options/online.py
