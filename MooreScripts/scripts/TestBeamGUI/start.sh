#!/bin/bash
###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
set -euo pipefail

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

if [ "$#" -ne 1 ]; then
    echo "usage: $0 ARCHITECTURE_XML_FILE"
    exit 2
fi

if [ -z "${DISPLAY-}" ]; then
    echo "DISPLAY is not set!"
    exit 3
fi

architecture="${1}"
architecture=$(realpath -e "$architecture")

export RDKAFKA_DIR=/group/online/dataflow/cmtuser/libraries/kafka/rdkafka-gcc-10.1.0-opt

if [ -z "${DIM_DNS_NODE-}" ]; then
   if [ -r /etc/sysconfig/dim ]; then
       . /etc/sysconfig/dim
   fi
fi
H3=`echo $DIM_DNS_NODE | tr a-z A-Z | cut -b 1-3`
MAINDNS=${DIM_DNS_NODE}
if test "${H3}" = "HLT"; then
    MAINDNS="ecstms01"
fi
if [[ $(hostname) == n8190[1-8]0[1-4] ]]; then
    # swdev cluster
    MAINDNS="ecstms01"
fi

echo "+++ Kafka directory: ${RDKAFKA_DIR}"
echo "+++ Binary tag:      ${BINARY_TAG}"
echo "+++ MAIN DNS node:   ${MAINDNS}"

# task_user=online
task_user=$(id -u -n)
task_group=$(id -g -n)

arch_name=$(basename ${architecture%.*})
arch_dir=$(dirname ${architecture})

# Create a temporary working directory for all tasks (and the controller).
WORKING_DIR=${arch_dir}/output/$(date "+%Y%m%dT%H%M%S")
mkdir -p ${WORKING_DIR}
# create a symlink to the latest working directory for convenience
ln -s -T -f $(basename $WORKING_DIR) ${WORKING_DIR}/../latest

declare -px > ${WORKING_DIR}/setup.vars

replacements=(
    MOORESCRIPTSROOT:${MOORESCRIPTSROOT}
    BINARY_TAG:${BINARY_TAG}
    USER:${task_user}
    GROUP:${task_group}
    WORKING_DIR:${WORKING_DIR}
    DATA_DIR:${XDG_RUNTIME_DIR}
)
replacements=$(printf ",%s" "${replacements[@]}")
replacements=${replacements:1}

gentest.exe libTestBeamGui.so testbeam_node_gui \
    -user=${task_user} \
    -maxinst=1 -instances=1 -partition=TESTBEAMGUI -host=${HOSTNAME} -maindns=${MAINDNS} \
    -replacements=${replacements} \
    -ctrl_script=${SCRIPT_DIR}/runController.sh \
    -runinfo=${arch_dir}/OnlineEnvBase.py \
    -architecture=${architecture}
