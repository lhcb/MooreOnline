#!/bin/bash
###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
set -euo pipefail
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# Extract environment from parameters (similarly to createEnvironment.sh).
# For any task other than the controller we can rely on replacements and not do this.
BINARY_TAG=$(echo "$@" | tr " " "\n" | grep replacements | tr "=," "\n" | grep BINARY_TAG)
export BINARY_TAG=${BINARY_TAG#*:}
WORKING_DIR=$(echo "$@" | tr " " "\n" | grep replacements | tr "=," "\n" | grep WORKING_DIR)
export WORKING_DIR=${WORKING_DIR#*:}

# TODO find a better way to locate the setup script / bootstrap the environment
source "$DIR/../../job/setupTask.sh"

cd_working_dir
dump_environment

# The following is a copy of
# https://gitlab.cern.ch/lhcb/Online/-/blob/6801e504ea1a9d6292c6f43107013bba03c2542b/TestBeam/job/Controller.sh

HOST=`hostname -s | tr a-z A-Z`;
#
if test -z "${LOGFIFO}"; then
    export LOGFIFO=/run/fmc/logSrv.fifo;
fi;
if test "${LOGFIFO}" = "/run/fmc/logSrv.fifo" -a -e "/dev/shm/logs.dev"; then
  export LOGFIFO=/dev/shm/logs.dev;
fi;
if test -z "${TMS_DNS}"; then
    export TMS_DNS=${HOST_LONG};
fi;
if test -z "${SMI_DNS}"; then
    export SMI_DNS=${HOST_LONG};
fi;
if test -z "${SMI_FILE-}"; then
    export SMI_FILE=${SMICONTROLLERROOT}/options/MonNode
fi;
if test -z "${SMI_DOMAIN-}"; then
    export SMI_DOMAIN=${PARTITION_NAME}_${HOST}_SMI;
fi;
if test -z "${DIM_DNS_NODE}"; then
    export DIM_DNS_NODE=${HOST_LONG};
fi;
THREAD_INSTANCE_SERVICE=none;
THREAD_INSTANCE_SERVICE="/${HOST}/NumInstances";
#
exec -a ${UTGID} `which genRunner.exe` libSmiController.so smi_controller \
    -print=4 		         \
    -logger=fifo 	         \
    -part=${PARTITION_NAME}	 \
    -dns=${DIM_DNS_NODE}   	 \
    -tmsdns=${TMS_DNS} 	         \
    -smidns=${SMI_DNS} 	         \
    -smidomain=${SMI_DOMAIN}     \
    -smidebug=0                  \
    -smifile=${SMI_FILE}         \
    -count=${NBOFSLAVES}         \
    -service=${THREAD_INSTANCE_SERVICE}  \
    -runinfo=${RUNINFO}          \
    -taskconfig=${ARCH_FILE}     \
    "${CONTROLLER_REPLACEMENTS}" \
    -standalone=1                \
    -bindcpus=0
