#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import argparse
import asyncio
import logging
import os
import socket

logging.basicConfig(
    format='%(asctime)s %(levelname)-8s %(message)s  [%(filename)s:%(lineno)d]',
    level=logging.INFO)

from MooreScripts.testbench.emulator import measure_throughput

HOST = socket.gethostname()
TASK1 = f"TEST_{HOST.upper()}_HLT2_0"
TASK2 = f"TEST_{HOST.upper()}_HLT2_1"

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Measure the throughput of HLT2")
    parser.add_argument(
        '--duration', type=float, default=122, help='Duration in seconds')
    args = parser.parse_args()

    # TODO do this automatically in DimService?
    os.environ["DIM_DNS_NODE"] = "localhost"

    asyncio.run(measure_throughput([TASK1, TASK2], max_duration=args.duration))
