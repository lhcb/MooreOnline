#!/bin/bash
###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# This script is the entry point for dataflow tasks. It is referenced in
# architecture xml files. It is only used for testing in MooreOnline.
# The production version is in the Online project and is more sophisticated.

set -euo pipefail
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
# TODO find a better way to locate the setup script / bootstrap the environment
source "$DIR/../job/setupTask.sh"

OPTIONS=$(realpath ${OPTIONS})

echo "+++ OPTIONS:     ${OPTIONS}"
echo "+++ DIM_DNS_NODE:     ${DIM_DNS_NODE}"

setup_options_path

cd_working_dir
dump_environment

# For HLT1 tests there is a separate Events buffer per NUMA
# domain. Setting this here allows a single .opts file to be used for both
if [[ "$NBOFSLAVES" == "0" ]]; then
    export EVENTS_INSTANCE_BUFFER="Events"
else
    instance=$(echo $UTGID | grep -oP '[0-9]+$' )
    export EVENTS_INSTANCE_BUFFER="Events_${instance}"
fi

cmd=()
if test -n "${BIND_NUMA-}"; then
    numa_domains_num=$(lscpu -p=NODE | grep -oP '^[0-9]+$' | sort | uniq | wc -l)
    numa_domain=$(( $(echo $UTGID | grep -oP '[0-9]+$') % $numa_domains_num ))
    cmd+=(numactl -N $numa_domain -m $numa_domain)
fi
ARCH="x86_64"
if [[ $BINARY_TAG == "arm"* ]]; then
    ARCH="arm64"
fi
cmd+=(
    setarch ${ARCH} --addr-no-randomize bash -c 'exec -a "$0" "$@"'
    ${UTGID}
    genRunner.exe libDataflow.so dataflow_run_task
    -msg=Dataflow_OutputLogger -mon=Dataflow_DIMMonitoring -class=${CLASS}
    -opts=${OPTIONS}
)

if test -f "${OPTIONS}" -a -n "`echo ${OPTIONS} | grep .opts`"; then
    echo "${cmd[@]}"
    exec  "${cmd[@]}"
else
    echo "'${OPTIONS}' does not exist does not end with .opts"
    exit 123
fi
