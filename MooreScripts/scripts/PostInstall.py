#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import sys, os

#sys.argv[1] must be *where* to put the output file
if len(sys.argv) < 2:
    raise AttributeError(
        "Not enough inputs..., give me the directory I should write into")

setupDir = sys.argv[1]

from MooreScripts.CreateRunEFF import CreateRunMooreOnline_EFF

for split, stage in {'Hlt1': 'Moore', 'Hlt2': 'Moore', '': 'L0'}.iteritems():
    f = os.path.join(setupDir, "run%s%sOnline_EFF.sh" % (stage, split))
    if os.path.exists(f):
        print "# WARNING: EFF File exists! Remove if you want me to recreate it! " + f
    else:
        CreateRunMooreOnline_EFF(f, split, stage if stage != 'Moore' else '')
