###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import argparse
import os
import sys
import xenv as EnvConfig
#
# First items on the blacklist is the stuff used by the startup scripts.
# Do not remove these!
#
blacklist = [
    "BASH_FUNC_module()",
    "BASH_FUNC_root()",
    "PARTITION_NAME",
    "RUNINFO",
    "TASK_TYPE",
    "ARCH_FILE",
    "UTGID",
    "LOGFIFO",
    "NBOFSLAVES",
    "RUN_TYPE",
    "ACTIVITY",
    "ONLINE_ENV_DIR",
    "MOOREONLINE_VERSION",
    "MOORE_VERSION",
    "ONLINE_VERSION",
    "OUTPUT_LEVEL",
    "MBM_SETUP_OPTIONS",
    "TAE_PROCESSING",
    "MOORESTARTUP_MODE",
    "CHECKPOINT_DIR",
    "_",
    "DIM_DNS_NODE",
    "GNOME_DESKTOP_SESSION_ID",
    "MAIL",
    "USER",
    "OS",
    "HOST",
    "HOSTNAME",
    "HOMEDIR",
    "HOME",
    "HISTCONTROL",
    "HISTIGNORE",
    "KRB5CCNAME",
    "SCRATCH",
    "EDITOR",
    "PWD",
    "GAUDICVS",
    "LHCBCVS",
    "MYGROUPDIR",
    "SRVCLASS",
    "CVS_RSH",
    "G_BROKEN_FILENAMES",
    "PYTHON_BINOFFSET",
    "VERBOSE",
    "LANG",
    "SSH_CONNECTION",
    "SSH_ASKPASS",
    "SSH_AUTH_SOCK",
    "SSH_CLIENT",
    "SSH_TTY",
    "SELINUX_ROLE_REQUESTED",
    "SELINUX_USE_CURRENT_RANGE",
    "SHLVL",
    "SHELL",
    "QT_GRAPHICSSYSTEM",
    "KDEDIRS",
    "SWDIR",
    "DISPLAY",
    "KDE_IS_PRELINKED",
    "TMP_ENV",
    "TERM",
    "LOGNAME",
    "MOZ_NO_REMOTE",
    "LS_COLORS",
    "XAUTHORITY",
    "LESSOPEN",
]

soft = '/cvmfs/lhcb.cern.ch'
lcg_soft = soft + '/lib/lcg'
lhcb_soft = soft + '/lib/lhcb'


def make_env(xml):
    output = sys.stdout
    args = ['--sh', '--xml', xml]
    s = EnvConfig.Script(args)
    s._makeEnv()
    print('if test -d /sw; then export SW_DIR=/sw;', file=output)
    print('else export SW_DIR=' + soft + '; fi;', file=output)
    print('export SW_DIR=' + soft + ';   ### Hard-coded value!', file=output)
    print('export SW_LCG=${SW_DIR}/lib/lcg;', file=output)
    print('export SW_LHCB=${SW_DIR}/lib/lhcb;', file=output)
    for k, v in s.env.items():
        if k not in blacklist:
            #print('echo "++ export %s=%s";'%(k,v),file=output)
            v = v.replace(lhcb_soft, '${SW_LHCB}')
            v = v.replace(lcg_soft, '${SW_LCG}')
            v = v.replace(soft, '${SW_DIR}')
            print('export %s="%s";' % (k, v), file=output)
    print(
        "if [ -e ${ONLINEBASEROOT}/scripts/onlinepatches.sh ]; then "
        ". ${ONLINEBASEROOT}/scripts/onlinepatches.sh; fi ;",
        file=output)


binary_tag = os.getenv("BINARY_TAG") or os.getenv("CMTCONFIG")
default_prefix = (
    os.getcwd() + "/InstallArea/" + binary_tag) if binary_tag else None

parser = argparse.ArgumentParser()
parser.add_argument(
    "prefix",
    nargs="?" if default_prefix else None,
    default=default_prefix,
    help="Full install prefix (including platform)")
args = parser.parse_args()

xenv_file = None
for f in os.listdir(args.prefix):
    if not f.endswith('-build.xenv') and f.endswith('.xenv'):
        xenv_file = os.path.join(args.prefix, f)
        break

if xenv_file is None:
    print('echo "Environment generation failed. Project XML was not found."')
    sys.exit(1)

if os.environ.get('GAUDIAPPNAME'):
    del os.environ['GAUDIAPPNAME']

make_env(xenv_file)
