#!/usr/bin/env python3
###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import argparse
import os
import pathlib
import sys
import urllib.request

# unset proxy in case it is set
try:
    del os.environ["http_proxy"]
except:
    pass


def dir_path(s):
    p = pathlib.Path(s)
    if p.is_dir():
        return p
    raise NotADirectoryError(s)


def report_progress(a, b, c):
    toolbar_width = 40
    try:
        last = report_progress.last
    except:
        last = 0
        sys.stdout.write("[%s]" % (" " * toolbar_width))
        sys.stdout.flush()
        sys.stdout.write(
            "\b" * (toolbar_width + 1))  # return to start of line, after '['

    progress = int(a * b / c * toolbar_width)
    report_progress.last = progress
    if progress > last:
        sys.stdout.write("-")
    if progress >= toolbar_width:
        del report_progress.last
        sys.stdout.write("]\n")  # this ends the progress bar
    sys.stdout.flush()


parser = argparse.ArgumentParser(description="Download data from HLT1 storage")
parser.add_argument("run", type=int, help="Run number")
parser.add_argument(
    "dest",
    type=dir_path,
    default=".",
    nargs="?",
    help="Destination directory")
parser.add_argument(
    "-n",
    type=int,
    default=1,
    help="Number of files to download (defaults to '.')")
args = parser.parse_args()

url_list = f'http://bbmd:4242/list?prefix=LHCb/{args.run:010}'
files = []
with urllib.request.urlopen(url_list) as f:
    for line in f:
        path, _, status = line.decode("utf-8").strip().split(",")
        if status == "CLOSED":
            files.append(path)

print(f"Found {len(files)} files for run {args.run}")

if args.n < 1:
    args.n = len(files)

try:
    for file in files[:args.n]:
        url_file = "http://bbmd:4242/objects/" + file
        dest = args.dest / os.path.basename(file)
        dest_part = dest.with_suffix('.part')
        print(str(dest) + ": ", end="")
        urllib.request.urlretrieve(
            url_file, dest_part, reporthook=report_progress)
        dest_part.rename(dest)
except KeyboardInterrupt:
    dest_part.unlink()
