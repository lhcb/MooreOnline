#!/usr/bin/env python
###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import argparse
import asyncio
import importlib
import logging
import re
import os
import pathlib
import subprocess
import inspect
from contextlib import AsyncExitStack
from datetime import datetime
from MooreScripts.testbench import architecture, emulator
from MooreScripts.testbench import scenarios
import MooreScripts.testbench.scenarios.default as default_scenario
from DDDB.CheckDD4Hep import UseDD4Hep

log = logging.getLogger(__name__)


def scenario(s):
    return importlib.import_module("." + s, scenarios.__package__)


parser = argparse.ArgumentParser(allow_abbrev=False)
parser.add_argument(
    "architecture",
    type=pathlib.Path,
    help="Path to task architecture XML",
)
# TODO make this a subcommand
parser.add_argument(
    "scenario",
    nargs="?",
    type=scenario,
    # choices=["default", "alignment"], # TODO get this list automatically
    default=default_scenario,
    help="Scenario to run")
parser.add_argument(
    "--working-dir",
    type=pathlib.Path,
    default=pathlib.Path.cwd(),
    help="Working directory (created if needed)",
)
parser.add_argument(
    "--dim-dns-port",
    type=int,
    help="DIM DNS port (must be unused)",
)
parser.add_argument(
    "--partition",
    type=str.upper,
    default="TEST",
    help="Partition name to use",
)
parser.add_argument(
    "--output-level",
    type=int,
    choices=[1, 2, 3, 4, 5],
    help="Gaudi OutputLevel",
)
parser.add_argument(
    "--test-file-db-key",
    help="Key for TestFileDB data to be downloaded to --data-dir",
)
parser.add_argument(
    "--data-dir",
    type=pathlib.Path,
    default=pathlib.Path("input_data"),
    help=("Path to directory for input files " +
          "(if relative, with respect to --working-dir)."),
)
parser.add_argument(
    "--geometry",
    default="run3/2024.Q1.2-v00.00",
    help="Geometry version to use")
parser.add_argument(
    "--conditions",
    default="master",
    help="Conditions version to use",
)
parser.add_argument(
    "--hlt-type",
    default="",
    help="HLT type; interpreted as sequence for HLT1")
parser.add_argument(
    "-d",
    "--debug",
    action="store_const",
    dest="log_level",
    const=logging.DEBUG,
    default=logging.INFO,
    help="Enable debug messages from the testbench",
)
parser.add_argument(
    "--log-file",
    default="emu.log",
    help="Location of logfile (if relative, with respect to --working-dir).",
)
parser.add_argument(
    "--measure-throughput",
    default=10,
    type=float,
    help="How long to measure throughput for.",
)
parser.add_argument(
    "--delay-tp-measurement",
    default=60,
    type=float,
    help="How long to measure throughput for.",
)
parser.add_argument(
    "--tfdb-nfiles",
    type=int,
    help="Number of files to download from the TestFileDB entry",
)
parser.add_argument(
    "--write-encoding-keys",
    action="store_true",
    help=
    "Enables writing of the encoding keys by setting env WRITE_ENCODING_KEYS=1.",
)
parser.add_argument(
    "--tck-from-odin",
    action="store_true",
    help=
    "Enables writing of the encoding keys by setting env WRITE_ENCODING_KEYS=1.",
)
parser.add_argument(
    "-n",
    "--instances",
    type=int,
    default=None,
    help="Number of instances of the main task to run",
)
args, unknown_argv = parser.parse_known_args()
args.data_dir = args.working_dir / args.data_dir

if wd := args.working_dir:
    if not wd.exists():
        wd.mkdir()

args.working_dir = args.working_dir.resolve()

# Make sure tests use unique partitions so that they don't interfere with
# each other in the nightlies.
# Ideally, the nightlies would properly isolate test jobs.
# Another approach would be to convince Online to be able to control
# where the shared resources go (/tmp/... and /dev/shm/...).
PARTITION_MAX_LEN = 16
build_id = os.getenv("BUILD_ID", "")
args.partition = args.partition + build_id[-(
    PARTITION_MAX_LEN - len(args.partition)):]
if len(args.partition) > PARTITION_MAX_LEN:
    raise RuntimeError(f"Partition name is too long (len({args.partition!r})="
                       + f"{len(args.partition)} > {PARTITION_MAX_LEN})")

emulator.setup_logging(
    args.working_dir / args.log_file, console_level=args.log_level)

subprocess.check_call([
    "/bin/bash", "-c", "declare -px | sed '/BASH_FUNC/d' > " + str(
        args.working_dir / "setup.vars")
])
# TODO filter out some variables as done in cmsetup.py

replacements = {
    "PARTITION": args.partition,
    "RUNINFO": args.working_dir.resolve() / "OnlineEnvBase.py",
    "MOORESCRIPTSROOT": os.environ["MOORESCRIPTSROOT"],
    "BINARY_TAG": os.environ["BINARY_TAG"],
    "WORKING_DIR": args.working_dir.resolve(),
}

arch = architecture.read_xml(args.architecture, args.instances)
if args.write_encoding_keys:
    arch = architecture.overwrite_dict_value(
        arch,
        {"WRITE_ENCODING_KEYS": "1"},
    )
if args.tck_from_odin:
    arch = architecture.overwrite_dict_value(
        arch,
        {"IGNORE_ODIN_TCK": "0"},
    )
task_instance_args = architecture.instance_args(arch, replacements)

emulator.check_for_orphans([a["args"][0] for a in task_instance_args])

if args.test_file_db_key:
    args.data_dir.mkdir(exist_ok=True)
    import GaudiKernel.ConfigurableDb
    GaudiKernel.ConfigurableDb.log.setLevel(logging.WARNING)
    from PRConfig.TestFileDB import test_file_db
    # TODO ConfigurableDb code is running because of the following import
    #      Moore/__init__.py imports config.py, which imports Configurables
    from Moore.qmtest.context import download_mdf_inputs_locally
    log.info(f"Downloading input data to {args.data_dir}")
    test_file_entry = test_file_db[args.test_file_db_key]
    input_files = download_mdf_inputs_locally(
        test_file_entry.filenames[:args.tfdb_nfiles], dest_dir=args.data_dir)
    if UseDD4Hep:
        geometry_tag = test_file_entry.qualifiers.get('GeometryVersion',
                                                      'run3/trunk')
        conddb_tag = test_file_entry.qualifiers.get('ConditionsVersion',
                                                    'master')
    else:
        geometry_tag = test_file_entry.qualifiers["DDDB"]
        conddb_tag = test_file_entry.qualifiers["CondDB"]
else:
    geometry_tag, conddb_tag = args.geometry, args.conditions

# Support repository:0x1XXXXXXX as a format for --hlt-type to
# configure AllenOnline from TCK
hlt_type = args.hlt_type
if (m := re.search(r"0x[a-fA-F0-9]{8}", hlt_type)):
    tck = int(m.group(0), 16)
    if ':' in hlt_type:
        hlt_type = str(pathlib.Path(hlt_type.split(':')[0]).resolve())
else:
    tck = 0

# We will generate OnlineEnv.opts and OnlineEnvBase.py starting from the
# files with the same names coming from the directory containing the
# architecture XML. Then, we append the following options in addition
# (which will overwrite existing options with the same keys):
online_env_common = {
    "PartitionName": args.partition,
}
if args.output_level:
    online_env_common["OutputLevel"] = args.output_level
online_env_opts = online_env_common.copy()
scenario_name = pathlib.Path(inspect.getfile(default_scenario)).stem
loop_on_input = args.measure_throughput > 0 or scenario_name == 'start_stop_start'
if args.data_dir.exists():
    online_env_opts |= {
        "Reader_Rescan": loop_on_input,
        "Reader_Directories": [str(args.data_dir.resolve())],
        "Reader_FilePrefix": "",
        "Reader_Preload": loop_on_input,
    }
online_env_base = online_env_common.copy() | {
    "HltArchitecture": "dummy",
    "OnlineVersion": "v0",
    "MooreVersion": "v0",
    "MooreOnlineVersion": "v0",
    "DDDBTag": geometry_tag,
    "CondDBTag": conddb_tag,
    "HLTType": hlt_type,
    "InitialTCK": tck,
    "TAE": 3,  # make this configurable?
    "Tasks": [t[0] for t in arch],
}


def copy_file(filename, f, comment_prefix):
    with open(filename) as src:
        data = src.read()
    f.write(
        f"{comment_prefix*4} copy of {filename}\n{data}{comment_prefix*30}\n")


with open(args.working_dir / "OnlineEnv.opts", "w") as f:
    copy_file(args.architecture.parent.resolve() / "OnlineEnv.opts", f, "//")
    emulator.dump_opts(online_env_opts, f)
with open(args.working_dir / "OnlineEnvBase.py", "w") as f:
    f.write("#Auto generated RunInfo options for " +
            f"partition:{args.partition} activity:{{Activity}}  " +
            f"{datetime.now():%Y.%m.%d %H:%M:%S.000}\n")
    copy_file(args.architecture.parent.resolve() / "OnlineEnvBase.py", f, "#")
    for k, v in online_env_base.items():
        f.write(f"{k} = {v!r}\n")


async def main():
    os.chdir(args.working_dir)
    try:
        async with AsyncExitStack() as stack:
            kwargs = {}
            if args.dim_dns_port:
                kwargs = {"ports": [args.dim_dns_port]}
            await stack.enter_async_context(emulator.start_dim_dns(**kwargs))
            tasks = []
            for task_args in task_instance_args:
                short_utgid = "_".join(task_args["args"][0].split("_")[2:])
                task_log_server = emulator.logs.LogServerThread(
                    name=short_utgid,
                    console=False,
                    fifo_path=args.working_dir,
                    output_path=args.working_dir / f"{short_utgid}.log")
                stack.enter_context(task_log_server)
                task = emulator.Task(task_args, task_log_server)
                await stack.enter_async_context(task)
                tasks.append(task)

            return await args.scenario.run(tasks, args, unknown_argv)

    except asyncio.CancelledError:
        log.warning("Event loop was cancelled")
        return 101


exit(asyncio.run(main(), debug=True))
