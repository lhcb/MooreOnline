###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import OnlineEnvBase
import re
from MooreScripts.testbench.adders import Adder, AdderApp
from GaudiOnline import Class1

partition = OnlineEnvBase.PartitionName
print("[INFO] Configure adder application")
tasks = OnlineEnvBase.Tasks
adder_services = []

for task_name in [t for t in tasks if not re.search(r"(Adder|Saver)", t)]:
    adder = Adder(task_name, None, partition, "counter")
    adder.obj.TrackSources = 1
    adder.obj.HaveTimer = 0
    adder_services.append(adder.obj)
    adder = Adder(task_name, None, partition, "hists")
    adder.obj.TrackSources = 1
    adder_services.append(adder.obj)

app = AdderApp(
    output_level=OnlineEnvBase.OutputLevel,
    partition=OnlineEnvBase.PartitionName,
    adders=adder_services,
)
app.config.classType = Class1
