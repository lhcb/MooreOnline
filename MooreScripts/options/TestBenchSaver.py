###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import OnlineEnvBase
from MooreScripts.testbench.adders import Adder, AdderApp
from GaudiOnline import Class1
from Configurables import GenStatSvc
import socket
import os
import re

partition = OnlineEnvBase.PartitionName
print("[INFO] Configure saver application")
tasks = OnlineEnvBase.Tasks
adder_services = []
node = socket.gethostname().upper()

for task_name in [t for t in tasks if not re.search(r'(Adder|Saver)', t)]:
    adder = Adder(task_name, 'Adder', partition, 'hists').enableSaving(
        12, 600, os.path.realpath('.'))
    adder.obj.TrackSources = 1
    adder_services.append(adder.obj)

app = AdderApp(
    output_level=OnlineEnvBase.OutputLevel,
    partition=OnlineEnvBase.PartitionName,
    adders=adder_services,
)
app.config.classType = Class1
