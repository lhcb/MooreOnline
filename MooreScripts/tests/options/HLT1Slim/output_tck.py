###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import glob
from Moore import options
from DDDB.CheckDD4Hep import UseDD4Hep

options.set_conds_from_testfiledb('2022_mep_253895')
options.print_freq = 100
options.data_type = 'Upgrade'
options.simulation = not UseDD4Hep
options.input_type = 'MDF'
options.input_files = glob.glob("hlt1tck/*.mdf")
