###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
###############################################################################
import os
import sys
import argparse
import glob
from collections import defaultdict
from itertools import chain
from pathlib import Path
from Configurables import LHCbApp
from Configurables import GaudiSequencer
from Configurables import ApplicationMgr
from Configurables import (AuditorSvc, SequencerTimerTool)
from Configurables import IODataManager
from Configurables import createODIN, HltDecReportsDecoder
from Configurables import LHCb__UnpackRawEvent as UnpackRawEvent
from DDDB.CheckDD4Hep import UseDD4Hep
from GaudiConf import IOHelper
from GaudiPython.Bindings import AppMgr, gbl
from PyConf.application import configured_ann_svc
from Allen.tck import property_from_git
from PyConf.filecontent_metadata import metainfo_repos

parser = argparse.ArgumentParser()
parser.add_argument("tck_repo", nargs=1)
parser.add_argument("hlt1_line", nargs=1)
parser.add_argument("mdfs", nargs='+')
args = parser.parse_args()

mdfs = args.mdfs
if len(mdfs) == 1:
    # Shell expansion of wildcards doesn't work when launching from a
    # qmtest, so do the glob here
    mdfs = glob.glob(mdfs[0])

app = LHCbApp()
app.DataType = "Upgrade"
app.EvtMax = -1
app.Simulation = not UseDD4Hep

if not UseDD4Hep:
    app.DDDBtag = "upgrade/dddb-20221004"
    app.CondDBtag = "upgrade/mu_VP_SciFi_macromicrosurvey_from20220923"
else:
    app.DDDBtag = "run3/trunk"
    app.CondDBtag = "master"

check_seq = GaudiSequencer("CheckODINSeq")

unpack_raw = UnpackRawEvent(
    RawEventLocation='DAQ/RawEvent',
    RawBankLocations=['DAQ/RawBanks/ODIN', 'DAQ/RawBanks/HltDecReports'],
    BankTypes=['ODIN', 'HltDecReports'])

dec_reports = HltDecReportsDecoder(
    RawBanks='DAQ/RawBanks/HltDecReports',
    SourceID='Hlt1',
    DecoderMapping="TCKANNSvc",
    OutputHltDecReportsLocation='Hlt1/DecReports')

check_seq.Members = [unpack_raw, createODIN(), dec_reports]

ApplicationMgr().TopAlg = [check_seq]

IOHelper('MDF').inputFiles(mdfs, clear=True)

# Use the metainfo repository created by the create_hlt1_tck test
metainfo_repos.global_bind(repos=[('lhcb-metainfo/.git', 'master')])

# Some extra stuff for timing table
ApplicationMgr().ExtSvc += ['ToolSvc', 'AuditorSvc']
ApplicationMgr().ExtSvc += [configured_ann_svc(name='TCKANNSvc')]

ApplicationMgr().AuditAlgorithms = True
AuditorSvc().Auditors += ['TimingAuditor']
SequencerTimerTool().OutputLevel = 4

# Some extra stuff to save histograms
ApplicationMgr().HistogramPersistency = "NONE"

# No error messages when reading MDF
IODataManager().DisablePFNWarning = True

gaudi = AppMgr()
gaudi.initialize()
TES = gaudi.evtSvc()

events = set()

decs = {}
tcks = {}

n_evt = 0
while app.EvtMax == -1 or n_evt < app.EvtMax:
    gaudi.run(1)
    if not TES['/Event']:
        break

    n_evt += 1

    odin = TES['DAQ/ODIN']
    run = odin.runNumber()
    reps = TES['Hlt1/DecReports']
    tck = reps.configuredTCK()
    if run not in tcks:
        tcks[run] = tck
    elif tcks[run] != tck:
        sys.exit(f"Found multiple TCKs per run: 0x{tcks[run]:08x} 0x{tck:08x}")

    if tck not in decs:
        decs[tck] = defaultdict(int)
    for n in reps.decisionNames():
        decs[tck][str(n)[:-8]] += reps.decReport(n).decision()

# Get the prescales from the TCK
prescales = {}
for tck in tcks.values():
    lines = property_from_git(
        Path(args.tck_repo[0]), f"0x{tck:08x}", f"{args.hlt1_line[0]}",
        "pre_scaler")
    if not lines:
        sys.exit(
            f"Failed to find the prescales for {args.hlt1_line[0]} in 0x{tck:08x}"
        )
    prescale = float(
        next(chain.from_iterable(props.values() for props in lines.values())))
    prescales[tck] = prescale

tck_without_events, tck_with_events = [
    e[0] for e in sorted(prescales.items(), key=lambda e: e[1])
]
if decs[tck_with_events][args.hlt1_line[0]] == 0:
    sys.exit(
        f"ERROR: Line {args.hlt1_line[0]} didn't fire in file with TCK 0x{tck_with_events:08x}"
    )
if decs[tck_without_events][args.hlt1_line[0]] != 0:
    sys.exit(
        f"ERROR: Change of prescale not working; found decisions of {args.hlt1_line[0]} in file with TCK 0x{tck_without_events:08x}"
    )
