###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import json
from Allen.tck import sequence_from_git, sequence_to_git

seq_str, info = sequence_from_git('config.git', '0x10000001')

# Get the JSON representaion of the sequence
seq = json.loads(seq_str)

# Update a prescale
seq['Hlt1DiMuonHighMass']['pre_scaler'] = 0.
seq['dec_reporter']['tck'] = 0x10000002

try:
    sequence_to_git('config.git', seq, info['type'], info['label'], 0x10000002,
                    info['metadata']['stack']['name'],
                    {'settings': info['metadata']['settings']}, True)
except RuntimeError as e:
    print(e)
    sys.exit(1)
