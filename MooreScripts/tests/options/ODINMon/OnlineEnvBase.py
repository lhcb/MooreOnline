###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
PartitionID = 65535
PartitionName = "TESTBEAMGUI"
Activity = "PHYSICS"
HltArchitecture = "dummy"
OnlineVersion = "v0"
MooreVersion = "v0"
MooreOnlineVersion = "v0"
OutputLevel = 3
