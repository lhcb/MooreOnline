###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
script = """#!/bin/bash
ulimit -v 3221225472
export UTGID;
export DIM_DNS_NODE=$1
export PARTITION_NAME=$2
export NBOFSLAVES=${3:-0}
#
. /group/hlt/MOORE/${MOOREONLINE_VERSION}/InstallArea/${CMTCONFIG}/setupMoore.sh;
#
# Enable next line for Debug printout
#python ${FARMCONFIGROOT}/job/ConfigureCheckpoint.py -r ${RUNINFO} -s;
eval `python ${FARMCONFIGROOT}/job/ConfigureCheckpoint.py -r ${RUNINFO} -s %(WhichMoore)s`;
renice -n %(Priority)s -p $$>>/dev/null
#
# Run numactl -s to show current binding, get the list of "0 1 2 ..." and replace ' ' with ','
# to get a format that numactl understands
numa_nodes=$(numactl -s | grep "^cpubind:" | cut -d ' ' -f2- | perl -pe 's/([ ]([1-9]))/,$2/g')
#
if test "${MOORESTARTUP_MODE}" = "RESTORE";      ## RunInfo flag=2
    then
    # Enable next line for Debug printout
    #python ${FARMCONFIGROOT}/job/ConfigureFromCheckpoint.py;
    #echo "RESTORE command: ${RESTORE_CMD}";
    eval "python ${FARMCONFIGROOT}/job/ConfigureFromCheckpoint.py | ${RESTORE_CMD}";
else
    echo "[DEBUG] Starting ${UTGID} bound to NUMA node(s) ${numa_nodes}"
    exec /usr/bin/numactl --cpunodebind=${numa_nodes} --membind=${numa_nodes} sh -c "exec -a ${UTGID} \\
    GaudiCheckpoint.exe libGaudiOnline.so OnlineTask \\
        -msgsvc=LHCb::FmcMessageSvc \\
        -tasktype=LHCb::Class1Task \\
        -main=%(MainOpts)s \\
        -opt=command=\\"from MooreScripts import run%(stage)sOnline; run%(stage)sOnline.start(NbOfSlaves = ${NBOFSLAVES}, Split = '%(split)s', %(checkOdin)s WriterRequires = ['%(split)s']  )\\" \\
        ${APP_STARTUP_OPTS};"
fi
"""


def CreateRunMooreOnline_EFF(output, split='', stage=''):
    from os import makedirs, chmod
    from os.path import exists, dirname

    #print '# INFO: using ' + setup
    print '# INFO: generating ' + output
    target_dir = dirname(output)
    if not exists(target_dir): makedirs(target_dir)
    ofile = open(output, 'w+')
    ofile.write(script % ({
        'split': split,
        'stage': stage,
        'Priority': {
            'Hlt1': '${NiceLevel:-10}',
            'Hlt2': '${NiceLevel:-17}'
        }.get(split),
        'WhichMoore': {
            'Hlt1': '',
            'Hlt2': '-t Moore2'
        }.get(split),
        'checkOdin': {
            'Hlt1': '',
            'Hlt2': 'CheckOdin = False, '
        }.get(split),
        'MainOpts': {
            'Hlt1': '/group/online/dataflow/templates/options/Main.opts',
            'Hlt2': '${FARMCONFIGROOT}/options/Hlt2Main.opts'
        }.get(split)
    }))

    from stat import S_IRUSR, S_IRGRP, S_IROTH, S_IWUSR, S_IWGRP, S_IXUSR, S_IXGRP, S_IXOTH
    import os
    orig = os.stat(output)[0]
    rwxrwxrx = S_IRUSR | S_IRGRP | S_IROTH | S_IWUSR | S_IWGRP | S_IXUSR | S_IXGRP | S_IXOTH
    if orig | rwxrwxrx != orig:
        print '%s has permissions %d -- want %d instead' % (output, orig,
                                                            orig | rwxrwxrx)
        try:
            chmod(output, rwxrwxrx)
            print 'updated permission of %s' % (output)
        except:
            print 'WARNING: could not update permissions of %s -- please make sure it is executable!' % output
