###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Minimal async version of the PyDIM interface.

See the PyDIM documentation at
https://lhcbdoc.web.cern.ch/lhcbdoc/pydim/guide/index.html

"""
import asyncio
import logging
import os
import random
import sys
import datetime

# Before importing pydim, we need to set LD_PRELOAD, which is set in case
# pydim was built with sanitizers.
if not os.getenv("LD_PRELOAD") and (san := os.getenv("PRELOAD_SANITIZER_LIB")):
    os.environ["LD_PRELOAD"] = san
    os.execv(sys.argv[0], sys.argv)
import pydim  # noqa
# del os.environ["LD_PRELOAD"]  TODO why can't I have this?

log = logging.getLogger(__name__)

_DIM_MESSAGE_LEVELS = {
    0: logging.INFO,
    1: logging.WARNING,
    2: logging.ERROR,
    3: logging.FATAL
}


def dim_message_handler(severity, errcode, reason):
    if errcode == 0x30: return  # DIMDNSCNERR
    level = _DIM_MESSAGE_LEVELS[severity]
    log.log(level, f"{reason} ({errcode=})")


pydim.dic_add_error_handler(dim_message_handler)


def _safe_callback(callback):
    loop = asyncio.get_event_loop()

    def _callback(*args):
        ts = datetime.datetime.now()
        # TODO extend PyDIM to support dic_info_service_stamped
        try:
            loop.call_soon_threadsafe(callback, ts, *args)
        except RuntimeError:  # Event loop is closed
            pass

    return _callback


class DimService:
    def __init__(self, name, description):
        self._name = name
        self._tag = random.randint(0, 100000)
        self._queue = asyncio.Queue()

        def callback(timestamp, tag, value):
            assert tag == self._tag
            if isinstance(value, str):
                if not value[-1] == "\0":
                    msg = f"Unexpected DIM response {value!r}"
                    log.fatal(msg)
                    raise RuntimeError(msg)
                value = value[:-1]
            log.debug(f"{self._name}: callback got {value!r}")
            self._queue.put_nowait((timestamp, value))

        timeout = 0
        default_value = None
        self._service = pydim.dic_info_service(name, description,
                                               _safe_callback(callback),
                                               pydim.MONITORED, timeout,
                                               self._tag, default_value)
        self._value = (None, None)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.release()

    async def get(self, ignore_republish=True):
        old = self._value
        new = await self._queue.get()
        while ignore_republish and new[1] == old[1]:
            new = await self._queue.get()
        self._value = new
        # log.debug(f"{self._name}: {old} -> {new}")
        return (old, new)

    async def get_all(self):
        old, new = await self.get(ignore_republish=False)
        values = [new]
        while not self._queue.empty():
            values.append(self._queue.get_nowait())
        self._value = values[-1]
        return values

    def release(self):
        if self._service is not None:
            log.debug(f"Releasing DimService({self._name})")
            pydim.dic_release_service(self._service)
            self._service = None

    def __del__(self):
        self.release()


def send_command(name, arguments, description):
    loop = asyncio.get_event_loop()
    future = loop.create_future()
    req_tag = random.randint(0, 100000)

    def callback(timestamp, tag, retcode):
        assert tag == req_tag
        if retcode != 1:
            raise RuntimeError(f"Failed to send command {arguments} to {name}")
        if not future.done():
            future.set_result((timestamp, retcode))

    pydim.dic_cmnd_callback(name, arguments, description,
                            _safe_callback(callback), req_tag)
    return future
