###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import socket
from GaudiOnline import Application
from Configurables import EventLoopMgr, ApplicationMgr
from GauchoAppl import GauchoApplConf


class Adder:
    def __init__(self,
                 task,
                 tasks,
                 partition,
                 adder_type,
                 debug=False,
                 run_aware=False):
        task_name = str(task)
        nodes = socket.gethostname().upper()

        if adder_type == "counter":
            adder = GauchoApplConf.AdderSvc("C_" + task_name)
            adder.ServicePattern = f"MON_<part>_{nodes}_{task_name}(_[0-9]+)?/Counter/"
        else:
            adder = GauchoApplConf.AdderSvc("H_" + task_name)
            adder.ServicePattern = f"MON_<part>_{nodes}_{task_name}(_[0-9]+)?/Histos/"

        adder.MyName = f"<part>_<node>_{task_name}"
        adder.PartitionName = partition

        if tasks and tasks == '*':
            adder.TaskPattern = f"<part>_{nodes}_(.*)"
        elif tasks:
            adder.TaskPattern = f"<part>_{nodes}_{str(tasks)}_(.*)"
        else:
            adder.TaskPattern = f"<part>_{nodes}_{task_name}"

        adder.AdderClass = adder_type
        adder.RunAware = run_aware
        adder.DebugOn = debug
        self.task_name = task_name
        self.obj = adder

    def setTaskPattern(self, pattern):
        self.obj.TaskPattern = pattern
        return self

    def setServicePattern(self, pattern):
        self.obj.ServicePattern = pattern
        return self

    def enableSaving(self, recv_tmo, interval, saveset_dir):
        self.obj.ReceiveTimeout = recv_tmo
        self.obj.IsSaver = True
        self.obj.SaveInterval = interval
        self.obj.SaveRootDir = saveset_dir
        self.obj.SaveSetTaskName = self.task_name
        return self


class AdderApp(Application):
    def __init__(self,
                 output_level,
                 partition="OFFLINE",
                 partition_id=0xFFFF,
                 adders=[]):
        import fifo_log

        Application.__init__(
            self,
            outputLevel=output_level,
            partitionName=partition,
            partitionID=partition_id,
        )
        # First setup printing device:
        self.config.logDeviceType = "RTL::Logger::LogDevice"
        self.config.logDeviceFormat = "%-8LEVEL %-24SOURCE"
        self.config.logDeviceType = "fifo"
        fifo_log.logger_set_tag(partition)
        fifo_log.logger_start()
        # Now the rest:
        self.config.numEventThreads = 0
        self.config.numStatusThreads = 0
        self.config.autoStart = False
        self.app = ApplicationMgr()
        self.app.MessageSvcType = "MessageSvc"
        self.app.EvtSel = "NONE"
        self.app.EvtMax = -1
        self.app.AppName = ""  # utgid
        self.app.HistogramPersistency = "NONE"
        self.setup_monitoring_service()
        self.monSvc.HaveRates = False
        self.app.ExtSvc = [self.monSvc] + adders
        EventLoopMgr().Warnings = False
        self.enableUI()
