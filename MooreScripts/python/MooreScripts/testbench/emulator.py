###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Utilities to emulate online dataflow infrastructure.

- log server replacement
- start and manage a DIM DNS
- start and manage tasks (like "tmSrv")

"""

import asyncio
import atexit
import logging
import os
import random
import re
import time
import signal
import subprocess
import sys
import shutil
from collections import defaultdict
from pathlib import Path
from contextlib import asynccontextmanager, AsyncExitStack
from datetime import datetime
from pathlib import Path

from . import asyncdim
from . import logs
from .logs import setup_logging  # noqa

log = logging.getLogger("")


def dump_opts(opts, f, key_prepend="OnlineEnv."):
    logging.getLogger("PropertyProxy").setLevel(logging.WARNING)
    logging.getLogger("ConfigurableDb").setLevel(logging.WARNING)
    from Gaudi.Main import toOpt  # indirectly imports ConfigurableDb

    for k, v in opts.items():
        f.write(f"{key_prepend}{k} = {toOpt(v)};\n")


async def _create_subprocess(*, exit_callback=None, **kwargs):
    args = kwargs.pop("args")
    log.debug(f"Starting process args={args}, kwargs={kwargs}")
    p = await asyncio.create_subprocess_exec(*args, **kwargs)

    exit_task = None
    if exit_callback is not None:

        async def callback():
            retcode = await p.wait()
            log.debug(f"{p.pid} exited with code {retcode}")
            exit_callback(p)

        exit_task = asyncio.create_task(callback())

    return p, exit_task


def cancel_all_tasks():
    loop = asyncio.get_event_loop()
    for task in asyncio.all_tasks(loop):
        task.cancel()


class AddressAlreadyInUse(RuntimeError):
    pass


@asynccontextmanager
async def start_dim_dns(*,
                        executable="dns.exe",
                        ports=list(range(25050, 25150)),
                        debug=False,
                        sample_ports=3):
    if isinstance(ports, int):
        ports = [ports]
    if sample_ports:
        ports = random.sample(ports, min(len(ports), sample_ports))
    for i, port in enumerate(ports):
        if i == 0:
            log.info(f"Starting DIM DNS on port {port} ...")
        else:
            log.warning(f"Retry starting DIM DNS on port {port} ...")
        try:
            async with _start_dim_dns(
                    executable=executable, port=port, debug=debug):
                yield
            return
        except AddressAlreadyInUse:
            pass
    msg = f"Failed to start DIM DNS on ports {ports}"
    log.fatal(msg)
    raise RuntimeError(msg)


@asynccontextmanager
async def _start_dim_dns(*, executable="dns.exe", port=25050, debug=False):
    args = [executable]
    if debug:
        args.append("-d")

    with logs.LogServerThread("DIM_DNS") as log_server:
        ready = False

        def exit_callback(p):
            log.error(f"DIM DNS exited too early with code {p.returncode}")
            if ready:
                cancel_all_tasks()
            else:
                for line in log_server.readlines():
                    if "Address already in use" in line:
                        raise AddressAlreadyInUse(
                            "DIM DNS port is already in use")

        log.info(log_server.fifo_path())
        p, exit_task = await _create_subprocess(
            exit_callback=exit_callback,
            args=args,
            stdout=log_server.fifo(),
            stderr=log_server.fifo(),
            preexec_fn=os.setpgrp,
            env={
                "DIM_DNS_NODE": "localhost",
                "DIM_DNS_PORT": str(port),
                "PATH": os.environ["PATH"],
                "LD_LIBRARY_PATH": os.environ["LD_LIBRARY_PATH"],
            })

        _all_tasks.append(p)

        # pydim.dis_set_dns_node("localhost")  => does not work...
        os.environ["DIM_DNS_NODE"] = "localhost"
        os.environ["DIM_DNS_PORT"] = str(port)
        # Wait for the process to respond and check PID
        # First, wait a bit to avoid a (harmless) connection refused error
        # TODO can we avoid that somehow?
        await asyncio.sleep(1)
        if exit_task.done():
            if e := exit_task.exception():
                raise e
        with asyncdim.DimService("DIS_DNS/SERVER_LIST", "C") as ds:
            value = (await ds.get())[1][1]
            servers, pids = [x.split("|") for x in value.split("\0")]
            assert len(servers) == len(pids)
            for server, pid in zip(servers, pids):
                if server.split("@", 1)[0] == "DIS_DNS":
                    if int(pid) != p.pid:
                        # NOTE this check might pass even if this is not our
                        # dns in case we're running in a docker container
                        # (with --network host).
                        msg = ("PID of dns not consistent: " +
                               f"expected {p.pid} got {pid}. " +
                               f"Was DNS running on port {port} already? ")
                        log.error(msg)
                        raise AddressAlreadyInUse(
                            "DIM DNS port is already in use")

        ready = True
        try:
            yield
        finally:
            exit_task.cancel()
            await asyncdim.send_command("DIS_DNS/EXIT", (0, ), "L")
            await p.wait()


_all_tasks = []


def _killpg(process, sig):
    signalnum = getattr(signal, sig)
    if process.returncode is None:
        log.warning(f"Sending {sig} to process group {process.pid}")
        try:
            os.killpg(process.pid, signalnum)
        except ProcessLookupError:
            pass


def _cleanup_tasks():
    if all(p.returncode is not None for p in _all_tasks):
        return
    log.warning("Shutting down stray processes...")

    for p in _all_tasks:
        _killpg(p, "SIGTERM")

    for _ in range(50):
        time.sleep(0.1)
        if all(p.returncode is not None for p in _all_tasks):
            log.warning('...done')
            return

    for p in _all_tasks:
        _killpg(p, "SIGKILL")
    log.warning('...done')


atexit.register(_cleanup_tasks)


class Task:
    def __init__(self, args, log_server):
        self._args = args
        self._log_server = log_server
        self._status = None
        self._status_lock = asyncio.Lock()
        self._status_task = None
        self.process = None
        self._perf_data = None
        self._perf_control = None
        self.utgid = args["args"][0]
        for a in args["args"]:
            if a.startswith("-type="):
                self.type = a.removeprefix("-type=")

    def use_perf(self, control=False):
        if self.process is not None:
            raise RuntimeError("use_perf() must be called before load()")
        if shutil.which("perf") is not None:
            self._perf_data = Path(f"{self.utgid}.perf.data").absolute()
            if control:
                self._perf_control = Path("perf_control.fifo").absolute()
                try:
                    os.mkfifo(self._perf_control)
                except FileExistsError:
                    pass
        else:
            log.warning(
                "Requested to run perf but no executable found. Continuing without profiling."
            )

    async def __aenter__(self):
        pass

    async def load(self):
        args = self._args
        log_server = self._log_server
        utgid = self.utgid

        # TODO propagate DIM_DNS_PORT/NODE more explicitly...
        args["env"]["DIM_DNS_NODE"] = os.environ["DIM_DNS_NODE"]
        args["env"]["DIM_DNS_PORT"] = os.environ["DIM_DNS_PORT"]
        args["env"]["UTGID"] = utgid
        args["env"]["LOGFIFO"] = log_server.fifo_path()

        if self._perf_data:
            args["args"][0] = args["executable"]
            perf_cmd = [
                "perf",
                "record",
                f"--output={self._perf_data}",
                "--event=cycles:up",
                "--aio",
                "--call-graph=dwarf,65528",
                "--count=100000000",
            ]
            if self._perf_control:
                perf_cmd.append(f"--control=fifo:{self._perf_control}")
            args["args"] = perf_cmd + args["args"]
            args["executable"] = "perf"

        def exit_callback(p):
            if self._status_task is not None:
                log.warning(f"{utgid} exited with code {p.returncode}" +
                            " while waiting for status")
                self._status_task.cancel()
                return
            log.critical(
                f"{utgid} exited unexpectedly with code {p.returncode}")
            cancel_all_tasks()
            self.process = None

        p, exit_task = await _create_subprocess(
            exit_callback=exit_callback,
            stdout=log_server.fifo(),
            stderr=log_server.fifo(),
            # detach from parent group, i.e. do not inherit signals
            preexec_fn=os.setpgrp,
            **args)
        log.info(f"Started {utgid} with pid {p.pid}")
        _all_tasks.append(p)

        self._status = asyncdim.DimService(utgid + "/status", "C")
        self.utgid = utgid
        self.process = p
        self._exit_task = exit_task

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        self._status.__exit__(exc_type, exc_val, exc_tb)
        self._status = None
        self._exit_task.cancel()
        if self.process:
            log.warning(f"Killing {self.utgid} and children...")
            _killpg(self.process, "SIGTERM")
            time.sleep(0.2)
        self.process = None

    async def status(self):
        if self.process is None:
            raise RuntimeError("Task is not running")
        # No concurrency for status() calls
        async with self._status_lock:
            loop = asyncio.get_event_loop()
            self._status_task = loop.create_task(self._status.get())
            await self._status_task
            (_, old), (ts, value) = self._status_task.result()
            self._status_task = None
            log.info(f"{self.utgid} status changed {old} to {value}" +
                     f" at {ts.isoformat()}")
            return value

    async def send_command(self, command):
        log.info(f"{self.utgid} sending {command!r}")
        return await asyncdim.send_command(self.utgid, (command, ), "C")

    async def wait(self):
        self._exit_task.cancel()
        exit_code = await self.process.wait()
        self.process = None
        if self._perf_data:
            log.info("Creating flamegraph...")
            cmd = " | ".join([
                f"perf script -i {self._perf_data}",
                os.path.expandvars(
                    "$PRCONFIGROOT/python/MooreTests/stackcollapse-perf.pl"),
                "/cvmfs/sft.cern.ch/lcg/releases/clang/14.0.6-14bdb/x86_64-centos9/bin/llvm-cxxfilt",
                os.path.expandvars(
                    "$PRCONFIGROOT/python/MooreTests/flamegraph.pl") +
                " --hash --title 'Flame Graph' --minwidth 2 --width 1600 > {}.svg"
                .format(self.utgid)
            ])
            p = await asyncio.create_subprocess_shell(cmd)
            await p.wait()
        return exit_code


async def tasks_load(tasks):
    return await asyncio.gather(*[t.load() for t in tasks])


async def tasks_wait_for_status(tasks, status, *, skip=[], utgid=None):
    statuses = {}
    for t in tasks:
        if utgid is not None and not re.match(utgid, t.utgid):
            continue
        st = await t.status()
        while st != status and st in skip:
            st = await t.status()
        statuses[t.utgid] = st
    if not all(s == status for s in statuses.values()):
        message = f"Unexpected statuses: {statuses}"
        log.error(message)
        raise RuntimeError(message)


async def tasks_wait_for_exit(tasks):
    return await asyncio.gather(*[t.wait() for t in tasks])


async def tasks_send_command(tasks, command, *, timeout=None):
    return await asyncio.wait_for(
        asyncio.gather(*[t.send_command(command) for t in tasks]), timeout)


async def async_input(prompt=None):
    """Async version of input()."""
    reader = asyncio.StreamReader()
    loop = asyncio.get_event_loop()
    transport, protocol = await loop.connect_read_pipe(
        lambda: asyncio.StreamReaderProtocol(reader), sys.stdin)

    if prompt is not None:
        print(prompt, end='', flush=True)
    line = await reader.readline()
    # FIXME we can't really close here as a second call to async_input fails
    # transport.close()
    return line.decode("utf8").rstrip("\n")


def check_for_orphans(names):
    # Note: must not have asan in LD_PRELOAD => use clean environment
    output = subprocess.check_output(["ps", "-e", "-o", "pid=", "-o", "args="],
                                     encoding="ascii",
                                     env={})
    matches = []
    for line in output.splitlines():
        pid, *args = line.split()
        if len(args) > 0 and args[0] in names:
            matches.append(f"{args[0]} ({pid})")
    if matches:
        raise RuntimeError("Found already running processes:\n" +
                           "\n".join(matches))


async def measure_throughput(utgids, max_duration, print_throughput, counter):
    def throughput(start, end):
        return (end[1] - start[1]) / (end[0] - start[0]).total_seconds()

    ta = None
    async with AsyncExitStack() as stack:
        services = [
            stack.enter_context(
                asyncdim.DimService(f"{u}/Events/{counter}", "X"))
            for u in utgids
        ]
        # get the first data point per task
        meas = [[(await s.get())[1]] for s in services]
        log.debug(str(meas))
        start_time = datetime.now()
        while (datetime.now() - start_time).total_seconds() < max_duration:
            for s, m, utgid in zip(services, meas, utgids):
                m.extend(x for x in await s.get_all() if x[1] is not None)
                log.debug(str(meas))
                t = throughput(m[-2], m[-1])
                m_start = 0 if len(m) < 4 else 2
                ta = throughput(m[m_start], m[-1])
                n_evt = m[-1][1] - m[m_start][1]
                log.info(
                    f"{utgid}: {t:.1f} ({ta:.1f}) Events/s  ({n_evt} Events)")
            if len(utgids) >= 2:
                t = sum(throughput(m[-2], m[-1]) for m in meas)
                ta = sum(throughput(m[0], m[-1]) for m in meas)
                n_evt = sum(m[-1][1] - m[0][1] for m in meas)
                log.info(f"{'Total':{len(utgids[0])}}: {t:.1f}" +
                         f" ({ta:.1f}) Events/s  ({n_evt} Events)")

    if ta is not None and print_throughput:
        # The LHCbPR handler "ThroughputProfileHandler" expects to find
        # exactly one match of the form "Evts/s = 123.456"
        log.info(f"Average total throughput: Evts/s = {ta:.1f}")

    return ta


async def tasks_measure_throughput(tasks,
                                   max_duration,
                                   type_pattern=r".*(HLT|Mon).*",
                                   print_throughput=True,
                                   counter="OUT"):
    utgids = [t.utgid for t in tasks]
    utgids = [u for u in utgids if re.match(type_pattern, u.split("_")[2])]
    return await measure_throughput(utgids, max_duration, print_throughput,
                                    counter)


async def hlt1_wait_for_output(tasks, prod_svc, proc_svc):
    async with AsyncExitStack() as stack:
        task_services = {}
        for t in tasks:
            task_services[t.utgid] = [
                stack.enter_context(
                    asyncdim.DimService(f"{t.utgid}/{s}", "X"))
                for s in (prod_svc, proc_svc)
            ]

        processed = []
        meas = {u: [[] for s in svcs] for u, svcs in task_services.items()}
        while True:
            for u, svcs in task_services.items():
                for i, s in enumerate(svcs):
                    meas[u][i].extend(
                        x[1] for x in await s.get_all() if x[1] is not None)

            log.debug(str(meas))
            done = []
            diff = 0
            for u, (prod_counts, proc_counts) in meas.items():
                latest = min(len(prod_counts), len(proc_counts))
                if latest >= 2:
                    done.append((prod_counts[latest - 1] -
                                 proc_counts[latest - 1]) <= 0)
                    diff += (proc_counts[latest - 1] - proc_counts[latest - 2])
            processed.append(diff)

            if done and all(done) and len(processed) > 2 and processed[
                    -1] == 0 and processed[-2] == 0:
                break


async def tasks_get_counter(tasks, counter_name):
    async with AsyncExitStack() as stack:
        services = [
            stack.enter_context(
                asyncdim.DimService(t.utgid + "/" + counter_name, "X"))
            for t in tasks
        ]
        return [(await s.get())[1][1] for s in services]


async def tasks_wait_for_value(tasks, counter_name, predicate):
    async with AsyncExitStack() as stack:
        services = [
            stack.enter_context(
                asyncdim.DimService(t.utgid + "/" + counter_name, "X"))
            for t in tasks
        ]
        while True:
            values = [(await s.get())[1][1] for s in services]
            log.debug(f"values for {counter_name}: {values}")
            # if (s := sum(values)) > value:
            if predicate(values):
                return values


async def postprocess_savesets(working_dir):
    from ROOT import TFileMerger
    import shutil

    to_merge = defaultdict(list)
    for d, sds, files in os.walk(working_dir):
        d = Path(d)
        rel = d.relative_to(working_dir)
        s = str(rel).split('/')
        if any(f.endswith('.root') for f in files) and len(s) == 5:
            year, partition, task, month, day = s
            if 'HLT1' in task:
                to_merge[task] += [d / f for f in files if 'EOR' in f]
            else:
                to_merge[task] += [d / f for f in files]

    for task, files in to_merge.items():
        merged = str(working_dir / f"{task}.root")
        if len(files) == 1:
            shutil.copy(files[0], merged)
        else:
            merger = TFileMerger(False)
            if not merger.OutputFile(merged):
                log.warning(f"Failed to open {merged} for saveset merging")
            for f in files:
                if not merger.AddFile(str(f), False):
                    log.warning(
                        f"Failed to add {str(f)} to {task} saveset merger")
            if not merger.Merge():
                log.warning(f"Failed to merge {task} savesets")
