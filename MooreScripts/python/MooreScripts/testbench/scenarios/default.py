###############################################################################
# (c) Copyright 2022-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import argparse
import asyncio
import logging
from pathlib import Path
import re
from MooreScripts.testbench import emulator, asyncdim
from MooreScripts.testbench.emulator import (
    tasks_load,
    tasks_wait_for_status,
    tasks_send_command,
    tasks_wait_for_exit,
    tasks_measure_throughput,
    hlt1_wait_for_output,
    tasks_wait_for_value,
    async_input,
    postprocess_savesets,
)
from typing import List

log = logging.getLogger(__name__)


async def run(tasks: List[emulator.Task], args, extra_argv):
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--use-perf",
        action="store_true",
        help="perf record the main tasks and create a flamegraph")
    parser.add_argument(
        "--use-perf-control",
        action="store_true",
        help="perf record the event loop and create a flamegraph")
    parser.add_argument(
        "--wait-after-load",
        action="store_true",
        help="Wait for input after loading (can attach debugger)")
    extra_args = parser.parse_args(extra_argv)

    prod_tasks = [t for t in tasks if "Prod" in t.utgid]
    main_tasks = [
        t for t in tasks if re.match(r".*(HLT|Mon).*",
                                     t.utgid.split("_")[2])
    ]
    if args.measure_throughput == 0:
        if len(prod_tasks) != 1:
            raise ValueError("There must be exactly one *Prod task")
        prod_task = prod_tasks[0]

    if extra_args.use_perf or extra_args.use_perf_control:
        for t in main_tasks:
            t.use_perf(control=extra_args.use_perf_control)

    await tasks_load(tasks)
    # TODO for some reason HLT2 publishes OFFLINE before NOT_READY, but only sometimes
    await tasks_wait_for_status(tasks, "NOT_READY", skip=["OFFLINE"])

    if extra_args.wait_after_load:
        await async_input(
            "Waiting after tasks loaded. Press enter to continue.")

    await tasks_send_command(tasks, "configure")
    await tasks_wait_for_status(tasks, "READY")

    monitors = [t for t in tasks if t.type.endswith("Mon")]
    if monitors:
        if len(monitors) > 1:
            raise ValueError("Too many *Mon tasks in architecture")
        monitor = monitors[0]
        asyncdim.DimService(f"{args.partition}/{monitor.type}/SAVESETLOCATION",
                            "C").__enter__()
        # TODO this needs to run in a dedicated task and have proper cleanup

    await tasks_send_command(tasks, "start")
    await tasks_wait_for_status(tasks, "RUNNING")

    prod_counter = "SLICED" if "HLT1" in main_tasks[0].utgid else "OUT"

    if args.measure_throughput > 0:
        # wait a bit for things to settle and measure throughput
        # in case we are running the perf profile, we don't want to report the throughput
        await asyncio.sleep(args.delay_tp_measurement)
        await tasks_measure_throughput(
            tasks,
            max_duration=args.measure_throughput,
            print_throughput=not (extra_args.use_perf
                                  or extra_args.use_perf_control),
            counter=prod_counter)
    else:
        # wait for the reader task to get to a PAUSED state (no more input)
        await tasks_wait_for_status(prod_tasks, "PAUSED")
        dim_prod_out = asyncdim.DimService(prod_task.utgid + "/Events/OUT",
                                           "X")

    # stop producing new data
    await tasks_send_command(prod_tasks, "stop")
    await tasks_wait_for_status(prod_tasks, "READY")

    if not args.measure_throughput > 0:
        if "HLT1" in main_tasks[0].utgid:
            log.info(f"Waiting until all events have been processed")
            await hlt1_wait_for_output(main_tasks, "Events/" + prod_counter,
                                       "MBMOutput/NProcessed")
        elif "HLT2" in main_tasks[0].utgid:
            # Get last published value
            # TODO can we make it such that the number of events put in the buffer keeps being published after stop?
            #      Markus says MDFReader may need to be Class 1 but are there side effects?
            n_events_produced = next(
                v for ts, v in reversed(await dim_prod_out.get_all())
                if v is not None)
            log.info(
                f"Waiting until all {n_events_produced} events have been processed"
            )
            n_events_processed = sum(await tasks_wait_for_value(
                main_tasks,
                "Events/OUT",
                lambda vs: sum(vs) >= n_events_produced,
            ))
            if n_events_processed > n_events_produced:
                log.error(f"Produced {n_events_produced} but processed " +
                          f"more: {n_events_processed}")

    await tasks_send_command([t for t in tasks if t not in prod_tasks], "stop")
    await tasks_wait_for_status([t for t in tasks if t not in prod_tasks],
                                "READY")

    await tasks_send_command(tasks, "reset")
    await tasks_wait_for_status(tasks, "NOT_READY")

    # FIXME: the following is hack
    for task in tasks:
        task._exit_task.cancel()

    await tasks_send_command(tasks, "unload")
    await tasks_wait_for_status(tasks, "OFFLINE")

    # Wait for the tasks to close
    exit_codes = await tasks_wait_for_exit(tasks)
    if set(exit_codes) != {0}:
        for t, ec in zip(tasks, exit_codes):
            if ec != 0:
                log.error(f"{t.utgid} exited with non-zero code {ec}")
        return 102

    # If there is a Saver, postprocess the savesets
    if any("Saver" in task.utgid for task in tasks):
        await postprocess_savesets(args.working_dir)

    return 0
