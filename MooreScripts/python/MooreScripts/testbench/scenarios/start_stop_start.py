###############################################################################
# (c) Copyright 2022-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import argparse
import asyncio
import logging
import re
from MooreScripts.testbench import emulator
from MooreScripts.testbench.emulator import (
    tasks_load,
    tasks_wait_for_status,
    tasks_send_command,
    tasks_wait_for_exit,
    tasks_measure_throughput,
    hlt1_wait_for_output,
)
from typing import List

log = logging.getLogger(__name__)


async def run(tasks: List[emulator.Task], args, extra_argv):
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--wait-after-load",
        action="store_true",
        help="Wait for input after loading (can attach debugger)")
    parser.add_argument(
        "--n-stop-start",
        type=int,
        default=2,
        help=
        "Number of times to stop and start again in start_stop_start scenario",
    )
    extra_args = parser.parse_args(extra_argv)

    await tasks_load(tasks)
    # TODO for some reason HLT2 publishes OFFLINE before NOT_READY, but only sometimes
    await tasks_wait_for_status(tasks, "NOT_READY", skip=["OFFLINE"])

    if extra_args.wait_after_load:
        await async_input(
            "Waiting after tasks loaded. Press enter to continue.")

    await tasks_send_command(tasks, "configure")
    await tasks_wait_for_status(tasks, "READY")

    prod_tasks = [t for t in tasks if "Prod" in t.utgid]
    main_tasks = [
        t for t in tasks if re.match(r".*(HLT|Mon).*",
                                     t.utgid.split("_")[2])
    ]
    non_prod_tasks = [t for t in tasks if "Prod" not in t.utgid]

    prod_counter = "SLICED" if "HLT1" in main_tasks[0].utgid else "OUT"

    for i in range(extra_args.n_stop_start):
        await tasks_send_command(tasks, "start")
        await tasks_wait_for_status(tasks, "RUNNING")

        # let things run for a little bit
        await asyncio.sleep(30)

        # stop the reader
        await tasks_send_command(prod_tasks, "stop")
        await tasks_wait_for_status(prod_tasks, "READY")

        if "HLT1" in main_tasks[0].utgid:
            log.info(f"Waiting until all events have been processed")
            await hlt1_wait_for_output(main_tasks, "Events/" + prod_counter,
                                       "MBMOutput/NProcessed")

        non_prod_tasks = [t for t in tasks if "Prod" not in t.utgid]
        await tasks_send_command(non_prod_tasks, "stop")
        await tasks_wait_for_status(non_prod_tasks, "READY")

    await tasks_send_command(tasks, "reset")
    await tasks_wait_for_status(tasks, "NOT_READY")

    # FIXME: the following is hack
    for task in tasks:
        task._exit_task.cancel()

    await tasks_send_command(tasks, "unload")
    await tasks_wait_for_status(tasks, "OFFLINE")

    # Wait for the tasks to close
    exit_codes = await tasks_wait_for_exit(tasks)
    if set(exit_codes) != {0}:
        for t, ec in zip(tasks, exit_codes):
            if ec != 0:
                log.error(f"{t.utgid} exited with non-zero code {ec}")
        return 102

    return 0
