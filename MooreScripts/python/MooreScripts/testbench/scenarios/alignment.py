###############################################################################
# (c) Copyright 2022-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import argparse
import logging
from MooreScripts.testbench.emulator import (
    tasks_load,
    tasks_wait_for_status,
    tasks_send_command,
    tasks_wait_for_exit,
    tasks_measure_throughput,
    async_input,
    dump_opts,
)

log = logging.getLogger(__name__)


async def run(tasks, args, extra_argv):
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--runs", nargs="*", type=int, help="Run numbers for the RunList.opts")
    parser.add_argument(
        "--numiter", type=int, default=14, help="Max number of iterations")
    extra_args = parser.parse_args(extra_argv)

    analyzers = [t for t in tasks if "Wrk" in t.type]
    iterator, = [t for t in tasks if "Drv" in t.type]
    assert len(tasks) == len(analyzers) + 1

    # Write the alignment-specific options
    with open("RunList.opts", "w") as f:
        dump_opts({
            "DeferredRuns": set(f"{run:010}" for run in extra_args.runs)
        }, f)
    with open("NodeList.opts", "w") as f:
        # usually the NodeList would contain only the node name
        # since we run one instance per node. When testing locally
        # however we use multiple instances
        dump_opts({"NodeList": {t.utgid for t in analyzers}}, f)

    await tasks_load(tasks)
    # TODO for some reason HLT2 publishes OFFLINE before NOT_READY, but only sometimes
    await tasks_wait_for_status(tasks, "NOT_READY", skip=["OFFLINE"])

    await tasks_send_command(tasks, "configure")
    await tasks_wait_for_status(tasks, "READY")

    # await async_input("Press enter to continue...")

    await tasks_send_command(tasks, "start")
    await tasks_wait_for_status(tasks, "RUNNING")

    for iteration in range(extra_args.numiter):
        log.info(f"Running analyzer for iteration {iteration} ...")
        await tasks_wait_for_status(analyzers, "PAUSED")

        await tasks_send_command(analyzers, "stop")
        await tasks_wait_for_status(analyzers, "READY")

        # TODO check that derivatives are written

        await tasks_send_command([iterator], "pause")
        await tasks_wait_for_status([iterator], "PAUSED")

        status = await iterator.status()
        if status == "READY":  # we have converged
            log.info("Iterator converged")
            break
        elif status == "RUNNING":  # we haven't converged
            log.info("Iterator did not converge")
            await tasks_send_command(analyzers, "start")
            await tasks_wait_for_status(analyzers, "RUNNING")
            continue
        else:
            message = f"Unexpected status for iterator: {status}"
            log.error(message)
            raise RuntimeError(message)

    await tasks_send_command(tasks, "reset")
    await tasks_wait_for_status(tasks, "NOT_READY")

    # FIXME: the following is hack
    for task in tasks:
        task._exit_task.cancel()

    await tasks_send_command(tasks, "unload")
    await tasks_wait_for_status(tasks, "OFFLINE")

    # Wait for the tasks to close
    exit_codes = await tasks_wait_for_exit(tasks)
    if set(exit_codes) != {0}:
        for t, ec in zip(tasks, exit_codes):
            if ec != 0:
                log.error(f"{t.utgid} exited with non-zero code {ec}")
        return 102

    return 0
