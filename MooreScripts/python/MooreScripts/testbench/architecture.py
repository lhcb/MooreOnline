###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
import re
import platform
import xml.etree.ElementTree as ET
from string import Template
from typing import Any


def node_name():
    return platform.node().split('.', 1)[0].upper()


def rinterp(obj, mapping):
    """Recursively interpolate object with a dict of values."""
    return _rinterp(obj, mapping)


def _rinterp(obj, mapping):
    try:
        return {k: _rinterp(v, mapping) for k, v in obj.items()}
    except AttributeError:
        pass
    try:
        return Template(obj).safe_substitute(mapping)
    except TypeError:
        pass
    try:
        return [_rinterp(v, mapping) for v in obj]
    except TypeError:
        return obj


def parse_task_tree(task, instances):
    """Parse task tree into name, n_instances, subprocess.Popen arguments."""

    name = task.attrib["name"]

    instance_match = re.match(R"(\d+|(NUMBER_OF_INSTANCES)(?::(\d+))?)",
                              task.attrib.get("instances", "1"))
    if instance_match.group(2) is None:
        # Default number of instances or specified number
        n_instances = int(instance_match.group(1))
    elif instance_match.groups()[1:] == ('NUMBER_OF_INSTANCES', None):
        # NUMBER_OF_INSTANCES
        n_instances = 1 if instances is None else instances
    elif instances is not None and instances < int(instance_match.group(3)):
        # NUMBER_OF_INSTANCES:N with instances != None
        raise ValueError(
            f"This architecture requires at least {int(instance_match.groups(3))} instances to run"
        )
    else:
        # NUMBER_OF_INSTANCES:N
        n_instances = int(
            instance_match.group(3)) if instances is None else instances

    params = {"args": [], "env": {}, "cwd": "/"}
    # The controller passes populates `-instances`` as the instances argument
    # in the architecture minus one. `createEnvironment.sh` then populates
    # the NBOFSLAVES variable directly from `-instances`.
    params["args"].append(f"-instances={n_instances-1}")
    command = task.find("command").text
    for argument in task.findall("argument"):
        arg = argument.attrib["name"]
        if "value" in argument.attrib:
            arg += "=" + argument.attrib["value"]
        params["args"].append(arg)
    for fmcparam in task.findall("fmcparam"):
        if fmcparam.attrib["name"] == "wd":
            params["cwd"] = fmcparam.attrib["value"]
        elif fmcparam.attrib["name"] == "utgid":
            task_name = fmcparam.attrib["value"]
        elif fmcparam.attrib["name"] == "define":
            k, v = fmcparam.attrib["value"].split("=", 1)
            params["env"][k] = v

    params["executable"] = os.path.join(params["cwd"], command)
    params["args"].insert(0, task_name)
    return name, n_instances, params


def read_xml(path, main_instances):
    """"Parse architecture file into a list of task specs."""
    tree = ET.parse(path)
    tasks_inventory = tree.getroot()
    return [parse_task_tree(task, main_instances) for task in tasks_inventory]


def instance_args(tasks, replacements):
    """Return Popen arguments for each task instance."""
    result = []
    for name, n_instances, args in tasks:
        for instance in range(n_instances):
            result.append(
                rinterp(
                    args, replacements | {
                        "NODE": node_name(),
                        "NAME": name,
                        "INSTANCE": instance,
                    }))
    return result


def overwrite_dict_value(data: Any, mapping: dict) -> Any:
    """Recursively loop through data and overwrite item's value with corresponding mapping's value.

    Args:
        data (Any): Iterable input datra
        mapping (dict): Key value pairs to overwrite.

    Returns:
        Any: Overwritten data.
    """
    if isinstance(data, dict):
        return {
            k: mapping[k] if k in mapping.keys() else overwrite_dict_value(
                v, mapping)
            for k, v in data.items()
        }
    elif isinstance(data, list):
        return [overwrite_dict_value(item, mapping) for item in data]
    elif isinstance(data, tuple):
        return tuple(overwrite_dict_value(item, mapping) for item in data)
    else:
        return data
