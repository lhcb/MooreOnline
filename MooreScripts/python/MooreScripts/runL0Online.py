#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#import sys
# never search for anything from current directory...
#if sys.path[0]=='' : sys.path.pop(0)
import os, logging
from GaudiKernel.ProcessJobOptions import PrintOff, InstallRootLoggingHandler
PrintOff(999)
InstallRootLoggingHandler(level=logging.CRITICAL)

#import sys
# never search for anything from current directory...
#if sys.path[0]=='' : sys.path.pop(0)
#from GaudiKernel.ProcessJobOptions import PrintOff, InstallRootLoggingHandler
#import logging
#PrintOff(999)
#InstallRootLoggingHandler(level = logging.CRITICAL)


def start(**kwargs):
    from MooreOnlineConf.Configuration import L0Online
    import OnlineEnv

    l0Online = L0Online()

    ### pick up requested DB tags
    import ConditionsMap

    def fwdOnlineEnv(attr, cfg, attr2=None):
        setattr(cfg, attr2 if attr2 else attr, getattr(ConditionsMap, attr))

    fwdOnlineEnv('RunChangeHandlerConditions', l0Online)

    ## Enable RunChangeHandlerSvc
    l0Online.EnableRunChangeHandler = (OnlineEnv.HLTType not in [
        'PassThrough', 'Commissioning_OTCosmics'
    ])
    l0Online.EnableUpdateAndReset = False

    from Configurables import MooreExpert
    for k, v in kwargs.iteritems():
        #iterate through the available configurables to set required properties
        if k in l0Online.__slots__ or hasattr(l0Online, k):
            setattr(l0Online, k, v)
        else:
            print "# WARNING: skipping setting '" + str(k) + ":" + str(
                v) + "' because no configurable has that option"

    # Forward all attributes of 'OnlineEnv' python module to the job options service (using the OnlineEnv configurable)...
    from GaudiKernel.Proxy.Configurable import ConfigurableGeneric
    c = ConfigurableGeneric("OnlineEnv")
    #[ setattr(c,k,v) for (k,v) in OnlineEnv.__dict__.items() if k not in OnlineConfig.__dict__ ]
    c.AcceptRate = OnlineEnv.AcceptRate

    l0Online.REQ1 = "EvType=2;TriggerMask=0xffffffff,0xffffffff,0xffffffff,0xffffffff;VetoMask=0,0,0,0;MaskType=ANY;UserType=ONE;Frequency=PERC;Perc=100.0"

    from GaudiKernel.Configurable import applyConfigurableUsers
    applyConfigurableUsers()

    OnlineEnv.end_config(False)
