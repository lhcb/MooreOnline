###############################################################################
# (c) Copyright 2019-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from GaudiTesting.BaseTest import LineSkipper
from Moore.qmtest.exclusions import remove_known_warnings

remove_known_warnings = remove_known_warnings + LineSkipper(strings=[
    # Some of the events have issues independent from HLT1
    r"Misordered large cluster",
    r"Size of link is bad (<= 6)",
])
