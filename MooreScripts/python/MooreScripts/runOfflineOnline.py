#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os


def configure(**kwargs):
    # Add some expected stuff to OnlineEnv
    import OnlineEnv
    from Gaudi.Configuration import INFO, WARNING
    output_level = kwargs.pop('OutputLevel', WARNING)
    OnlineEnv.OutputLevel = output_level

    moore_tests = __import__("MooreTests", globals(), locals(),
                             [kwargs.get('UserPackage')])
    user_package = getattr(moore_tests, kwargs.pop('UserPackage'))
    input_type = kwargs.pop('InputType', 'MEP')

    # Only a single setting directly for Moore
    from Moore.Configuration import Moore, MooreExpert
    MooreExpert().TimingTest = True
    moore = Moore()
    moore.OutputLevel = output_level
    moore.RunOnline = True

    # We need MooreOnline to setup the buffer manager infrastructure etc, but we
    # don't want to use things like the RunChangeHandler and database snapshots.
    from MooreOnlineConf.Configuration import MooreOnline
    mooreOnline = MooreOnline()
    mooreOnline.RunOnline = False
    mooreOnline.EnableTimer = True
    mooreOnline.EnableRunChangeHandler = None
    mooreOnline.UseDBSnapshot = False
    mooreOnline.CheckOdin = False
    mooreOnline.EnableUpdateAndReset = False

    # Hack the CondDB services to stop them from spawning a
    # thread that will segfault on finalize with forking
    def no_timeout():
        from Gaudi.Configuration import allConfigurables
        from Configurables import CondDBAccessSvc
        for conf in allConfigurables.itervalues():
            if type(conf) == CondDBAccessSvc:
                conf.ConnectionTimeOut = 0

    from Gaudi.Configuration import appendPostConfigAction
    appendPostConfigAction(no_timeout)

    userOptions = user_package.MooreOptions
    # This is the stuff that should come from the PRConfig user module
    # This is the stuff that should come from the PRConfig user module
    for conf, d in {
            moore: {
                'DDDBtag': str,
                'CondDBtag': str
            },
            mooreOnline: {
                'UseTCK': bool,
                'Simulation': bool,
                'DataType': tuple(str(y) for y in range(2011, 2019)),
                'HltLevel': ('Hlt1', 'Hlt2', 'Hlt1Hlt2')
            }
    }.iteritems():
        for a, t in d.iteritems():
            ua = userOptions.pop(a)
            if hasattr(t, '__iter__'):
                if ua not in t:
                    raise ValueError('Property %s should be one of %s, not %s.'
                                     % (a, t, ua))
            else:
                if type(ua) != t:
                    raise ValueError(
                        'Property %s should be of type %s, not %s.' % (a, t,
                                                                       ua))
            conf.setProp(a, ua)

    if 'InitialTCK' in userOptions:
        tck = userOptions['InitialTCK']
        moore.setProp('InitialTCK', tck)
        OnlineEnv.InitialTCK = tck

    if userOptions.pop('Split', None):
        print 'WARNING: Split property is ignored, value from HltLevel will be used instead.'

    if mooreOnline.getProp('HltLevel') == 'Hlt1Hlt2':
        moore.setProp('Split', '')
    else:
        moore.setProp('Split', mooreOnline.getProp('HltLevel'))

    if input_type == 'MEP' and 'Hlt1' in mooreOnline.HltLevel:
        mooreOnline.REQ1 = "EvType=1;TriggerMask=0xffffffff,0xffffffff,0xffffffff,0xffffffff;VetoMask=0,0,0,0;MaskType=ANY;UserType=ONE;Frequency=PERC;Perc=100.0"
    elif input_type == 'MDF' and 'Hlt1' in mooreOnline.HltLevel:
        mooreOnline.ForceMDFInput = True
        mooreOnline.REQ1 = "EvType=2;TriggerMask=0xffffffff,0xffffffff,0xffffffff,0xffffffff;VetoMask=0,0,0,0;MaskType=ANY;UserType=ONE;Frequency=PERC;Perc=100.0"
    elif mooreOnline.HltLevel == 'Hlt2':
        mooreOnline.REQ1 = "EvType=2;TriggerMask=0xffffffff,0xffffffff,0xffffffff,0xffffffff;VetoMask=0,0,0,0;MaskType=ANY;UserType=ONE;Frequency=PERC;Perc=100.0"

    # Apparently we need to set this, otherwise something goes wrong with
    # default properties being retrieved that have the wrong type.
    from Gaudi.Configuration import EventSelector
    moore.inputFiles = []
    EventSelector().Input = []

    # Extra options
    from Configurables import MooreExpert
    userOptions.update(kwargs)
    for k, v in userOptions.iteritems():
        #iterate through the available configurables to set required properties
        found = False
        for conf in [mooreOnline, moore, MooreExpert()]:
            if k in conf.__slots__ or hasattr(conf, k):
                conf.setProp(k, v)
                found = True
                break
        if not found:
            print "# WARNING: skipping setting '" + str(k) + ":" + str(
                v) + "' because no configurable has that option"

    # Monitoring transform
    from Moore import Funcs
    moniTrans = {
        "^GaudiSequencer/Hlt$": {
            "Members": {
                "HltMonitorSequence": "HltOnlineMonitorSequence"
            }
        }
    }
    Funcs._mergeTransform(moniTrans)

    from GaudiKernel.Configurable import applyConfigurableUsers
    applyConfigurableUsers()

    user_package.configure()
    OnlineEnv.end_config(False)
