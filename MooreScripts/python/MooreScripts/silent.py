###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
## The list of algorithms for which warnings are silenced
silent_algs = (
    #".*Hlt2.*Fitter$",
    ".*Hlt2SelReportsWriter",  ## harmless Warning of the type: "Hlt2SelReports is huge. Saved in 2 separate RawBanks ## sometimes also 3,4 or 5 RawBanks"
    ".*MomentumCombiner",  ## harmless Warning of the type: "MassErr^2 < -999 -> Fit aborted"
    #".*TrackEff.*[01]N3Body.*",
    ".*Hlt2.*TrackMasterExtrapolator",  ## harmless Warning of the type: "Transport to wall using..." "Protect against absurd tracks"
    #".*DBAccessor",
    #r".*\.PhotonMatch",
    r".*ToolSvc.*ParticleTransporter",  ## harmless Warning of the type: "Error from StateProvider::state"
    #".*LoKiSvc.REPORT",
    #r".*L0DUConfig",
    r".*MergedPi0RecForHlt2_PID.*",  ## harmless Warning of the type: "CaloShowerOverlapTool:: E,X and Y of cluster could not be evaluated!"
    r".*ToolSvc.TrackStateProvider.Interpolator.Extrapolator",  ## harmless Warning of the type: "Protect against absurd tracks"
    r".*HCRawBankDecoderHlt",  ## harmless Warning because it will appear if Herschel is excluded from run control
    r".*EcalShareForHlt.PhotonShowerOverlap",  ## harmless Warning of the type: "CaloShowerOverlapTool:: E,X and Y of cluster could not be evaluated!"
    r".*Hlt2TrackV0Finder",  ## harmless Warning of the type: "Hlt2TrackV0Finder: inversion error in constrainToVertex"
)
